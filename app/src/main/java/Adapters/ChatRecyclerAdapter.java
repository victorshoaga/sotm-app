package Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glt.sotm.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import Models.ChatModel;
import Models.User;
import Utility.SessionManager;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Author: Kartik Sharma
 * Created on: 10/16/2016 , 10:36 AM
 * Project: FirebaseChat
 */

public class ChatRecyclerAdapter extends RecyclerView.Adapter<ChatRecyclerAdapter.UserMessageHolder> {

    private static final String TAG = ChatRecyclerAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_ME = 1;
    private static final int VIEW_TYPE_OTHER = 2;

    private List<ChatModel> mChats;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    User self, talking_to;
    Context context;

    public ChatRecyclerAdapter(List<ChatModel> chats, Context context, User self, User talking_to) {
        this.context = context;
        mChats = chats;
        sessionManager = new SessionManager(context);
        hashMap = sessionManager.getUserDetails();
        this.self = self;
        this.talking_to = talking_to;
    }

    public void add(ChatModel chat) {
        mChats.add(chat);
        notifyItemInserted(mChats.size() - 1);
    }

    @Override
    public UserMessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        UserMessageHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_ME:
                View viewChatMine = layoutInflater.inflate(R.layout.item_message_sent, parent, false);
                viewHolder = new UserMessageHolder(viewChatMine);
                break;
            case VIEW_TYPE_OTHER:
                View viewChatOther = layoutInflater.inflate(R.layout.item_message_received, parent, false);
                viewHolder = new UserMessageHolder(viewChatOther);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UserMessageHolder holder, int position) {

        ChatModel chat = mChats.get(position);
        Log.i(TAG, chat.toString());
        boolean isNewDay = false;
        // If there is at least one item preceding the current one, check the previous message.
        try
        {
            if (!mChats.get(position - 1).toString().isEmpty()) {
                try{
                    ChatModel prevMessage = mChats.get(position - 1);
                    if (!Utility.DateUtils.hasSameDate(chat.getTimeStamp(), prevMessage.getTimeStamp())) {
                        isNewDay = true;
                        Log.i("NEW DAY_", String.valueOf(isNewDay));
                    }
                }
                catch (ArrayIndexOutOfBoundsException e)
                {
                    Log.e(TAG, e.getMessage());
                }

            }
            else {
                isNewDay = true;
                Log.i("_NEW DAY", String.valueOf(isNewDay));
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
            isNewDay = true;
        }

        holder.setMessage(chat.getMessage());
        holder.setTime(String.valueOf(chat.getTimeStamp()));
        holder.showDate(String.valueOf(chat.getTimeStamp()), isNewDay);

    }


    @Override
    public int getItemCount() {
        if (mChats != null) {
            return mChats.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (TextUtils.equals(mChats.get(position).getSelf(),
                hashMap.get(SessionManager.KEY_ID))) {
            return VIEW_TYPE_ME;
        } else {
            return VIEW_TYPE_OTHER;
        }
    }

    private CharSequence convertTimestamp(String mileSeconds){
        return DateUtils.getRelativeTimeSpanString(Long.parseLong(mileSeconds), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }

    public class UserMessageHolder extends RecyclerView.ViewHolder {
        //TextView nicknameText, messageText, timeText, dateText;
        //CircleImageView image_message_profile;
        TextView text_message_name, text_message_body, text_message_time, dateText;

        UserMessageHolder(View itemView) {
            super(itemView);


            dateText = (TextView) itemView.findViewById(R.id.text_open_chat_date);
            //image_message_profile = (CircleImageView) itemView.findViewById(R.id.message_profile_layout);
            //text_message_name = (TextView) itemView.findViewById(R.id.text_message_name);
            text_message_body = (TextView) itemView.findViewById(R.id.text_message_body);
            text_message_time = (TextView) itemView.findViewById(R.id.text_message_time);
        }


        // Show the date if the message was sent on a different date than the previous one.
        public void showDate(String timestamp, boolean isNewDay)
        {
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                if(Utility.DateUtils.isToday(Long.valueOf(timestamp)))
                {
                    dateText.setText("TODAY");
                }
                else
                {
                    dateText.setText(Utility.DateUtils.formatDate(Long.valueOf(timestamp)));
                }
            } else {
                dateText.setVisibility(View.GONE);
            }
        }

//        public void setImgAndName(String img_url, String fullname, boolean show)
//        {
//            if(show)
//            {
//                Picasso.with(context)
//                        .load(img_url)
//                        .placeholder(R.drawable.head_photo)
//                        .into(image_message_profile);
//                text_message_name.setText(fullname);
//            }
//            else
//            {
//                image_message_profile.setVisibility(View.INVISIBLE);
//                text_message_name.setVisibility(View.GONE);
//            }
//        }

        public void setMessage(String message)
        {
            text_message_body.setText(message);
        }

        public void setTime(String timestamp){
            text_message_time.setText(Utility.DateUtils.formatTime(Long.valueOf(timestamp)));
        }


    }

}
