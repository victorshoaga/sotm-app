package Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.glt.sotm.R;
import com.glt.sotm.UserFriendChatActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import Models.MessageDataModel;
import Models.MyConnectsDocs;
import Models.Response_User;
import Models.User;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by inventor on 9/27/17.
 */
public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.MyConnectsAdapterDataHolderObject>{

    private static final String TAG = ChatsAdapter.class.getSimpleName();
    List<MessageDataModel> docsList;
    Context context;
    User self;
    ProgressDialog progressDialog;
    String token, user_id;
    Activity activity;

    public ChatsAdapter(Activity activity, Context context, List<MessageDataModel> docsList, String token, String user_id) {
        this.context = context;
        this.docsList = docsList;
        this.token = token;
        this.user_id = user_id;
        this.activity = activity;
        progressDialog = new ProgressDialog(context);
    }

    @Override
    public MyConnectsAdapterDataHolderObject onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_chat, parent, false);
        MyConnectsAdapterDataHolderObject myConnectsAdapterDataHolderObject = new MyConnectsAdapterDataHolderObject(view);
        return myConnectsAdapterDataHolderObject;
    }

    @Override
    public void onBindViewHolder(MyConnectsAdapterDataHolderObject viewHolder, int position) {
        final MessageDataModel model = docsList.get(position);
        viewHolder.img_partner.setVisibility(View.VISIBLE);
        viewHolder.setFullName(model.getUser().getName());
        //viewHolder.setTxt_country(model.getUser().getCountry() + " (" + model.getUser().getCity() + ")");
        viewHolder.setTxt_last_message(model.getChatModel().getMessage());
       // viewHolder.setTxt_last_timestamp(Utility.DateUtils.formatTime(Long.valueOf(model.getChatModel().getTimeStamp())));
        viewHolder.showDate(String.valueOf(model.getChatModel().getTimeStamp()));
        viewHolder.setThumbnail(model.getUser().getPicture());
        if(model.getUser().isAPartner() == true)
        {
            viewHolder.setImg_partner();
        }
        else
        {
            viewHolder.setImg_partner_not();
        }


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // go to chat page of user
                getSelfProfile(token, user_id, model);

            }
        });
    }

    @Override
    public int getItemCount() {
        return docsList.size();
    }


    public class MyConnectsAdapterDataHolderObject extends RecyclerView.ViewHolder
    {
        TextView txt_last_message, txt_fullName, txt_last_timestamp;
        ImageView img_thumb, img_partner;
        public MyConnectsAdapterDataHolderObject(View itemView) {
            super(itemView);
            txt_last_message = (TextView)itemView.findViewById(R.id.txt_last_message);
            txt_last_timestamp = (TextView)itemView.findViewById(R.id.txt_last_timestamp);
            txt_fullName = (TextView) itemView.findViewById(R.id.txt_fullname);
            img_thumb = (ImageView) itemView.findViewById(R.id.img_thumb);
            img_partner = (ImageView) itemView.findViewById(R.id.img_partner);
        }
        public void setFullName(String name){
            if (txt_fullName == null)return;
            txt_fullName.setText(name);
        }

        private void setTxt_last_message(String message)
        {
            if(txt_last_message == null) return;
            txt_last_message.setText(message);
        }

        private void setTxt_last_timestamp(String timestamp)
        {
            if(txt_last_timestamp == null) return;
            txt_last_timestamp.setText(timestamp);
        }

        private void setThumbnail(String uri)
        {
            Picasso.with(img_thumb.getContext())
                    .load(uri)
                    .placeholder(R.drawable.head_photo)
                    .into(img_thumb);
        }
        private void setImg_partner()
        {
            img_partner.setVisibility(View.VISIBLE);
        }

        private void setImg_partner_not()
        {
            img_partner.setVisibility(View.INVISIBLE);
        }

        public void showDate(String timestamp)
        {
            if(Utility.DateUtils.isToday(Long.valueOf(timestamp)))
            {
                txt_last_timestamp.setText("TODAY");
            }
            else
            {
                txt_last_timestamp.setText(Utility.DateUtils.formatDate(Long.valueOf(timestamp)));
            }
        }
    }

    private void getSelfProfile(final String token, final String user_id, final MessageDataModel model) {
        showPDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_User> call = service.getUserProfile(token, user_id);
        call.enqueue(new Callback<Response_User>() {
            @Override
            public void onResponse(Call<Response_User> call,
                                   Response<Response_User> response) {
                hidePDialog();
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Log.i(TAG, response.body().toString());
                    self = response.body().getData().getUser();
                    Log.i(TAG + " getSelfProfile", response.body().toString());
                    Log.i(TAG + " getSelfProfile", self.toString());
                    Intent intent = new Intent(context, UserFriendChatActivity.class);
                    intent.putExtra("friend_data", model.getUser());
                    intent.putExtra("self_data", self);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    activity.finish();
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(context, errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_User> call, Throwable t) {
                hidePDialog();
                call.cancel();
                Toast.makeText(context, Html.fromHtml(context.getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showPDialog()
    {
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void hidePDialog()
    {
        progressDialog.dismiss();
    }
}
