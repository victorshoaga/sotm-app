package Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glt.sotm.R;

import java.util.ArrayList;

import Dialog_Fragments.CityDialog;

/**
 * Created by inventor on 1/23/18.
 */

public class CityDialogAdapter extends RecyclerView.Adapter<CityDialogAdapter.ViewHolder>{

    private static final String TAG = CityDialogAdapter.class.getSimpleName();

    ArrayList<String> values;
    Context context;
    CityDialog cityDialog;

    public CityDialogAdapter (Context context, ArrayList<String> values, CityDialog cityDialog){
        this.values = values;
        this.context = context;
        this.cityDialog = cityDialog;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public ViewHolder(View v){
            super(v);
            textView = (TextView) v.findViewById(R.id.txt_city);
        }
    }

    @Override
    public CityDialogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view1 = LayoutInflater.from(context).inflate(R.layout.recyclerview_city_items,parent,false);
        ViewHolder viewHolder1 = new ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Vholder, int position){
        Vholder.textView.setText(values.get(position));
        Vholder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selected_city = Vholder.textView.getText().toString().trim();
                cityDialog.dialog_dismiss();
                CityDialog.SelectCityListener selectCityListener = (CityDialog.SelectCityListener) context;
                selectCityListener.onCityReturnValue(selected_city);
            }
        });
    }

    @Override
    public int getItemCount(){
        return values.size();
    }
}
