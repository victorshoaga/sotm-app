package Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.glt.sotm.ImageResultActivity;
import com.glt.sotm.ProfileActivity_Friend;
import com.glt.sotm.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.percolate.mentions.Mentionable;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.w3c.dom.Comment;

import java.util.HashMap;
import java.util.List;

import Models.AppMentionable;
import Models.CommentModel;
import Models.User;
import Utility.SessionManager;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by inventor on 1/26/18.
 */

public class CommentRecyclerAdapter extends RecyclerView.Adapter<CommentRecyclerAdapter.UserMessageHolder> {

    private static final String TAG = CommentRecyclerAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_ME = 1;
    private static final int VIEW_TYPE_OTHER = 2;

    private List<CommentModel> commments;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    Context context;
    ActivityOptions options;
    Activity activity;
    String pic = "", name_ = "";
    LovelyProgressDialog lovelyProgressDialog;

    public CommentRecyclerAdapter(Activity activity, List<CommentModel> commments, Context context, LovelyProgressDialog lovelyProgressDialog) {
        this.context = context;
        this.activity = activity;
        this.commments = commments;
        sessionManager = new SessionManager(context);
        hashMap = sessionManager.getUserDetails();
        this.lovelyProgressDialog = lovelyProgressDialog;
    }

    public void add(CommentModel commentModel) {
        commments.add(commentModel);
        notifyItemInserted(commments.size() - 1);
    }

    @Override
    public UserMessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        UserMessageHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_ME:
                View viewChatMine = layoutInflater.inflate(R.layout.item_message_comment, parent, false);
                viewHolder = new UserMessageHolder(viewChatMine);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final UserMessageHolder holder, int position) {
        pic = "";
        name_ = "";
        final CommentModel commentModel = commments.get(position);
        Log.i(TAG, commentModel.toString());
        holder.setMessage(commentModel.getComment());
        highlightMentions(holder.text_message_body, commentModel.getMentions());
        holder.setTime(String.valueOf(commentModel.getTimestamp()));
        holder.setUserName(commentModel.getUsername());
        try{
            FirebaseDatabase.getInstance().getReference().child("Users").child(commentModel.getId()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try{
                        if (dataSnapshot.child("picture").getValue().toString() != null) {
                            Picasso.with(context)
                                    .load(dataSnapshot.child("picture").getValue().toString())
                                    .placeholder(R.drawable.head_photo)
                                    .into(holder.img_thumb);
                        }else{
                            holder.img_thumb.setImageResource(R.drawable.head_photo);
                        }

                    }catch(Exception e)
                    {
                        Log.e(TAG, e.getMessage());
                        holder.img_thumb.setImageResource(R.drawable.head_photo);
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.i(TAG, databaseError.getMessage());
                    holder.img_thumb.setImageResource(R.drawable.head_photo);
                }
            });
        }catch(Exception e){
            Log.e(TAG, e.getMessage());
            holder.img_thumb.setImageResource(R.drawable.head_photo);
        }

//        holder.img_thumb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Uri picUri = Uri.parse(pic);
//                Log.i(TAG, String.valueOf(picUri));
//                //ImageResultActivity.startWithUri(getActivity(), picUri, hashMap.get(SessionManager.KEY_NAME), getActivity());
//                Intent intent = new Intent(context, ImageResultActivity.class);
//                intent.setData(picUri);
//                intent.putExtra("name", "@"+commentModel.getUsername());
//                Pair[] pairs = new Pair[1];
//                pairs[0] = new Pair<View, String>(holder.img_thumb, "imgTransition");
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                    options = ActivityOptions.makeSceneTransitionAnimation(activity, pairs);
//                }
//                // Closing all the Activities
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                // Add new Flag to start new Activity
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent, options.toBundle());
//            }
//        });
        holder.text_message_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog("Fetching data from our server centre...");
                if(commentModel.getId().equals(hashMap.get(SessionManager.KEY_ID))){
                    Toast.makeText(context, "This is you", Toast.LENGTH_SHORT).show();
                    hideDialog();
                }else {
                    FirebaseDatabase.getInstance().getReference().child("Users").child(commentModel.getId()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            hideDialog();
                            try{
                                pic = dataSnapshot.child("picture").getValue().toString();
                                name_ = dataSnapshot.child("name").getValue().toString();
                                Log.i(TAG, name_);
                                Log.i(TAG, pic);
                                Intent intent = new Intent(context, ProfileActivity_Friend.class);
                                intent.putExtra("message", "from_friends");
                                intent.putExtra("is_friend", "no");
                                intent.putExtra("name", name_);
                                intent.putExtra("picture", pic);
                                intent.putExtra("friend_id", commentModel.getId());
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);

                            }catch(Exception e)
                            {
                                Log.e(TAG, e.getMessage());
                            }

                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            hideDialog();
                            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        if (commments != null) {
            return commments.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ME;
    }

    public class UserMessageHolder extends RecyclerView.ViewHolder {
        //TextView nicknameText, messageText, timeText, dateText;
        TextView text_message_name, text_message_time, text_username;
        CircleImageView img_thumb;
        EmojiconTextView text_message_body;

        UserMessageHolder(View itemView) {
            super(itemView);
            text_message_name = (TextView) itemView.findViewById(R.id.text_username);
            text_message_body = (EmojiconTextView) itemView.findViewById(R.id.text_comment);
            text_message_time = (TextView) itemView.findViewById(R.id.text_time);
            img_thumb = (CircleImageView) itemView.findViewById(R.id.img_thumb);
        }

        public void setUserName(String username)
        {
            text_message_name.setText("@"+username);
        }

        public void setMessage(String message)
        {
            text_message_body.setText(message);
        }

        public void setTime(String timestamp){
            String resulting_time = "";
            resulting_time += Utility.DateUtils.formatTime(Long.valueOf(timestamp));
            resulting_time += "  ";
            if(Utility.DateUtils.isToday(Long.valueOf(timestamp)))
            {
                resulting_time += "\u2022";
                resulting_time += "  Today";
            }
            else{
                resulting_time += "\u2022";
                resulting_time += "  ";
                resulting_time += Utility.DateUtils.formatDate(Long.valueOf(timestamp));
            }
            text_message_time.setText(resulting_time);
        }
    }

    /**
     * Highlights all the {@link Mentionable}s in the test {@link Comment}.
     */
    private void highlightMentions(final TextView commentTextView, final List<AppMentionable> mentions) {
        if(commentTextView != null && mentions != null && !mentions.isEmpty()) {
            final Spannable spannable = new SpannableString(commentTextView.getText());

            for (final AppMentionable mention: mentions) {
                if (mention != null) {
                    final int start = mention.getMentionOffset();
                    final int end = start + mention.getMentionLength();

                    if (commentTextView.length() >= end) {
                        //spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.mentions_default_color)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        spannable.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                Log.i(TAG, mention.getMentionName());
                                showDialog("Fetching data from our server centre...");
                                if(mention.getMentionId().equals(hashMap.get(SessionManager.KEY_ID))){
                                    Toast.makeText(context, "This is you", Toast.LENGTH_SHORT).show();
                                    hideDialog();
                                }else {
                                    FirebaseDatabase.getInstance().getReference().child("Users").child(mention.getMentionId()).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            hideDialog();
                                            try{
                                                pic = dataSnapshot.child("picture").getValue().toString();
                                                name_ = dataSnapshot.child("name").getValue().toString();
                                                Log.i(TAG, name_);
                                                Log.i(TAG, pic);
                                                Intent intent = new Intent(context, ProfileActivity_Friend.class);
                                                intent.putExtra("message", "from_friends");
                                                intent.putExtra("is_friend", "no");
                                                intent.putExtra("name", name_);
                                                intent.putExtra("picture", pic);
                                                intent.putExtra("friend_id", mention.getMentionId());
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                context.startActivity(intent);

                                            }catch(Exception e)
                                            {
                                                Log.e(TAG, e.getMessage());
                                            }

                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            hideDialog();
                                            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                                            Log.e(TAG, databaseError.getMessage());
                                        }
                                    });
                                }
                            }
                            @Override
                            public void updateDrawState(TextPaint textPaint) {
                                textPaint.setColor(ContextCompat.getColor(context, R.color.linkedin_blue));    // you can use custom color
                                textPaint.setUnderlineText(true);    // this remove the underline
                            }
                        }, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        commentTextView.setMovementMethod(LinkMovementMethod.getInstance());
                        commentTextView.setText(spannable, TextView.BufferType.SPANNABLE);
                    } else {
                        //Something went wrong.  The expected text that we're trying to highlight does not
                        // match the actual text at that position.
                        Log.w("Mentions Sample", "Mention lost. [" + mention + "]");
                    }
                }
            }
        }
    }

    private void showDialog(String text_to_display)
    {
        lovelyProgressDialog.setIcon(R.drawable.ic_cast_connected_white_36dp)
                .setTitle("Please wait")
                .setTopColorRes(R.color.endColor)
                .setMessage(text_to_display)
                .setMessageGravity(Gravity.CENTER_HORIZONTAL)
                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .show();
    }

    private void hideDialog()
    {
        lovelyProgressDialog.dismiss();
    }
}
