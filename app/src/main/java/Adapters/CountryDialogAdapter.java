package Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glt.sotm.R;

import java.util.ArrayList;

import Dialog_Fragments.CountryDialog;


/**
 * Created by inventor on 1/20/18.
 */

public class CountryDialogAdapter extends RecyclerView.Adapter<CountryDialogAdapter.ViewHolder>{

    private static final String TAG = CountryDialogAdapter.class.getSimpleName();

    ArrayList<String> values;
    Context context;
    CountryDialog countryDialog;

    public CountryDialogAdapter (Context context, ArrayList<String> values, CountryDialog countryDialog){
        this.values = values;
        this.context = context;
        this.countryDialog = countryDialog;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public ViewHolder(View v){
            super(v);
            textView = (TextView) v.findViewById(R.id.txt_country);
        }
    }

    @Override
    public CountryDialogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view1 = LayoutInflater.from(context).inflate(R.layout.recyclerview_country_items,parent,false);
        ViewHolder viewHolder1 = new ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Vholder, int position){
        Vholder.textView.setText(values.get(position));
        Vholder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selected_country = Vholder.textView.getText().toString().trim();
                countryDialog.dialog_dismiss();
                CountryDialog.SelectCountryListener selectCountryListener = (CountryDialog.SelectCountryListener) context;
                selectCountryListener.onReturnValue(selected_country);
            }
        });
    }

    @Override
    public int getItemCount(){
        return values.size();
    }



}
