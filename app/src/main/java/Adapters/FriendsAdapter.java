package Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.glt.sotm.FriendsActivity;
import com.glt.sotm.ImageResultActivity;
import com.glt.sotm.ProfileActivity_Friend;
import com.glt.sotm.R;
import com.glt.sotm.UserFriendChatActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import Models.MyFriendsDocs;
import Models.Response_FindConnects;
import Models.Response_FindFriends;
import Models.Response_User;
import Models.User;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import Utility.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by inventor on 9/27/17.
 */
public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.FriendsAdapterDataHolderObject> implements Filterable {

    private static final String TAG = FriendsAdapter.class.getSimpleName();

    List<MyFriendsDocs> docsList;
    List<MyFriendsDocs> docsListFiltered;
    Context context;
    ProgressDialog progressDialog;
    User self;
    String token, user_id;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    ActivityOptions options;
    Activity activity;
    List<MyFriendsDocs> online_docsList;

    public FriendsAdapter(Activity activity, Context context, ArrayList<MyFriendsDocs> docsList, String token, String user_id) {
        this.activity = activity;

        this.context = context;
        this.docsList = docsList;
        this.docsListFiltered = docsList;
        this.token = token;
        this.user_id = user_id;
        progressDialog = new ProgressDialog(context);
        hashMap = new HashMap<>();
        sessionManager = new SessionManager(context);
        hashMap = sessionManager.getUserDetails();
    }

    @Override
    public FriendsAdapterDataHolderObject onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_friends, parent, false);
        FriendsAdapterDataHolderObject myConnectsAdapterDataHolderObject = new FriendsAdapterDataHolderObject(view);
        return myConnectsAdapterDataHolderObject;
    }

    @Override
    public void onBindViewHolder(final FriendsAdapterDataHolderObject viewHolder, int position) {
        Log.i(TAG, "onBindViewHolder");
        final MyFriendsDocs model = docsListFiltered.get(position);
        viewHolder.txt_send_message.setVisibility(View.GONE);
        viewHolder.img_partner.setVisibility(View.VISIBLE);
        viewHolder.setFullName(model.getUser().getName());
        viewHolder.setTxt_country(model.getUser().getCountry() + " (" + model.getUser().getCity() + ")");
        viewHolder.setThumbnail(model.getUser().getPicture());
        viewHolder.img_thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri picUri = Uri.parse(model.getUser().getPicture());
                Log.i(TAG, String.valueOf(picUri));
                //ImageResultActivity.startWithUri(getActivity(), picUri, hashMap.get(SessionManager.KEY_NAME), getActivity());
                Intent intent = new Intent(context, ImageResultActivity.class);
                intent.setData(picUri);
                intent.putExtra("name", model.getUser().getName());
//                    Pair[] pairs = new Pair[1];
//                    pairs[0] = new Pair<View, String>(viewHolder.img_thumb, "imgTransition");
//                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                        options = ActivityOptions.makeSceneTransitionAnimation(activity, pairs);
//                    }
                //context.startActivity(intent, options.toBundle());
                context.startActivity(intent);
            }
        });
        try{

            viewHolder.show_viewProfile(model.isAFriend());
            Log.i(TAG, "onBindViewHolder1");
            FirebaseDatabase.getInstance().getReference().child("Users").child(model.getUser().getId()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i(TAG, "onBindViewHolder2");
                    try{
                        Log.i(TAG, "onBindViewHolder3");
                        if(dataSnapshot.hasChild("online")) {

                            String online = dataSnapshot.child("online").getValue().toString();
                            Log.i(TAG, online);
                            Log.i(TAG, model.getUser().getName());
                            if(online.equals("true")) {
                                Log.i(TAG, "onBindViewHolder**");
                                viewHolder.online_icon.setVisibility(View.VISIBLE);
                                viewHolder.online_icon.setImageResource(R.color.cpb_green);
                                Log.i(TAG, model.getUser().getUsername() + " is online");
                                //online_docsList.add(model);
                            } else {
                                Log.i(TAG, "onBindViewHolder##");
                                viewHolder.online_icon.setVisibility(View.VISIBLE);
                                viewHolder.online_icon.setImageResource(R.color.grey_text_);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.i(TAG, "onBindViewHolder4");
                        Log.e(TAG, e.getMessage());
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.i(TAG, "onBindViewHolder5");
                    Log.e(TAG, databaseError.getMessage());
                }
            });

            if (model.getUser().isAPartner()) {
                viewHolder.setImg_partner(R.drawable.p_icon);
            } else {
                viewHolder.setImg_partner_not();
            }


            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProfileActivity_Friend.class);
                    intent.putExtra("message", "from_friends");
                    if(context.getClass().getSimpleName().equals("FriendsActivity")){
                        intent.putExtra("is_friend", "yes");
                    }else {
                        intent.putExtra("is_friend", "no");
                    }
                    intent.putExtra("name", model.getUser().getName());
                    intent.putExtra("picture", model.getUser().getPicture());
                    intent.putExtra("friend_id", model.getUser().getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if(context.getClass().getSimpleName().equals("FriendsFriendActivity"))
                    {
                        activity.finish();
                    }
                    context.startActivity(intent);
                }
            });
        }catch (Exception e){
            Log.e(TAG, e.getMessage());
        }


//        viewHolder.txt_send_message.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //go to message chat
//                getSelfProfile(token, user_id, model);
//            }
//        });
    }

    @Override
    public int getItemCount() {

        return docsListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    docsListFiltered = docsList;
                } else {
                    List<MyFriendsDocs> filteredList = new ArrayList<>();
                    for (MyFriendsDocs row : docsListFiltered) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getUser().getUsername().toLowerCase().contains(charString.toLowerCase()) || row.getUser().getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    docsListFiltered = filteredList;
                }
                filterResults.values = docsListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                List list = new ArrayList();
                Log.i(TAG, "here");
                list = (ArrayList<MyFriendsDocs>) filterResults.values;
                docsListFiltered = list;
                notifyDataSetChanged();
            }
        };
    }


    public class FriendsAdapterDataHolderObject extends RecyclerView.ViewHolder {
        TextView txt_country, txt_fullName, txt_send_message;
        ImageView img_thumb, img_partner;
        CircleImageView online_icon;
        Button btn_view_profile;

        public FriendsAdapterDataHolderObject(View itemView) {
            super(itemView);
            txt_country = (TextView) itemView.findViewById(R.id.txt_country);
            txt_send_message = (TextView) itemView.findViewById(R.id.txt_send_message);
            txt_fullName = (TextView) itemView.findViewById(R.id.txt_fullname);
            img_thumb = (ImageView) itemView.findViewById(R.id.civProfilePic);
            img_partner = (ImageView) itemView.findViewById(R.id.img_partner);
            btn_view_profile = (Button) itemView.findViewById(R.id.btn_view_profile);
            online_icon = (CircleImageView) itemView.findViewById(R.id.status);
            img_partner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "This user is a partner.", Toast.LENGTH_SHORT).show();
                }
            });
        }

        public void setFullName(String name) {
            if (txt_fullName == null) return;
            txt_fullName.setText(name);
        }

        private void setTxt_country(String country) {
            if (txt_country == null) return;
            txt_country.setText(country);
        }

        private void setThumbnail(String uri) {
            Picasso.with(img_thumb.getContext())
                    .load(uri)
                    .placeholder(R.drawable.head_photo)
                    .into(img_thumb);
        }

        private void setImg_partner(int resId) {
            img_partner.setVisibility(View.VISIBLE);
        }

        private void setImg_partner_not() {
            img_partner.setVisibility(View.GONE);
        }

        private void show_viewProfile(boolean isAFriend)
        {
            if(isAFriend)
            {
                btn_view_profile.setVisibility(View.GONE);
            }
            else
            {
                btn_view_profile.setVisibility(View.VISIBLE);
            }
        }

    }

    private void getSelfProfile(final String token, final String user_id, final MyFriendsDocs model) {
        showPDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_User> call = service.getUserProfile(token, user_id);
        call.enqueue(new Callback<Response_User>() {
            @Override
            public void onResponse(Call<Response_User> call,
                                   Response<Response_User> response) {
                hidePDialog();
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if (response.code() == Utils.STATUS_200_OK) {
                    Log.i(TAG, response.body().toString());
                    self = response.body().getData().getUser();
                    Log.i(TAG + " getSelfProfile", response.body().toString());
                    Log.i(TAG + " getSelfProfile", self.toString());
                    Intent intent = new Intent(context, UserFriendChatActivity.class);
                    intent.putExtra("friend_data", model.getUser());
                    intent.putExtra("self_data", self);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(context, errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_User> call, Throwable t) {
                hidePDialog();
                call.cancel();
                Toast.makeText(context, Html.fromHtml(context.getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showPDialog() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void hidePDialog() {
        progressDialog.dismiss();
    }

    public void removeItem(int position) {
        docsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, docsList.size());
    }

    public List<MyFriendsDocs> getOnlineFriends()
    {
        return online_docsList;
    }
}
