package Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glt.sotm.R;

import java.util.ArrayList;

import Dialog_Fragments.GenderDialog;

/**
 * Created by inventor on 1/23/18.
 */

public class GenderDialogAdapter extends RecyclerView.Adapter<GenderDialogAdapter.ViewHolder>{

    ArrayList<String> values;
    Context context;
    GenderDialog genderDialog;

    public GenderDialogAdapter(Context context,ArrayList<String> values, GenderDialog genderDialog){
        this.values = values;
        this.context = context;
        this.genderDialog = genderDialog;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public ViewHolder(View v){
            super(v);
            textView = (TextView) v.findViewById(R.id.txt_gender);
        }
    }

    @Override
    public GenderDialogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view1 = LayoutInflater.from(context).inflate(R.layout.recyclerview_gender_items,parent,false);
        ViewHolder viewHolder1 = new ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Vholder, int position){
        Vholder.textView.setText(values.get(position));
        Vholder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selected_gender = Vholder.textView.getText().toString().trim();
                genderDialog.dialog_dismiss();
                GenderDialog.SelectGenderListener selectGenderListener = (GenderDialog.SelectGenderListener) context;
                selectGenderListener.onGenderReturnValue(selected_gender);
            }
        });
    }

    @Override
    public int getItemCount(){
        return values.size();
    }

}
