package Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import Models.AppMentionable;
import Models.Messages;

import com.glt.sotm.ProfileActivity_Friend;
import com.glt.sotm.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.percolate.mentions.Mentionable;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import java.util.HashMap;
import java.util.List;

import Utility.SessionManager;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by AkshayeJH on 24/07/17.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder>{

    private static final String TAG = MessageAdapter.class.getSimpleName();

    private List<Messages> mMessageList;
    private DatabaseReference mUserDatabase;
    private static final int VIEW_TYPE_ME = 1;
    private static final int VIEW_TYPE_OTHER = 2;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    Context context;
    LovelyProgressDialog lovelyProgressDialog;

    public MessageAdapter(Context context, List<Messages> mMessageList, LovelyProgressDialog lovelyProgressDialog) {

        this.context = context;
        this.mMessageList = mMessageList;
        sessionManager = new SessionManager(context);
        hashMap = new HashMap<>();
        hashMap = sessionManager.getUserDetails();
        this.lovelyProgressDialog = lovelyProgressDialog;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

//        View v = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.message_single_layout ,parent, false);
//
//        return new MessageViewHolder(v);

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        MessageViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_ME:
                View viewChatMine = layoutInflater.inflate(R.layout.message_single_layout, parent, false);
                viewHolder = new MessageViewHolder(viewChatMine);
                break;
            case VIEW_TYPE_OTHER:
                View viewChatOther = layoutInflater.inflate(R.layout.item_message_received, parent, false);
                viewHolder = new MessageViewHolder(viewChatOther);
                break;
        }
        return viewHolder;

    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public EmojiconTextView messageText;
        public CircleImageView profileImage;
        TextView dateText, timeText;
//        public TextView displayName;
//        public ImageView messageImage;

        public MessageViewHolder(View view) {
            super(view);

            dateText = (TextView) itemView.findViewById(R.id.text_open_chat_date);
            messageText = (EmojiconTextView) view.findViewById(R.id.text_message_body);
            profileImage = (CircleImageView) view.findViewById(R.id.img_profile);
            timeText = (TextView) view.findViewById(R.id.text_time_body);
//            displayName = (TextView) view.findViewById(R.id.name_text_layout);
//            messageImage = (ImageView) view.findViewById(R.id.message_image_layout);

        }

        public void showDate(String timestamp, boolean isNewDay)
        {
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                if(Utility.DateUtils.isToday(Long.valueOf(timestamp)))
                {
                    dateText.setText("TODAY");
                }
                else
                {
                    dateText.setText(Utility.DateUtils.formatDate(Long.valueOf(timestamp)));
                }
            } else {
                dateText.setVisibility(View.GONE);
            }
        }

        public String setTime(Long timestamp){
            return Utility.DateUtils.formatTime(timestamp);
        }
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder viewHolder, final int position) {

        Messages c = mMessageList.get(position);

        String from_user = c.getFrom();
        String message_type = c.getType();
        boolean isNewDay = false;


        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(from_user);

//        mUserDatabase.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                String name = dataSnapshot.child("name").getValue().toString();
//                String image = dataSnapshot.child("picture").getValue().toString();
//
//                //viewHolder.displayName.setText(name);
//                try{
//                    if(TextUtils.equals(mMessageList.get(position - 1).getFrom(),
//                            hashMap.get(SessionManager.KEY_ID))){
//                        viewHolder.profileImage.setVisibility(View.INVISIBLE);
//                    }else{
//                        Picasso.with(viewHolder.profileImage.getContext()).load(image)
//                                .placeholder(R.drawable.head_photo).into(viewHolder.profileImage);
//                    }
//                }catch (Exception e){
//                    Log.e(TAG, e.getMessage());
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        if(message_type.equals("text")) {

            viewHolder.messageText.setText(c.getMessage());
            highlightMentions(viewHolder.messageText, c.getMentions());
            viewHolder.timeText.setText(viewHolder.setTime(c.getTime()));
            //viewHolder.messageImage.setVisibility(View.INVISIBLE);
            try
            {
                if (!mMessageList.get(position - 1).toString().isEmpty()) {
                    try{
                        Messages prevMessage = mMessageList.get(position - 1);
                        if (!Utility.DateUtils.hasSameDate(c.getTime(), prevMessage.getTime())) {
                            isNewDay = true;
                            Log.i("NEW DAY_", String.valueOf(isNewDay));
                        }
                    }
                    catch (ArrayIndexOutOfBoundsException e)
                    {
                        Log.e(TAG, e.getMessage());
                    }

                }
                else {
                    isNewDay = true;
                    Log.i("_NEW DAY", String.valueOf(isNewDay));
                }
            }
            catch (Exception e)
            {
                Log.e(TAG, e.getMessage());
                isNewDay = true;
            }
            viewHolder.showDate(String.valueOf(c.getTime()), isNewDay);
        }
// else {
//
//            viewHolder.messageText.setVisibility(View.INVISIBLE);
//            Picasso.with(viewHolder.profileImage.getContext()).load(c.getMessage())
//                    .placeholder(R.drawable.head_photo).into(viewHolder.messageImage);
//
//        }

    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (TextUtils.equals(mMessageList.get(position).getFrom(),
                hashMap.get(SessionManager.KEY_ID))) {
            return VIEW_TYPE_ME;
        } else {
            return VIEW_TYPE_OTHER;
        }
    }



    private void highlightMentions(final TextView commentTextView, final List<AppMentionable> mentions) {
        if(commentTextView != null && mentions != null && !mentions.isEmpty()) {
            final Spannable spannable = new SpannableString(commentTextView.getText());

            for (final AppMentionable mention: mentions) {
                if (mention != null) {
                    final int start = mention.getMentionOffset();
                    final int end = start + mention.getMentionLength();

                    if (commentTextView.length() >= end) {
//                        spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.dark_blue_)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        spannable.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                Log.i(TAG, mention.getMentionName());
                                Log.i(TAG, mention.getMentionId());
                                showDialog("Fetching data from our server centre...");
                                if(mention.getMentionId().equals(hashMap.get(SessionManager.KEY_ID))){
                                    Toast.makeText(context, "This is you", Toast.LENGTH_SHORT).show();
                                    hideDialog();
                                }else {
                                    FirebaseDatabase.getInstance().getReference().child("Users").child(mention.getMentionId()).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            hideDialog();
                                            try{
                                                Intent intent = new Intent(context, ProfileActivity_Friend.class);
                                                intent.putExtra("message", "from_friends");
                                                intent.putExtra("is_friend", "no");
                                                intent.putExtra("name", dataSnapshot.child("name").getValue().toString());
                                                intent.putExtra("picture",  dataSnapshot.child("picture").getValue().toString());
                                                intent.putExtra("friend_id", mention.getMentionId());
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                context.startActivity(intent);

                                            }catch(Exception e)
                                            {
                                                Log.e(TAG, e.getMessage());
                                            }

                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            hideDialog();
                                            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                                            Log.e(TAG, databaseError.getMessage());
                                        }
                                    });
                                }
                            }
                            @Override
                            public void updateDrawState(TextPaint textPaint) {
                                textPaint.setColor(ContextCompat.getColor(context, R.color.linkedin_blue));    // you can use custom color
                                textPaint.setUnderlineText(true);    // this remove the underline
                            }
                        }, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        commentTextView.setMovementMethod(LinkMovementMethod.getInstance());
                        commentTextView.setText(spannable, TextView.BufferType.SPANNABLE);
                    } else {
                        //Something went wrong.  The expected text that we're trying to highlight does not
                        // match the actual text at that position.
                        Log.w("Mentions Sample", "Mention lost. [" + mention + "]");
                    }
                }


            }
        }


    }


    private void showDialog(String text_to_display)
    {
        lovelyProgressDialog.setIcon(R.drawable.ic_cast_connected_white_36dp)
                .setTitle("Please wait")
                .setTopColorRes(R.color.endColor)
                .setMessage(text_to_display)
                .setMessageGravity(Gravity.CENTER_HORIZONTAL)
                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .show();
    }

    private void hideDialog()
    {
        lovelyProgressDialog.dismiss();
    }

}
