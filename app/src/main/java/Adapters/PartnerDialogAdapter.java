package Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glt.sotm.R;

import java.util.ArrayList;

import Dialog_Fragments.PartnerDialog;

/**
 * Created by inventor on 1/23/18.
 */

public class PartnerDialogAdapter extends RecyclerView.Adapter<PartnerDialogAdapter.ViewHolder>{

    ArrayList<String> values;
    Context context;
    PartnerDialog partnerDialog;

    public PartnerDialogAdapter(Context context, ArrayList<String> values, PartnerDialog partnerDialog){
        this.values = values;
        this.context = context;
        this.partnerDialog = partnerDialog;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public ViewHolder(View v){
            super(v);
            textView = (TextView) v.findViewById(R.id.txt_partner);
        }
    }

    @Override
    public PartnerDialogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view1 = LayoutInflater.from(context).inflate(R.layout.recyclerview_partner_items,parent,false);
        ViewHolder viewHolder1 = new ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Vholder, int position){
        Vholder.textView.setText(values.get(position));
        Vholder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selected_gender = Vholder.textView.getText().toString().trim();
                partnerDialog.dialog_dismiss();
                PartnerDialog.SelectPartnerListener selectGenderListener = (PartnerDialog.SelectPartnerListener) context;
                selectGenderListener.onPartnerReturnValue(selected_gender);
            }
        });
    }

    @Override
    public int getItemCount(){
        return values.size();
    }
}
