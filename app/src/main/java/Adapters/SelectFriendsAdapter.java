package Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.glt.sotm.ImageResultActivity;
import com.glt.sotm.ProfileActivity_Friend;
import com.glt.sotm.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Models.MyFriendsDocs;
import Models.User;
import Utility.SessionManager;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by inventor on 8/13/18.
 */

public class SelectFriendsAdapter extends RecyclerView.Adapter<SelectFriendsAdapter.SelectFriendsAdapterDataHolderObject>{

    private static final String TAG = SelectFriendsAdapter.class.getSimpleName();

    List<MyFriendsDocs> docsList, selectedDocsList;
    private List<String> selectedIds = new ArrayList<>();
    Context context;
    ProgressDialog progressDialog;
    String token, user_id;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    Activity activity;

    public SelectFriendsAdapter(Activity activity, Context context, ArrayList<MyFriendsDocs> docsList, String token, String user_id, ArrayList<MyFriendsDocs> selectedDocsList) {
        this.activity = activity;

        this.context = context;
        this.docsList = docsList;
        this.selectedDocsList = selectedDocsList;
        this.token = token;
        this.user_id = user_id;
        progressDialog = new ProgressDialog(context);
        hashMap = new HashMap<>();
        sessionManager = new SessionManager(context);
        hashMap = sessionManager.getUserDetails();
    }

    @Override
    public SelectFriendsAdapterDataHolderObject onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_friends, parent, false);
        SelectFriendsAdapterDataHolderObject myConnectsAdapterDataHolderObject = new SelectFriendsAdapterDataHolderObject(view);
        return myConnectsAdapterDataHolderObject;
    }

    @Override
    public void onBindViewHolder(final SelectFriendsAdapterDataHolderObject viewHolder, int position) {
        Log.i(TAG, "onBindViewHolder");
        final MyFriendsDocs model = docsList.get(position);
        if (selectedIds.contains(model.getUser().getId())){
            //if item is selected then,set foreground color of FrameLayout.
            viewHolder.rootview.setBackground(new ColorDrawable(ContextCompat.getColor(context,R.color.colorControlActivated)));
        }
        else {
            //else remove selected item color.
            viewHolder.rootview.setBackground(new ColorDrawable(ContextCompat.getColor(context,android.R.color.transparent)));
        }
        viewHolder.txt_send_message.setVisibility(View.GONE);
        viewHolder.img_partner.setVisibility(View.VISIBLE);
        viewHolder.setFullName(model.getUser().getName());
        viewHolder.setTxt_country(model.getUser().getCountry() + " (" + model.getUser().getCity() + ")");
        //viewHolder.setThumbnail(model.getUser().getPicture());
        if(selectedDocsList.contains(model)){
            viewHolder.setThumbnail_(R.drawable.check_g);
        }else{
            viewHolder.setThumbnail(model.getUser().getPicture());
        }
        viewHolder.img_thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri picUri = Uri.parse(model.getUser().getPicture());
                Log.i(TAG, String.valueOf(picUri));
                //ImageResultActivity.startWithUri(getActivity(), picUri, hashMap.get(SessionManager.KEY_NAME), getActivity());
                Intent intent = new Intent(context, ImageResultActivity.class);
                intent.setData(picUri);
                intent.putExtra("name", model.getUser().getName());
                context.startActivity(intent);
            }
        });
        try{

            viewHolder.show_viewProfile(model.isAFriend());
            Log.i(TAG, "onBindViewHolder1");
            FirebaseDatabase.getInstance().getReference().child("Users").child(model.getUser().getId()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i(TAG, "onBindViewHolder2");
                    try{
                        Log.i(TAG, "onBindViewHolder3");
                        if(dataSnapshot.hasChild("online")) {

                            String online = dataSnapshot.child("online").getValue().toString();
                            Log.i(TAG, online);
                            Log.i(TAG, model.getUser().getName());
                            if(online.equals("true")) {
                                Log.i(TAG, "onBindViewHolder**");
                                viewHolder.online_icon.setVisibility(View.VISIBLE);
                                viewHolder.online_icon.setImageResource(R.color.cpb_green);
                                Log.i(TAG, model.getUser().getUsername() + " is online");
                                //online_docsList.add(model);
                            } else {
                                Log.i(TAG, "onBindViewHolder##");
                                viewHolder.online_icon.setVisibility(View.VISIBLE);
                                viewHolder.online_icon.setImageResource(R.color.grey_text_);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.i(TAG, "onBindViewHolder4");
                        Log.e(TAG, e.getMessage());
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.i(TAG, "onBindViewHolder5");
                    Log.e(TAG, databaseError.getMessage());
                }
            });

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewHolder.setThumbnail_(R.drawable.check_g);
                    selectedDocsList.add(model);
                }
            });
        }catch (Exception e){
            Log.e(TAG, e.getMessage());
        }

    }

    @Override
    public int getItemCount() {

        return docsList.size();
    }

    public class SelectFriendsAdapterDataHolderObject extends RecyclerView.ViewHolder {
        TextView txt_country, txt_fullName, txt_send_message;
        ImageView img_thumb, img_partner;
        CircleImageView online_icon;
        Button btn_view_profile;
        RelativeLayout rootview;

        public SelectFriendsAdapterDataHolderObject(View itemView) {
            super(itemView);
            rootview = (RelativeLayout) itemView.findViewById(R.id.rootview);
            txt_country = (TextView) itemView.findViewById(R.id.txt_country);
            txt_send_message = (TextView) itemView.findViewById(R.id.txt_send_message);
            txt_fullName = (TextView) itemView.findViewById(R.id.txt_fullname);
            img_thumb = (ImageView) itemView.findViewById(R.id.civProfilePic);
            img_partner = (ImageView) itemView.findViewById(R.id.img_partner);
            btn_view_profile = (Button) itemView.findViewById(R.id.btn_view_profile);
            online_icon = (CircleImageView) itemView.findViewById(R.id.status);
            img_partner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "This user is a partner.", Toast.LENGTH_SHORT).show();
                }
            });
        }

        public void setFullName(String name) {
            if (txt_fullName == null) return;
            txt_fullName.setText(name);
        }

        private void setTxt_country(String country) {
            if (txt_country == null) return;
            txt_country.setText(country);
        }

        private void setThumbnail(String uri) {
            Picasso.with(img_thumb.getContext())
                    .load(uri)
                    .placeholder(R.drawable.head_photo)
                    .into(img_thumb);
        }

        private void setThumbnail_(int icon) {
            Picasso.with(img_thumb.getContext())
                    .load(icon)
                    .into(img_thumb);
        }

        private void show_viewProfile(boolean isAFriend)
        {
            if(isAFriend)
            {
                btn_view_profile.setVisibility(View.GONE);
            }
            else
            {
                btn_view_profile.setVisibility(View.VISIBLE);
            }
        }

    }

}
