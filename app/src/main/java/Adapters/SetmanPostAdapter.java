package Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.glt.sotm.PlayYoutubeActivity;
import com.glt.sotm.R;
import com.glt.sotm.SetmanPostWebView;
import com.glt.sotm.UserCommentActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.joooonho.SelectableRoundedImageView;
import com.percolate.caffeine.ViewUtils;
import com.percolate.mentions.Mentionable;
import com.percolate.mentions.Mentions;
import com.percolate.mentions.QueryListener;
import com.percolate.mentions.SuggestionsListener;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import Models.AppMentionable;
import Models.ChatModel;
import Models.CommentModel;
import Models.HomeFeed;
import Models.Mention;
import Models.Response_DeletePost;
import Models.User;
import Models.UserFriend;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.Constants;
import Utility.MentionsLoaderUtils;
import Utility.NewSimpleDividerItemDecoration;
import Utility.SessionManager;
import Utility.Utils;
import chat.ChatContract;
import chat.ChatPresenter;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by inventor on 6/26/17.
 */
public class SetmanPostAdapter extends RecyclerView.Adapter<SetmanPostAdapter.SetmanPostAdapterDataHolderObject> implements ChatContract.View{

    private static final String TAG = SetmanPostAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_NORMAL = 1;
    private static final int VIEW_TYPE_LEVERAGE = 2;
    private static final int VIEW_TYPE_SOG = 3;
    private List<HomeFeed> homeFeeds;
    Context context;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    ChatPresenter mChatPresenter;
    Activity activity;
    ActivityOptions options;
    LovelyProgressDialog lovelyProgressDialog;
    LovelyStandardDialog lovelyStandardDialog;
    int[] colors = {R.color.accent};


    public SetmanPostAdapter(Activity activity, Context context, List<HomeFeed> homeFeeds, LovelyProgressDialog lovelyProgressDialog, LovelyStandardDialog lovelyStandardDialog) {
        this.context = context;
        this.homeFeeds = homeFeeds;
        this.activity = activity;
        this.lovelyProgressDialog = lovelyProgressDialog;
        this.lovelyStandardDialog = lovelyStandardDialog;
        sessionManager = new SessionManager(context);
        mChatPresenter = new ChatPresenter(this);
        hashMap = new HashMap<>();
        hashMap = sessionManager.getUserDetails();
    }

    @Override
    public SetmanPostAdapterDataHolderObject onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        SetmanPostAdapterDataHolderObject viewHolder = null;
        switch (viewType)
        {
            case VIEW_TYPE_NORMAL:
                View viewNormal = layoutInflater.inflate(R.layout.recyclerview_setman_posts, parent, false);
                viewHolder = new SetmanPostAdapterDataHolderObject(viewNormal);
                break;
            case VIEW_TYPE_LEVERAGE:
                View viewLeverage = layoutInflater.inflate(R.layout.recyclerview_leverage, parent, false);
                viewHolder = new SetmanPostAdapterDataHolderObject(viewLeverage);
                break;
            case VIEW_TYPE_SOG:
                View viewSOG = layoutInflater.inflate(R.layout.recyclerview_sog, parent, false);
                viewHolder = new SetmanPostAdapterDataHolderObject(viewSOG);
                break;
        }
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        if (homeFeeds.get(position).getType() == "SPHERES_OF_GRACE") {
            return VIEW_TYPE_SOG;
        } else if(homeFeeds.get(position).getType() == "LEVERAGE"){
            return VIEW_TYPE_LEVERAGE;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public void onBindViewHolder(final SetmanPostAdapterDataHolderObject viewHolder, final int position) {
        final HomeFeed model = homeFeeds.get(position);
        Log.i("SetmanPostAdapter", model.getType());
        viewHolder.setCoverImage(model.getUrl());
        try{model.getTitle().replace("&#8217;", "'");}catch (Exception e){Log.e(TAG, e.getMessage());}
        viewHolder.setTitle(model.getTitle());
        viewHolder.setTag(model.getType());
        getNumberComments_Shares(viewHolder, model.getId());
//        if(!sessionManager.isSetManUser()){
//            viewHolder.btn_delete.setVisibility(GONE);
//        }
//        else if(sessionManager.isSetManUser() && model.getType().toString().trim().contains("SPHERE")){
//            viewHolder.btn_delete.setVisibility(View.GONE);
//        }else if(sessionManager.isSetManUser() && model.getType().toString().trim().contains("LEVERAGE")){
//            viewHolder.btn_delete.setVisibility(View.GONE);
//        }
        viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lovelyStandardDialog.setIcon(R.drawable.ic_info_outline_white_36dp)
                        .setTitle("Delete Post")
                        .setTopColorRes(R.color.endColor)
                        .setMessage("Are you sure you want to delete?")
                        .setMessageGravity(Gravity.CENTER_HORIZONTAL)
                        .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                        .setCancelable(false)
                        .setNegativeButton("CANCEL", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //do nothing : dismiss dialog
                                lovelyStandardDialog.dismiss();
                            }
                        })
                        .setPositiveButton("YES", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //do delete here
                                deletePost(hashMap.get(SessionManager.KEY_TOKEN), model.getId(), position);
                            }
                        })
                        .show();
            }
        });
        viewHolder.btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(model.getTitle()).toString() + "\n" + Html.fromHtml(model.getDesc()).toString());
                context.startActivity(Intent.createChooser(intent, "Choose a Platform"));
            }
        });
        viewHolder.btn_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UserCommentActivity.class);
                intent.putExtra("post_id", model.getId());
                intent.putExtra("self_id", hashMap.get(SessionManager.KEY_ID));
                intent.putExtra("self_uname", hashMap.get(SessionManager.KEY_USERNAME));
                context.startActivity(intent);
            }
        });
        viewHolder.btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!viewHolder.edit_comment.getText().toString().isEmpty())
                {
                    sendComment(viewHolder.edit_comment.getText().toString().trim(),
                            hashMap.get(SessionManager.KEY_USERNAME),
                            hashMap.get(SessionManager.KEY_ID),
                            model.getId());
                    viewHolder.edit_comment.setText("");
                }
            }
        });
        //viewHolder.setDate(model.getDate().substring(0,10));
        String desc = Html.fromHtml(model.getDesc()).toString();
        //String desc = Html.fromHtml(Html.fromHtml((String) model.getDesc()).toString());
        Log.i(TAG, desc);
        desc.replace("\n", "").replace("\r","");
//        try{
//            viewHolder.setDesc(desc.substring(0, 150));
//        }catch(Exception e){
//            Log.e(TAG, e.getMessage());
//            viewHolder.setDesc(desc.substring(0, desc.length()-2));
//        }
        viewHolder.btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("SetmanPostAdapter", model.getType());

                if(model.getType().toString().trim().contains("SPHERE"))
                {
                    //show youtube player
                    Intent intent = new Intent(context, PlayYoutubeActivity.class);
                    intent.putExtra("video_id", model.getId());
                    intent.putExtra("toolbar_title", "Sphere Of Grace");
                    intent.putExtra("post_id", model.getId());
                    intent.putExtra("self_id", hashMap.get(SessionManager.KEY_ID));
                    intent.putExtra("self_uname", hashMap.get(SessionManager.KEY_USERNAME));
                    context.startActivity(intent);
                }
                else if(model.getType().toString().trim().contains("LEVERAGE"))
                {
                    Intent intent = new Intent(context, SetmanPostWebView.class);
                    intent.putExtra("type", model.getType());
                    intent.putExtra("toolbar_title", "Leverage Devotional");
                    intent.putExtra("title", model.getTitle());
                    intent.putExtra("htmlString", model.getDesc());
                    intent.putExtra("cover_image", model.getUrl());
                    intent.putExtra("post_id", model.getId());
                    intent.putExtra("self_uname", hashMap.get(SessionManager.KEY_USERNAME));
                    context.startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(context, SetmanPostWebView.class);
                    intent.putExtra("type", model.getType());
                    intent.putExtra("title", model.getTitle());
                    intent.putExtra("toolbar_title", model.getTitle());
                    intent.putExtra("htmlString", model.getDesc());
                    intent.putExtra("cover_image", model.getUrl());
                    intent.putExtra("post_id", model.getId());
                    intent.putExtra("self_id", hashMap.get(SessionManager.KEY_ID));
                    intent.putExtra("self_uname", hashMap.get(SessionManager.KEY_USERNAME));
                    context.startActivity(intent);
                }
            }
        });
        viewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("SetmanPostAdapter", model.getType());

                if(model.getType().toString().trim().contains("SPHERE"))
                {
                    //show youtube player
                    Intent intent = new Intent(context, PlayYoutubeActivity.class);
                    intent.putExtra("video_id", model.getId());
                    intent.putExtra("toolbar_title", "Sphere Of Grace");
                    intent.putExtra("post_id", model.getId());
                    intent.putExtra("self_id", hashMap.get(SessionManager.KEY_ID));
                    intent.putExtra("self_uname", hashMap.get(SessionManager.KEY_USERNAME));
                    context.startActivity(intent);
                }
                else if(model.getType().toString().trim().contains("LEVERAGE"))
                {
                    Intent intent = new Intent(context, SetmanPostWebView.class);
                    intent.putExtra("type", model.getType());
                    intent.putExtra("toolbar_title", "Leverage Devotional");
                    intent.putExtra("title", model.getTitle());
                    intent.putExtra("htmlString", model.getDesc());
                    intent.putExtra("cover_image", model.getUrl());
                    intent.putExtra("post_id", model.getId());
                    intent.putExtra("self_uname", hashMap.get(SessionManager.KEY_USERNAME));
//                    Pair[] pairs = new Pair[2];
//                    pairs[0] = new Pair<View, String> (viewHolder.cover_image, "imageTransition");
//                    pairs[1] = new Pair<View, String> (viewHolder.desc_text, "textTransition");
//
//                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                        options = ActivityOptions.makeSceneTransitionAnimation(activity, pairs);
//                    }
//                    context.startActivity(intent, options.toBundle());
                    context.startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(context, SetmanPostWebView.class);
                    intent.putExtra("type", model.getType());
                    intent.putExtra("title", model.getTitle());
                    intent.putExtra("toolbar_title", model.getTitle());
                    intent.putExtra("htmlString", model.getDesc());
                    intent.putExtra("cover_image", model.getUrl());
                    intent.putExtra("post_id", model.getId());
                    intent.putExtra("self_id", hashMap.get(SessionManager.KEY_ID));
                    intent.putExtra("self_uname", hashMap.get(SessionManager.KEY_USERNAME));
//                    if(model.getUrl() == null)
//                    {
//                        try{
//                            Pair[] pairs = new Pair[1];
//                            pairs[0] = new Pair<View, String> (viewHolder.desc_text, "textTransition");
//
//                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                                options = ActivityOptions.makeSceneTransitionAnimation(activity, pairs);
//                            }
//                        }
//                        catch (Exception e)
//                        {
//                            Log.e(TAG, e.getMessage());
//                        }
//                    }
//                    else
//                    {
//                        try
//                        {
//                            Pair[] pairs = new Pair[2];
//                            pairs[0] = new Pair<View, String> (viewHolder.cover_image, "imageTransition");
//                            pairs[1] = new Pair<View, String> (viewHolder.desc_text, "textTransition");
//
//                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                                options = ActivityOptions.makeSceneTransitionAnimation(activity, pairs);
//                            }
//                        }
//                        catch (Exception e)
//                        {
//                            Log.e(TAG, e.getMessage());
//                        }
//                    }
//
//                    context.startActivity(intent, options.toBundle());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeFeeds.size();
    }

    private void sendComment(String comment, String self_uname, String self_id, String post_id) {

        CommentModel commentModel = new CommentModel(self_uname, self_id,
                comment, System.currentTimeMillis(), new ArrayList<AppMentionable>());
        mChatPresenter.sendComment(context,
                commentModel,
                self_id, post_id);
    }

    @Override
    public void onSendMessageSuccess() {
        Toast.makeText(context, "Comment posted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSendMessageFailure(String message) {

    }

    @Override
    public void onGetMessagesSuccess(ChatModel chat) {

    }

    @Override
    public void onGetMessagesFailure(String message) {

    }

    @Override
    public void onGetCommentsSuccess(CommentModel commentModel) {

    }

    public class SetmanPostAdapterDataHolderObject extends RecyclerView.ViewHolder
    {
        SelectableRoundedImageView cover_image;
        TextView title_text, date_text, tag_text;
        ReadMoreTextView desc_text;
        View vw_line;
        CardView card_view;
        Button btn_comment, btn_share, btn_comments, btn_view, btn_delete;
        EmojiconEditText edit_comment;

        private ImageView btEmoji;
        private EmojIconActions emojIcon;

        public SetmanPostAdapterDataHolderObject(View itemView) {
            super(itemView);
            cover_image = (SelectableRoundedImageView) itemView.findViewById(R.id.cover_image);
            title_text = (TextView) itemView.findViewById(R.id.title_text);
            desc_text = (ReadMoreTextView) itemView.findViewById(R.id.desc_text);
            desc_text.setVisibility(GONE);
            date_text = (TextView) itemView.findViewById(R.id.date_text);
            vw_line = (View) itemView.findViewById(R.id.vw_line);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            tag_text = (TextView) itemView.findViewById(R.id.tag_text);
            btn_comments = (Button) itemView.findViewById(R.id.btn_comments);
            btn_view = (Button) itemView.findViewById(R.id.btn_view);
            btn_delete = (Button) itemView.findViewById(R.id.btn_delete);
            btn_comment = (Button) itemView.findViewById(R.id.btn_comment);
            btn_share = (Button) itemView.findViewById(R.id.btn_share);

            edit_comment = (EmojiconEditText)itemView.findViewById(R.id.edit_comment);

            btEmoji = (ImageView)itemView.findViewById(R.id.buttonEmoji);
            emojIcon = new EmojIconActions(context,itemView,edit_comment,btEmoji);
            emojIcon.ShowEmojIcon();
        }

        private void setCoverImage(String image_url){
            Random rand = new Random();
            //int c = colors[rand.nextInt(colors.length)];
            int c = colors[0];
            if(image_url == null)
            {
                Log.i(TAG, "null");
                Log.i(TAG, String.valueOf(c));
                cover_image.setVisibility(View.VISIBLE);
                if (cover_image == null)return;
                try{
                    Picasso.with(context)
                            .load(c)
                            .placeholder(c)
                            .error(c)
                            .into(cover_image);}catch(IllegalArgumentException e){Log.e(TAG, e.getMessage());}

            }
            else
            {
                Log.i(TAG, "not null");
                cover_image.setVisibility(View.VISIBLE);
                if (cover_image == null)return;
                try{
                    Picasso.with(context)
                            .load(image_url)
                            .placeholder(c)
                            .error(c)
                            .into(cover_image);}catch(IllegalArgumentException e){Log.e(TAG, e.getMessage());}
            }
        }

        private void setTitle(String title)
        {
            if(title_text == null) return;
            title_text.setText(title);
        }

        private void setTag(String title)
        {
            if(tag_text == null)
                return;
            if(title.toString().trim().contains("SPHERE"))
            {
                btn_delete.setVisibility(GONE);
                tag_text.setVisibility(View.VISIBLE);
                tag_text.setText("Sphere of Grace");
            }
            else if(title.toString().trim().contains("LEVERAGE"))
            {
                btn_delete.setVisibility(GONE);
                tag_text.setVisibility(View.VISIBLE);
                tag_text.setText("Leverage Devotional");
            }
            else
            {
                if(sessionManager.isSetManUser()){
                    btn_delete.setVisibility(View.VISIBLE);
                }
                tag_text.setVisibility(GONE);
                tag_text.setText("Post");
            }
        }

//        private void setDesc(String desc)
//        {
//            if(desc_text == null || desc_text.getText().toString().isEmpty()){
//                desc_text.setVisibility(GONE);
//            }
//            else {
//                try {
//                    desc_text.setText(desc);
//                } catch (Exception e) {
//                    Log.e(TAG, e.getMessage());
//                }
//            }
//        }

        private void setDate(String date)
        {
            if(date_text == null)return;
            date_text.setText(date);
        }

        private void setBtn_comments(String nComments)
        {
            if(btn_comments == null) return;
            btn_comments.setText(nComments);
        }

    }

    private CharSequence convertTimestamp(String mileSeconds){
        return DateUtils.getRelativeTimeSpanString(Long.parseLong(mileSeconds), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }

    private void getNumberComments_Shares(final SetmanPostAdapterDataHolderObject viewHolder, final String post_id){
        viewHolder.setBtn_comments("LOADING...");
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child(Constants.ARG_COMMENT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild(post_id))
                {
                    viewHolder.setBtn_comments(String.valueOf(dataSnapshot.child(post_id).child(Constants.ARG_COMMENTS).getChildrenCount()) + "  Comments");
                }
                else
                {
                    viewHolder.setBtn_comments("0  Comments");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                viewHolder.setBtn_comments("0  Comments");
            }
        });
    }

    private void deletePost(String token, String post_id, final int position){
        showDialog("Deleting post");
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_DeletePost> call = service.deletePost(token, post_id);
        call.enqueue(new Callback<Response_DeletePost>() {
            @Override
            public void onResponse(Call<Response_DeletePost> call,
                                   Response<Response_DeletePost> response) {
                hideDialog();
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Log.i(TAG, response.body().toString());
                    Log.i(TAG + " Response body", response.body().toString());
                    homeFeeds.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, homeFeeds.size());
                    notifyDataSetChanged();
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(context, errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_DeletePost> call, Throwable t) {
                hideDialog();
                call.cancel();
                Toast.makeText(context, Html.fromHtml(context.getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showDialog(String text_to_display)
    {
        lovelyProgressDialog.setIcon(R.drawable.ic_info_outline_white_36dp)
                .setTitle("Please wait")
                .setTopColorRes(R.color.endColor)
                .setMessage(text_to_display)
                .setMessageGravity(Gravity.CENTER_HORIZONTAL)
                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .show();
    }

    private void hideDialog()
    {
        lovelyProgressDialog.dismiss();
    }

}
