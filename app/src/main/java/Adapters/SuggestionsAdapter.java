package Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.glt.sotm.FriendsActivity;
import com.glt.sotm.ProfileActivity_Friend;
import com.glt.sotm.R;
import com.glt.sotm.UserFriendChatActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import Models.MyFriendsDocs;
import Models.Response_FindConnects;
import Models.Response_FindFriends;
import Models.Response_User;
import Models.User;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import Utility.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by inventor on 9/27/17.
 */
public class SuggestionsAdapter extends RecyclerView.Adapter<SuggestionsAdapter.SuggestionsAdapterDataHolderObject>{

    private static final String TAG = FriendsAdapter.class.getSimpleName();

    List<MyFriendsDocs> docsList;
    List<MyFriendsDocs> docsListFiltered;
    Context context;
    ProgressDialog progressDialog;
    User self;
    String token, user_id;
    ArrayList<MyFriendsDocs> filteredList;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;

    public SuggestionsAdapter(Context context, ArrayList<MyFriendsDocs> docsList, String token, String user_id) {
        this.context = context;
        this.docsList = docsList;
        this.docsListFiltered = docsList;
        this.token = token;
        this.user_id = user_id;
        progressDialog = new ProgressDialog(context);
        filteredList = new ArrayList<>();
        hashMap = new HashMap<>();
        sessionManager = new SessionManager(context);
        hashMap = sessionManager.getUserDetails();
    }

    @Override
    public SuggestionsAdapterDataHolderObject onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_suggested_friends, parent, false);
        SuggestionsAdapterDataHolderObject myConnectsAdapterDataHolderObject = new SuggestionsAdapterDataHolderObject(view);
        return myConnectsAdapterDataHolderObject;
    }

    @Override
    public void onBindViewHolder(final SuggestionsAdapterDataHolderObject viewHolder, int position) {
        try {
            final MyFriendsDocs model = docsListFiltered.get(position);
            viewHolder.txt_send_message.setVisibility(View.GONE);
            viewHolder.img_partner.setVisibility(View.VISIBLE);
            viewHolder.setFullName(model.getUser().getName());
            viewHolder.setTxt_country(model.getUser().getCountry() + " (" + model.getUser().getCity() + ")");
            viewHolder.setThumbnail(model.getUser().getPicture());
            viewHolder.show_viewProfile(model.isAFriend());

            if (model.getUser().isAPartner()) {
                viewHolder.setImg_partner();
            } else {
                viewHolder.setImg_partner_not();
            }


            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProfileActivity_Friend.class);
                    intent.putExtra("message", "from_friends");
                    intent.putExtra("is_friend", "no");
                    intent.putExtra("name", model.getUser().getName());
                    intent.putExtra("picture", model.getUser().getPicture());
                    intent.putExtra("friend_id", model.getUser().getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public int getItemCount() {

        return docsListFiltered.size();
    }


    public class SuggestionsAdapterDataHolderObject extends RecyclerView.ViewHolder {
        TextView txt_country, txt_fullName, txt_send_message;
        ImageView img_thumb, img_partner, online_icon;
        Button btn_view_profile;

        public SuggestionsAdapterDataHolderObject(View itemView) {
            super(itemView);
            txt_country = (TextView) itemView.findViewById(R.id.txt_country);
            txt_send_message = (TextView) itemView.findViewById(R.id.txt_send_message);
            txt_fullName = (TextView) itemView.findViewById(R.id.txt_fullname);
            img_thumb = (ImageView) itemView.findViewById(R.id.img_thumb);
            img_partner = (ImageView) itemView.findViewById(R.id.img_partner);
            btn_view_profile = (Button) itemView.findViewById(R.id.btn_view_profile);
            online_icon = (ImageView) itemView.findViewById(R.id.online_icon);
            img_partner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "This user is a partner.", Toast.LENGTH_SHORT).show();
                }
            });
        }

        public void setFullName(String name) {
            if (txt_fullName == null) return;
            txt_fullName.setText(name);
        }

        private void setTxt_country(String country) {
            if (txt_country == null) return;
            txt_country.setText(country);
        }

        private void setThumbnail(String uri) {
            Picasso.with(img_thumb.getContext())
                    .load(uri)
                    .placeholder(R.drawable.head_photo)
                    .into(img_thumb);
        }

        private void setImg_partner() {
            img_partner.setVisibility(View.VISIBLE);
        }

        private void setImg_partner_not() {
            img_partner.setVisibility(View.GONE);
        }

        private void show_viewProfile(boolean isAFriend) {
            if (isAFriend) {
                btn_view_profile.setVisibility(View.GONE);
            } else {
                btn_view_profile.setVisibility(View.VISIBLE);
            }
        }

    }

    private void getSelfProfile(final String token, final String user_id, final MyFriendsDocs model) {
        showPDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_User> call = service.getUserProfile(token, user_id);
        call.enqueue(new Callback<Response_User>() {
            @Override
            public void onResponse(Call<Response_User> call,
                                   Response<Response_User> response) {
                hidePDialog();
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if (response.code() == Utils.STATUS_200_OK) {
                    Log.i(TAG, response.body().toString());
                    self = response.body().getData().getUser();
                    Log.i(TAG + " getSelfProfile", response.body().toString());
                    Log.i(TAG + " getSelfProfile", self.toString());
                    Intent intent = new Intent(context, UserFriendChatActivity.class);
                    intent.putExtra("friend_data", model.getUser());
                    intent.putExtra("self_data", self);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(context, errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_User> call, Throwable t) {
                hidePDialog();
                call.cancel();
                Toast.makeText(context, Html.fromHtml(context.getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showPDialog() {
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void hidePDialog() {
        progressDialog.dismiss();
    }

    public void removeItem(int position) {
        docsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, docsList.size());
    }
}
