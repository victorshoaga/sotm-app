package Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.glt.sotm.ProfileActivity_Friend;
import com.glt.sotm.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import Models.User;
import Models.UserData;
import Models.User_Friend_Request;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by inventor on 9/25/17.
 */
public class SwipeStackAdapter extends BaseAdapter {

    private List<User_Friend_Request> docsList;
    private Context context;

    public SwipeStackAdapter(Context context, List<User_Friend_Request> docsList) {
        this.context = context;
        this.docsList = docsList;
    }

    @Override
    public int getCount() {
        int size = 0;
        if(docsList != null)
        {
            size = docsList.size();
        }
        return size;
    }

    @Override
    public Object getItem(int position) {
        Object object = null;
        if(docsList != null)
        {
            object = docsList.get(position);
        }
        return object;
    }

    @Override
    public long getItemId(int position) {
        int pos = 0;
        if(docsList != null)
        {
            pos = position;
        }
        return pos;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swipe_stack_card, parent, false);
        }
        CircleImageView img_profilepic = (CircleImageView) view.findViewById(R.id.civProfilePic);
        TextView txt_ProfileName = (TextView) view.findViewById(R.id.txt_ProfileName);
        TextView txt_ProfileFollowers = (TextView) view.findViewById(R.id.txt_ProfileFollowers);
        TextView profile_edt_uname = (TextView) view.findViewById(R.id.profile_edt_uname);
        TextView profile_edt_email = (TextView) view.findViewById(R.id.profile_edt_mail);
        TextView profile_edt_location = (TextView) view.findViewById(R.id.profile_edt_location);
        TextView profile_edt_gender = (TextView) view.findViewById(R.id.profile_edt_gender);
        //EditText profile_edt_partner = (EditText) view.findViewById(R.id.profile_edt_partner);
        if(docsList != null)
        {
            final User_Friend_Request model = docsList.get(position);
            Picasso.with(parent.getContext())
                    .load(model.getUser().getPicture())
                    .placeholder(R.drawable.head_photo)
                    .into(img_profilepic);
            txt_ProfileName.setText(model.getUser().getName());
            txt_ProfileFollowers.setText(String.valueOf(model.getUser().getFriends()) + " Friend(s)");
            profile_edt_uname.setText("@"+model.getUser().getUsername());
            profile_edt_email.setText(model.getUser().getEmail());
            profile_edt_email.setVisibility(View.GONE);
            profile_edt_location.setText(model.getUser().getCountry() + " (" + model.getUser().getCity() + ")");
            profile_edt_gender.setText(model.getUser().getGender());
            //profile_edt_partner.setVisibility(View.GONE);
            //profile_edt_partner.setText(maskPartner(String.valueOf(model.getUser().isAPartner())));
            Picasso.with(context)
                    .load(model.getUser().getPicture())
                    .placeholder(R.drawable.head_photo)
                    .into(img_profilepic);
            img_profilepic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProfileActivity_Friend.class);
                    intent.putExtra("message", "from_friends");
                    intent.putExtra("is_friend", "no");
                    intent.putExtra("name", model.getUser().getName());
                    intent.putExtra("picture", model.getUser().getPicture());
                    intent.putExtra("friend_id", model.getUser().getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }

        return view;
    }

    private String maskPartner(String b)
    {
        String s = "";
        if(b == "true")
        {
            s = "Yes";
        }
        else
        {
            s = "No";
        }
        return s;
    }
}
