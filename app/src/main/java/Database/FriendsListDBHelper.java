package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import Models.MyFriendsDocs;
import Models.User;
import Models.UserFriend;


/**
 * Created by inventor on 5/12/16.
 */
public class FriendsListDBHelper {

    public static final String FRIEND_ID = "friend_id";
    public static final String FRIEND_UID = "friend_uid";
    public static final String FRIEND_EMAIL = "friend_email";
    public static final String FRIEND_USERNAME = "friend_username";
    public static final String FRIEND_NAME = "friend_name";
    public static final String FRIEND_PICTURE = "friend_picture";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    private static final String DATABASE_NAME = "FriendsDB.db";
    private static final int DATABASE_VERSION = 1;
    private static final String FRIENDSLIST_TABLE = "FriendTable";
    private static final String CREATE_PROFILE_TABLE = "create table "
            + FRIENDSLIST_TABLE + " (" + FRIEND_ID
            + " integer primary key autoincrement, " + FRIEND_UID
            + " text not null, " + FRIEND_EMAIL + " text not null , "
            + FRIEND_USERNAME + " text not null, "
            + FRIEND_NAME + " text not null, "
            + FRIEND_PICTURE + " text not null);";

    private Context mCtx;
    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_PROFILE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + FRIENDSLIST_TABLE);
            onCreate(db);
        }
    }

    public void Reset() {
        mDbHelper.onUpgrade(this.mDb, 1, 1);
    }

    public FriendsListDBHelper(Context ctx) {
        mCtx = ctx;
        mDbHelper = new DatabaseHelper(mCtx);
    }

    public FriendsListDBHelper openW() throws SQLException {
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public FriendsListDBHelper openR() throws SQLException
    {
        mDb = mDbHelper.getReadableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public void insertFriend(UserFriend userFriend) {

        ContentValues cv = new ContentValues();
        cv.put(FRIEND_UID, userFriend.getId());
        cv.put(FRIEND_EMAIL, userFriend.getEmail());
        cv.put(FRIEND_USERNAME, userFriend.getUsername());
        cv.put(FRIEND_NAME, userFriend.getName());
        cv.put(FRIEND_PICTURE, userFriend.getPicture());
        mDb.insert(FRIENDSLIST_TABLE, null , cv);

    }

    public Integer deleteFriendsList() throws SQLException
    {
        return mDb.delete(FRIENDSLIST_TABLE,
                null,
                new String[] {});
    }

    public ArrayList<UserFriend> getFriendsList() throws SQLException {
        ArrayList<UserFriend> docs = new ArrayList<>();
        Cursor cur = mDb.query(true, FRIENDSLIST_TABLE, new String[] {FRIEND_UID, FRIEND_EMAIL, FRIEND_USERNAME,
                        FRIEND_NAME, FRIEND_PICTURE},
                null, null, null, null, null, null);
        if (cur.getCount() > 0)
        {
            for (int i = 0; i < cur.getCount(); i++)
            {
                cur.moveToNext();
                String id = cur.getString(cur.getColumnIndex(FRIEND_UID));
                String email = cur.getString(cur.getColumnIndex(FRIEND_EMAIL));
                String username = cur.getString(cur.getColumnIndex(FRIEND_USERNAME));
                String name = cur.getString(cur.getColumnIndex(FRIEND_NAME));
                String picture = cur.getString(cur.getColumnIndex(FRIEND_PICTURE));
                docs.add(new UserFriend(id, email, picture, username, name));
            }
        }
        cur.close();
        return docs;
    }
}