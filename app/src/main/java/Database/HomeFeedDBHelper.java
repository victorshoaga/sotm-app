package Database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import Models.HomeFeed;


/**
 * Created by inventor on 5/12/16.
 */
public class HomeFeedDBHelper {

    public static final String HOMEFEED_ID = "home_feed_id";
    public static final String HOMEFEED_UID = "home_feed_uid";
    public static final String HOMEFEED_URL= "home_feed_url";
    public static final String HOMEFEED_DATE = "home_feed_date";
    public static final String HOMEFEED_TITLE = "home_feed_title";
    public static final String HOMEFEED_DESC = "home_feed_desc";
    public static final String HOMEFEED_TYPE = "home_feed_type";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    private static final String DATABASE_NAME = "HomeFeedDB.db";
    private static final int DATABASE_VERSION = 1;
    private static final String HOMEFEED_TABLE = "HomeFeed";
    private static final String CREATE_PROFILE_TABLE = "create table "
            + HOMEFEED_TABLE + " (" + HOMEFEED_ID
            + " integer primary key autoincrement, " + HOMEFEED_UID
            + " text not null, " + HOMEFEED_URL + " text default '', "
            + HOMEFEED_DATE + " text not null, "
            + HOMEFEED_TITLE + " text not null, "
            + HOMEFEED_DESC + " text not null, "
            + HOMEFEED_TYPE + " text not null);";

    private Context mCtx;
    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_PROFILE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + HOMEFEED_TABLE);
            onCreate(db);
        }
    }

    public void Reset() {
        mDbHelper.onUpgrade(this.mDb, 1, 1);
    }

    public HomeFeedDBHelper(Context ctx) {
        mCtx = ctx;
        mDbHelper = new DatabaseHelper(mCtx);
    }

    public HomeFeedDBHelper openW() throws SQLException {
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public HomeFeedDBHelper openR() throws SQLException
    {
        mDb = mDbHelper.getReadableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public void insertHomeFeed(HomeFeed homeFeed) {

        ContentValues cv = new ContentValues();
        cv.put(HOMEFEED_UID, homeFeed.getId());
        cv.put(HOMEFEED_URL, homeFeed.getUrl());
        cv.put(HOMEFEED_DATE, homeFeed.getDate());
        cv.put(HOMEFEED_TITLE, homeFeed.getTitle());
        cv.put(HOMEFEED_DESC, homeFeed.getDesc());
        cv.put(HOMEFEED_TYPE, homeFeed.getType());
        mDb.insert(HOMEFEED_TABLE, null , cv);

    }

    public Integer deleteHomeFeed() throws SQLException
    {
        return mDb.delete(HOMEFEED_TABLE,
                null,
                new String[] {});
    }

    public ArrayList<HomeFeed> getHomeFeeds() throws SQLException {
        ArrayList<HomeFeed> homeFeeds = new ArrayList<>();
        Cursor cur = mDb.query(true, HOMEFEED_TABLE, new String[] {HOMEFEED_UID, HOMEFEED_URL, HOMEFEED_DATE,
                HOMEFEED_TITLE, HOMEFEED_DESC, HOMEFEED_TYPE},
                null, null, null, null, null, null);
        if (cur.getCount() > 0)
        {
            for (int i = 0; i < cur.getCount(); i++)
            {
                cur.moveToNext();
                String id = cur.getString(cur.getColumnIndex(HOMEFEED_UID));
                String url = cur.getString(cur.getColumnIndex(HOMEFEED_URL));
                String date = cur.getString(cur.getColumnIndex(HOMEFEED_DATE));
                String title = cur.getString(cur.getColumnIndex(HOMEFEED_TITLE));
                String desc = cur.getString(cur.getColumnIndex(HOMEFEED_DESC));
                String type = cur.getString(cur.getColumnIndex(HOMEFEED_TYPE));
                homeFeeds.add(new HomeFeed(id, url, date, title, desc, type));
            }
        }
        cur.close();
        return homeFeeds;
    }
}