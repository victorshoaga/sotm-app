package Dialog_Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.glt.sotm.R;

import java.util.ArrayList;

import Adapters.CityDialogAdapter;
import Utility.NewSimpleDividerItemDecoration;

/**
 * Created by inventor on 1/23/18.
 */

public class CityDialog extends android.support.v4.app.DialogFragment {

    private static final String TAG = CityDialog.class.getSimpleName();

    RecyclerView rvListMessage;
    LinearLayoutManager mLinearLayoutManager;
    ArrayList<String> city_list;
    CityDialogAdapter cityDialogAdapter;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static CityDialog newInstance(ArrayList<String> city_list) {
        CityDialog f = new CityDialog();
        Bundle args = new Bundle();
        args.putStringArrayList("cities", city_list);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        city_list = getArguments().getStringArrayList("cities");
        for(int i = 0; i< city_list.size(); i++)
        {
            Log.i(TAG, city_list.get(i));
        }
        cityDialogAdapter = new CityDialogAdapter(getActivity(), city_list, this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_city, container, false);
        //setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
        getDialog().requestWindowFeature(Window.FEATURE_LEFT_ICON);
        //getDialog().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_place_black_24dp);
        bindViews(rootView);
        rvListMessage.setAdapter(cityDialogAdapter);
        return rootView;
    }

    private void bindViews(View view){
        rvListMessage = (RecyclerView)view.findViewById(R.id.recycler_view_cities);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        rvListMessage.setHasFixedSize(true);
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        rvListMessage.addItemDecoration(new NewSimpleDividerItemDecoration(
                getActivity(), NewSimpleDividerItemDecoration.DEFAULT_PADDING_LEFT
        ));
        rvListMessage.setItemAnimator(new DefaultItemAnimator());
    }

    public interface SelectCityListener {
        void onCityReturnValue(String city_selected);
    }

    public void dialog_dismiss()
    {
        getDialog().dismiss();
    }
}
