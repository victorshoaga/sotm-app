package Dialog_Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.glt.sotm.R;

import java.util.ArrayList;

import Adapters.CountryDialogAdapter;
import Utility.NewSimpleDividerItemDecoration;

/**
 * Created by inventor on 1/20/18.
 */

public class CountryDialog extends DialogFragment {

    private static final String TAG = CountryDialog.class.getSimpleName();

    RecyclerView rvListMessage;
    LinearLayoutManager mLinearLayoutManager;
    ArrayList<String> country_list;
    CountryDialogAdapter countryDialogAdapter;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static CountryDialog newInstance(ArrayList<String> country_list) {
        CountryDialog f = new CountryDialog();
        Bundle args = new Bundle();
        args.putStringArrayList("countries", country_list);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        country_list = getArguments().getStringArrayList("countries");
        for(int i = 0; i< country_list.size(); i++)
        {
            Log.i(TAG, country_list.get(i));
        }
        countryDialogAdapter = new CountryDialogAdapter(getActivity(), country_list, this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_country, container, false);
        //setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
        getDialog().requestWindowFeature(Window.FEATURE_LEFT_ICON);
        //getDialog().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_place_black_24dp);
        bindViews(rootView);
        rvListMessage.setAdapter(countryDialogAdapter);
        return rootView;
    }

    private void bindViews(View view){
        rvListMessage = (RecyclerView)view.findViewById(R.id.recycler_view_countries);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        rvListMessage.setHasFixedSize(true);
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        rvListMessage.addItemDecoration(new NewSimpleDividerItemDecoration(
                getActivity(), NewSimpleDividerItemDecoration.DEFAULT_PADDING_LEFT
        ));
        rvListMessage.setItemAnimator(new DefaultItemAnimator());
    }

    public interface SelectCountryListener {
        void onReturnValue(String country_selected);
    }

    public void dialog_dismiss()
    {
        getDialog().dismiss();
    }

}
