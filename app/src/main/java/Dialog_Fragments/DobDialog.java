package Dialog_Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.glt.sotm.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import Adapters.GenderDialogAdapter;

/**
 * Created by inventor on 1/23/18.
 */

public class DobDialog extends DialogFragment{

    private static final String TAG = DobDialog.class.getSimpleName();
    MaterialCalendarView calendarView;
    String selectedDate = "";

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static DobDialog newInstance() {
        DobDialog f = new DobDialog();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_dob, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_LEFT_ICON);
        //getDialog().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_place_black_24dp);
        bindViews(rootView);
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                TimeZone tz = TimeZone.getTimeZone("GMT");
                DateFormat df = new SimpleDateFormat("MM/DD/yyyy", Locale.getDefault()); // Quoted "Z" to indicate UTC, no timezone offset
                df.setTimeZone(tz);
                selectedDate = df.format(date.getDate());
                dialog_dismiss();
                Log.i(TAG, "Selected Date:\n" + selectedDate);
                DobDialog.SelectDobListener selectDobListener = (DobDialog.SelectDobListener) getActivity();
                selectDobListener.onDobReturnValue(selectedDate);
            }
        });
        return rootView;
    }

    private void bindViews(View view){
        calendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        calendarView.setCurrentDate(new CalendarDay(1999, 1, 1));
    }

    public interface SelectDobListener {
        void onDobReturnValue(String dob_selected);
    }

    public void dialog_dismiss()
    {
        getDialog().dismiss();
    }
}
