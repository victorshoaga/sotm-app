package Dialog_Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.glt.sotm.R;

import java.util.ArrayList;

import Adapters.GenderDialogAdapter;
import Utility.NewSimpleDividerItemDecoration;

/**
 * Created by inventor on 1/23/18.
 */

public class GenderDialog extends DialogFragment{

    private static final String TAG = GenderDialog.class.getSimpleName();

    RecyclerView rvListMessage;
    LinearLayoutManager mLinearLayoutManager;
    ArrayList<String> gender_list;
    GenderDialogAdapter genderDialogAdapter;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static GenderDialog newInstance(ArrayList<String> gender_list) {
        GenderDialog f = new GenderDialog();
        Bundle args = new Bundle();
        args.putStringArrayList("gender", gender_list);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gender_list = getArguments().getStringArrayList("gender");
        for(int i = 0; i< gender_list.size(); i++)
        {
            Log.i(TAG, gender_list.get(i));
        }
        genderDialogAdapter = new GenderDialogAdapter(getActivity(), gender_list, this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_gender, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_LEFT_ICON);
        //setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
        //getDialog().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_place_black_24dp);
        bindViews(rootView);
        rvListMessage.setAdapter(genderDialogAdapter);
        return rootView;
    }

    private void bindViews(View view){
        rvListMessage = (RecyclerView)view.findViewById(R.id.recycler_view_gender);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        rvListMessage.setHasFixedSize(true);
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        rvListMessage.addItemDecoration(new NewSimpleDividerItemDecoration(
                getActivity(), NewSimpleDividerItemDecoration.DEFAULT_PADDING_LEFT
        ));
        rvListMessage.setItemAnimator(new DefaultItemAnimator());
    }

    public interface SelectGenderListener {
        void onGenderReturnValue(String gender_selected);
    }

    public void dialog_dismiss()
    {
        getDialog().dismiss();
    }
}
