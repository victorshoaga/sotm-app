package Dialog_Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.glt.sotm.R;

import java.util.ArrayList;

import Adapters.PartnerDialogAdapter;
import Utility.NewSimpleDividerItemDecoration;

/**
 * Created by inventor on 1/23/18.
 */

public class PartnerDialog extends DialogFragment{

    private static final String TAG = PartnerDialog.class.getSimpleName();

    RecyclerView rvListMessage;
    LinearLayoutManager mLinearLayoutManager;
    ArrayList<String> partner_list;
    PartnerDialogAdapter partnerDialogAdapter;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static PartnerDialog newInstance(ArrayList<String> partner_list) {
        PartnerDialog f = new PartnerDialog();
        Bundle args = new Bundle();
        args.putStringArrayList("partner", partner_list);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        partner_list = getArguments().getStringArrayList("partner");
        for(int i = 0; i< partner_list.size(); i++)
        {
            Log.i(TAG, partner_list.get(i));
        }
        partnerDialogAdapter = new PartnerDialogAdapter(getActivity(), partner_list, this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_partner, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_LEFT_ICON);
        //getDialog().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_place_black_24dp);
        bindViews(rootView);
        rvListMessage.setAdapter(partnerDialogAdapter);
        return rootView;
    }

    private void bindViews(View view){
        rvListMessage = (RecyclerView)view.findViewById(R.id.recycler_view_partner);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        rvListMessage.setHasFixedSize(true);
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        rvListMessage.addItemDecoration(new NewSimpleDividerItemDecoration(
                getActivity(), NewSimpleDividerItemDecoration.DEFAULT_PADDING_LEFT
        ));
        rvListMessage.setItemAnimator(new DefaultItemAnimator());
    }

    public interface SelectPartnerListener {
        void onPartnerReturnValue(String partner_selected);
    }

    public void dialog_dismiss()
    {
        getDialog().dismiss();
    }
}
