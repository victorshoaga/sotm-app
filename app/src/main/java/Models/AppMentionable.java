package Models;

/**
 * Created by inventor on 4/15/18.
 */

public class AppMentionable {

    int mentionOffset;
    int mentionLength;
    String mentionName;
    String mentionId;

    public AppMentionable() {
    }

    public AppMentionable(int mentionOffset, int mentionLength, String mentionName, String mentionId) {
        this.mentionOffset = mentionOffset;
        this.mentionLength = mentionLength;
        this.mentionName = mentionName;
        this.mentionId = mentionId;
    }

    public int getMentionOffset() {
        return mentionOffset;
    }

    public void setMentionOffset(int mentionOffset) {
        this.mentionOffset = mentionOffset;
    }

    public int getMentionLength() {
        return mentionLength;
    }

    public void setMentionLength(int mentionLength) {
        this.mentionLength = mentionLength;
    }

    public String getMentionName() {
        return mentionName;
    }

    public void setMentionName(String mentionName) {
        this.mentionName = mentionName;
    }

    public String getMentionId() {
        return mentionId;
    }

    public void setMentionId(String mentionId) {
        this.mentionId = mentionId;
    }

    @Override
    public String toString() {
        return "AppMentionable{" +
                "mentionOffset=" + mentionOffset +
                ", mentionLength=" + mentionLength +
                ", mentionName='" + mentionName + '\'' +
                ", mentionId='" + mentionId + '\'' +
                '}';
    }
}
