package Models;


/**
 * Created by inventor on 3/9/17.
 */
public class ChatModel {

    private String talking_to;
    private String self;
    private String message;
    private Long timestamp;
    private boolean unread;


    public ChatModel() {
    }

    public ChatModel(String message, String self, Long timestamp) {
        this.message = message;
        this.self = self;
        this.timestamp = timestamp;
    }

    public ChatModel(String self, Long timestamp) {
        this.self = self;
        this.timestamp = timestamp;
    }

    public ChatModel(String message, String self, Long timestamp, String talking_to) {
        this.message = message;
        this.self = self;
        this.timestamp = timestamp;
        this.talking_to = talking_to;
    }
    public Long getTimeStamp() {
        return timestamp;
    }

    public void setTimeStamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getSelf() {
        return self;
    }

    public void setUser(String self) {
        this.self = self;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTalking_to() {
        return talking_to;
    }

    public void setTalking_to(String talking_to) {
        this.talking_to = talking_to;
    }


    @Override
    public String toString() {
        return "ChatModel{" +
                "message='" + message + '\'' +
                ", user=" + self +
                ", talking_to=" + talking_to +
                ", timeStamp='" + String.valueOf(timestamp) + '\'' +
                '}';
    }
}
