package Models;

import com.percolate.mentions.Mentionable;

import java.util.List;

/**
 * Created by inventor on 1/26/18.
 */

public class CommentModel {

    private String id;
    private String username;
    private String comment;
    private Long timestamp;
    private List<AppMentionable> mentions;

    public CommentModel() {
    }

    public CommentModel(String username, String id, String comment, Long timestamp, List<AppMentionable> mentions) {
        this.id = id;
        this.comment = comment;
        this.timestamp = timestamp;
        this.username = username;
        this.mentions = mentions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public List<AppMentionable> getMentions() {
        return mentions;
    }

    public void setMentions(List<AppMentionable> mentions) {
        this.mentions = mentions;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "CommentModel{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", comment='" + comment + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
