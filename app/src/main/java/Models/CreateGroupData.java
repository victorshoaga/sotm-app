package Models;

/**
 * Created by inventor on 8/19/18.
 */

public class CreateGroupData {
    private Group group;

    public CreateGroupData(Group group) {
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "CreateGroupData{" +
                "group=" + group +
                '}';
    }
}
