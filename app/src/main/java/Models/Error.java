package Models;

import java.util.List;

/**
 * Created by excellence.ilesanmi on 01/11/2017.
 */

public class Error {
    private boolean err;
    private String msg;
    private List<String> errors;

    public boolean isErr() {
        return err;
    }

    public String getMsg() {
        return msg;
    }

    public List<String> getErrors() {
        return errors;
    }
}
