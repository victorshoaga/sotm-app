package Models;

/**
 * Created by inventor on 12/17/17.
 */

public class FeedBackModel {

    private String feedback;
    private long timeStamp;

    public FeedBackModel(String feedback, long timeStamp) {
        this.feedback = feedback;
        this.timeStamp = timeStamp;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "FeedBackModel{" +
                "feedback='" + feedback + '\'' +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
