package Models;

/**
 * Created by inventor on 8/19/18.
 */

public class Group {

    private String id;
    private String creator_id;
    private String topic;
    private int posts;
    private int members;
    private String createdAt;
    private String updatedAt;

    public Group(String id, String creator_id, String topic, int posts, int members, String createdAt, String updatedAt) {
        this.id = id;
        this.creator_id = creator_id;
        this.topic = topic;
        this.posts = posts;
        this.members = members;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getPosts() {
        return posts;
    }

    public void setPosts(int posts) {
        this.posts = posts;
    }

    public int getMembers() {
        return members;
    }

    public void setMembers(int members) {
        this.members = members;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id='" + id + '\'' +
                ", creator_id='" + creator_id + '\'' +
                ", topic='" + topic + '\'' +
                ", posts=" + posts +
                ", members=" + members +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}
