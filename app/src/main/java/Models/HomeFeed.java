package Models;

import android.support.annotation.NonNull;

import java.sql.Date;

/**
 * Created by inventor on 1/27/18.
 */

public class HomeFeed{

    private String id;
    private String url;
    private String date;
    private String title;
    private String desc;
    private String type;

    public HomeFeed(String id, String url, String date, String title, String desc, String type) {
        this.id = id;
        this.url = url;
        this.date = date;
        this.title = title;
        this.desc = desc;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "HomeFeed{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", date='" + date + '\'' +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

}
