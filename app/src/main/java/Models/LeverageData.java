package Models;

/**
 * Created by inventor on 1/27/18.
 */

public class LeverageData {

    private String id;
    private String title;
    private String excerpt;
    private String content;
    private Thumbnails thumbnails;
    private String type;
    private String publishedAt;
    //private String dateRaw;

    public LeverageData(String id, String title, String excerpt, String content, Thumbnails thumbnails, String type, String publishedAt) {
        this.id = id;
        this.title = title;
        this.excerpt = excerpt;
        this.content = content;
        this.thumbnails = thumbnails;
        this.type = type;
        this.publishedAt = publishedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Thumbnails getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(Thumbnails thumbnails) {
        this.thumbnails = thumbnails;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    @Override
    public String toString() {
        return "LeverageData{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", excerpt='" + excerpt + '\'' +
                ", content='" + content + '\'' +
                ", thumbnails=" + thumbnails +
                ", type='" + type + '\'' +
                ", publishedAt='" + publishedAt + '\'' +
                '}';
    }

    public class Thumbnails
    {
        private Full full; //-----> ~215kb
        private ShopSingle shop_single;  //-----> ~30kb

        public Thumbnails(Full full) {
            this.full = full;
        }

        public Thumbnails(ShopSingle shopSingle) {
            this.shop_single = shopSingle;
        }

        public Full getFull() {
            return full;
        }

        public ShopSingle getShop_single() {
            return shop_single;
        }

//        @Override
//        public String toString() {
//            return "Thumbnails{" +
//                    "full=" + full +
//                    '}';
//        }

        public class Full
        {
            private String url;

            public Full(String url) {
                this.url = url;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            @Override
            public String toString() {
                return "Full{" +
                        "url='" + url + '\'' +
                        '}';
            }
        }

        public class ShopSingle
        {
            private String url;

            public ShopSingle(String url) {
                this.url = url;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            @Override
            public String toString() {
                return "ShopSingle{" +
                        "url='" + url + '\'' +
                        '}';
            }
        }
    }

}
