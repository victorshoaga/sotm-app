package Models;

/**
 * Created by inventor on 2/22/18.
 */

public class MessageDataModel {

    private User user;
    private ChatModel chatModel;

    public MessageDataModel() {
    }

    public MessageDataModel(User user, ChatModel chatModel) {
        this.user = user;
        this.chatModel = chatModel;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ChatModel getChatModel() {
        return chatModel;
    }

    public void setChatModel(ChatModel chatModel) {
        this.chatModel = chatModel;
    }
}
