package Models;

import java.util.List;

/**
 * Created by inventor on 9/28/17.
 */
public class MyConnectsData {

    private List<MyConnectsDocs> docs;
    private int total;
    private int total_pages;
    private int current_page;
    private int pages_left;

    public MyConnectsData(int current_page, List<MyConnectsDocs> docs, int pages_left, int total, int total_pages) {
        this.current_page = current_page;
        this.docs = docs;
        this.pages_left = pages_left;
        this.total = total;
        this.total_pages = total_pages;
    }

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public List<MyConnectsDocs> getDocs() {
        return docs;
    }

    public void setDocs(List<MyConnectsDocs> docs) {
        this.docs = docs;
    }

    public int getPages_left() {
        return pages_left;
    }

    public void setPages_left(int pages_left) {
        this.pages_left = pages_left;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    @Override
    public String toString() {
        return "MyConnectsData{" +
                "current_page=" + current_page +
                ", docs=" + docs +
                ", total=" + total +
                ", total_pages=" + total_pages +
                ", pages_left=" + pages_left +
                '}';
    }
}
