package Models;

/**
 * Created by inventor on 9/28/17.
 */
public class MyConnectsDocs {

    private User user;

    public MyConnectsDocs(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "MyConnectsDocs{" +
                "user=" + user +
                '}';
    }
}
