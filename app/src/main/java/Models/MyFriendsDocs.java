package Models;

/**
 * Created by inventor on 2/27/18.
 */

public class MyFriendsDocs {

    private boolean isAFriend;
    private User user;

    public MyFriendsDocs(boolean isAFriend, User user) {
        this.isAFriend = isAFriend;
        this.user = user;
    }

    public boolean isAFriend() {
        return isAFriend;
    }

    public void setAFriend(boolean AFriend) {
        isAFriend = AFriend;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "MyFriendsDocs{" +
                "isAFriend=" + isAFriend +
                ", user=" + user +
                '}';
    }
}
