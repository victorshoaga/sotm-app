package Models;

/**
 * Created by inventor on 1/26/18.
 * {
 "err": false,
 "msg": "Success",
 "data": {
 "post": {
 "_id": "5a67cdd755cb2006af2eeb84",
 "title": "A New Post",
 "content": "Content of a new Post",
 "scope": 0,
 "updatedAt": "2018-01-24T00:05:43.161Z",
 "createdAt": "2018-01-24T00:05:43.161Z"
 }
 }
 }
 */

public class Post {

    private String _id;
    private String title;
    private String content;
    private int scope;
    private String updatedAt;
    private String createdAt;
    private String image;
    private String publishedAt;

    public Post(String _id, String title, String content, int scope, String updatedAt, String createdAt, String image, String publishedAt) {
        this._id = _id;
        this.title = title;
        this.content = content;
        this.scope = scope;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.image = image;
        this.publishedAt = publishedAt;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getScope() {
        return scope;
    }

    public void setScope(int scope) {
        this.scope = scope;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    @Override
    public String toString() {
        return "Post{" +
                "_id='" + _id + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", scope=" + scope +
                ", updatedAt='" + updatedAt + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", image='" + image + '\'' +
                ", publishedAt='" + publishedAt + '\'' +
                '}';
    }
}
