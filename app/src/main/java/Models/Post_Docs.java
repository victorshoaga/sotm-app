package Models;

/**
 * Created by inventor on 1/26/18.
 */

public class Post_Docs {

    private Post post;
    private User author;

    public Post_Docs(Post post, User author) {
        this.post = post;
        this.author = author;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Post_Docs{" +
                "post=" + post +
                ", author=" + author +
                '}';
    }
}
