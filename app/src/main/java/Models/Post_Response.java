package Models;

import java.util.List;

/**
 * Created by inventor on 1/26/18.
 */

public class Post_Response {

    private boolean err;
    private String msg;
    private GetPostData data;

    public Post_Response(boolean err, String msg, GetPostData data) {
        this.err = err;
        this.msg = msg;
        this.data = data;
    }

    public boolean isErr() {
        return err;
    }

    public void setErr(boolean err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public GetPostData getData() {
        return data;
    }

    public void setData(GetPostData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Post_Response{" +
                "err=" + err +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public class GetPostData
    {
        private List<Post_Docs> docs;
        private int total;
        private int total_pages;
        private int current_page;
        private int pages_left;

        public GetPostData(List<Post_Docs> docs, int total, int total_pages, int current_page, int pages_left) {
            this.docs = docs;
            this.total = total;
            this.total_pages = total_pages;
            this.current_page = current_page;
            this.pages_left = pages_left;
        }

        public List<Post_Docs> getDocs() {
            return docs;
        }

        public void setDocs(List<Post_Docs> docs) {
            this.docs = docs;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getTotal_pages() {
            return total_pages;
        }

        public void setTotal_pages(int total_pages) {
            this.total_pages = total_pages;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getPages_left() {
            return pages_left;
        }

        public void setPages_left(int pages_left) {
            this.pages_left = pages_left;
        }

        @Override
        public String toString() {
            return "PostData{" +
                    "docs=" + docs +
                    ", total=" + total +
                    ", total_pages=" + total_pages +
                    ", current_page=" + current_page +
                    ", pages_left=" + pages_left +
                    '}';
        }
    }
}
