package Models;

/**
 * Created by inventor on 8/19/18.
 */

public class Response_AddGroupMember {

    private boolean err;
    private String msg;

    public Response_AddGroupMember(boolean err, String msg) {
        this.err = err;
        this.msg = msg;
    }

    public boolean isErr() {
        return err;
    }

    public void setErr(boolean err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Response_AddGroupMember{" +
                "err=" + err +
                ", msg='" + msg + '\'' +
                '}';
    }
}
