package Models;

/**
 * Created by inventor on 9/22/17.
 */
public class Response_Connect {

    private String err;
    private String msg;

    public Response_Connect(String err, String msg) {
        this.err = err;
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
