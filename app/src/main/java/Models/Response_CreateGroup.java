package Models;

/**
 * Created by inventor on 8/19/18.
 */

public class Response_CreateGroup {

    private boolean err;
    private String msg;
    private CreateGroupData data;

    public Response_CreateGroup(boolean err, String msg, CreateGroupData data) {
        this.err = err;
        this.msg = msg;
        this.data = data;
    }

    public boolean isErr() {
        return err;
    }

    public void setErr(boolean err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CreateGroupData getData() {
        return data;
    }

    public void setData(CreateGroupData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response_CreateGroup{" +
                "err=" + err +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
