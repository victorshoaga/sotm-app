package Models;

/**
 * Created by inventor on 4/17/18.
 */

public class Response_DeletePost {

    private String err;
    private String msg;

    public Response_DeletePost(String err, String msg) {
        this.err = err;
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Response_DeletePost{" +
                "err='" + err + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
