package Models;

import java.util.List;

/**
 * Created by inventor on 12/29/17.
 */

public class Response_FindConnects {
    private String err;
    private String msg;
    private FindConnectsResultData data;

    public Response_FindConnects(String err, String msg, FindConnectsResultData data) {
        this.err = err;
        this.msg = msg;
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public FindConnectsResultData getData() {
        return data;
    }

    public void setData(FindConnectsResultData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response_FindConnects{" +
                "err='" + err + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public class FindConnectsResultData
    {
        List<User_Friend_Request> docs;
        private int total;
        private int total_pages;
        private int current_page;
        private int pages_left;

        public FindConnectsResultData(List<User_Friend_Request> docs, int total, int total_pages, int current_page, int pages_left) {
            this.docs = docs;
            this.total = total;
            this.total_pages = total_pages;
            this.current_page = current_page;
            this.pages_left = pages_left;
        }

        public List<User_Friend_Request> getDocs() {
            return docs;
        }

        public void setDocs(List<User_Friend_Request> docs) {
            this.docs = docs;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getTotal_pages() {
            return total_pages;
        }

        public void setTotal_pages(int total_pages) {
            this.total_pages = total_pages;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getPages_left() {
            return pages_left;
        }

        public void setPages_left(int pages_left) {
            this.pages_left = pages_left;
        }

        @Override
        public String toString() {
            return "FindConnectsResultData{" +
                    "docs=" + docs +
                    ", total=" + total +
                    ", total_pages=" + total_pages +
                    ", current_page=" + current_page +
                    ", pages_left=" + pages_left +
                    '}';
        }
    }

}
