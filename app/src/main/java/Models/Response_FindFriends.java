package Models;

import java.util.List;

/**
 * Created by inventor on 12/29/17.
 */

public class Response_FindFriends {
    private String err;
    private String msg;
    private FindFriendsResultData data;

    public Response_FindFriends(String err, String msg, FindFriendsResultData data) {
        this.err = err;
        this.msg = msg;
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public FindFriendsResultData getData() {
        return data;
    }

    public void setData(FindFriendsResultData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response_FindConnects{" +
                "err='" + err + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public class FindFriendsResultData
    {
        List<USer_Friend> docs;
        private int total;
        private int total_pages;
        private int current_page;
        private int pages_left;

        public FindFriendsResultData(List<USer_Friend> docs, int total, int total_pages, int current_page, int pages_left) {
            this.docs = docs;
            this.total = total;
            this.total_pages = total_pages;
            this.current_page = current_page;
            this.pages_left = pages_left;
        }

        public List<USer_Friend> getDocs() {
            return docs;
        }

        public void setDocs(List<USer_Friend> docs) {
            this.docs = docs;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getTotal_pages() {
            return total_pages;
        }

        public void setTotal_pages(int total_pages) {
            this.total_pages = total_pages;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public int getPages_left() {
            return pages_left;
        }

        public void setPages_left(int pages_left) {
            this.pages_left = pages_left;
        }

        @Override
        public String toString() {
            return "FindFriendsResultData{" +
                    "docs=" + docs +
                    ", total=" + total +
                    ", total_pages=" + total_pages +
                    ", current_page=" + current_page +
                    ", pages_left=" + pages_left +
                    '}';
        }
    }

    public class USer_Friend
    {
        private boolean isAFriend;
        private User user;

        public USer_Friend(boolean isAFriend, User user) {
            this.isAFriend = isAFriend;
            this.user = user;
        }

        public boolean isAFriend() {
            return isAFriend;
        }

        public void setAFriend(boolean AFriend) {
            isAFriend = AFriend;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        @Override
        public String toString() {
            return "USer_Friend{" +
                    "isAFriend=" + isAFriend +
                    ", user=" + user +
                    '}';
        }
    }

}
