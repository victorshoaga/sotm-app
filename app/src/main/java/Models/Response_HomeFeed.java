package Models;

import java.util.List;

/**
 * Created by inventor on 1/27/18.
 */

public class Response_HomeFeed {

    private boolean err;
    private String msg;
    private HomeFeedData data;

    public Response_HomeFeed(boolean err, String msg, HomeFeedData data) {
        this.err = err;
        this.msg = msg;
        this.data = data;
    }

    public boolean isErr() {
        return err;
    }

    public void setErr(boolean err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public HomeFeedData getData() {
        return data;
    }

    public void setData(HomeFeedData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response_HomeFeed{" +
                "err=" + err +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public class HomeFeedData
    {
        private List<SOGData> sphereOfGraceData;
        private List<LeverageData> leverageData;
        private List<Post_Docs> setManPosts;

        public HomeFeedData(List<SOGData> sphereOfGraceData, List<LeverageData> leverageData, List<Post_Docs> setManPosts) {
            this.sphereOfGraceData = sphereOfGraceData;
            this.leverageData = leverageData;
            this.setManPosts = setManPosts;
        }

        public List<SOGData> getSphereOfGraceData() {
            return sphereOfGraceData;
        }

        public void setSphereOfGraceData(List<SOGData> sphereOfGraceData) {
            this.sphereOfGraceData = sphereOfGraceData;
        }

        public List<LeverageData> getLeverageData() {
            return leverageData;
        }

        public void setLeverageData(List<LeverageData> leverageData) {
            this.leverageData = leverageData;
        }

        public List<Post_Docs> getSetManPosts() {
            return setManPosts;
        }

        public void setSetManPosts(List<Post_Docs> setManPosts) {
            this.setManPosts = setManPosts;
        }

        @Override
        public String toString() {
            return "HomeFeedData{" +
                    "sphereOfGraceData=" + sphereOfGraceData +
                    ", leverageData=" + leverageData +
                    ", setManPosts=" + setManPosts +
                    '}';
        }
    }


}
