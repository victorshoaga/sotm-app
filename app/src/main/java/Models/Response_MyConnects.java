package Models;

/**
 * Created by inventor on 9/28/17.
 */
public class Response_MyConnects {

    private String err;
    private String msg;
    private MyConnectsData data;

    public Response_MyConnects(MyConnectsData data, String err, String msg) {
        this.data = data;
        this.err = err;
        this.msg = msg;
    }

    public MyConnectsData getData() {
        return data;
    }

    public void setData(MyConnectsData data) {
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Response_MyConnects{" +
                "data=" + data +
                ", err='" + err + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
