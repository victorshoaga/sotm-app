package Models;

/**
 * Created by inventor on 1/26/18.
 *   {
 "err": false,
 "msg": "Success",
 "data": {
 "post": {
 "_id": "5a67cdd755cb2006af2eeb84",
 "title": "A New Post",
 "content": "Content of a new Post",
 "scope": 0,
 "updatedAt": "2018-01-24T00:05:43.161Z",
 "createdAt": "2018-01-24T00:05:43.161Z"
 }
 }
 }
 */

public class Response_Post {

    private boolean err;
    private String msg;
    private PostData data;

    public Response_Post(boolean err, String msg, PostData data) {
        this.err = err;
        this.msg = msg;
        this.data = data;
    }

    public boolean isErr() {
        return err;
    }

    public void setErr(boolean err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PostData getData() {
        return data;
    }

    public void setData(PostData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response_Post{" +
                "err=" + err +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public class PostData
    {
        private Post post;

        public PostData(Post post) {
            this.post = post;
        }

        public Post getPost() {
            return post;
        }

        public void setPost(Post post) {
            this.post = post;
        }

        @Override
        public String toString() {
            return "PostData{" +
                    "post=" + post +
                    '}';
        }
    }
}
