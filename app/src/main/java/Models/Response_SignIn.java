package Models;

/**
 * Created by inventor on 1/20/18.
 */

public class Response_SignIn {

    private boolean err;
    private String msg;
    private UserData data;

    public Response_SignIn(boolean err, String msg, UserData data) {
        this.err = err;
        this.msg = msg;
        this.data = data;
    }

    public boolean isErr() {
        return err;
    }

    public void setErr(boolean err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response_SignUp{" +
                "err=" + err +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
