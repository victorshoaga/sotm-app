package Models;

/**
 * Created by inventor on 1/27/18.
 */

public class SOGData
{
    private String id;
    private String title;
    private String description;
    private String videoId;
    private Thumbnails thumbnails;
    private String type;
    private String publishedAt;
    //private String dateRaw;

    public SOGData(String id, String title, String description, String videoId, Thumbnails thumbnails, String type, String publishedAt) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.videoId = videoId;
        this.thumbnails = thumbnails;
        this.type = type;
        this.publishedAt = publishedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public Thumbnails getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(Thumbnails thumbnails) {
        this.thumbnails = thumbnails;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    @Override
    public String toString() {
        return "SOGData{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", videoId='" + videoId + '\'' +
                ", thumbnails=" + thumbnails +
                ", type='" + type + '\'' +
                ", publishedAt='" + publishedAt + '\'' +
                '}';
    }

    public class Thumbnails
    {
        private High high;

        public Thumbnails(High high) {
            this.high = high;
        }

        public High getHigh() {
            return high;
        }

        public void setHigh(High high) {
            this.high = high;
        }

        @Override
        public String toString() {
            return "Thumbnails{" +
                    "high=" + high +
                    '}';
        }

        public class High
        {
            private String url;

            public High(String url) {
                this.url = url;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            @Override
            public String toString() {
                return "High{" +
                        "url='" + url + '\'' +
                        '}';
            }
        }
    }
}