package Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by inventor on 9/22/17.
 */
public class UpdateFCMTokenResponse {

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("msg")
    @Expose
    private String msg;

    public UpdateFCMTokenResponse(boolean error, String msg) {
        this.error = error;
        this.msg = msg;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String toString() {
        return "Response{" +
                "error='" + error + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }

}
