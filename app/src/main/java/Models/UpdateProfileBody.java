package Models;

/**
 * Created by inventor on 1/24/18.@Field("password") String password,
 @Field("name") String name,
 @Field("email") String email,
 @Field("username") String username,
 @Field("phone_number") String phone_number,
 @Field("dob") String dob,
 @Field("gender") String gender,
 @Field("city") String city,
 @Field("country") String country,
 @Field("is_a_partner") boolean is_a_partner);
 */

public class UpdateProfileBody {
    private String name;
    private String email;
    private String username;
    private String phone_number;
    private String dob;
    private String gender;
    private String city;
    private String country;
    private boolean is_a_partner;

    public UpdateProfileBody(String name, String email, String username, String phone_number, String dob, String gender, String city, String country, boolean is_a_partner) {
        this.name = name;
        this.email = email;
        this.username = username;
        this.phone_number = phone_number;
        this.dob = dob;
        this.gender = gender;
        this.city = city;
        this.country = country;
        this.is_a_partner = is_a_partner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean is_a_partner() {
        return is_a_partner;
    }

    public void setIs_a_partner(boolean is_a_partner) {
        this.is_a_partner = is_a_partner;
    }

    @Override
    public String toString() {
        return "UpdateProfileBody{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", is_a_partner=" + is_a_partner +
                '}';
    }
}
