package Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by inventor on 1/20/18.
 */

public class User implements Parcelable{

    private String id;
    private String email;
    private String username;
    private String name;
    private String phoneNumber;
    private String city;
    private String country;
    private String gender;
    private String role;
    private String dob;
    private String picture;
    private String token;
    private String fcm_token;
    private boolean isAPartner;
    private boolean isProfileComplete;
    private boolean isProfilePublic;
    private boolean isConfirmed;
    private int friends;

    public User() {
    }

    public User(String id, String email, String username, String name, String phoneNumber, String city, String country, String gender, String role, String dob, String picture, String token, String fcm_token, boolean isAPartner, boolean isProfileComplete, boolean isProfilePublic, boolean isConfirmed, int friends) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.city = city;
        this.country = country;
        this.gender = gender;
        this.role = role;
        this.dob = dob;
        this.picture = picture;
        this.token = token;
        this.fcm_token = fcm_token;
        this.isAPartner = isAPartner;
        this.isProfileComplete = isProfileComplete;
        this.isProfilePublic = isProfilePublic;
        this.isConfirmed = isConfirmed;
        this.friends = friends;
    }

    protected User(Parcel in) {
        id = in.readString();
        email = in.readString();
        username = in.readString();
        name = in.readString();
        phoneNumber = in.readString();
        city = in.readString();
        country = in.readString();
        gender = in.readString();
        role = in.readString();
        dob = in.readString();
        picture = in.readString();
        token = in.readString();
        fcm_token = in.readString();
        isAPartner = in.readByte() != 0;
        isProfileComplete = in.readByte() != 0;
        isProfilePublic = in.readByte() != 0;
        isConfirmed = in.readByte() != 0;
        friends = in.readInt();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public boolean isAPartner() {
        return isAPartner;
    }

    public void setAPartner(boolean APartner) {
        isAPartner = APartner;
    }

    public boolean isProfileComplete() {
        return isProfileComplete;
    }

    public void setProfileComplete(boolean profileComplete) {
        isProfileComplete = profileComplete;
    }

    public boolean isProfilePublic() {
        return isProfilePublic;
    }

    public void setProfilePublic(boolean profilePublic) {
        isProfilePublic = profilePublic;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public int getFriends() {
        return friends;
    }

    public void setFriends(int friends) {
        this.friends = friends;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", gender='" + gender + '\'' +
                ", role='" + role + '\'' +
                ", dob='" + dob + '\'' +
                ", picture='" + picture + '\'' +
                ", token='" + token + '\'' +
                ", fcm_token='" + fcm_token + '\'' +
                ", isAPartner=" + isAPartner +
                ", isProfileComplete=" + isProfileComplete +
                ", isProfilePublic=" + isProfilePublic +
                ", isConfirmed=" + isConfirmed +
                ", friends=" + friends +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(email);
        parcel.writeString(username);
        parcel.writeString(name);
        parcel.writeString(phoneNumber);
        parcel.writeString(city);
        parcel.writeString(country);
        parcel.writeString(gender);
        parcel.writeString(role);
        parcel.writeString(dob);
        parcel.writeString(picture);
        parcel.writeString(token);
        parcel.writeString(fcm_token);
        parcel.writeByte((byte) (isAPartner ? 1 : 0));
        parcel.writeByte((byte) (isProfileComplete ? 1 : 0));
        parcel.writeByte((byte) (isProfilePublic ? 1 : 0));
        parcel.writeByte((byte) (isConfirmed ? 1 : 0));
        parcel.writeInt(friends);
    }
}
