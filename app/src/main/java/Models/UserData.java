package Models;

/**
 * Created by inventor on 1/20/18.
 */

public class UserData {
    private User user;

    public UserData(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "user=" + user +
                '}';
    }
}
