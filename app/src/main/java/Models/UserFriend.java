package Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by inventor on 3/13/18.
 */

public class UserFriend implements Parcelable{

    private String id;
    private String email;
    private String picture;
    private String username;
    private String name;

    public UserFriend(String id, String email, String picture, String username, String name) {
        this.id = id;
        this.email = email;
        this.picture = picture;
        this.username = username;
        this.name = name;
    }

    protected UserFriend(Parcel in) {
        id = in.readString();
        email = in.readString();
        picture = in.readString();
        username = in.readString();
        name = in.readString();
    }

    public static final Creator<UserFriend> CREATOR = new Creator<UserFriend>() {
        @Override
        public UserFriend createFromParcel(Parcel in) {
            return new UserFriend(in);
        }

        @Override
        public UserFriend[] newArray(int size) {
            return new UserFriend[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserFriend{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", picture='" + picture + '\'' +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(picture);
        dest.writeString(username);
        dest.writeString(name);
    }
}
