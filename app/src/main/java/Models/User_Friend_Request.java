package Models;

/**
 * Created by inventor on 1/25/18.
 */

public class User_Friend_Request {

    private User user;
    private Request request;

    public User_Friend_Request(User user, Request request) {
        this.user = user;
        this.request = request;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "User_Friend_Request{" +
                "user=" + user +
                ", request=" + request +
                '}';
    }

    private class Request
    {
        private String user_from_id;
        private String since;

        public Request(String user_from_id, String since) {
            this.user_from_id = user_from_id;
            this.since = since;
        }

        public String getUser_from_id() {
            return user_from_id;
        }

        public void setUser_from_id(String user_from_id) {
            this.user_from_id = user_from_id;
        }

        public String getSince() {
            return since;
        }

        public void setSince(String since) {
            this.since = since;
        }

        @Override
        public String toString() {
            return "Request{" +
                    "user_from_id='" + user_from_id + '\'' +
                    ", since='" + since + '\'' +
                    '}';
        }
    }

}
