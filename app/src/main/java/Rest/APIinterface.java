package Rest;

import Models.Post_Response;
import Models.Response_AddGroupMember;
import Models.Response_Connect;
import Models.Response_CreateGroup;
import Models.Response_DeletePost;
import Models.Response_FindConnects;
import Models.Response_FindFriends;
import Models.Response_HomeFeed;
import Models.Response_MyConnects;
import Models.Response_Post;
import Models.Response_SignIn;
import Models.Response_SignUp;
import Models.Response_UpdateProfile;
import Models.Response_User;
import Models.UpdateFCMTokenResponse;
import Models.UpdateProfileBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by inventor on 5/16/17.
 */
public interface APIinterface {

    @FormUrlEncoded
    @POST("signup")
    Call<Response_SignUp> signUpUser(@Field("name") String name,
                                     @Field("username") String username,
                                     @Field("email") String email,
                                     @Field("password") String password,
                                     @Field("phone_number") String phoneNumber);

    @FormUrlEncoded
    @POST("login")
    Call<Response_SignIn> logInUser(@Field("username") String username,
                                    @Field("password") String password);

    @POST("account/profile/update/{user_id}")
    Call<Response_UpdateProfile> updateTradUserProfile(@Header("Authorization") String token,
                                                       @Path("user_id") String user_id,
                                                       @Body UpdateProfileBody body);

    @Multipart
    @POST("account/avatar/update/{user_id}")
    Call<RequestBody> uploadAvatar(@Header("Authorization") String token,
                                   @Path("user_id") String user_id,
                                   @Part MultipartBody.Part avatar);

    @Multipart
    @POST("user/setman/post/update_image/{post_id}")
    Call<RequestBody> uploadCoverImage(@Header("Authorization") String token,
                                   @Path("post_id") String post_id,
                                   @Part MultipartBody.Part cover_image);

    @GET("user/profile/{user_id}")
    Call<Response_User> getUserProfile(@Header("Authorization") String token,
                                       @Path("user_id") String user_id);

    @GET("user/friends/{user_id}/{page}")
    Call<Response_MyConnects> getUserFriends(@Header("Authorization") String token,
                                                 @Path("user_id") String user_id,
                                                 @Path("page") String page);

    @GET("users/suggest")
    Call<Response_MyConnects> getSuggestions(@Header("Authorization") String token);

    @POST("account/fcm/update/{fcm_token}")
    Call<UpdateFCMTokenResponse> updateTokenOnServer(@Header("Authorization") String token, @Path("fcm_token") String fcm_token);

    @FormUrlEncoded
    @POST("friend_request/send")
    Call<Response_Connect> sendRequest(@Header("Authorization") String token,
                                       @Field("user_from_id") String id_request_from,
                                       @Field("user_to_id") String id_request_to);

    @FormUrlEncoded
    @POST("friend_request/accept")
    Call<Response_Connect> acceptRequest(@Header("Authorization") String token,
                                         @Field("user_from_id") String id_request_from,
                                         @Field("user_to_id") String id_request_to);

    @FormUrlEncoded
    @POST("friend_request/decline")
    Call<Response_Connect> declineRequest(@Header("Authorization") String token,
                                         @Field("user_from_id") String id_request_from,
                                         @Field("user_to_id") String id_request_to);

    @FormUrlEncoded
    @POST("user/setman/post/create")
    Call<Response_Post> createPost(@Header("Authorization") String token,
                                   @Field("title") String title,
                                   @Field("content") String content,
                                   @Field("scope") int scope);

    @FormUrlEncoded
    @POST("user/setman/post/update/{post_id}")
    Call<Response_Post> updatePost(@Header("Authorization") String token,
                                   @Path("post_id") String post_id,
                                   @Field("title") String title,
                                   @Field("content") String content,
                                   @Field("scope") int scope);

    @POST("user/setman/post/remove/{post_id}")
    Call<Response_DeletePost> deletePost(@Header("Authorization") String token,
                                         @Path("post_id") String post_id);

    @GET("users/search/{query}/{page}")
    Call<Response_FindConnects> findFriends(@Header("Authorization") String token,
                                            @Path("query") String query,
                                            @Path("page") int page);

    @GET("users/search/{query}/{page}")
    Call<Response_FindFriends> findFriends_Adapter(@Header("Authorization") String token,
                                           @Path("query") String query,
                                           @Path("page") int page);

    @GET("user/friend_requests_received/{user_id}/{page}")
    Call<Response_FindConnects> getFriendRequests(@Header("Authorization") String token,
                                            @Path("user_id") String user_id,
                                            @Path("page") int page);

    @GET("user/setman/posts/all/{page}")
    Call<Post_Response> getPosts_general(@Header("Authorization") String token,
                                         @Path("page") int page);

    @GET("home-feed")
    Call<Response_HomeFeed> getHomeFeed(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("group")
    Call<Response_CreateGroup> createGroup(@Header("Authorization") String token,
                                           @Field("topic") String topic);

    @POST("group/member/{group_id}/{user_id}")
    Call<Response_AddGroupMember> addGroupMember(@Header("Authorization") String token,
                                                 @Path("group_id") String group_id,
                                                 @Path("user_id") String user_id);
}
