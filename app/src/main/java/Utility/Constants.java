package Utility;

/**
 * Created by excellence.ilesanmi on 06/11/2017.
 */

public class Constants {
    public static final String EVENTBRITE_TOKEN = "YOVMYAVPPUZ4KKXTJ7FW";
    public static final String ARG_RECEIVER = "receiver";
    public static final String ARG_RECEIVER_UID = "receiver_uid";
    public static final String ARG_CHAT_ROOMS = "chat_rooms";
    public static final String ARG_COMMENT_ROOMS = "setman-posts";
    public static final String ARG_FEEDBACKS = "users_feed_back";
    public static final String ARG_MESSAGES = "messages";
    public static final String ARG_COMMENTS = "comments";
    public static final String ARG_NUM_COMMENTS = "number_of_comments";
    public static final String ARG_USERS = "users";
    public static final String ARG_FIREBASE_TOKEN = "firebaseToken";
    public static final String ARG_FRIENDS = "friends";
    public static final String ARG_UID = "uid";
}
