package Utility;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {
    private static Pattern pattern;
    private Matcher matcher;

    public EmailValidator(){
        pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    }
    public boolean validateEmail(String email){
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}