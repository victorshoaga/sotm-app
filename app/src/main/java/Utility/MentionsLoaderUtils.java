package Utility;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import Database.FriendsListDBHelper;
import Models.UserFriend;

public class MentionsLoaderUtils {

    private final Context context;
    private final List<UserFriend> userList;
    FriendsListDBHelper friendsListDBHelper;

    public MentionsLoaderUtils(final Context _context) {
        this.context = _context;
        friendsListDBHelper = new FriendsListDBHelper(context);
        userList = loadUsers();
    }

    /**
     * Loads user friends from SQLITE.
     */
    private List<UserFriend> loadUsers() {
        friendsListDBHelper.openR();
        List<UserFriend> users = new ArrayList<UserFriend>();
        users = friendsListDBHelper.getFriendsList();
        friendsListDBHelper.close();
        return users;
    }

    /**
     * Search for user with name matching {@code query}.
     *
     * @return a list of users that matched {@code query}.
     */
    public List<UserFriend> searchUsers(String query) {
        final List<UserFriend> searchResults = new ArrayList<>();
        if (StringUtils.isNotBlank(query)) {
            query = query.toLowerCase(Locale.US);
            if (userList != null && !userList.isEmpty()) {
                for (UserFriend user : userList) {
                    final String name = user.getName().toLowerCase();
                    final String username = user.getUsername().toLowerCase();
                    if (name.startsWith(query) || username.startsWith(query)) {
                        searchResults.add(user);
                    }
                }
            }

        }
        return searchResults;
    }

}
