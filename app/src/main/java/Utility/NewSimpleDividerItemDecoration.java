package Utility;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.glt.sotm.R;


/**
 * Created by inventor on 5/20/16.
 */
public class NewSimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDivider;
    public static int DEFAULT_PADDING_LEFT = 125;
    private int paddingLeft;

    public NewSimpleDividerItemDecoration(Context context, int paddingLeft) {
        mDivider = context.getResources().getDrawable(R.drawable.new_line_divider);
        this.paddingLeft = paddingLeft;
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft() + this.paddingLeft;
        int right = parent.getWidth() - parent.getPaddingRight() - 30 ;

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}