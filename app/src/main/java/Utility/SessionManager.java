package Utility;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.glt.sotm.SignInActivity;

import java.util.HashMap;

/**
 * Created by inventor on 1/20/18.
 */

public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "SOTMPref";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_PHONENUMBER = "phoneNumber";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_ROLE= "role";
    public static final String KEY_DOB = "dob";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_PICTURE = "picture";
    public static final String KEY_ISPARTNER = "partner";
    public static final String KEY_ISPROFILECOMPLETE = "profileComplete";
    public static final String KEY_ISPROFILEPUBLIC = "profilePublic";
    public static final String KEY_ISCONFIRMED = "confirmed";
    public static final String KEY_COUNTRY = "country";
    public static final String KEY_CITY = "city";
    public static final String KEY_FRIENDS = "friends";

    public static final String DISTANCE = "distance";
    public static final String SOUND_ENABLED = "soundEnabled";
    public static final String PUSH_ENABLED = "pushEnabled";
    public static final String LOW_RES_ENABLED = "lowResEnabled";
    public static final String OFFLINE_ENABLED = "offlineEnabled";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String SETMAN_POST = "setman_post";
    public static final String REMEMBER_TO_SWIPE = "remember_to_swipe";

    // Constructor
    public SessionManager(Context context){
        _context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String id, String name, String username, String email, String password, String token,
                                   String picture, boolean isProfileComplete, boolean isProfilePublic, boolean isConfirmed,
                                   String country, String city, String phoneNumber, String gender, String role, String dob,
                                   boolean isPartner, int friends){

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_PASSWORD, password);
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_PICTURE, picture);
        editor.putBoolean(KEY_ISPROFILECOMPLETE, isProfileComplete);
        editor.putBoolean(KEY_ISPROFILEPUBLIC, isProfilePublic);
        editor.putBoolean(KEY_ISCONFIRMED, isConfirmed);
        editor.putBoolean(KEY_ISPARTNER, isPartner);
        editor.putString(KEY_COUNTRY, country);
        editor.putString(KEY_CITY, city);
        editor.putString(KEY_PHONENUMBER, phoneNumber);
        editor.putString(KEY_GENDER, gender);
        editor.putString(KEY_ROLE, role);
        editor.putString(KEY_DOB, dob);
        editor.putInt(KEY_FRIENDS, friends);
        editor.commit();

    }

    public String getSETMAN_POST()
    {
        return pref.getString(SETMAN_POST, null);
    }

    public void updateSETMAN_POST(String post)
    {
        editor.putString(SETMAN_POST, post);
        editor.commit();
    }

    public void clearSETMAN_POST()
    {
        editor.putString(SETMAN_POST, null);
        editor.commit();
    }

    public void updateData(String name, String username, String email,
                                   String picture, boolean isProfileComplete, boolean isProfilePublic, boolean isConfirmed,
                                   String country, String city, String phoneNumber, String gender, String role, String dob,
                                   boolean isPartner, int friend){
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_PICTURE, picture);
        editor.putBoolean(KEY_ISPROFILECOMPLETE, isProfileComplete);
        editor.putBoolean(KEY_ISPROFILEPUBLIC, isProfilePublic);
        editor.putBoolean(KEY_ISCONFIRMED, isConfirmed);
        editor.putBoolean(KEY_ISPARTNER, isPartner);
        editor.putString(KEY_COUNTRY, country);
        editor.putString(KEY_CITY, city);
        editor.putString(KEY_PHONENUMBER, phoneNumber);
        editor.putString(KEY_GENDER, gender);
        editor.putString(KEY_ROLE, role);
        editor.putString(KEY_DOB, dob);
        editor.putInt(KEY_FRIENDS, friend);
        editor.commit();

    }

    public void setRememberToSwipe(boolean t){
        if(t){
            editor.putBoolean(REMEMBER_TO_SWIPE, t);
            editor.commit();
        }
    }

    public void updateToken(String fcm_token)
    {
        editor.putString(FCM_TOKEN, fcm_token);
        editor.commit();
    }

    public void updatePhoneNumber(String phoneNumber)
    {
        editor.putString(KEY_PHONENUMBER, phoneNumber);
        editor.commit();
    }

    public boolean getRememberToSwipe(){
        return pref.getBoolean(REMEMBER_TO_SWIPE, false);
    }
    public String getToken()
    {
        String fcm_token = pref.getString(FCM_TOKEN, "");
        return fcm_token;
    }

    public void updateDistanceSetting(int distance){
        editor.putInt(DISTANCE, distance);
        editor.commit();
    }

    public void updatePushSetting(boolean pushEnabled){
        editor.putBoolean(PUSH_ENABLED, pushEnabled);
        editor.commit();
    }

    public void updateSoundSetting(boolean soundEnabled){
        editor.putBoolean(SOUND_ENABLED, soundEnabled);
        editor.commit();
    }

    public void updateLowResSetting(boolean highResEnabled){
        editor.putBoolean(LOW_RES_ENABLED, highResEnabled);
        editor.commit();
    }

    public void updateOfflineSetting(boolean offlineEnabled){
        editor.putBoolean(OFFLINE_ENABLED, offlineEnabled);
        editor.commit();
    }

    public void updateAvatar(String pictureURL)
    {
        editor.putString(KEY_PICTURE, pictureURL);
        editor.commit();
    }

    public void updateScope(Boolean isPublic)
    {
        editor.putBoolean(KEY_ISPROFILEPUBLIC, isPublic);
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        if(!this.isLoggedIn()){
            Intent i = new Intent(_context, SignInActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }

    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));
        user.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null));
        user.put(KEY_PICTURE, pref.getString(KEY_PICTURE, null));
        user.put(KEY_COUNTRY, pref.getString(KEY_COUNTRY, null));
        user.put(KEY_CITY, pref.getString(KEY_CITY, null));
        user.put(KEY_GENDER, pref.getString(KEY_GENDER, null));
        user.put(KEY_ROLE, pref.getString(KEY_ROLE, null));
        user.put(KEY_DOB, pref.getString(KEY_DOB, null));
        user.put(KEY_FRIENDS, String.valueOf(pref.getInt(KEY_FRIENDS, 0)));
        user.put(KEY_ISPROFILECOMPLETE, String.valueOf(pref.getBoolean(KEY_ISPROFILECOMPLETE, false)));
        user.put(KEY_ISPROFILEPUBLIC, String.valueOf(pref.getBoolean(KEY_ISPROFILEPUBLIC, false)));
        user.put(KEY_ISCONFIRMED, String.valueOf(pref.getBoolean(KEY_ISCONFIRMED, false)));
        user.put(KEY_ISPARTNER, String.valueOf(pref.getBoolean(KEY_ISPARTNER, false)));
        return user;
    }

    public HashMap<String, String> getFirebaseUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_PICTURE, pref.getString(KEY_PICTURE, null));
        user.put(KEY_COUNTRY, pref.getString(KEY_COUNTRY, null));
        user.put(KEY_CITY, pref.getString(KEY_CITY, null));
        user.put(KEY_GENDER, pref.getString(KEY_GENDER, null));
        user.put(KEY_ROLE, pref.getString(KEY_ROLE, null));
        user.put(KEY_DOB, pref.getString(KEY_DOB, null));
        user.put(KEY_FRIENDS, String.valueOf(pref.getInt(KEY_FRIENDS, 0)));
        user.put(KEY_ISPROFILECOMPLETE, String.valueOf(pref.getBoolean(KEY_ISPROFILECOMPLETE, false)));
        user.put(KEY_ISPROFILEPUBLIC, String.valueOf(pref.getBoolean(KEY_ISPROFILEPUBLIC, false)));
        user.put(KEY_ISCONFIRMED, String.valueOf(pref.getBoolean(KEY_ISCONFIRMED, false)));
        user.put(KEY_ISPARTNER, String.valueOf(pref.getBoolean(KEY_ISPARTNER, false)));
        return user;
    }

    /**
     * Get stored session data
     * */
    public Settings getUserSettings(){
        int distance = pref.getInt(DISTANCE, 10);
        boolean pushEnabled = pref.getBoolean(PUSH_ENABLED, true);
        boolean soundEnabled = pref.getBoolean(SOUND_ENABLED, true);
        boolean highResEnabled = pref.getBoolean(LOW_RES_ENABLED, false);
        boolean offlineEnabled = pref.getBoolean(OFFLINE_ENABLED, true);

        Settings settings = new Settings(distance, pushEnabled, soundEnabled, highResEnabled, offlineEnabled);

        return settings;
    }


    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, SignInActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    public boolean isProfileCompete(){
        return pref.getBoolean(KEY_ISPROFILECOMPLETE, false);
    }

    public boolean isPartner()
    {
        return pref.getBoolean(KEY_ISPARTNER, false);
    }

    public boolean isSetManUser()
    {
        boolean b;
        switch(pref.getString(KEY_ROLE, ""))
        {
            case ("user"):
                b = false;
                break;
            case ("super_user"):
                b = false;
                break;
            case ("set_man"):
                b = true;
                break;
            default:
                b = false;
                break;
        }
        return b;
    }

    public boolean isSuperUser()
    {
        boolean b;
        switch(pref.getString(KEY_ROLE, ""))
        {
            case ("user"):
                b = false;
                break;
            case ("superuser"):
                b = true;
                break;
            case ("setman"):
                b = false;
                break;
            default:
                b = false;
                break;
        }
        return b;
    }

    public class Settings {
        private int distance;
        private boolean pushEnabled;
        private boolean soundEnabled;
        private boolean low_res_enabled;
        private boolean go_offline_enabled;

        public Settings(int distance, boolean pushEnabled, boolean soundEnabled, boolean low_res_enabled, boolean go_offline_enabled) {
            this.distance = distance;
            this.pushEnabled = pushEnabled;
            this.soundEnabled = soundEnabled;
            this.low_res_enabled = low_res_enabled;
            this.go_offline_enabled = go_offline_enabled;
        }

        public int getDistance() {
            return distance;
        }

        public boolean isPushEnabled() {
            return pushEnabled;
        }

        public boolean isSoundEnabled() {
            return soundEnabled;
        }

        public boolean isLow_res_enabled() {
            return low_res_enabled;
        }

        public boolean isGo_offline_enabled() {
            return go_offline_enabled;
        }
    }
}
