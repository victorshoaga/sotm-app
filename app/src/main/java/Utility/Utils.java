package Utility;

/**
 * Created by inventor on 5/4/16.
 */

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.glt.sotm.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import Models.Error;
import retrofit2.Response;

import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by Inventor
 */
public class Utils {

    public static final String URL_STORAGE_REFERENCE = "gs://light-ab136.appspot.com";
    public static final String FOLDER_STORAGE_IMG = "gallery";
    public static final int STATUS_200_OK = 200;
    public static final int STATUS_201_CREATED = 201;
    public static final int STATUS_202_ACCEPTED = 202;
    public static final int STATUS_400_BAD_REQUEST = 400;
    public static final int STATUS_401_AUTH_FAILED = 401;
    public static final int STATUS_403_FORBIDDEN = 403;
    public static final int STATUS_404_REQUEST_NOT_FOUND = 404;
    public static final int STATUS_405_METHOD_NOT_ALLOWED = 405;
    public static final int STATUS_409_CONFLICT = 409;
    public static final int STATUS_412_PRECONDITION_FAILED = 412;
    public static final int STATUS_413_REQUEST_ENTITY_TOO_LARGE = 413;
    public static final int STATUS_500_INTERNAL_SERVER_ERROR = 500;
    public static final int STATUS_501_NOT_IMPLEMENTED = 501;
    public static final int STATUS_503_SERVICE_UNAVAILABLE = 503;
    private static final String TAG = "Utils";
    private static final String DEFAULT_ERROR_MESSAGE = "Utils.getErrorResponse";


    public static int getToolbarHeight(Context context) {
        int height = (int) context.getResources().getDimension(R.dimen.abc_action_bar_default_height_material);
        return height;
    }

//    public static int getStatusBarHeight(Context context) {
//        int height = (int) context.getResources().getDimension(R.dimen.statusbar_size);
//        return height;
//    }

    public static Drawable tintMyDrawable(Drawable drawable, int color) {
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    public static boolean verifyConnection(Context context) {
        boolean conected;
        ConnectivityManager conectivtyManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        conected = conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected();
        return conected;
    }

    public static void initToast(Context c, String message) {
        Toast.makeText(c, message, Toast.LENGTH_SHORT).show();
    }

    public static void initToast(Context context, String message, boolean LONG) {
        if (LONG) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public static String getTimeStamp(String dateStr, SimpleDateFormat format) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        final long millisToAdd = 3_600_000; //one hours
        String timestamp = "";

        if (format == null) {
            format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getDisplayName()));
        }

        Calendar calendar = Calendar.getInstance();
        String today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            date.setTime(date.getTime() - millisToAdd);
//            format.setTimeZone(TimeZone.getTimeZone("GMT"));
            dateStr = format.format(date);
            date = format.parse(dateStr);

            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return timestamp;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static class CountryIsoUtil {
        public CountryIsoUtil() {

        }

        private final Set<String> ISO_LANGUAGES = new HashSet<String>
                (Arrays.asList(Locale.getISOLanguages()));
        private final Set<String> ISO_COUNTRIES = new HashSet<String>
                (Arrays.asList(Locale.getISOCountries()));

        public boolean isValidISOLanguage(String s) {
            return ISO_LANGUAGES.contains(s);
        }

        public boolean isValidISOCountry(String s) {
            return ISO_COUNTRIES.contains(s);
        }
    }

    public static <T> String getErrorMessage(Response<T> response) {
        try {
            if (response.errorBody() != null) {
                Gson gson = new GsonBuilder().create();
                Error errorObj = new Error();

                String errMsg = response.errorBody().string();
                Log.e(TAG, errMsg);

                errorObj = gson.fromJson(errMsg, Error.class);
                List<String> errors = errorObj.getErrors();
                if (errors != null && errors.size() > 0) {
                    errMsg = errors.get(0);
                } else {
                    errMsg = errorObj.getMsg();
                }

                return errMsg;
            } else {
                return response.body().toString();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        return DEFAULT_ERROR_MESSAGE;
    }

    public static void disableEditText(EditText mEditText)
    {
        mEditText.setCustomSelectionActionModeCallback(new Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
    }
}