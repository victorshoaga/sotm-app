package chat;

import android.content.Context;

import Models.ChatModel;
import Models.CommentModel;
import Models.User;


/**
 * Author: Kartik Sharma
 * Created on: 8/28/2016 , 11:06 AM
 * Project: FirebaseChat
 */

public interface ChatContract {
    interface View {
        void onSendMessageSuccess();

        void onSendMessageFailure(String message);

        void onGetMessagesSuccess(ChatModel chat);

        void onGetMessagesFailure(String message);

        void onGetCommentsSuccess(CommentModel commentModel);
    }

    interface Presenter {
        void sendMessage(Context context, ChatModel chat, String receiverFirebaseToken, User self, User talking_to);

        void getMessage(String senderUid, String receiverUid);

         void sendComment(Context context, CommentModel comment, String username, String post_id);
         void getComment(String senderUid, String post_id);
    }

    interface Interactor {
        void sendMessageToFirebaseUser(Context context, ChatModel chat, String receiverFirebaseToken, User self, User talking_to);
        void sendCommentToFirebaseUser(Context context, CommentModel comment, String username, String post_id);
        void getMessageFromFirebaseUser(String senderUid, String receiverUid);
        void getCommentFromFirebaseUser(String senderUid, String post_id);
    }

    interface OnSendMessageListener {
        void onSendMessageSuccess();

        void onSendMessageFailure(String message);
    }

    interface OnGetMessagesListener {
        void onGetMessagesSuccess(ChatModel chat);

        void onGetMessagesFailure(String message);

        void onGetCommentsSuccess(CommentModel commentModel);

    }
}
