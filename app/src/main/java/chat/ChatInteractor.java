package chat;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import Models.ChatModel;
import Models.CommentModel;
import Models.User;
import Utility.Constants;
import fcm.FcmNotificationBuilder;

/**
 * Author: Kartik Sharma
 * Created on: 9/2/2016 , 10:08 PM
 * Project: FirebaseChat
 */

public class ChatInteractor implements ChatContract.Interactor {
    private static final String TAG = "ChatInteractor";

    private ChatContract.OnSendMessageListener mOnSendMessageListener;
    private ChatContract.OnGetMessagesListener mOnGetMessagesListener;

    public ChatInteractor(ChatContract.OnSendMessageListener onSendMessageListener) {
        this.mOnSendMessageListener = onSendMessageListener;
    }

    public ChatInteractor(ChatContract.OnGetMessagesListener onGetMessagesListener) {
        this.mOnGetMessagesListener = onGetMessagesListener;
    }

    public ChatInteractor(ChatContract.OnSendMessageListener onSendMessageListener,
                          ChatContract.OnGetMessagesListener onGetMessagesListener) {
        this.mOnSendMessageListener = onSendMessageListener;
        this.mOnGetMessagesListener = onGetMessagesListener;
    }

    @Override
    public void sendMessageToFirebaseUser(final Context context, final ChatModel chat, final String receiverFirebaseToken, final User self, final User talking_to) {
        final String room_type_1 = self.getId() + "_" + talking_to.getId();
        final String room_type_2 = talking_to.getId() + "_" + self.getId();
        try
        {
            Log.i(TAG + " fcm_tok", self.getFcm_token());
            Log.i(TAG + " fcm_tok", receiverFirebaseToken);
            Log.i(TAG + " self", self.toString());
            Log.i(TAG + " talkn_to", talking_to.toString());
        }catch (NullPointerException e)
        {
            Log.i(TAG, e.getMessage());
        }

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child(Constants.ARG_CHAT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(room_type_1)) {
                    Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_1 + " exists");
                    //databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_1).child(String.valueOf(chat.getTimeStamp())).setValue(chat);
                    databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_1).child(Constants.ARG_MESSAGES).push().setValue(chat);


                } else if (dataSnapshot.hasChild(room_type_2)) {
                    Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_2 + " exists");
                    //databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_2).child(String.valueOf(chat.getTimeStamp())).setValue(chat);
                    databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_2).child(Constants.ARG_MESSAGES).push().setValue(chat);
                    databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_2).child(Constants.ARG_USERS).child(self.getId()).setValue(self);
                    databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_2).child(Constants.ARG_USERS).child(talking_to.getId()).setValue(talking_to);
                } else {
                    Log.e(TAG, "sendMessageToFirebaseUser: success");
                    //databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_1).child(String.valueOf(chat.getTimeStamp())).setValue(chat);
                    databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_1).child(Constants.ARG_MESSAGES).push().setValue(chat);
                    databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_1).child(Constants.ARG_USERS).child(self.getId()).setValue(self);
                    databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_1).child(Constants.ARG_USERS).child(talking_to.getId()).setValue(talking_to);
                    getMessageFromFirebaseUser(self.getId(), talking_to.getId());
                }
                // send push notification to the receiver
                sendPushNotificationToReceiver(self.getName(),
                        chat.getMessage(),
                        talking_to.getId(),
                        self.getFcm_token(),
                        receiverFirebaseToken);
                mOnSendMessageListener.onSendMessageSuccess();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mOnSendMessageListener.onSendMessageFailure("Unable to send message: " + databaseError.getMessage());
            }
        });
    }

    @Override
    public void sendCommentToFirebaseUser(Context context, final CommentModel comment, String username, final String post_id) {
        final String room_type_1 = post_id;
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child(Constants.ARG_COMMENT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(room_type_1)) {
                    Log.e(TAG, "sendComment: " + room_type_1 + " exists");
                    //databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_1).child(String.valueOf(chat.getTimeStamp())).setValue(chat);
                    databaseReference.child(Constants.ARG_COMMENT_ROOMS).child(room_type_1).child(Constants.ARG_COMMENTS).push().setValue(comment);
                    Log.i(TAG, String.valueOf(dataSnapshot.getChildrenCount()));

                } else {
                    Log.e(TAG, "sendCommentToFirebase: success");
                    //databaseReference.child(Constants.ARG_CHAT_ROOMS).child(room_type_1).child(String.valueOf(chat.getTimeStamp())).setValue(chat);
                    databaseReference.child(Constants.ARG_COMMENT_ROOMS).child(room_type_1).child(Constants.ARG_COMMENTS).push().setValue(comment);
                    getCommentFromFirebaseUser("", post_id);
                }
                mOnSendMessageListener.onSendMessageSuccess();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mOnSendMessageListener.onSendMessageFailure("Unable to send message: " + databaseError.getMessage());
            }
        });
    }

    private void sendPushNotificationToReceiver(String username,
                                                String message,
                                                String uid,
                                                String firebaseToken,
                                                String receiverFirebaseToken) {
        FcmNotificationBuilder.initialize()
                .title(username)
                .message(message)
                .username(username)
                .uid(uid)
                .firebaseToken(firebaseToken)
                .receiverFirebaseToken(receiverFirebaseToken)
                .send();
    }

    @Override
    public void getMessageFromFirebaseUser(String senderUid, String receiverUid) {
        final String room_type_1 = senderUid + "_" + receiverUid;
        final String room_type_2 = receiverUid + "_" + senderUid;

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child(Constants.ARG_CHAT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(room_type_1)) {
                    Log.e(TAG, "getMessageFromFirebaseUser: " + room_type_1 + " exists");
                    FirebaseDatabase.getInstance()
                            .getReference()
                            .child(Constants.ARG_CHAT_ROOMS)
                            .child(room_type_1)
                            .child(Constants.ARG_MESSAGES)
                            .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            ChatModel chat = dataSnapshot.getValue(ChatModel.class);
                            mOnGetMessagesListener.onGetMessagesSuccess(chat);
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            mOnGetMessagesListener.onGetMessagesFailure("Unable to get message: " + databaseError.getMessage());
                        }
                    });
                } else if (dataSnapshot.hasChild(room_type_2)) {
                    Log.e(TAG, "getMessageFromFirebaseUser: " + room_type_2 + " exists");
                    FirebaseDatabase.getInstance()
                            .getReference()
                            .child(Constants.ARG_CHAT_ROOMS)
                            .child(room_type_2)
                            .child(Constants.ARG_MESSAGES)
                            .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            ChatModel chat = dataSnapshot.getValue(ChatModel.class);
                            mOnGetMessagesListener.onGetMessagesSuccess(chat);
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            mOnGetMessagesListener.onGetMessagesFailure("Unable to get message: " + databaseError.getMessage());
                        }
                    });
                } else {
                    mOnGetMessagesListener.onGetMessagesSuccess(null);
                    Log.e(TAG, "getMessageFromFirebaseUser: no such room available");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mOnGetMessagesListener.onGetMessagesFailure("Unable to get message: " + databaseError.getMessage());
            }
        });
    }

    @Override
    public void getCommentFromFirebaseUser(String senderUid, String post_id) {
        final String room_type_1 = post_id;
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child(Constants.ARG_COMMENT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(room_type_1)) {
                    Log.e(TAG, "getCommentFromFirebaseUser: " + room_type_1 + " exists");
                    FirebaseDatabase.getInstance()
                            .getReference()
                            .child(Constants.ARG_COMMENT_ROOMS)
                            .child(room_type_1)
                            .child(Constants.ARG_COMMENTS)
                            .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            CommentModel commentModel = dataSnapshot.getValue(CommentModel.class);
                            mOnGetMessagesListener.onGetCommentsSuccess(commentModel);
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            mOnGetMessagesListener.onGetMessagesFailure("Unable to get message: " + databaseError.getMessage());
                        }
                    });
                } else {
                    mOnGetMessagesListener.onGetCommentsSuccess(null);
                    Log.e(TAG, "getMessageFromFirebaseUser: no such room available");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mOnGetMessagesListener.onGetMessagesFailure("Unable to get message: " + databaseError.getMessage());
            }
        });
    }
}
