package chat;

import android.content.Context;

import Models.ChatModel;
import Models.CommentModel;
import Models.User;

/**
 * Author: Kartik Sharma
 * Created on: 9/2/2016 , 10:05 PM
 * Project: FirebaseChat
 */

public class ChatPresenter implements ChatContract.Presenter, ChatContract.OnSendMessageListener,
        ChatContract.OnGetMessagesListener {
    private ChatContract.View mView;
    private ChatInteractor mChatInteractor;

    public ChatPresenter(ChatContract.View view) {
        this.mView = view;
        mChatInteractor = new ChatInteractor(this, this);
    }

    @Override
    public void sendMessage(Context context, ChatModel chat, String receiverFirebaseToken, User self, User talking_to) {
        mChatInteractor.sendMessageToFirebaseUser(context, chat, receiverFirebaseToken, self, talking_to);
    }

    @Override
    public void getMessage(String senderUid, String receiverUid) {
        mChatInteractor.getMessageFromFirebaseUser(senderUid, receiverUid);
    }

    @Override
    public void sendComment(Context context, CommentModel comment, String username, String post_id) {
        mChatInteractor.sendCommentToFirebaseUser(context, comment, username, post_id);
    }

    @Override
    public void getComment(String senderUid, String post_id) {
        mChatInteractor.getCommentFromFirebaseUser(senderUid, post_id);
    }

    @Override
    public void onSendMessageSuccess() {
        mView.onSendMessageSuccess();
    }

    @Override
    public void onSendMessageFailure(String message) {
        mView.onSendMessageFailure(message);
    }

    @Override
    public void onGetMessagesSuccess(ChatModel chat) {
        mView.onGetMessagesSuccess(chat);
    }

    @Override
    public void onGetMessagesFailure(String message) {
        mView.onGetMessagesFailure(message);
    }

    @Override
    public void onGetCommentsSuccess(CommentModel comment) {
        mView.onGetCommentsSuccess(comment);
    }
}
