package com.glt.sotm;

//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.text.Html;
//import android.text.format.DateFormat;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewTreeObserver;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.firebase.ui.database.FirebaseRecyclerAdapter;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.database.ChildEventListener;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.Query;
//import com.google.firebase.database.ValueEventListener;
//import com.pnikosis.materialishprogress.ProgressWheel;
//import com.squareup.picasso.Picasso;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Locale;
//
//import Adapters.ChatsAdapter;
//import Models.ChatModel;
//import Models.MessageDataModel;
//import Models.MyConnectsDocs;
//import Models.Response_MyConnects;
//import Models.User;
//import Rest.APIclient;
//import Rest.APIinterface;
//import Utility.Constants;
//import Utility.Conv;
//import Utility.NewSimpleDividerItemDecoration;
//import Utility.SessionManager;
//import Utility.Utils;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import tr.xip.errorview.ErrorView;
//
//public class ChatsActivity extends AppCompatActivity {
//
//    private static final String TAG = ChatsActivity.class.getSimpleName();
//
//    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
//    private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;
//    private int PAGE_NUMBER = 1;
//
//    SessionManager sessionManager;
//    HashMap<String, String> hashMap;
//    RecyclerView rvListMessage;
//    LinearLayoutManager mLinearLayoutManager;
//    ProgressWheel progressWheel;
//    Button btn_load_more;
//    ErrorView bad_internet_errorview, empty_adapter_errorview;
//
//    List<MessageDataModel> docsList;
//    ChatsAdapter chatsAdapter;
//    ArrayList<User> users_list;
//    ArrayList<ChatModel> chatModel_list;
//    Iterable<DataSnapshot> snapshotIterable;
//
//    private boolean firstTime = false;
//
//    private RecyclerView mConvList;
//
//    private DatabaseReference mConvDatabase;
//    private DatabaseReference mMessageDatabase;
//    private DatabaseReference mUsersDatabase;
//
//    //private FirebaseAuth mAuth;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_chats);
//        sessionManager = new SessionManager(getApplicationContext());
//        hashMap = sessionManager.getUserDetails();
//        docsList = new ArrayList<MessageDataModel>();
//        users_list = new ArrayList<>();
//        chatModel_list = new ArrayList<>();
//        snapshotIterable = new Iterable<DataSnapshot>() {
//            @Override
//            public Iterator<DataSnapshot> iterator() {
//                return null;
//            }
//        };
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.chats_toolbar);
//        setSupportActionBar(toolbar);
//        toolbar.setTitleTextColor(Color.WHITE);
//        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
//
//            }
//        });
//
//        mConvList = (RecyclerView) findViewById(R.id.recycler_view_chats);
//
//        mConvDatabase = FirebaseDatabase.getInstance().getReference().child(Constants.ARG_CHAT_ROOMS);
//
//        mConvDatabase.keepSynced(true);
//        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child(Constants.ARG_USERS);
//        mMessageDatabase = FirebaseDatabase.getInstance().getReference().child(Constants.ARG_MESSAGES);
//        mUsersDatabase.keepSynced(true);
//
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setReverseLayout(true);
//        linearLayoutManager.setStackFromEnd(true);
//
//        mConvList.setHasFixedSize(true);
//        mConvList.setLayoutManager(linearLayoutManager);
//
//        bindViews();
////        getUserFriends(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID),
////                String.valueOf(PAGE_NUMBER), true);
//        getMessagedFriends(hashMap.get(SessionManager.KEY_ID));
//    }
//
//    private void bindViews(){
//        rvListMessage = (RecyclerView) findViewById(R.id.recycler_view_chats);
//        progressWheel = (ProgressWheel) findViewById(R.id.chats_progress_wheel);
//        bad_internet_errorview = (ErrorView) findViewById(R.id.chats_poor_internet_error_view);
//        empty_adapter_errorview = (ErrorView) findViewById(R.id.chats_empty_error_view);
//        btn_load_more = (Button) findViewById(R.id.btn_chats_load_more);
//        mLinearLayoutManager = new LinearLayoutManager(getApplicationContext());
//        rvListMessage.setHasFixedSize(true);
//        rvListMessage.setLayoutManager(mLinearLayoutManager);
//        rvListMessage.addItemDecoration(new NewSimpleDividerItemDecoration(
//                getApplicationContext(), NewSimpleDividerItemDecoration.DEFAULT_PADDING_LEFT
//        ));
//        rvListMessage.setItemAnimator(new DefaultItemAnimator());
//        btn_load_more.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btn_load_more.setText("please wait.....");
//                PAGE_NUMBER++;
////                getUserFriends(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID),
////                        String.valueOf(PAGE_NUMBER), false);
//                getMessagedFriends(hashMap.get(SessionManager.KEY_ID));
//            }
//        });
//    }
//
//    private void showDialog()
//    {
//        progressWheel.setVisibility(View.VISIBLE);
//        bad_internet_errorview.setVisibility(View.GONE);
//        empty_adapter_errorview.setVisibility(View.GONE);
//        btn_load_more.setVisibility(View.GONE);
//    }
//
//    private void hideDialog()
//    {
//        progressWheel.setVisibility(View.GONE);
//    }
//
//    private void showLoadMoreButton()
//    {
//        btn_load_more.setVisibility(View.VISIBLE);
//        btn_load_more.setText("Load more");
//        progressWheel.setVisibility(View.GONE);
//        bad_internet_errorview.setVisibility(View.GONE);
//        empty_adapter_errorview.setVisibility(View.GONE);
//    }
//
//    private void hideLoadMoreButton()
//    {
//        btn_load_more.setVisibility(View.GONE);
//    }
//
//    private void showErrorView(int type)
//    {
//        switch (type)
//        {
//            case 1:
//                bad_internet_errorview.setVisibility(View.VISIBLE);
//                empty_adapter_errorview.setVisibility(View.GONE);
//                progressWheel.setVisibility(View.GONE);
//                btn_load_more.setVisibility(View.GONE);
//                bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
//                    @Override
//                    public void onRetry() {
////                        getUserFriends(hashMap.get(SessionManager.KEY_TOKEN),
////                                hashMap.get(SessionManager.KEY_ID),
////                                String.valueOf(PAGE_NUMBER),true);
//                        getMessagedFriends(hashMap.get(SessionManager.KEY_ID));
//                    }
//                });
//                break;
//            case 2:
//                empty_adapter_errorview.setVisibility(View.VISIBLE);
//                bad_internet_errorview.setVisibility(View.GONE);
//                progressWheel.setVisibility(View.GONE);
//                btn_load_more.setVisibility(View.GONE);
//                break;
//        }
//    }
//
//////    private void getUserFriends(final String token, final String user_id, String page, final boolean dialogPresent)
//////    {
//////        if(dialogPresent) {
//////            showDialog();
//////        }
//////        APIinterface service = APIclient.createService(APIinterface.class, token);
//////        final Call<Response_MyConnects> call = service.getUserFriends(token, user_id, page);
//////        Log.i(TAG, call.request().url().toString());
//////        call.enqueue(new Callback<Response_MyConnects>() {
//////            @Override
//////            public void onResponse(Call<Response_MyConnects> call, Response<Response_MyConnects> response) {
//////                if(dialogPresent) {
//////                    hideDialog();
//////                }
//////                Log.i(TAG, "STATUS_CODE = " + response.code());
//////                if(response.code() == Utils.STATUS_200_OK)
//////                {
//////                    Log.i(TAG, response.body().toString());
//////                    if(response.body().getMsg().equals("Success"))
//////                    {
//////                        if(response.body().getData().getDocs().size() != 0)
//////                        {
//////                            Log.i(TAG, "Array Size = " + String.valueOf(response.body().getData().getDocs().size()));
//////                            for(int i = 0; i<response.body().getData().getDocs().size(); i++)
//////                            {
//////                                docsList.add(response.body().getData().getDocs().get(i));
//////                            }
//////                            chatsAdapter = new ChatsAdapter(ChatsActivity.this, docsList,
//////                                    hashMap.get(SessionManager.KEY_TOKEN),
//////                                    hashMap.get(SessionManager.KEY_ID));
//////                            rvListMessage.setAdapter(chatsAdapter);
//////
//////                            if(response.body().getData().getPages_left() != 0)
//////                            {
//////                                showLoadMoreButton();
//////                            }
//////                            else
//////                            {
//////                                hideLoadMoreButton();
//////                            }
//////                        }
//////                        else
//////                        {
//////                            showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
//////                        }
//////                    }
//////                }
//////                else  {
//////                    Utils.initToast(getApplicationContext(), Utils.getErrorMessage(response));
//////                }
//////            }
//////
//////            @Override
//////            public void onFailure(Call<Response_MyConnects> call, Throwable t) {
//////                if(dialogPresent){
//////                    hideDialog();
//////                }
//////                showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
//////                call.cancel();
//////                Toast.makeText(getApplicationContext(), Html.fromHtml(getString(R.string.reg_error_txt)) , Toast.LENGTH_SHORT).show();
//////            }
//////        });
//////    }
////
//    private void getMessagedFriends(final String user_id)
//    {
//        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
//
//        databaseReference.child(Constants.ARG_CHAT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                firstTime = true;
//                Log.i(TAG, "DATA CHANGE");
//                showDialog();
//                users_list.clear();
//                chatModel_list.clear();
//                docsList.clear();
//                getInvolvementsOfID(dataSnapshot.getChildren().iterator(), user_id);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                hideDialog();
//                showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
//            }
//        });
//    }
////
//    private void getInvolvementsOfID(Iterator<DataSnapshot> dataSnapshotIterator, final String user_id)
//    {
//        String friend_id;
//        Log.i(TAG, "here now");
//        while (dataSnapshotIterator.hasNext())
//        {
//            Log.i(TAG, "now now");
//            DataSnapshot dataSnapshot = (DataSnapshot) dataSnapshotIterator.next();
//            if(dataSnapshot.getKey().contains(user_id))
//            {
//                friend_id = dataSnapshot.getKey().split("_")[0];
//                if(friend_id.equals(user_id))
//                {
//                    friend_id = dataSnapshot.getKey().split("_")[1];
//                }
//                //Log.i(TAG + " friend ID", friend_id);
//                users_list.add(dataSnapshot.child("users").child(friend_id).getValue(User.class));
//                ArrayList<ChatModel> cmlist = new ArrayList<>();
//                cmlist.clear();
//                for(DataSnapshot ds : dataSnapshot.child("messages").getChildren())
//                {
//                    cmlist.add(ds.getValue(ChatModel.class));
//                }
//                Collections.sort(cmlist, new Comparator<ChatModel>() {
//                    @Override
//                    public int compare(ChatModel cm, ChatModel cm1) {
//                        //Log.i(TAG, String.valueOf(messageDataModel.getChatModel().getTimeStamp()));
//                        SimpleDateFormat dFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//                        SimpleDateFormat dFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//                        Date d = new Date(), d1 = new Date();
//                        try {
//                            d = dFormat.parse(getDate(cm.getTimeStamp()));
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                            Log.e(TAG, e.getMessage());
//                        }
//                        try {
//                            d1 = dFormat1.parse(getDate(cm1.getTimeStamp()));
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                            Log.e(TAG, e.getMessage());
//                        }
//                        return d1.compareTo(d);
//                    }
//                });
//                chatModel_list.add(cmlist.get(0));
//                Query lastMessageQuery = dataSnapshot.getRef().child("messages").limitToLast(1);
//                lastMessageQuery.addChildEventListener(new ChildEventListener() {
//                    @Override
//                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                        Log.i(TAG, "CHILD ADDED");
//                        if(!firstTime)
//                        {
//                            Log.i(TAG, "Just do it");
//                        }
//                    }
//
//                    @Override
//                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                        Log.i(TAG, "CHILD CHANGED");
//                    }
//
//                    @Override
//                    public void onChildRemoved(DataSnapshot dataSnapshot) {
//                        Log.i(TAG, "CHILD REMOVED");
//                    }
//
//                    @Override
//                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//                        Log.i(TAG, "CHILD MOVED");
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                        Log.i(TAG, "CANCELLED");
//                    }
//                });
//
//            }
//        }
//        Log.i(TAG, "here here");
//        Log.i(TAG + " chatModel Size", String.valueOf(chatModel_list.size()));
//        Log.i(TAG + " user Size", String.valueOf(users_list.size()));
//        docsList.clear();
//        int retVal = putInDocsList(users_list, chatModel_list);
//        Log.i(TAG, String.valueOf(retVal));
//        rvListMessage.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                //At this point the layout is complete and the
//                //dimensions of recyclerView and any child views are known.
//            }
//        });
//        if(retVal > 0)
//        {
//            Log.i(TAG, "done done");
//            firstTime = false;
//            hideDialog();
//        }
//
//    }
//
////    private void getNewMessages(Iterator<DataSnapshot> dataSnapshotIterator, final String user_id)
////    {
////        String friend_id;
////        while (dataSnapshotIterator.hasNext())
////        {
////            DataSnapshot dataSnapshot = (DataSnapshot) dataSnapshotIterator.next();
////            if(dataSnapshot.getKey().contains(user_id))
////            {
////                friend_id = dataSnapshot.getKey().split("_")[0];
////                if(friend_id.equals(user_id))
////                {
////                    friend_id = dataSnapshot.getKey().split("_")[1];
////                }
////                //Log.i(TAG + " friend ID", friend_id);
////                users_list.add(dataSnapshot.child("users").child(friend_id).getValue(User.class));
////                ArrayList<ChatModel> cmlist = new ArrayList<>();
////                cmlist.clear();
////                for(DataSnapshot ds : dataSnapshot.child("messages").getChildren())
////                {
////                    cmlist.add(ds.getValue(ChatModel.class));
////                }
////                Collections.sort(cmlist, new Comparator<ChatModel>() {
////                    @Override
////                    public int compare(ChatModel cm, ChatModel cm1) {
////                        //Log.i(TAG, String.valueOf(messageDataModel.getChatModel().getTimeStamp()));
////                        SimpleDateFormat dFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
////                        SimpleDateFormat dFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
////                        Date d = new Date(), d1 = new Date();
////                        try {
////                            d = dFormat.parse(getDate(cm.getTimeStamp()));
////                        } catch (ParseException e) {
////                            e.printStackTrace();
////                            Log.e(TAG, e.getMessage());
////                        }
////                        try {
////                            d1 = dFormat1.parse(getDate(cm1.getTimeStamp()));
////                        } catch (ParseException e) {
////                            e.printStackTrace();
////                            Log.e(TAG, e.getMessage());
////                        }
////                        return d1.compareTo(d);
////                    }
////                });
////                chatModel_list.add(cmlist.get(0));
////                Query lastMessageQuery = dataSnapshot.getRef().child("messages").limitToLast(1);
////                lastMessageQuery.addChildEventListener(new ChildEventListener() {
////                    @Override
////                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
////                        Log.i(TAG, "CHILD ADDED");
////                    }
////
////                    @Override
////                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
////                        Log.i(TAG, "CHILD CHANGED");
////                    }
////
////                    @Override
////                    public void onChildRemoved(DataSnapshot dataSnapshot) {
////                        Log.i(TAG, "CHILD REMOVED");
////                    }
////
////                    @Override
////                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
////                        Log.i(TAG, "CHILD MOVED");
////                    }
////
////                    @Override
////                    public void onCancelled(DatabaseError databaseError) {
////                        Log.i(TAG, "CANCELLED");
////                    }
////                });
////            }
////        }
////    }
////
//
//    private int putInDocsList(ArrayList<User> users, ArrayList<ChatModel> chatModel_list)
//    {
//        for(int i = 0; i < users.size(); i++)
//        {
//
//            Log.i(TAG, users.get(i).toString());
//            Log.i(TAG, chatModel_list.get(i).toString());
//            firstTime = false;
//            docsList.add(new MessageDataModel(users.get(i), chatModel_list.get(i)));
//
//        }
//
//        Collections.sort(docsList, new Comparator<MessageDataModel>() {
//            @Override
//            public int compare(MessageDataModel messageDataModel, MessageDataModel messageDataModel1) {
//                //Log.i(TAG, String.valueOf(messageDataModel.getChatModel().getTimeStamp()));
//                SimpleDateFormat dFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//                SimpleDateFormat dFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//                Date d = new Date(), d1 = new Date();
//                try {
//                    d = dFormat.parse(getDate(messageDataModel.getChatModel().getTimeStamp()));
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                    Log.e(TAG, e.getMessage());
//                }
//                try {
//                    d1 = dFormat1.parse(getDate(messageDataModel1.getChatModel().getTimeStamp()));
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                    Log.e(TAG, e.getMessage());
//                }
//                return d1.compareTo(d);
//            }
//        });
//
//        chatsAdapter = new ChatsAdapter(ChatsActivity.this, ChatsActivity.this, docsList,
//                                    hashMap.get(SessionManager.KEY_TOKEN),
//                                    hashMap.get(SessionManager.KEY_ID));
//        rvListMessage.setAdapter(chatsAdapter);
//        Log.i(TAG + " get item count", String.valueOf(chatsAdapter.getItemCount()));
//        return chatsAdapter.getItemCount();
//    }
//
//
//    private String getDate(long time) {
//        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
//        cal.setTimeInMillis(time);
//        String date = DateFormat.format("dd-MM-yyyy HH:mm:ss", cal).toString();
//        return date;
//    }
//
//    private void reset(boolean t)
//    {
//        if (t)
//        {
//            Intent intent = new Intent(ChatsActivity.this, ChatsActivity.class);
//            startActivity(intent);
//        }
//    }
//
//}

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import Utility.Conv;
import Utility.SessionManager;
import Utility.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import tr.xip.errorview.ErrorView;


/**
 * A simple {@link Fragment} subclass.
 *
 *
 */

public class ChatsActivity extends AppCompatActivity {

    private static final String TAG = ChatsActivity.class.getSimpleName();
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        sessionManager = new SessionManager(ChatsActivity.this);
        hashMap = new HashMap<>();
        hashMap = sessionManager.getUserDetails();

        Toolbar toolbar = (Toolbar) findViewById(R.id.chats_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

            }
        });

        if(savedInstanceState == null) {
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, new ChatsFragment(), "ChatsFragment");
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.commit();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }


    public static class ChatsFragment extends Fragment{

        private static final String TAG = ChatsFragment.class.getSimpleName();
        private RecyclerView mConvList;

        private DatabaseReference mConvDatabase;
        private DatabaseReference mMessageDatabase;
        private DatabaseReference mUsersDatabase;

        private SessionManager sessionManager;
        private HashMap<String, String> hashMap;
        ProgressWheel progressWheel;
        ErrorView bad_internet_errorview;
        ErrorView empty_adapter_errorview;
        private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
        private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;
        //private FirebaseAuth mAuth;

        private String mCurrent_user_id;

        private View mMainView;
        String fcm_token = "";



        public ChatsFragment() {
            // Required empty public constructor
        }




        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            mMainView = inflater.inflate(R.layout.fragment_chats, container, false);
            bad_internet_errorview = (ErrorView) mMainView.findViewById(R.id.friends_poor_internet_error_view);
            empty_adapter_errorview = (ErrorView) mMainView.findViewById(R.id.friends_empty_error_view);
            sessionManager = new SessionManager(getContext());
            hashMap = new HashMap<>();
            hashMap = sessionManager.getUserDetails();

            mConvList = (RecyclerView) mMainView.findViewById(R.id.conv_list);
            //mAuth = FirebaseAuth.getInstance();

            //mCurrent_user_id = mAuth.getCurrentUser().getUid();
            mCurrent_user_id = hashMap.get(SessionManager.KEY_ID);

            mConvDatabase = FirebaseDatabase.getInstance().getReference().child("Chat").child(mCurrent_user_id);

            mConvDatabase.keepSynced(true);
            mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
            mMessageDatabase = FirebaseDatabase.getInstance().getReference().child("messages").child(mCurrent_user_id);
            mUsersDatabase.keepSynced(true);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setReverseLayout(true);
            linearLayoutManager.setStackFromEnd(true);

            mConvList.setHasFixedSize(true);
            mConvList.setLayoutManager(linearLayoutManager);


            // Inflate the layout for this fragment
            return mMainView;
        }


        @Override
        public void onStart() {
            super.onStart();

            Query conversationQuery = mConvDatabase.orderByChild("timestamp");

            FirebaseRecyclerAdapter<Conv, ConvViewHolder> firebaseConvAdapter = new FirebaseRecyclerAdapter<Conv, ConvViewHolder>(
                    Conv.class,
                    R.layout.users_single_layout,
                    ConvViewHolder.class,
                    conversationQuery
            ) {
                @Override
                protected void populateViewHolder(final ConvViewHolder convViewHolder, final Conv conv, int i) {


                    if(getItemCount() < 1)
                    {
                        showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
                    }
                    final String list_user_id = getRef(i).getKey();

                    Query lastMessageQuery = mMessageDatabase.child(list_user_id).limitToLast(1);


                    lastMessageQuery.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                            String data = dataSnapshot.child("message").getValue().toString();
                            if(data.length() > 60)
                            {
                                convViewHolder.setMessage(data.substring(0, 59), conv.isSeen());
                            }
                            else{ convViewHolder.setMessage(data, conv.isSeen());}

                            convViewHolder.setNewMessageIcon(conv.isSeen());

                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });

                    Log.i("ChatsActivity", list_user_id);
                    mUsersDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            final String userName = dataSnapshot.child("name").getValue().toString();
                            final String userThumb = dataSnapshot.child("picture").getValue().toString();

                            try{ fcm_token = dataSnapshot.child("fcmToken").getValue().toString();}
                            catch (Exception e){Log.e(TAG, e.getMessage());}

                            if(dataSnapshot.hasChild("online")) {

                                String userOnline = dataSnapshot.child("online").getValue().toString();
                                convViewHolder.setUserOnline(userOnline);

                            }

                            if(dataSnapshot.hasChild("isTyping"))
                            {
                                String isTyping = dataSnapshot.child("isTyping").getValue().toString();
                                convViewHolder.setIsTyping(Boolean.valueOf(isTyping));
                            } else {
                                convViewHolder.setIsTyping(false);
                            }

                            convViewHolder.setName(userName);
                            convViewHolder.setUserImage(userThumb, getContext());
                            convViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent chatIntent = new Intent(getContext(), UserFriendChatActivity.class);
                                    chatIntent.putExtra("user_id", list_user_id);
                                    chatIntent.putExtra("user_name", userName);
                                    chatIntent.putExtra("picture_url", userThumb);
                                    chatIntent.putExtra("fcm_token", fcm_token);
                                    startActivity(chatIntent);

                                }
                            });
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });

                }
            };

            mConvList.setAdapter(firebaseConvAdapter);

        }

        public static class ConvViewHolder extends RecyclerView.ViewHolder {

            View mView;

            public ConvViewHolder(View itemView) {
                super(itemView);

                mView = itemView;

            }

            public void setMessage(String message, boolean isSeen){

                TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
                userStatusView.setText(message);

                if(!isSeen){
                    userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.BOLD);
                } else {
                    userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.NORMAL);
                }
            }

            public void setIsTyping(boolean isTyping)
            {
                TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
                if(isTyping)
                {
                    userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.BOLD);
                    userStatusView.setTextColor(mView.getResources().getColor(R.color.colorAccent));
                    userStatusView.setText("is typing...");
                }
            }

            public void setName(String name){

                TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
                userNameView.setText(name);
            }

            public void setUserImage(String thumb_image, Context ctx){

                //CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);
                CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.civProfilePic);
                Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.head_photo).into(userImageView);

            }

            public void setUserOnline(String online_status) {

                //ImageView userOnlineView = (ImageView) mView.findViewById(R.id.user_single_online_icon);
                ImageView userOnlineView = (ImageView) mView.findViewById(R.id.status);

                if(online_status.equals("true")){

                    userOnlineView.setVisibility(View.VISIBLE);
                    userOnlineView.setImageResource(R.color.cpb_green);

                } else {

                    userOnlineView.setVisibility(View.VISIBLE);
                    userOnlineView.setImageResource(R.color.grey_text_);

                }

            }

            public void setNewMessageIcon(boolean isSeen)
            {
                ImageView imageView = (ImageView) mView.findViewById(R.id.new_message_icon);
                if(!isSeen)
                {
                    imageView.setVisibility(View.VISIBLE);
                }
                else
                {
                    imageView.setVisibility(View.GONE);
                }
            }


        }


        private void showErrorView(int type)
        {
            switch (type)
            {
                case 1:
                    bad_internet_errorview.setVisibility(View.VISIBLE);
                    empty_adapter_errorview.setVisibility(View.GONE);
                    progressWheel.setVisibility(View.GONE);
                    //btn_load_more.setVisibility(View.GONE);
                    break;
                case 2:
                    empty_adapter_errorview.setVisibility(View.VISIBLE);
                    bad_internet_errorview.setVisibility(View.GONE);
                    progressWheel.setVisibility(View.GONE);
                    empty_adapter_errorview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), FriendSuggestionsActivity.class);
                            startActivity(intent);
                        }
                    });
                    //btn_load_more.setVisibility(View.GONE);
                    break;
            }
        }
    }


}

