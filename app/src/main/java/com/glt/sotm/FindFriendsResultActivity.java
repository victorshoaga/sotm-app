package com.glt.sotm;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapters.SwipeStackAdapter;
import Models.Response_Connect;
import Models.Response_FindConnects;
import Models.User;
import Models.User_Friend_Request;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import Utility.Utils;
import link.fls.swipestack.SwipeStack;
import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

public class FindFriendsResultActivity extends AppCompatActivity implements SwipeStack.SwipeStackListener{
    private static final String TAG = FindFriendsResultActivity.class.getSimpleName();
    private String payload;

    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
    private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;
    private int PAGE_NUMBER = 1;

    TextView info_txt;

    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    ProgressWheel progressWheel;
    ErrorView bad_internet_errorview, empty_adapter_errorview;
    List<User_Friend_Request> docsList;
    SwipeStackAdapter mAdapter;
    SwipeStack swipeStack;
    LovelyCustomDialog lovelyCustomDialog;

    public FindFriendsResultActivity() {
        //Required empty Constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_connects_result);
        lovelyCustomDialog = new LovelyCustomDialog(FindFriendsResultActivity.this);

        payload = (String) getIntent().getStringExtra("payload");
        Log.i(TAG, payload);

        sessionManager = new SessionManager(getApplicationContext());
        hashMap = sessionManager.getUserDetails();
        docsList = new ArrayList<User_Friend_Request>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.find_connects_result_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_action_clear), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

            }
        });
        bindViews();
        docsList = new ArrayList<>();
        mAdapter = new SwipeStackAdapter(FindFriendsResultActivity.this, docsList);
        swipeStack.setAdapter(mAdapter);
        swipeStack.setListener(this);
        if (payload != null) {
            postFindConnects(hashMap.get(SessionManager.KEY_TOKEN),
                    String.valueOf(PAGE_NUMBER), true);
        } else {
            Log.e(TAG, "No payload found");
        }
    }

    private void bindViews(){
        info_txt = (TextView) findViewById(R.id.info_txt);
        progressWheel = (ProgressWheel) findViewById(R.id.find_connects_result_progress_wheel);
        bad_internet_errorview = (ErrorView) findViewById(R.id.find_connects_result_poor_internet_error_view);
        empty_adapter_errorview = (ErrorView) findViewById(R.id.find_connects_result_empty_error_view);
        swipeStack = (SwipeStack) findViewById(R.id.find_connects_result_swipeStack);


    }

    private void showDialog()
    {
        info_txt.setVisibility(View.GONE);
        progressWheel.setVisibility(View.VISIBLE);
        bad_internet_errorview.setVisibility(View.GONE);
        empty_adapter_errorview.setVisibility(View.GONE);
    }

    private void hideDialog()
    {
        progressWheel.setVisibility(View.GONE);
        swipeStack.setVisibility(View.VISIBLE);
    }

    private void showErrorView(int type)
    {
        switch (type)
        {
            case 1:
                bad_internet_errorview.setVisibility(View.VISIBLE);
                empty_adapter_errorview.setVisibility(View.GONE);
                progressWheel.setVisibility(View.GONE);
                bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
                    @Override
                    public void onRetry() {
                        postFindConnects(hashMap.get(SessionManager.KEY_TOKEN),
                                String.valueOf(PAGE_NUMBER),true);
                    }
                });
                break;
            case 2:
                empty_adapter_errorview.setVisibility(View.VISIBLE);
                bad_internet_errorview.setVisibility(View.GONE);
                progressWheel.setVisibility(View.GONE);
                break;
        }
    }

    private void postFindConnects(String token, String page, final boolean dialogPresent)
    {
        if(dialogPresent) {
            showDialog();
        }
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_FindConnects> call = service.findFriends(token, payload, Integer.valueOf(page));
        Log.i(TAG, call.request().url().toString());
        Log.i(TAG, bodyToString(call.request().body()));
        call.enqueue(new Callback<Response_FindConnects>() {
            @Override
            public void onResponse(Call<Response_FindConnects> call, Response<Response_FindConnects> response) {
                if(dialogPresent) {
                    hideDialog();
                }
                Log.i(TAG, "STATUS_CODE = " + response.code());
                Log.i(TAG, bodyToString(call.request().body()));
                if(response.code() == Utils.STATUS_200_OK)
                {
                    if(response.body().getMsg().equals("Success"))
                    {
                        if(response.body().getData().getDocs().size() != 0)
                        {
                            info_txt.setVisibility(View.GONE);
                            if(!sessionManager.getRememberToSwipe()){
                                lovelyCustomDialog
                                        .setView(R.layout.dialog_swipe)
                                        .setTopColorRes(R.color.linkedin_blue)
                                        .setIcon(R.drawable.ic_info_outline_white_36dp)
                                        .setListener(R.id.ok_btn, true, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                sessionManager.setRememberToSwipe(true);
                                            }
                                        })
                                        .setListener(R.id.cancel_btn, true, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //do nothing specifically
                                            }
                                        })
                                        .setCancelable(false)
                                        .show();
                            }
                            Log.i(TAG, "Array Size = " + String.valueOf(response.body().getData().getDocs().size()));
                            for(int i = 0; i<response.body().getData().getDocs().size(); i++)
                            {
                                docsList.add(response.body().getData().getDocs().get(i));
                            }
                        }
                        else
                        {
                            showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
                        }
                    }
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);

                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_FindConnects> call, Throwable t) {
                if(dialogPresent){
                    hideDialog();
                }
                showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
                call.cancel();
                Toast.makeText(getApplicationContext(), Html.fromHtml(getString(R.string.reg_error_txt)) , Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onViewSwipedToLeft(final int position) {
        //do ignore here
        //Toast.makeText(getApplicationContext(), "Ignored @"+docsList.get(position).getUser().getUsername(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewSwipedToRight(final int position) {

        //do send connect here
        APIinterface sendConnect_service = APIclient.createService(APIinterface.class, hashMap.get(SessionManager.KEY_TOKEN), false);
        final Call<Response_Connect> sendConnect_call = sendConnect_service.sendRequest(hashMap.get(SessionManager.KEY_TOKEN),
                hashMap.get(SessionManager.KEY_ID), docsList.get(position).getUser().getId());
        sendConnect_call.enqueue(new Callback<Response_Connect>() {
            @Override
            public void onResponse(Call<Response_Connect> call, Response<Response_Connect> response) {
                Log.i(TAG, hashMap.get(SessionManager.KEY_ID));
                //Log.i(TAG, docsList.get(position).getPlassUser().getId());
                Log.i(TAG, String.valueOf(response.code()));
                if (response.code() == Utils.STATUS_200_OK) {
                    Toast.makeText(getApplicationContext(), "Sent connect to @"+docsList.get(position).getUser().getUsername(), Toast.LENGTH_SHORT).show();
                } else {
                    String errorMessage = Utils.getErrorMessage(response);

                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_Connect> call, Throwable t) {
                sendConnect_call.cancel();
                Toast.makeText(getApplicationContext(), Html.fromHtml(getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onStackEmpty() {

        //show progress dialog here
        PAGE_NUMBER++;
        postFindConnects(hashMap.get(SessionManager.KEY_TOKEN),
                String.valueOf(PAGE_NUMBER), true);
    }

    private String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }
}
