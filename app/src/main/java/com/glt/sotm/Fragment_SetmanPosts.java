package com.glt.sotm;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapters.SetmanPostAdapter;
import Models.Post;
import Models.Post_Docs;
import Models.Post_Response;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import Utility.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

/**
 * Created by inventor on 1/26/18.
 */

public class Fragment_SetmanPosts extends Fragment{

//    private static final String TAG = Fragment_SetmanPosts.class.getSimpleName();
//    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
//    private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;
//    private int PAGE_NUMBER = 1;
//
//    SessionManager sessionManager;
//    HashMap<String, String> hashMap;
//    RecyclerView rvListMessage;
//    LinearLayoutManager mLinearLayoutManager;
//    ProgressWheel progressWheel;
//    Button btn_load_more;
//    ErrorView bad_internet_errorview, empty_adapter_errorview;
//    SetmanPostAdapter setmanPostAdapter;
//    List<Post_Docs> docsList;
//
//    public Fragment_SetmanPosts() {
//        // Required empty public constructor
//    }
//
//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
//     * @return A new instance of fragment UsersNearbyFragment.
//     */
//    // TODO: Rename and change types and number of parameters
//    public static Fragment_SetmanPosts newInstance(String param1, String param2) {
//        Fragment_SetmanPosts fragment = new Fragment_SetmanPosts();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_setman_posts, container, false);
//
//        sessionManager = new SessionManager(getActivity());
//        hashMap = sessionManager.getUserDetails();
//        docsList = new ArrayList<Post_Docs>();
//        setmanPostAdapter = new SetmanPostAdapter(getActivity(), docsList);
//
//        bindViews(view);
//        Log.i(TAG + "token from pref", hashMap.get(SessionManager.KEY_TOKEN));
//        getPosts(hashMap.get(SessionManager.KEY_TOKEN),
//                String.valueOf(PAGE_NUMBER), true);
//
//        return view;
//    }
//
//    private void bindViews(View view){
//        rvListMessage = (RecyclerView) view.findViewById(R.id.recycler_view);
//        progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);
//        bad_internet_errorview = (ErrorView) view.findViewById(R.id.poor_internet_error_view);
//        empty_adapter_errorview = (ErrorView) view.findViewById(R.id.empty_error_view);
//        btn_load_more = (Button) view.findViewById(R.id.btn_load_more);
//        mLinearLayoutManager = new LinearLayoutManager(getActivity());
//        rvListMessage.setHasFixedSize(true);
//        rvListMessage.setLayoutManager(mLinearLayoutManager);
//        ;
//        btn_load_more.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btn_load_more.setText("Please wait.....");
//                PAGE_NUMBER++;
//                getPosts(hashMap.get(SessionManager.KEY_TOKEN),
//                        String.valueOf(PAGE_NUMBER), false);
//            }
//        });
//    }
//
//    private void showDialog()
//    {
//        progressWheel.setVisibility(View.VISIBLE);
//        bad_internet_errorview.setVisibility(View.GONE);
//        empty_adapter_errorview.setVisibility(View.GONE);
//        btn_load_more.setVisibility(View.GONE);
//    }
//
//    private void hideDialog()
//    {
//        progressWheel.setVisibility(View.GONE);
//    }
//
//    private void showLoadMoreButton()
//    {
//        btn_load_more.setVisibility(View.VISIBLE);
//        btn_load_more.setText("Load more");
//        progressWheel.setVisibility(View.GONE);
//        bad_internet_errorview.setVisibility(View.GONE);
//        empty_adapter_errorview.setVisibility(View.GONE);
//    }
//
//    private void hideLoadMoreButton()
//    {
//        btn_load_more.setVisibility(View.GONE);
//    }
//
//    private void showErrorView(int type)
//    {
//        switch (type)
//        {
//            case 1:
//                bad_internet_errorview.setVisibility(View.VISIBLE);
//                empty_adapter_errorview.setVisibility(View.GONE);
//                progressWheel.setVisibility(View.GONE);
//                btn_load_more.setVisibility(View.GONE);
//                bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
//                    @Override
//                    public void onRetry() {
//                        getPosts(hashMap.get(SessionManager.KEY_TOKEN),
//                                String.valueOf(PAGE_NUMBER),true);
//                    }
//                });
//                break;
//            case 2:
//                empty_adapter_errorview.setVisibility(View.VISIBLE);
//                bad_internet_errorview.setVisibility(View.GONE);
//                progressWheel.setVisibility(View.GONE);
//                btn_load_more.setVisibility(View.GONE);
//                break;
//        }
//    }
//
//    private void getPosts(String token, String page, final boolean dialogPresent)
//    {
//        if(dialogPresent) {
//            showDialog();
//        }
//        APIinterface service = APIclient.createService(APIinterface.class, token);
//        final Call<Post_Response> call = service.getPosts_general(token, Integer.valueOf(page));
//        Log.i(TAG, token);
//        Log.i(TAG, call.request().url().toString());
//        call.enqueue(new Callback<Post_Response>() {
//            @Override
//            public void onResponse(Call<Post_Response> call, Response<Post_Response> response) {
//                if(dialogPresent) {
//                    hideDialog();
//                }
//                Log.i(TAG, "STATUS_CODE = " + response.code());
//                if(response.code() == Utils.STATUS_200_OK)
//                {
//                    if(response.body().getMsg().equals("Success"))
//                    {
//                        if(response.body().getData().getDocs().size() != 0)
//                        {
//                            Log.i(TAG, "Array Size = " + String.valueOf(response.body().getData().getDocs().size()));
//                            for(int i = 0; i<response.body().getData().getDocs().size(); i++)
//                            {
//                                docsList.add(response.body().getData().getDocs().get(i));
//                            }
//                            rvListMessage.setAdapter(setmanPostAdapter);
//
//                            if(response.body().getData().getPages_left() != 0)
//                            {
//                                showLoadMoreButton();
//                            }
//                            else
//                            {
//                                hideLoadMoreButton();
//                            }
//                        }
//                        else
//                        {
//                            showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
//                        }
//                    }
//                }
//                else {
//                    String errorMessage = Utils.getErrorMessage(response);
//
//                    Log.w(TAG, errorMessage);
//                    if(isAdded()){Utils.initToast(getActivity(), errorMessage);}
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Post_Response> call, Throwable t) {
//                if(dialogPresent){
//                    hideDialog();
//                }
//                showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
//                call.cancel();
//                if(isAdded()){
//                    Toast.makeText(getActivity(), Html.fromHtml(getString(R.string.reg_error_txt)) ,Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }
}
