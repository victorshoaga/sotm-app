package com.glt.sotm;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapters.SuggestionsAdapter;
import Models.MyFriendsDocs;
import Models.Response_Connect;
import Models.Response_MyConnects;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.EndlessRecyclerViewScrollListener;
import Utility.NewSimpleDividerItemDecoration;
import Utility.SessionManager;
import Utility.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

import static android.view.View.GONE;

public class FriendSuggestionsActivity extends AppCompatActivity {

    private static final String TAG = FriendSuggestionsActivity.class.getSimpleName();

    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
    private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;

    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    ArrayList<MyFriendsDocs> docsList;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    ProgressWheel progressWheel;
    ErrorView bad_internet_errorview, empty_adapter_errorview;
    SuggestionsAdapter mySuggestionAdapter;
    EndlessRecyclerViewScrollListener scrollListener;
    boolean loadingComplete = false;
    Button btn_search;
    TextView txt_suggestions;
    //TextInputLayout input_layout_query;
    EditText edit_input_query;
    TextView info_txt;
    private int edit_position;
    private boolean add = false;
    private Paint p = new Paint();
    LovelyCustomDialog lovelyCustomDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_suggestions);
        sessionManager = new SessionManager(FriendSuggestionsActivity.this);
        hashMap = new HashMap<>();
        hashMap = sessionManager.getUserDetails();
        lovelyCustomDialog = new LovelyCustomDialog(FriendSuggestionsActivity.this);
        docsList = new ArrayList<MyFriendsDocs>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.friend_suggestion_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
            }
        });
        bindViews();
        docsList.clear();
        getSuggestions(hashMap.get(SessionManager.KEY_TOKEN), true);
    }

    private void bindViews(){
        btn_search = (Button) findViewById(R.id.btn_search);
        txt_suggestions = (TextView) findViewById(R.id.txt_suggestion);
        info_txt = (TextView) findViewById(R.id.info_txt);
        edit_input_query = (EditText) findViewById(R.id.edt_input_query);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edit_input_query.getText().length() > 0)
                {
                    Intent intent = new Intent(FriendSuggestionsActivity.this, FindFriendsResultActivity.class);
                    intent.putExtra("payload", edit_input_query.getText().toString().trim());
                    startActivity(intent);
                }
                else
                    {
                        Utils.initToast(FriendSuggestionsActivity.this, "Please input a valid name to search");
                    }
            }
        });
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_suggestions);
        progressWheel = (ProgressWheel) findViewById(R.id.suggest_progress_wheel);
        bad_internet_errorview = (ErrorView) findViewById(R.id.suggest_poor_internet_error_view);
        empty_adapter_errorview = (ErrorView) findViewById(R.id.suggest_empty_error_view);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new NewSimpleDividerItemDecoration(
                getApplicationContext(), NewSimpleDividerItemDecoration.DEFAULT_PADDING_LEFT
        ));
        //rvListMessage.setItemAnimator(new DefaultItemAnimator());
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //loadNextDataFromApi(page);
                if(!loadingComplete)
                {
//                    getSuggestions(hashMap.get(SessionManager.KEY_TOKEN), false);
                    Utils.initToast(FriendSuggestionsActivity.this, "end");
                }
            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);
        mySuggestionAdapter = new SuggestionsAdapter(FriendSuggestionsActivity.this, docsList, hashMap.get(SessionManager.KEY_TOKEN),
                hashMap.get(SessionManager.KEY_ID));


    }

    private void showDialog()
    {
        txt_suggestions.setVisibility(GONE);
        info_txt.setVisibility(GONE);
        progressWheel.setVisibility(View.VISIBLE);
        bad_internet_errorview.setVisibility(GONE);
        empty_adapter_errorview.setVisibility(GONE);
    }

    private void hideDialog()
    {
        progressWheel.setVisibility(GONE);
        txt_suggestions.setVisibility(View.VISIBLE);
        info_txt.setVisibility(View.GONE);
    }

    private void showErrorView(int type)
    {
        switch (type)
        {
            case 1:
                bad_internet_errorview.setVisibility(View.VISIBLE);
                empty_adapter_errorview.setVisibility(GONE);
                progressWheel.setVisibility(GONE);
                txt_suggestions.setVisibility(GONE);
                info_txt.setVisibility(GONE);
                //btn_load_more.setVisibility(View.GONE);
                bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
                    @Override
                    public void onRetry() {
                        docsList.clear();
                        scrollListener.resetState();
                        getSuggestions(hashMap.get(SessionManager.KEY_TOKEN),true);
                    }
                });
                break;
            case 2:
                empty_adapter_errorview.setVisibility(View.VISIBLE);
                bad_internet_errorview.setVisibility(GONE);
                progressWheel.setVisibility(GONE);
                txt_suggestions.setVisibility(GONE);
                info_txt.setVisibility(GONE);
                break;
        }
    }

    private void getSuggestions(String token, final boolean dialogPresent)
    {
        if(dialogPresent) {
            showDialog();
        }
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_MyConnects> call = service.getSuggestions(token);
        Log.i(TAG, call.request().url().toString());
        Log.i(TAG + "Token", token);
        call.enqueue(new Callback<Response_MyConnects>() {
            @Override
            public void onResponse(Call<Response_MyConnects> call, Response<Response_MyConnects> response) {
                if(dialogPresent) {
                    hideDialog();
                }
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    if(response.body().getMsg().equals("Success"))
                    {
                        if(response.body().getData().getDocs().size() != 0)
                        {
                            if(!sessionManager.getRememberToSwipe()){
                                lovelyCustomDialog
                                        .setView(R.layout.dialog_swipe)
                                        .setTopColorRes(R.color.linkedin_blue)
                                        .setIcon(R.drawable.ic_info_outline_white_36dp)
                                        .setListener(R.id.ok_btn, true, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                sessionManager.setRememberToSwipe(true);
                                            }
                                        })
                                        .setListener(R.id.cancel_btn, true, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //do nothing specifically
                                            }
                                        })
                                        .setCancelable(false)
                                        .show();
                            }
                            Log.i(TAG, "Array Size = " + String.valueOf(response.body().getData().getDocs().size()));
                            for(int i = 0; i<response.body().getData().getDocs().size(); i++)
                            {
                                docsList.add(new MyFriendsDocs(true, response.body().getData().getDocs().get(i).getUser()));
                            }
                            recyclerView.setAdapter(mySuggestionAdapter);
                            mySuggestionAdapter.notifyDataSetChanged();
                            initSwipe();
                            if(response.body().getData().getPages_left() != 0)
                            {
                                loadingComplete = false;
                            }
                            else
                            {
                                loadingComplete = true;
                            }
                        }
                        else
                        {
                            if(docsList.isEmpty())
                            {
                                showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
                            }

                        }
                    }
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_MyConnects> call, Throwable t) {
                if(dialogPresent){
                    hideDialog();
                }
                showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
                call.cancel();
                Log.e(TAG, t.getMessage());
                Toast.makeText(getApplicationContext(), Html.fromHtml(getString(R.string.reg_error_txt)) , Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void initSwipe(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT){
                    mySuggestionAdapter.removeItem(position);
                    if(mySuggestionAdapter.getItemCount() < 1)
                    {
                        getSuggestions(hashMap.get(SessionManager.KEY_TOKEN), true);
                    }
                } else {
                    edit_position = position;
                    String id = docsList.get(position).getUser().getId();
                    String uname = docsList.get(position).getUser().getUsername();
                    try{
                        mySuggestionAdapter.removeItem(position);
                        sendConnect(id, uname);
                    } catch (IndexOutOfBoundsException e) {
                        Log.e(TAG, e.getMessage());
                    }
                    if(mySuggestionAdapter.getItemCount() < 1)
                    {
                        getSuggestions(hashMap.get(SessionManager.KEY_TOKEN), true);
                    }
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if(dX > 0){
                        p.setColor(Color.parseColor("#388E3C"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                        c.drawRect(background,p);
                        //c.drawText("ACCEPT", (float) itemView.getRight() + width, (float) itemView.getRight() + 2*width, p);
                        //icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_add_white);
                        //RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                        //c.drawBitmap(icon,null,icon_dest,p);
                    } else {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background,p);
                        //c.drawText("IGNORE", (float) itemView.getRight() - 2*width, (float) itemView.getRight() - width, p);
                        //icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_sub_white);
                        //RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        //c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void removeView(View view){
        if(view.getParent()!=null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }

    private void sendConnect(String friend_id, final String friend_username)
    {
        APIinterface sendConnect_service = APIclient.createService(APIinterface.class, hashMap.get(SessionManager.KEY_TOKEN), false);
        final Call<Response_Connect> sendConnect_call = sendConnect_service.sendRequest(hashMap.get(SessionManager.KEY_TOKEN),
                hashMap.get(SessionManager.KEY_ID), friend_id);
        sendConnect_call.enqueue(new Callback<Response_Connect>() {
            @Override
            public void onResponse(Call<Response_Connect> call, Response<Response_Connect> response) {
                Log.i(TAG, hashMap.get(SessionManager.KEY_ID));
                //Log.i(TAG, docsList.get(position).getPlassUser().getId());
                Log.i(TAG, String.valueOf(response.code()));
                if (response.code() == Utils.STATUS_200_OK) {
                    Toast.makeText(getApplicationContext(), "Sent connect to @"+friend_username, Toast.LENGTH_SHORT).show();
                } else {
                    String errorMessage = Utils.getErrorMessage(response);

                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_Connect> call, Throwable t) {
                sendConnect_call.cancel();
                Toast.makeText(getApplicationContext(), Html.fromHtml(getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }
}
