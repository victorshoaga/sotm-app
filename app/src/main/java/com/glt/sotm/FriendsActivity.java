package com.glt.sotm;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.AbstractCursor;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import Adapters.FriendsAdapter;
import Database.FriendsListDBHelper;
import Models.MyConnectsDocs;
import Models.MyFriendsDocs;
import Models.Response_MyConnects;
import Models.UserFriend;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.EndlessRecyclerViewScrollListener;
import Utility.NewSimpleDividerItemDecoration;
import Utility.SessionManager;
import Utility.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

public class FriendsActivity extends AppCompatActivity {

    private static final String TAG = FriendsActivity.class.getSimpleName();

    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
    private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;
    private int PAGE_NUMBER = 1;

    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    RecyclerView rvListMessage, recyclerView2;
    LinearLayoutManager mLinearLayoutManager, mxLinearLayoutManager;
    View online_line, online_line_;
    ProgressWheel progressWheel;
    //Button btn_load_more;
    ErrorView bad_internet_errorview, empty_adapter_errorview;
    LovelyTextInputDialog lovelyTextInputDialog;

    ArrayList<MyFriendsDocs> docsList;
    List<MyFriendsDocs> docsList2;
    FriendsAdapter myConnectsAdapter;
    EndlessRecyclerViewScrollListener scrollListener;
    boolean loadingComplete = false;
    public static SearchView searchView;
    SwipeRefreshLayout swipeRefreshLayout;
    FriendsListDBHelper friendsListDBHelper;
    boolean isDataShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        sessionManager = new SessionManager(FriendsActivity.this);
        lovelyTextInputDialog = new LovelyTextInputDialog(this);
        hashMap = sessionManager.getUserDetails();
        docsList = new ArrayList<MyFriendsDocs>();
        docsList2 = new ArrayList<MyFriendsDocs>();
        friendsListDBHelper = new FriendsListDBHelper(FriendsActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.friends_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                    return;
                }
                else
                {
                    finish();
                    overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                }
            }
        });
        bindViews();
        docsList.clear();
        scrollListener.resetState();

        getUserFriends(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID),
                String.valueOf(PAGE_NUMBER), true);

    }

    private void bindViews(){
        online_line = (View) findViewById(R.id.online_line);
        online_line_ = (View) findViewById(R.id.online_line_);
        recyclerView2 = (RecyclerView) findViewById(R.id.recycler_view_online_friends);
        mxLinearLayoutManager = new LinearLayoutManager(this);
        recyclerView2.setHasFixedSize(true);
        recyclerView2.setLayoutManager(mxLinearLayoutManager);
        recyclerView2.addItemDecoration(new NewSimpleDividerItemDecoration(
                getApplicationContext(), NewSimpleDividerItemDecoration.DEFAULT_PADDING_LEFT
        ));
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimaryDark,
                R.color.linkedin_blue,
                R.color.cpb_red,
                R.color.cpb_green_dark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //swipeRefreshLayout.setRefreshing(false);
                getUserFriends(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID),
                        String.valueOf(PAGE_NUMBER), true);
            }
        });
        rvListMessage = (RecyclerView)findViewById(R.id.recycler_view_friends);
        progressWheel = (ProgressWheel) findViewById(R.id.friends_progress_wheel);
        bad_internet_errorview = (ErrorView) findViewById(R.id.friends_poor_internet_error_view);
        empty_adapter_errorview = (ErrorView) findViewById(R.id.friends_empty_error_view);
        //btn_load_more = (Button) findViewById(R.id.btn_friends_load_more);
        mLinearLayoutManager = new LinearLayoutManager(this);
        rvListMessage.setHasFixedSize(true);
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        rvListMessage.addItemDecoration(new NewSimpleDividerItemDecoration(
                getApplicationContext(), NewSimpleDividerItemDecoration.DEFAULT_PADDING_LEFT
        ));
        //rvListMessage.setItemAnimator(new DefaultItemAnimator());
        scrollListener = new EndlessRecyclerViewScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //loadNextDataFromApi(page);
                if(!loadingComplete)
                {
                    swipeRefreshLayout.setRefreshing(true);
                    PAGE_NUMBER++;
                    getUserFriends(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID),
                            String.valueOf(PAGE_NUMBER), false);
                }
            }
        };
        // Adds the scroll listener to RecyclerView
        rvListMessage.addOnScrollListener(scrollListener);
        myConnectsAdapter = new FriendsAdapter(FriendsActivity.this, FriendsActivity.this, docsList, hashMap.get(SessionManager.KEY_TOKEN),
                hashMap.get(SessionManager.KEY_ID));
    }

    private void showDialog()
    {
        progressWheel.setVisibility(View.VISIBLE);
        bad_internet_errorview.setVisibility(View.GONE);
        empty_adapter_errorview.setVisibility(View.GONE);
        //btn_load_more.setVisibility(View.GONE);
    }

    private void hideDialog()
    {
        progressWheel.setVisibility(View.GONE);
    }

    private void showErrorView(int type)
    {
        switch (type)
        {
            case 1:
                if(!isDataShown){
                    bad_internet_errorview.setVisibility(View.VISIBLE);
                    empty_adapter_errorview.setVisibility(View.GONE);
                    progressWheel.setVisibility(View.GONE);
                    //btn_load_more.setVisibility(View.GONE);
                    bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
                        @Override
                        public void onRetry() {
                            docsList.clear();
                            scrollListener.resetState();
                            getUserFriends(hashMap.get(SessionManager.KEY_TOKEN),
                                    hashMap.get(SessionManager.KEY_ID),
                                    String.valueOf(PAGE_NUMBER),true);
                        }
                    });
                }
                else{
                    Utils.initToast(FriendsActivity.this, "Your Friends list may not be up to date. Please check your internet connection");
                }

                break;
            case 2:
                if(!isDataShown)
                {
                    empty_adapter_errorview.setVisibility(View.VISIBLE);
                    bad_internet_errorview.setVisibility(View.GONE);
                    progressWheel.setVisibility(View.GONE);
                    empty_adapter_errorview.setOnRetryListener(new ErrorView.RetryListener() {
                        @Override
                        public void onRetry() {
                            Intent intent = new Intent(FriendsActivity.this, FriendSuggestionsActivity.class);
                            startActivity(intent);
                        }
                    });
                }
                else
                {
                    Utils.initToast(FriendsActivity.this, "Your Friends list may not be up to date. Please check your internet connection");
                }
                //btn_load_more.setVisibility(View.GONE);
                break;
        }
    }

    private void getUserFriends(String token, String user_id, String page, final boolean dialogPresent)
    {
        if(dialogPresent) {
            showDialog();
        }
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_MyConnects> call = service.getUserFriends(token, user_id, page);
        Log.i(TAG, call.request().url().toString());
        Log.i(TAG + "Token", token);
        call.enqueue(new Callback<Response_MyConnects>() {
            @Override
            public void onResponse(Call<Response_MyConnects> call, Response<Response_MyConnects> response) {
                swipeRefreshLayout.setRefreshing(false);
                if(dialogPresent) {
                    hideDialog();
                }
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    if(response.body().getMsg().equals("Success"))
                    {
                        if(response.body().getData().getDocs().size() != 0)
                        {
                            isDataShown = true;
                            friendsListDBHelper.openW();
                            friendsListDBHelper.deleteFriendsList();
                            friendsListDBHelper.close();
                            docsList.clear();
                            myConnectsAdapter.notifyDataSetChanged();
                            Log.i(TAG, "Array Size = " + String.valueOf(response.body().getData().getDocs().size()));
                            for(int i = 0; i<response.body().getData().getDocs().size(); i++)
                            {
                                docsList.add(new MyFriendsDocs(true, response.body().getData().getDocs().get(i).getUser()));
                                if(response.body().getData().getDocs().get(i).getUser().getId() != null &&
                                        response.body().getData().getDocs().get(i).getUser().getEmail() != null &&
                                        response.body().getData().getDocs().get(i).getUser().getPicture() != null &&
                                        response.body().getData().getDocs().get(i).getUser().getUsername() != null &&
                                        response.body().getData().getDocs().get(i).getUser().getName() != null)
                                {
                                    friendsListDBHelper.openW();
                                    friendsListDBHelper.insertFriend(new UserFriend(response.body().getData().getDocs().get(i).getUser().getId(),
                                            response.body().getData().getDocs().get(i).getUser().getEmail(),
                                            response.body().getData().getDocs().get(i).getUser().getPicture(),
                                            response.body().getData().getDocs().get(i).getUser().getUsername(),
                                            response.body().getData().getDocs().get(i).getUser().getName()));
                                    friendsListDBHelper.close();
                                }

                            }
                            rvListMessage.setAdapter(myConnectsAdapter);
                            if(response.body().getData().getPages_left() != 0)
                            {
                                loadingComplete = false;
                            }
                            else
                            {
                                loadingComplete = true;
                            }
                        }
                        else
                        {
                            if(docsList.isEmpty())
                            {
                                showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
                            }

                        }
                    }
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_MyConnects> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                if(dialogPresent){
                    hideDialog();
                }
                showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
                call.cancel();
                Log.e(TAG, t.getMessage());
                Toast.makeText(getApplicationContext(), Html.fromHtml(getString(R.string.reg_error_txt)) , Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.friend_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                Log.i(TAG, "onQueryTextSubmit");
                //myConnectsAdapter.getFilter().filter(query);
                Intent intent = new Intent(FriendsActivity.this, FindFriendsResultActivity.class);
                intent.putExtra("payload", query);
                startActivity(intent);
                return true;
            }


            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                Log.i(TAG, "onQueryTextChange");
                myConnectsAdapter.getFilter().filter(query);
                return true;
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        item.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // TODO Auto-generated method stub
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                myConnectsAdapter.getFilter().filter("");
                return true;
            }
        });


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }
        if (id == R.id.action_add_friend) {
            Intent intent = new Intent(FriendsActivity.this, FriendSuggestionsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }

}
