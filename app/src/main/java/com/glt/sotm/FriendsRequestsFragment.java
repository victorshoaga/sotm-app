package com.glt.sotm;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapters.SwipeStackAdapter;
import Models.Response_Connect;
import Models.Response_FindConnects;
import Models.User_Friend_Request;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import Utility.Utils;
import link.fls.swipestack.SwipeStack;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

public class FriendsRequestsFragment extends Fragment implements SwipeStack.SwipeStackListener {
    private static final String TAG = FriendsRequestsFragment.class.getSimpleName();

    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
    private static final int SHOW_EMPTY_HISTORY_ERRORVIEW = 2;
    private int PAGE_NUMBER = 1;
    private int OFFSET = 10;

    TextView info_txt;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    ProgressWheel progressWheel;
    ErrorView bad_internet_errorview, empty_history_errorview;
    List<User_Friend_Request> docsList;
    SwipeStackAdapter mAdapter;
    SwipeStack swipeStack;
    LovelyCustomDialog lovelyCustomDialog;


    public FriendsRequestsFragment() {
        // Required empty public constructor
    }
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connect_requests, container, false);

        if(isAdded())
        {
            Toast.makeText(getActivity(), "Swipe Left to Ignore, Right to Accept", Toast.LENGTH_SHORT).show();
        }
        sessionManager = new SessionManager(getActivity());
        hashMap = sessionManager.getUserDetails();
        lovelyCustomDialog = new LovelyCustomDialog(getActivity());


        bindViews(view);

        docsList = new ArrayList<>();
        mAdapter = new SwipeStackAdapter(getActivity(), docsList);
        swipeStack.setAdapter(mAdapter);
        swipeStack.setListener(this);

        Log.i(TAG + "token from pref", hashMap.get(SessionManager.KEY_TOKEN));
        getPendingConnectsReq(hashMap.get(SessionManager.KEY_TOKEN),
                hashMap.get(SessionManager.KEY_ID), String.valueOf(PAGE_NUMBER), OFFSET);

        return view;
    }

    private void bindViews(View view){
        info_txt = (TextView) view.findViewById(R.id.info_txt);
        progressWheel = (ProgressWheel) view.findViewById(R.id.connect_req_progress_wheel);
        bad_internet_errorview = (ErrorView) view.findViewById(R.id.connect_req_poor_internet_error_view);
        empty_history_errorview = (ErrorView) view.findViewById(R.id.connect_req_empty_error_view);
        swipeStack = (SwipeStack) view.findViewById(R.id.swipeStack);


    }

    private void getPendingConnectsReq(final String token, String user_id, String page, int offset)
    {
        showDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_FindConnects> call = service.getFriendRequests(token, user_id, Integer.valueOf(page));
        Log.i(TAG, call.request().url().toString());
        call.enqueue(new Callback<Response_FindConnects>() {
            @Override
            public void onResponse(Call<Response_FindConnects> call, Response<Response_FindConnects> response) {
                hideDialog();
                try{
                    Log.i(TAG, "STATUS_CODE = " + response.code());
                    Log.i(TAG, token);
                    Log.i(TAG, response.body().toString());
                    if(response.code() == Utils.STATUS_200_OK)
                    {
                        if(response.body().getMsg().equals("Success"))
                        {
                            if(response.body().getData().getDocs().size() != 0)
                            {
                                //info_txt.setVisibility(View.VISIBLE);
                                if(!sessionManager.getRememberToSwipe()){
                                    lovelyCustomDialog
                                            .setView(R.layout.dialog_swipe)
                                            .setTopColorRes(R.color.linkedin_blue)
                                            .setIcon(R.drawable.ic_info_outline_white_36dp)
                                            .setListener(R.id.ok_btn, true, new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    sessionManager.setRememberToSwipe(true);
                                                }
                                            })
                                            .setListener(R.id.cancel_btn, true, new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    //do nothing specifically
                                                }
                                            })
                                            .setCancelable(false)
                                            .show();
                                }
                                Log.i(TAG, "Student Array Size = " + String.valueOf(response.body().getData().getDocs().size()));
                                for(int i = 0; i<response.body().getData().getDocs().size(); i++)
                                {
                                    docsList.add(response.body().getData().getDocs().get(i));
                                }
                            }
                            else
                            {
                                showErrorView(SHOW_EMPTY_HISTORY_ERRORVIEW);
                            }
                        }
                    }
                    else {
                        String errorMessage = Utils.getErrorMessage(response);

                        Log.w(TAG, errorMessage);
                        if(isAdded())
                        {
                            Utils.initToast(getActivity(), errorMessage);
                        }
                    }
                }catch(Exception e){
                    //do nothing..well may be log data
                    Log.i(TAG, e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<Response_FindConnects> call, Throwable t) {
                hideDialog();
                showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
                call.cancel();
                //This issues a crash when uncommented when there is no response data in cases of bad internet
                //which will inevitably trigger this method.
                //Log.e(TAG, call.request().headers().get("Authorization"));
                //Log.e(TAG, t.getCause().getMessage());
                if(isAdded())
                {
                    Toast.makeText(getActivity(), Html.fromHtml(getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showDialog()
    {
        info_txt.setVisibility(View.GONE);
        progressWheel.setVisibility(View.VISIBLE);
        bad_internet_errorview.setVisibility(View.GONE);
        empty_history_errorview.setVisibility(View.GONE);
        swipeStack.setVisibility(View.GONE);
    }

    private void hideDialog()
    {
        progressWheel.setVisibility(View.GONE);
        swipeStack.setVisibility(View.VISIBLE);
    }

    private void showErrorView(int type)
    {
        switch (type)
        {
            case 1:
                bad_internet_errorview.setVisibility(View.VISIBLE);
                empty_history_errorview.setVisibility(View.GONE);
                progressWheel.setVisibility(View.GONE);
                swipeStack.setVisibility(View.GONE);
                bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
                    @Override
                    public void onRetry() {
//                        getCheckInHistory(hashMap.get(TradUserSessionManager.KEY_TOKEN), hashMap.get(TradUserSessionManager.KEY_ID),
//                                String.valueOf(PAGE_NUMBER), APP_SERVER_OFFSET, hashMap.get(TradUserSessionManager.KEY_FIRSTNAME),
//                                hashMap.get(TradUserSessionManager.KEY_LASTNAME), hashMap.get(TradUserSessionManager.KEY_PICTUREURL));
                        getPendingConnectsReq(hashMap.get(SessionManager.KEY_TOKEN),
                                hashMap.get(SessionManager.KEY_ID), String.valueOf(PAGE_NUMBER), OFFSET);
                    }
                });
                break;
            case 2:
                empty_history_errorview.setVisibility(View.VISIBLE);
                bad_internet_errorview.setVisibility(View.GONE);
                progressWheel.setVisibility(View.GONE);
                swipeStack.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onViewSwipedToLeft(final int position) {

        //do ignore connect here
        APIinterface ignoreConnect_service = APIclient.createService(APIinterface.class, hashMap.get(SessionManager.KEY_TOKEN), false);
        final Call<Response_Connect> ignoreConnect_call = ignoreConnect_service.declineRequest(hashMap.get(SessionManager.KEY_TOKEN),
                docsList.get(position).getUser().getId(), hashMap.get(SessionManager.KEY_ID));
        ignoreConnect_call.enqueue(new Callback<Response_Connect>() {
            @Override
            public void onResponse(Call<Response_Connect> call, Response<Response_Connect> response) {
                Log.i(TAG, String.valueOf(response.code()));
                if (response.code() == Utils.STATUS_200_OK) {
                    if(isAdded())
                    {
                        Toast.makeText(getActivity(), "Ignored @"+docsList.get(position).getUser().getUsername(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_Connect> call, Throwable t) {
                ignoreConnect_call.cancel();
                if(isAdded())
                {
                    Toast.makeText(getActivity(), Html.fromHtml(getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onViewSwipedToRight(final int position) {

        //do accept connect here
        APIinterface acceptConnect_service = APIclient.createService(APIinterface.class, hashMap.get(SessionManager.KEY_TOKEN), false);
        final Call<Response_Connect> acceptConnect_call = acceptConnect_service.acceptRequest(hashMap.get(SessionManager.KEY_TOKEN),
                docsList.get(position).getUser().getId(), hashMap.get(SessionManager.KEY_ID));
        acceptConnect_call.enqueue(new Callback<Response_Connect>() {
            @Override
            public void onResponse(Call<Response_Connect> call, Response<Response_Connect> response) {
                Log.i(TAG, hashMap.get(SessionManager.KEY_ID));
                //Log.i(TAG, docsList.get(position).getPlassUser().getId());
                Log.i(TAG, String.valueOf(response.code()));
                if (response.code() == Utils.STATUS_200_OK) {
                    if(isAdded())
                    {
                        Toast.makeText(getActivity(), "Connected with @"+docsList.get(position).getUser().getUsername(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    if(isAdded())
                    {
                        Utils.initToast(getActivity(), errorMessage);
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_Connect> call, Throwable t) {
                acceptConnect_call.cancel();
                if(isAdded())
                {
                    Toast.makeText(getActivity(), Html.fromHtml(getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onStackEmpty() {

        //show progress dialog here
        PAGE_NUMBER++;
        getPendingConnectsReq(hashMap.get(SessionManager.KEY_TOKEN),
                hashMap.get(SessionManager.KEY_ID), String.valueOf(PAGE_NUMBER), OFFSET);
    }


}