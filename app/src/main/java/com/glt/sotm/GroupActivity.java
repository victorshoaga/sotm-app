package com.glt.sotm;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.util.HashMap;

import Models.Response_CreateGroup;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import Utility.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupActivity extends AppCompatActivity {

    private static final String TAG = GroupActivity.class.getSimpleName();
    LovelyTextInputDialog lovelyTextInputDialog;
    LovelyProgressDialog lovelyProgressDialog;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        lovelyTextInputDialog = new LovelyTextInputDialog(this);
        lovelyProgressDialog = new LovelyProgressDialog(this);
        sessionManager = new SessionManager(this);
        hashMap = new HashMap<>();
        hashMap = sessionManager.getUserDetails();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add_group_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddGroupDialog();
            }
        });
    }


    private void showAddGroupDialog()
    {
        lovelyTextInputDialog
                .setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.LEFT | Gravity.TOP)
                .setTitle("Enter Group Name")
                .setHint("e.g. Attacking Evangelists")
                .setIcon(R.drawable.ic_groups_white)
                .setConfirmButton("CREATE", new LovelyTextInputDialog.OnTextInputConfirmListener() {
                    @Override
                    public void onTextInputConfirmed(final String text) {
                        if(text.isEmpty() || text.length() < 4){
                            Utils.initToast(getApplicationContext(), "Enter Valid Group Name");
                        } else{
                            lovelyTextInputDialog.dismiss();
                            create_group(hashMap.get(SessionManager.KEY_TOKEN), text);
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        lovelyTextInputDialog.dismiss();
                    }
                })
                .setMessage("Please go ahead and create a group to ease conversation amongst your fellow colleagues")
                .show();
    }

    private void create_group(String token, String topic)
    {
        showDialog();
        APIinterface apIinterface = APIclient.RetrofitBuilder().create(APIinterface.class);
        final Call<Response_CreateGroup> call = apIinterface.createGroup(token, topic);
        call.enqueue(new Callback<Response_CreateGroup>() {
            @Override
            public void onResponse(Call<Response_CreateGroup> call, Response<Response_CreateGroup> response) {
                hideDialog();
                Log.i(TAG, response.code() + "");
                Utils.initToast(GroupActivity.this, "Group has been successfully created");
                Intent intent = new Intent(GroupActivity.this, SelectFriendsActivity.class);
                intent.putExtra("group_id", response.body().getData().getGroup().getId());
                intent.putExtra("group_topic", response.body().getData().getGroup().getTopic());
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            }

            @Override
            public void onFailure(Call<Response_CreateGroup> call, Throwable t) {
                hideDialog();
                call.cancel();
                Utils.initToast(getApplicationContext(), "Sorry, you can try again!");
                Log.i(TAG, t.getMessage());
            }
        });
    }

    private void showDialog()
    {
        lovelyProgressDialog.setIcon(R.drawable.ic_cast_connected_white_36dp)
                .setTitle("Please wait, creating group...")
                .setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .show();
    }

    private void hideDialog()
    {
        lovelyProgressDialog.dismiss();

    }

}
