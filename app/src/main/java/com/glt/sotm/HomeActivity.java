package com.glt.sotm;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ActionMenuView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import Adapters.SetmanPostAdapter;
import Database.HomeFeedDBHelper;
import Models.FeedBackModel;
import Models.HomeFeed;
import Models.Post_Docs;
import Models.Post_Response;
import Models.Response_HomeFeed;
import Models.Response_Post;
import Models.UpdateFCMTokenResponse;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.Constants;
import Utility.Conv;
import Utility.CustomTabActivityHelper;
import Utility.SessionManager;
import Utility.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = HomeActivity.class.getSimpleName();

    private CustomTabActivityHelper mCustomTabActivityHelper;
    //private static final String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTY4MmQ4ZTAyMTFlOTA3NDljM2FjYTIiLCJzZWFyY2hTdHIiOiJsb25ueWlsZXNhbm1pbG9uIiwidXBkYXRlZEF0IjoiMjAxOC0wMS0yNlQxODo0MjowNy45MTRaIiwiY3JlYXRlZEF0IjoiMjAxOC0wMS0yNFQwNjo1NDowNi45NzlaIiwiZW1haWwiOiJhbGJlcnRAZ21haWwuY29tIiwicG9zdHMiOltdLCJyb2xlIjoidXNlciIsInVzZXJzQmxvY2tlZEJ5IjpbXSwiYmxvY2tlZFVzZXJzIjpbXSwiZnJpZW5kcyI6W10sInJlcXVlc3RzUmVjZWl2ZWQiOltdLCJyZXF1ZXN0c1NlbnQiOltdLCJpc0NvbmZpcm1lZCI6ZmFsc2UsImZjbV90b2tlbiI6bnVsbCwiaWF0IjoxNTE3MDEzNzk1LCJleHAiOjE2MDM0MTM3OTV9.SsDiwhnqyZ_UYnOvpl8YXi_MJuAsHobqdnbSrpBnsAo";
    LovelyCustomDialog lovelyCustomDialog;
    LovelyStandardDialog lovelyStandardDialog;

    ProgressDialog progressDialog;

    CircleImageView nav_header_img, status;
    TextView nav_header_name, nav_header_username;

    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
    private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;
    private int PAGE_NUMBER = 1;

    IntentFilter mIntent;
    SessionManager sessionManager;
    HashMap<String, String> hashMap, firebaseHashMap;
    RecyclerView rvListMessage;
    LinearLayoutManager mLinearLayoutManager;
    ProgressWheel progressWheel;
    Button btn_load_more;
    ErrorView bad_internet_errorview, empty_adapter_errorview;
    SetmanPostAdapter setmanPostAdapter;
    //List<Post_Docs> docsList;
    List<HomeFeed> homeFeedsList;
    FloatingActionButton fab;
    SwipeRefreshLayout swipeRefreshLayout;
    Button btn_view_profile;
    TextView friend_notif, message_notif, notif_notif;
    int unread_message_counter = 0;
    LovelyTextInputDialog lovelyTextInputDialog;
    boolean isDataShown = false;
    HomeFeedDBHelper homeFeedDBHelper;
    LovelyProgressDialog lovelyProgressDialog;

    long n;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        progressDialog = new ProgressDialog(this);
        lovelyTextInputDialog = new LovelyTextInputDialog(this);
        mCustomTabActivityHelper = new CustomTabActivityHelper();
        sessionManager = new SessionManager(HomeActivity.this);
        lovelyProgressDialog = new LovelyProgressDialog(HomeActivity.this);
        lovelyStandardDialog = new LovelyStandardDialog(HomeActivity.this);
        hashMap = new HashMap<>();
        firebaseHashMap = new HashMap<>();
        lovelyCustomDialog = new LovelyCustomDialog(HomeActivity.this);
        homeFeedDBHelper = new HomeFeedDBHelper(HomeActivity.this);

        hashMap = sessionManager.getUserDetails();
        firebaseHashMap = sessionManager.getFirebaseUserDetails();

        FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).setValue(firebaseHashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                //Log.i(TAG, "Completed");
                //Toast.makeText(HomeActivity.this, "You are online", Toast.LENGTH_SHORT).show();
            }
        });
        mIntent = new IntentFilter("NOW");

        //docsList = new ArrayList<Post_Docs>();
        homeFeedsList = new ArrayList<>();

        setmanPostAdapter = new SetmanPostAdapter(HomeActivity.this, HomeActivity.this, homeFeedsList, lovelyProgressDialog, lovelyStandardDialog);

        bindViews();
        //Log.i(TAG + "token from pref", hashMap.get(SessionManager.KEY_TOKEN));


        homeFeedDBHelper.openR();
        if (homeFeedDBHelper.getHomeFeeds().size() > 0) {
            //Log.i(TAG + " list_DB size", homeFeedsList.size() + "");
            //Log.i(TAG + " getItemCount", "" + setmanPostAdapter.getItemCount());
            setmanPostAdapter.notifyDataSetChanged();
            homeFeedsList.clear();
            for (int i = 0; i < homeFeedDBHelper.getHomeFeeds().size(); i++) {
                homeFeedsList.add(homeFeedDBHelper.getHomeFeeds().get(i));
            }
            Collections.sort(homeFeedsList, new Comparator<HomeFeed>() {
                @Override
                public int compare(HomeFeed homeFeed, HomeFeed homeFeed1) {
                    SimpleDateFormat homefeedDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    SimpleDateFormat homefeedDateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                    Date d = new Date(), d1 = new Date();
                    try {
                        d = homefeedDateFormat.parse(homeFeed.getDate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                        Log.e(TAG, e.getMessage());
                    }
                    try {
                        d1 = homefeedDateFormat1.parse(homeFeed1.getDate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                        Log.e(TAG, e.getMessage());
                    }

                    return d1.compareTo(d);
                }
            });

            rvListMessage.setAdapter(setmanPostAdapter);
            isDataShown = true;
        }
        homeFeedDBHelper.close();

        getHomeFeed(hashMap.get(SessionManager.KEY_TOKEN));



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //check these parameters

        if(!sessionManager.isProfileCompete())
        {
            lovelyCustomDialog
                .setView(R.layout.dialog_complete_profile)
                .setTopColorRes(R.color.xml_centerColor)
                .setTitle("Please complete your profile")
                .setTitleGravity(Gravity.CENTER)
                .setIcon(R.drawable.ic_info_outline_white_36dp)
                .setListener(R.id.btn_complete_profile_account, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(HomeActivity.this, UpdateProfileActivity.class);
                        finish();
                        startActivity(intent);
                    }
                })
                .setCancelable(false)
                .show();
        }

        if(!sessionManager.isSetManUser())
        {
            fab.setVisibility(View.GONE);
        }
        else
        {
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, WritePostActivity.class);
                    startActivity(intent);
                    //createPost(hashMap.get(SessionManager.KEY_TOKEN), "Segun Obadje Teaching Ministries", "Hello beloved, Thank you for connecting with me on this platform. God bless you.", 0);
                }
            });
        }


        sendRegistrationTokenToServer(hashMap.get(SessionManager.KEY_TOKEN), FirebaseInstanceId.getInstance().getToken());
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        nav_header_img = (CircleImageView) headerView.findViewById(R.id.civProfilePic);
        status = (CircleImageView) headerView.findViewById(R.id.status);
        nav_header_name = (TextView) headerView.findViewById(R.id.nav_header_name);
        nav_header_username = (TextView) headerView.findViewById(R.id.nav_header_username);
        btn_view_profile = (Button) headerView.findViewById(R.id.btn_view_profile);

        friend_notif = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_friend));

        message_notif = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_message));

        notif_notif = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_notification));

        Picasso.with(HomeActivity.this)
                .load(hashMap.get(SessionManager.KEY_PICTURE))
                .placeholder(R.drawable.head_photo)
                .into(nav_header_img);
        nav_header_name.setText(hashMap.get(SessionManager.KEY_NAME));
        nav_header_username.setText(hashMap.get(SessionManager.KEY_EMAIL));

        navigationView.setNavigationItemSelectedListener(this);
        btn_view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_reload) {

            getHomeFeed(hashMap.get(SessionManager.KEY_TOKEN));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
        } else if (id == R.id.nav_friend) {
            Intent intent = new Intent(HomeActivity.this, FriendsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        }else if (id == R.id.nav_message) {
            Intent intent = new Intent(HomeActivity.this, ChatsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        }else if (id == R.id.nav_become_partner) {
            if(Utils.verifyConnection(HomeActivity.this))
            {
                Uri uri = Uri.parse("https://docs.google.com/forms/d/1T0D4SanPAE-3dXYQ2H9tkuMALiP14WdafF8gxNql9ys/edit?ts=5a6c495b");
                openWebView(uri, "Reg. Form");
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please make sure you are connected to the internet. Thank you", Toast.LENGTH_SHORT).show();
            }
        }
//        else if (id == R.id.nav_partner_update) {
//
//        }
        else if (id == R.id.nav_payment_portal) {
            if(Utils.verifyConnection(HomeActivity.this))
            {
                Uri uri = Uri.parse("http://payment.segunobadje.org/");
                openWebView(uri, "Payment portal");
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please make sure you are connected to the internet. Thank you", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_notification) {
            Intent intent = new Intent(HomeActivity.this, NotificationsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        }
//        else if (id == R.id.nav_leverage) {
//
//        } else if (id == R.id.nav_sog) {
//
//        }
        else if (id == R.id.nav_radio) {
            if(Utils.verifyConnection(HomeActivity.this))
            {
                Uri uri = Uri.parse("http://segunobadje.org/myradio/");
                openCustomChromeTab(uri);
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please make sure you are connected to the internet. Thank you", Toast.LENGTH_SHORT).show();
            }
        }else if (id == R.id.nav_livestream) {
            if(Utils.verifyConnection(HomeActivity.this))
            {
                Uri uri = Uri.parse("http://live.glt.church");
                openCustomChromeTab(uri);
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please make sure you are connected to the internet. Thank you", Toast.LENGTH_SHORT).show();
            }

        }else if (id == R.id.nav_estore) {
            if(Utils.verifyConnection(HomeActivity.this))
            {
                Uri uri = Uri.parse("http://estore.segunobadje.org");
                openWebView(uri, "E-Store");
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Please make sure you are connected to the internet. Thank you", Toast.LENGTH_SHORT).show();
            }

        }
//        else if(id == R.id.nav_group){
//            Intent intent = new Intent(HomeActivity.this, GroupActivity.class);
//            startActivity(intent);
//            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
//        }
        else if(id == R.id.nav_feedback)
        {
            showFeedBackDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        try
        {
            mCustomTabActivityHelper.bindCustomTabsService(this);
            DatabaseReference mMessageDatabase = FirebaseDatabase.getInstance().getReference().child("messages").child(hashMap.get(SessionManager.KEY_ID));
            DatabaseReference mConvDatabase = FirebaseDatabase.getInstance().getReference().child("Chat").child(hashMap.get(SessionManager.KEY_ID));
            Query conversationQuery = mConvDatabase.orderByChild("timestamp");
            conversationQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Conv conv = dataSnapshot.getValue(Conv.class);
                    if(!conv.isSeen())
                    {
                        unread_message_counter++;
                        //Log.i(TAG, "Unread Message");
                    }
                    initializeCount(unread_message_counter);
                    unread_message_counter = 0;
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("fcmToken").setValue(sessionManager.getToken());

        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            mCustomTabActivityHelper.unbindCustomTabsService(this);
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }

    private void openCustomChromeTab(Uri uri) {
        CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
        // set toolbar colors
        intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        // set start and exit animations
        intentBuilder.setStartAnimations(this, R.anim.slide_in_right, R.anim.slide_out_left);
        intentBuilder.setExitAnimations(this, android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);

        // build custom tabs intent
        CustomTabsIntent customTabsIntent = intentBuilder.build();
        // call helper to open custom tab
        CustomTabActivityHelper.openCustomTab(this, customTabsIntent, uri, new CustomTabActivityHelper.CustomTabFallback() {
            @Override
            public void openUri(Activity activity, Uri uri) {
                // fall back, call open open webview
                //openWebView(uri);
            }
        });
    }

    private void openWebView(Uri uri, String title) {
        Intent webViewIntent = new Intent(this, WebViewActivity.class);
        webViewIntent.putExtra(WebViewActivity.EXTRA_URL, uri.toString());
        webViewIntent.putExtra("title", title);
        startActivity(webViewIntent);
    }

    private void sendRegistrationTokenToServer(String token, final String fcmToken) {
        sessionManager.updateToken(fcmToken);
        // TODO: Implement this method to send token to your app server.
        APIinterface apIinterface = APIclient.createService(APIinterface.class, token, false);
        final Call<UpdateFCMTokenResponse> call = apIinterface.updateTokenOnServer(token, fcmToken);
        call.enqueue(new Callback<UpdateFCMTokenResponse>() {
            @Override
            public void onResponse(Call<UpdateFCMTokenResponse> call, Response<UpdateFCMTokenResponse> response) {
                if(response.isSuccessful()) {
                    //Log.i(TAG, fcmToken);
                    try{
                        Log.i(TAG, sessionManager.getToken());}catch (Exception e){
                        Log.i(TAG, e.getMessage());
                    }
                    //Log.i(TAG, "post submitted to API." + response.body().toString());
                } else {
                    try {
                        Log.i(TAG, response.errorBody().string() + " " + response.code());
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<UpdateFCMTokenResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "Unable to submit post to Server.");
            }
        });
    }
//    class ViewPagerAdapter extends FragmentPagerAdapter {
//        private final List<Fragment> mFragmentList = new ArrayList<>();
//        private final List<String> mFragmentTitleList = new ArrayList<>();
//
//        public ViewPagerAdapter(FragmentManager manager) {
//            super(manager);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return mFragmentList.get(position);
//        }
//
//        @Override
//        public int getCount() {
//            return mFragmentList.size();
//        }
//
//        public void addFragment(Fragment fragment, String title) {
//            mFragmentList.add(fragment);
//            mFragmentTitleList.add(title);
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
//        }
//    }

    private void bindViews(){
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimaryDark,
                R.color.linkedin_blue,
                R.color.cpb_red);
        rvListMessage = (RecyclerView) findViewById(R.id.recycler_view);
        progressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        bad_internet_errorview = (ErrorView) findViewById(R.id.poor_internet_error_view);
        empty_adapter_errorview = (ErrorView) findViewById(R.id.empty_error_view);
        btn_load_more = (Button) findViewById(R.id.btn_load_more);
        mLinearLayoutManager = new LinearLayoutManager(HomeActivity.this);
        rvListMessage.setHasFixedSize(true);
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        ;
        btn_load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_load_more.setText("Please wait.....");
                PAGE_NUMBER++;
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHomeFeed(hashMap.get(SessionManager.KEY_TOKEN));
            }
        });
    }

    private void showDialog()
    {
        //progressWheel.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(true);
        bad_internet_errorview.setVisibility(View.GONE);
        empty_adapter_errorview.setVisibility(View.GONE);
        btn_load_more.setVisibility(View.GONE);
    }

    private void hideDialog()
    {
        //progressWheel.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void showLoadMoreButton()
    {
        btn_load_more.setVisibility(View.VISIBLE);
        btn_load_more.setText("Load more");
        progressWheel.setVisibility(View.GONE);
        bad_internet_errorview.setVisibility(View.GONE);
        empty_adapter_errorview.setVisibility(View.GONE);
    }

    private void hideLoadMoreButton()
    {
        btn_load_more.setVisibility(View.GONE);
    }

    private void showErrorView(int type)
    {
        switch (type)
        {
            case 1:
                bad_internet_errorview.setVisibility(View.VISIBLE);
                empty_adapter_errorview.setVisibility(View.GONE);
                progressWheel.setVisibility(View.GONE);
                btn_load_more.setVisibility(View.GONE);
                bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
                    @Override
                    public void onRetry() {
                        getHomeFeed(hashMap.get(SessionManager.KEY_TOKEN));
                    }
                });
                break;
            case 2:
                empty_adapter_errorview.setVisibility(View.VISIBLE);
                bad_internet_errorview.setVisibility(View.GONE);
                progressWheel.setVisibility(View.GONE);
                btn_load_more.setVisibility(View.GONE);
                break;
        }
    }

//    private void getPosts(String token, String page, final boolean dialogPresent)
//    {
////        if(!dialogPresent) {
////            showDialog();
////        }
//        APIinterface service = APIclient.createService(APIinterface.class, token);
//        final Call<Post_Response> call = service.getPosts_general(token, Integer.valueOf(page));
//        //Log.i(TAG, token);
//        //Log.i(TAG, call.request().url().toString());
//        call.enqueue(new Callback<Post_Response>() {
//            @Override
//            public void onResponse(Call<Post_Response> call, Response<Post_Response> response) {
////                if(dialogPresent) {
////                    hideDialog();
////                }
//                //Log.i(TAG, "STATUS_CODE = " + response.code());
//                if(response.code() == Utils.STATUS_200_OK)
//                {
//                    if(response.body().getMsg().equals("Success"))
//                    {
//                        if(response.body().getData().getDocs().size() != 0)
//                        {
//                            //Log.i(TAG, "Array Size = " + String.valueOf(response.body().getData().getDocs().size()));
//                            for(int i = 0; i<response.body().getData().getDocs().size(); i++)
//                            {//docsList.add(response.body().getData().getDocs().get(i));
//                                homeFeedsList.add(new HomeFeed(response.body().getData().getDocs().get(i).getPost().get_id(),response.body().getData().getDocs().get(i).getPost().getImage(),
//                                        response.body().getData().getDocs().get(i).getPost().getCreatedAt(),
//                                        "@" + response.body().getData().getDocs().get(i).getAuthor().getUsername(),
//                                        response.body().getData().getDocs().get(i).getPost().getContent(),
//                                        "NORMAL"));
//                            }
//                            rvListMessage.setAdapter(setmanPostAdapter);
//
////                            if(response.body().getData().getPages_left() != 0)
////                            {
////                                showLoadMoreButton();
////                            }
////                            else
////                            {
////                                hideLoadMoreButton();
////                            }
//                        }
//                        else
//                        {
//                            //showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
//                            //showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
//                        }
//                    }
//                }
//                else {
//                    String errorMessage = Utils.getErrorMessage(response);
//
//                    Log.w(TAG, errorMessage);
//                    Utils.initToast(getApplicationContext(), errorMessage);
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Post_Response> call, Throwable t) {
////                if(dialogPresent){
////                    hideDialog();
////                }
//                //showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
//                call.cancel();
//                Toast.makeText(HomeActivity.this, Html.fromHtml(getString(R.string.reg_error_txt)) ,Toast.LENGTH_SHORT).show();
//
//            }
//        });
//    }


    private void getHomeFeed(final String token)
    {
        //homeFeedsList.clear();
        //rvListMessage.setAdapter(setmanPostAdapter);
        showDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_HomeFeed> call = service.getHomeFeed(token);
        //Log.i(TAG +"homeFeed", token);
        //Log.i(TAG +"homeFeed", call.request().url().toString());
        call.enqueue(new Callback<Response_HomeFeed>() {
            @Override
            public void onResponse(Call<Response_HomeFeed> call, Response<Response_HomeFeed> response) {
                //Log.i(TAG+"homeFeed", "STATUS_CODE = " + response.code());
                //Log.i(TAG, String.valueOf(token.length()));
                hideDialog();
                if(response.code() == Utils.STATUS_200_OK)
                {
                    if(response.body().getMsg().equals("Success"))
                    {
//                        //Log.i(TAG+"homeFeed", String.valueOf(response.body().getData().getSphereOfGraceData().size()));
//                        //Log.i(TAG+"homeFeed", response.body().getData().getSphereOfGraceData().toString());
//                        //Log.i(TAG+"homeFeed", String.valueOf(response.body().getData().getLeverageData().size()));
//                        //Log.i(TAG+"homeFeed", response.body().getData().getLeverageData().toString());
//                        bad_internet_errorview.setVisibility(View.GONE);
//                        empty_adapter_errorview.setVisibility(View.GONE);
//                        progressWheel.setVisibility(View.GONE);
                        if(response.body().getData().getSphereOfGraceData().size() != 0)
                        {
                            homeFeedDBHelper.openW();
                            homeFeedDBHelper.deleteHomeFeed();
                            homeFeedDBHelper.close();
                            homeFeedsList.clear();
                            setmanPostAdapter.notifyDataSetChanged();
                            isDataShown = true;
                            try
                            {
                                for(int i = 0; i<response.body().getData().getSphereOfGraceData().size(); i++)
                                {
                                    try{
                                        //Log.i(TAG + " SetManPost", response.body().getData().getSetManPosts().get(i).getPost().getPublishedAt() +
                                                //" " + response.body().getData().getSetManPosts().get(i).getPost().getTitle());
                                        HomeFeed homeFeed_posts = new HomeFeed(response.body().getData().getSetManPosts().get(i).getPost().get_id(),response.body().getData().getSetManPosts().get(i).getPost().getImage(),
                                                response.body().getData().getSetManPosts().get(i).getPost().getPublishedAt(),
                                                "@" + response.body().getData().getSetManPosts().get(i).getAuthor().getUsername(),
                                                response.body().getData().getSetManPosts().get(i).getPost().getContent(),
                                                "NORMAL");
                                        if(homeFeed_posts.getUrl() == null)
                                        {
                                            homeFeed_posts.setUrl("");
                                        }
                                        homeFeedsList.add(homeFeed_posts);
                                        if(homeFeed_posts != null)
                                        {
                                            homeFeedDBHelper.openW();
                                            homeFeedDBHelper.insertHomeFeed(homeFeed_posts);
                                            homeFeedDBHelper.close();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Log.e(TAG, e.getMessage());
                                    }
                                    try{
                                        //Log.i(TAG + " Leverage", response.body().getData().getLeverageData().get(i).getPublishedAt() +
                                                //" " + response.body().getData().getLeverageData().get(i).getTitle());
                                        if(sessionManager.getUserSettings().isLow_res_enabled()){
                                            HomeFeed homeFeed_leverage = new HomeFeed(response.body().getData().getLeverageData().get(i).getId(),response.body().getData().getLeverageData().get(i).getThumbnails().getShop_single().getUrl(),
                                                    response.body().getData().getLeverageData().get(i).getPublishedAt(),
                                                    response.body().getData().getLeverageData().get(i).getTitle(),
                                                    response.body().getData().getLeverageData().get(i).getContent(),
                                                    response.body().getData().getLeverageData().get(i).getType());
                                            homeFeedsList.add(homeFeed_leverage);
                                            if(homeFeed_leverage != null)
                                            {
                                                homeFeedDBHelper.openW();
                                                homeFeedDBHelper.insertHomeFeed(homeFeed_leverage);
                                                homeFeedDBHelper.close();
                                            }
                                        }else{
                                            HomeFeed homeFeed_leverage = new HomeFeed(response.body().getData().getLeverageData().get(i).getId(),response.body().getData().getLeverageData().get(i).getThumbnails().getFull().getUrl(),
                                                    response.body().getData().getLeverageData().get(i).getPublishedAt(),
                                                    response.body().getData().getLeverageData().get(i).getTitle(),
                                                    response.body().getData().getLeverageData().get(i).getContent(),
                                                    response.body().getData().getLeverageData().get(i).getType());
                                            homeFeedsList.add(homeFeed_leverage);
                                            if(homeFeed_leverage != null)
                                            {
                                                homeFeedDBHelper.openW();
                                                homeFeedDBHelper.insertHomeFeed(homeFeed_leverage);
                                                homeFeedDBHelper.close();
                                            }
                                        }



                                    }
                                    catch (Exception e)
                                    {
                                        Log.e(TAG, e.getMessage());
                                    }
                                    try{
                                        //Log.i(TAG + " Sphere of Grace", response.body().getData().getSphereOfGraceData().get(i).getPublishedAt() +
                                                //" " + response.body().getData().getSphereOfGraceData().get(i).getTitle());
                                        HomeFeed homeFeed_sog = new HomeFeed(response.body().getData().getSphereOfGraceData().get(i).getVideoId(), response.body().getData().getSphereOfGraceData().get(i).getThumbnails().getHigh().getUrl(),
                                                response.body().getData().getSphereOfGraceData().get(i).getPublishedAt(),
                                                response.body().getData().getSphereOfGraceData().get(i).getTitle(),
                                                response.body().getData().getSphereOfGraceData().get(i).getDescription(),
                                                response.body().getData().getSphereOfGraceData().get(i).getType());
                                        homeFeedsList.add(homeFeed_sog);
                                        if(homeFeed_sog != null)
                                        {
                                            homeFeedDBHelper.openW();
                                            homeFeedDBHelper.insertHomeFeed(homeFeed_sog);
                                            homeFeedDBHelper.close();
                                        }
                                    }catch (Exception e){
                                        Log.e(TAG, e.getMessage());
                                    }


                                    //Log.i(TAG + " " + homeFeedsList.get(i).getType(), homeFeedsList.get(i).getDate());
                                }
                            }
                            catch (Exception e)
                            {
                                Log.e(TAG, e.getMessage());
                            }

//                            for(int i = 0; i<response.body().getData().getLeverageData().size(); i++)
//                            {
//                                homeFeedsList.add(new HomeFeed(response.body().getData().getLeverageData().get(i).getId(),response.body().getData().getLeverageData().get(i).getThumbnails().getFull().getUrl(),
//                                        "",
//                                        response.body().getData().getLeverageData().get(i).getTitle(),
//                                        response.body().getData().getLeverageData().get(i).getExcerpt() + "\n" + response.body().getData().getLeverageData().get(i).getContent(),
//                                        response.body().getData().getLeverageData().get(i).getType()));
//                            }

                            Collections.sort(homeFeedsList, new Comparator<HomeFeed>() {
                                @Override
                                public int compare(HomeFeed homeFeed, HomeFeed homeFeed1) {
                                    SimpleDateFormat homefeedDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                    SimpleDateFormat homefeedDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");

                                    Date d = new Date(), d1 = new Date();
                                    try {
                                        d = homefeedDateFormat.parse(homeFeed.getDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        Log.e(TAG, e.getMessage());
                                    }
                                    try {
                                        d1 = homefeedDateFormat1.parse(homeFeed1.getDate());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        Log.e(TAG, e.getMessage());
                                    }

                                    return d1.compareTo(d);
                                }
                            });

                            rvListMessage.setAdapter(setmanPostAdapter);
                        }
                        else
                        {
                            if(!isDataShown)
                            {
                                showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
                            }
                            else
                            {
                                Utils.initToast(HomeActivity.this, "Please reload!!");
                            }
                        }
                    }
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    if(!isDataShown)
                    {
                        showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
                    }
                    else
                    {
                        Utils.initToast(HomeActivity.this, "Some issues with our data center. Please check back later. Thanks");
                    }
                    Log.w(TAG +"homeFeed", errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_HomeFeed> call, Throwable t) {
                hideDialog();
                Log.w(TAG +"homeFeed", "error");
                homeFeedDBHelper.openR();
                if (homeFeedDBHelper.getHomeFeeds().size() > 0) {
                    //Log.i(TAG + " list_DB size", homeFeedsList.size() + "");
                    //Log.i(TAG + " getItemCount", "" + setmanPostAdapter.getItemCount());
                    setmanPostAdapter.notifyDataSetChanged();
                    homeFeedsList.clear();
                    for (int i = 0; i < homeFeedDBHelper.getHomeFeeds().size(); i++) {
                        homeFeedsList.add(homeFeedDBHelper.getHomeFeeds().get(i));
                    }
                    Collections.sort(homeFeedsList, new Comparator<HomeFeed>() {
                        @Override
                        public int compare(HomeFeed homeFeed, HomeFeed homeFeed1) {
                            SimpleDateFormat homefeedDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            SimpleDateFormat homefeedDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");

                            Date d = new Date(), d1 = new Date();
                            try {
                                d = homefeedDateFormat.parse(homeFeed.getDate());
                            } catch (ParseException e) {
                                e.printStackTrace();
                                Log.e(TAG, e.getMessage());
                            }
                            try {
                                d1 = homefeedDateFormat1.parse(homeFeed1.getDate());
                            } catch (ParseException e) {
                                e.printStackTrace();
                                Log.e(TAG, e.getMessage());
                            }

                            return d1.compareTo(d);
                        }
                    });

                    rvListMessage.setAdapter(setmanPostAdapter);
                    isDataShown = true;
                }
                homeFeedDBHelper.close();

                if(!isDataShown)
                {
                    showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
                }
                else
                {
                    Utils.initToast(HomeActivity.this, "Poor internet. Pleas reload!!");
                }
                call.cancel();
                //Toast.makeText(HomeActivity.this, Html.fromHtml(getString(R.string.reg_error_txt)) ,Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(HomeActivity.this).registerReceiver(broadcastReceiver, mIntent);
        try{
            //check for unread message
            DatabaseReference mConvDatabase = FirebaseDatabase.getInstance().getReference().child("Chat").child(hashMap.get(SessionManager.KEY_ID));
            Query conversationQuery = mConvDatabase.orderByChild("timestamp");
            conversationQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Conv conv = dataSnapshot.getValue(Conv.class);
                    if(!conv.isSeen())
                    {
                        unread_message_counter++;
                        //Log.i(TAG, "Unread Message");
                    }
                    initializeCount(unread_message_counter);
                    unread_message_counter = 0;
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

            //get pending friend requests
            FirebaseDatabase.getInstance().getReference().child("pendingFriendRequests").child(hashMap.get(SessionManager.KEY_ID)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try{
                        String friend_req = dataSnapshot.child("pendingFriendRequests").getValue().toString();
                        initializeCount_(Integer.valueOf(friend_req));
                        //Log.i(TAG, friend_req);
                    }catch(Exception e){ Log.e(TAG, e.getMessage());}
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            //send online to firebase
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");

            //check if user is online
            try
            {
                //checks if user is online
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try{
                            String online = dataSnapshot.child("online").getValue().toString();
                            if(online.equals("true")) {
                                status.setVisibility(View.VISIBLE);
                                status.setImageResource(R.color.cpb_green);
                            }
                        }
                        catch (Exception e)
                        {
                            Log.e(TAG, e.getMessage());
                            status.setVisibility(View.VISIBLE);
                            status.setImageResource(R.color.grey_text_);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        status.setVisibility(View.VISIBLE);
                        status.setImageResource(R.color.grey_text_);
                    }
                });
            }catch (Exception e){
                Log.e(TAG, e.getMessage());
            }

        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String val = intent.getStringExtra("fromSettingsActivity");
            String val1 = intent.getStringExtra("fromWritePostActivity");
            try {
                if (val.equals("logout")) {
                    HomeActivity.this.finish();
                    sessionManager.logoutUser();
                }
                if (val1.equals("post_done")){
                    getHomeFeed(hashMap.get(SessionManager.KEY_TOKEN));
                }
            }
            catch (NullPointerException e)
            {
                //Log.i(TAG, e.getMessage());
            }
        }
    };

    private void initializeCount(final int message_count)
    {
        //Gravity property aligns the text
        friend_notif.setGravity(Gravity.CENTER_VERTICAL);
        friend_notif.setTypeface(null, Typeface.BOLD);
        friend_notif.setTextColor(getResources().getColor(R.color.cpb_green_dark));
        final int num_of_friends = Integer.valueOf (hashMap.get(SessionManager.KEY_FRIENDS));
        if(num_of_friends >= 10)
        {
            double d = (((double) num_of_friends) + 5)/10;
            double to_tens = Math.ceil(d) * 10;
            friend_notif.setText(String.valueOf((int)(to_tens-10)) + "+");
            friend_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LovelyInfoDialog lovelyInfoDialog = new LovelyInfoDialog(HomeActivity.this);
                    lovelyInfoDialog.setMessage("Your friends are waiting for you, Lets reach out to them!!!");
                    lovelyInfoDialog.setMessageGravity(Gravity.CENTER);
                    lovelyInfoDialog.setTitle("You have " + String.valueOf(num_of_friends) + " Friends");
                    lovelyInfoDialog.setTopTitleColor(R.color.colorAccent);
                    lovelyInfoDialog.setIcon(R.drawable.ic_action_friend_white);
                    lovelyInfoDialog.setTopColorRes(R.color.xml_centerColor)
                            .setTitleGravity(Gravity.CENTER)
                            .show();
                }
            });
        }
        else if(num_of_friends > 0 && num_of_friends <= 9)
        {
            friend_notif.setText(String.valueOf(num_of_friends));
            friend_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LovelyInfoDialog lovelyInfoDialog = new LovelyInfoDialog(HomeActivity.this);
                    lovelyInfoDialog.setMessage("Lets get more friends!!");
                    lovelyInfoDialog.setMessageGravity(Gravity.CENTER);
                    lovelyInfoDialog.setTitle("You have " + String.valueOf(num_of_friends) + " Friend");
                    lovelyInfoDialog.setTopTitleColor(R.color.colorAccent);
                    lovelyInfoDialog.setIcon(R.drawable.ic_action_friend_white);
                    lovelyInfoDialog.setTopColorRes(R.color.xml_centerColor)
                            .setTitleGravity(Gravity.CENTER)
                            .show();

                }
            });
        }
        else {
            friend_notif.setText(String.valueOf(num_of_friends));
            friend_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LovelyInfoDialog lovelyInfoDialog = new LovelyInfoDialog(HomeActivity.this);
                    lovelyInfoDialog.setMessage("Go get friends!!");
                    lovelyInfoDialog.setMessageGravity(Gravity.CENTER);
                    lovelyInfoDialog.setTitle("You do not have any friend");
                    lovelyInfoDialog.setTopTitleColor(R.color.colorAccent);
                    lovelyInfoDialog.setIcon(R.drawable.ic_action_friend_white);
                    lovelyInfoDialog.setTopColorRes(R.color.xml_centerColor)
                            .setTitleGravity(Gravity.CENTER)
                            .show();

                }
            });
        }



        message_notif.setGravity(Gravity.CENTER_VERTICAL);
        message_notif.setTypeface(null,Typeface.BOLD);
        message_notif.setTextColor(getResources().getColor(R.color.colorAccent));
//        ViewGroup.LayoutParams p = message_notif.getLayoutParams();
//        p.width=ViewGroup.LayoutParams.WRAP_CONTENT;
//        p.height=ViewGroup.LayoutParams.WRAP_CONTENT;
//        message_notif.setLayoutParams(p);
        //message_notif.setHighlightColor(getResources().getColor(R.color.cpb_green_dark));

        //message_notif.setText(String.valueOf(message_count) + "0+");
        if(message_count >= 10)
        {
            double d_ = (((double)message_count) + 5)/10;
            double to_tens_ = Math.ceil(d_) * 10;
            message_notif.setText(String.valueOf((int)(to_tens_-10)) + "+");
            message_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LovelyInfoDialog lovelyInfoDialog = new LovelyInfoDialog(HomeActivity.this);
                    lovelyInfoDialog.setMessage("Some messages are waiting for your response. Common! lets reply");
                    lovelyInfoDialog.setMessageGravity(Gravity.CENTER);
                    lovelyInfoDialog.setTitle("You have " + String.valueOf(message_count) + " pending messages");
                    lovelyInfoDialog.setTopTitleColor(R.color.colorAccent);
                    lovelyInfoDialog.setIcon(R.drawable.ic_info_outline_white_36dp);
                    lovelyInfoDialog.setTopColorRes(R.color.xml_centerColor)
                            .setTitleGravity(Gravity.CENTER)
                            .show();

                }
            });
        }
        else if(message_count > 0 && message_count <= 9)
        {
            message_notif.setText(String.valueOf(message_count));
            message_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LovelyInfoDialog lovelyInfoDialog = new LovelyInfoDialog(HomeActivity.this);
                    lovelyInfoDialog.setMessage("Some messages are waiting for your response. Common! lets reply");
                    lovelyInfoDialog.setMessageGravity(Gravity.CENTER);
                    lovelyInfoDialog.setTitle("You have " + String.valueOf(message_count) + " pending message");
                    lovelyInfoDialog.setTopTitleColor(R.color.colorAccent);
                    lovelyInfoDialog.setIcon(R.drawable.ic_info_outline_white_36dp);
                    lovelyInfoDialog.setTopColorRes(R.color.xml_centerColor)
                            .setTitleGravity(Gravity.CENTER)
                            .show();
                }
            });
        }
        else if(message_count == 0)
        {
            message_notif.setText(String.valueOf(message_count));
            message_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LovelyInfoDialog lovelyInfoDialog = new LovelyInfoDialog(HomeActivity.this);
                    lovelyInfoDialog.setMessage("Go get more friends!!");
                    lovelyInfoDialog.setMessageGravity(Gravity.CENTER);
                    lovelyInfoDialog.setTitle("You do not have any pending message");
                    lovelyInfoDialog.setTopTitleColor(R.color.colorAccent);
                    lovelyInfoDialog.setIcon(R.drawable.ic_info_outline_white_36dp);
                    lovelyInfoDialog.setTopColorRes(R.color.xml_centerColor)
                            .setTitleGravity(Gravity.CENTER)
                            .show();
                }
            });
        }
    }

    private void initializeCount_(final int pendingFriendRequests_count)
    {
        notif_notif.setGravity(Gravity.CENTER_VERTICAL);
        notif_notif.setTypeface(null,Typeface.BOLD);
        notif_notif.setTextColor(getResources().getColor(R.color.dark_blue));
        if(pendingFriendRequests_count >= 10)
        {
            double d__ = (((double)pendingFriendRequests_count) + 5)/10;
            double to_tens__ = Math.ceil(d__) * 10;
            notif_notif.setText(String.valueOf((int)(to_tens__-10)) + "+");
            notif_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LovelyInfoDialog lovelyInfoDialog = new LovelyInfoDialog(HomeActivity.this);
                    lovelyInfoDialog.setMessage("Some pending notifications are waiting for your response.");
                    lovelyInfoDialog.setMessageGravity(Gravity.CENTER);
                    lovelyInfoDialog.setTitle("You have " + String.valueOf(pendingFriendRequests_count) + " pending notifications");
                    lovelyInfoDialog.setTopTitleColor(R.color.colorAccent);
                    lovelyInfoDialog.setIcon(R.drawable.ic_info_outline_white_36dp);
                    lovelyInfoDialog.setTopColorRes(R.color.xml_centerColor)
                            .setTitleGravity(Gravity.CENTER)
                            .show();

                }
            });
        }
        else if(pendingFriendRequests_count > 0 && pendingFriendRequests_count <= 9)
        {
            notif_notif.setText(String.valueOf(pendingFriendRequests_count));
            notif_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LovelyInfoDialog lovelyInfoDialog = new LovelyInfoDialog(HomeActivity.this);
                    lovelyInfoDialog.setMessage("Some pending notifications are waiting for your response.");
                    lovelyInfoDialog.setMessageGravity(Gravity.CENTER);
                    lovelyInfoDialog.setTitle("You have " + String.valueOf(pendingFriendRequests_count) + " pending notification");
                    lovelyInfoDialog.setTopTitleColor(R.color.colorAccent);
                    lovelyInfoDialog.setIcon(R.drawable.ic_info_outline_white_36dp);
                    lovelyInfoDialog.setTopColorRes(R.color.xml_centerColor)
                            .setTitleGravity(Gravity.CENTER)
                            .show();
                }
            });
        }
        else if (pendingFriendRequests_count == 0) {
            notif_notif.setText(String.valueOf(pendingFriendRequests_count));
            notif_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LovelyInfoDialog lovelyInfoDialog = new LovelyInfoDialog(HomeActivity.this);
                    lovelyInfoDialog.setMessage("Go get more friends!!");
                    lovelyInfoDialog.setMessageGravity(Gravity.CENTER);
                    lovelyInfoDialog.setTitle("You do not have any pending notification");
                    lovelyInfoDialog.setTopTitleColor(R.color.colorAccent);
                    lovelyInfoDialog.setIcon(R.drawable.ic_info_outline_white_36dp);
                    lovelyInfoDialog.setTopColorRes(R.color.xml_centerColor)
                            .setTitleGravity(Gravity.CENTER)
                            .show();
                }
            });
        }
    }

    private void showFeedBackDialog()
    {
        lovelyTextInputDialog
                .setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.LEFT | Gravity.TOP)
                .setTitle("Compose Feedback")
                .setHint("e.g. I found a bug in Dashboard")
                .setIcon(R.mipmap.ic_n_white_chat_bubble)
                .setConfirmButton("SEND", new LovelyTextInputDialog.OnTextInputConfirmListener() {
                    @Override
                    public void onTextInputConfirmed(final String text) {
                        hideFeedBackDialog();
                        final FeedBackModel feedBackModel = new FeedBackModel(text, System.currentTimeMillis());
                        final String user_id = hashMap.get(SessionManager.KEY_ID);
                        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                        databaseReference.child(Constants.ARG_FEEDBACKS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Toast.makeText(getApplicationContext(), "Feedback sent successfully. Thank you. " , Toast.LENGTH_SHORT).show();

                                if (dataSnapshot.hasChild(user_id)) {
                                    Log.e(TAG, "sendFeedbackToFirebaseUser: " + user_id + " exists");
                                    databaseReference.child(Constants.ARG_FEEDBACKS).child(user_id).push().setValue(feedBackModel);


                                }else {
                                    Log.e(TAG, "sendFeedbackToFirebaseUser: success");
                                    databaseReference.child(Constants.ARG_FEEDBACKS).child(user_id).push().setValue(feedBackModel);
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Toast.makeText(getApplicationContext(), Html.fromHtml(getString(R.string.reg_error_txt)) , Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                })
                .setNegativeButton(android.R.string.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideFeedBackDialog();
                    }
                })
                .setMessage(R.string.feedBackText)
                .show();
    }

    private void hideFeedBackDialog()
    {
        lovelyTextInputDialog.dismiss();
    }

}
