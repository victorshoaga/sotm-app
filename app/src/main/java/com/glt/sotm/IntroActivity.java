package com.glt.sotm;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

import Utility.SessionManager;

public class IntroActivity extends AppCompatActivity {

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }
    Thread splashTread;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(IntroActivity.this);
        setContentView(R.layout.activity_intro);
        splashPause();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private void splashPause() {

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 1500) {
                        sleep(100);
                        waited += 100;
                    }
                    handOver();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    IntroActivity.this.finish();
                }

            }
        };
        splashTread.start();
    }
    private void handOver() {
        if(sessionManager.isLoggedIn())
        {
            Intent i = new Intent(IntroActivity.this, HomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            finish();
            startActivity(i);
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        }
        else
        {
            Intent i = new Intent(IntroActivity.this, WelcomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            finish();
            startActivity(i);
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
        }
    }

}
