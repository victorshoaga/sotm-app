package com.glt.sotm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

/**
 * Created by inventor on 1/27/18.
 */

public class JobSchedulerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {

        //SOTMFirebaseMessagingService.start(context);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent mIntent = new Intent();
        mIntent.setAction("com.hoanganhtuan95ptit.JobSchedulerReceiver");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, mIntent, 0);

        if (alarmManager == null) return;
        if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 2 * 60, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 2 * 60, pendingIntent);
        }

//        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
//            AlarmManager alarmManger = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
//
//            Intent nAlarmIntent = new Intent(context, NotificationSenderService.class);
//            PendingIntent nAlarmPenginIntent = PendingIntent.getBroadcast(context, 0, nAlarmIntent, 0);
//
//            alarmManger.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
//                    AlarmManager.INTERVAL_FIFTEEN_MINUTES,
//                    AlarmManager.INTERVAL_FIFTEEN_MINUTES, nAlarmPenginIntent);
//        }
    }
}