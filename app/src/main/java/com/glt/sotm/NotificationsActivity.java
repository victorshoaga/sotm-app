package com.glt.sotm;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Utility.CustomViewPager;
import Utility.SessionManager;
import Utility.Utils;

public class NotificationsActivity extends AppCompatActivity {

    private static final String TAG = NotificationsActivity.class.getSimpleName();

    private TabLayout tabLayout;
    private CustomViewPager viewPager;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        sessionManager = new SessionManager(this);
        hashMap = new HashMap<>();
        hashMap = sessionManager.getUserDetails();
        Toolbar toolbar = (Toolbar) findViewById(R.id.notification_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

            }
        });

        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.notification_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FriendsRequestsFragment(), "Friends Request");
        adapter.addFragment(new GroupEventsFragment(), "Group Events");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }
}