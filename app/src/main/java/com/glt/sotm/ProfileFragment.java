package com.glt.sotm;


import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;
import java.util.HashMap;

import Models.Response_User;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import Utility.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends android.app.Fragment{

    private static final String TAG = ProfileFragment.class.getSimpleName();

    CircleImageView img_profilepic, status;
    TextView txt_ProfileName, txt_ProfileFollowers;
    EditText profile_edt_uname, profile_edt_email, profile_edt_location, profile_edt_gender;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    ImageView img_backButton;
    ImageButton btn_edit;
    ProgressWheel progressWheel;
    ActivityOptions options;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        sessionManager = new SessionManager(getActivity());
        hashMap = sessionManager.getUserDetails();

        img_profilepic = (CircleImageView) view.findViewById(R.id.civProfilePic);
        status = (CircleImageView) view.findViewById(R.id.status);
        txt_ProfileName = (TextView) view.findViewById(R.id.txt_ProfileName);
        txt_ProfileFollowers = (TextView) view.findViewById(R.id.txt_ProfileFollowers);
        profile_edt_uname = (EditText) view.findViewById(R.id.profile_edt_uname);
        profile_edt_email = (EditText) view.findViewById(R.id.profile_edt_mail);
        profile_edt_location = (EditText) view.findViewById(R.id.profile_edt_location);
        profile_edt_gender = (EditText) view.findViewById(R.id.profile_edt_gender);
        img_backButton = (ImageView) view.findViewById(R.id.img_backButton);
        btn_edit = (ImageButton) view.findViewById(R.id.button_edit);
        progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);

        img_backButton.setImageDrawable(Utils.tintMyDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_back_white_36dp), Color.WHITE));
        img_backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UpdateProfileActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            }
        });
        txt_ProfileName.setText(hashMap.get(SessionManager.KEY_NAME));
        txt_ProfileFollowers.setText(hashMap.get(SessionManager.KEY_FRIENDS) + " Friend(s)");
        txt_ProfileFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FriendsActivity.class);
                startActivity(intent);
            }
        });
        Utils.disableEditText(profile_edt_uname);
        Utils.disableEditText(profile_edt_email);
        Utils.disableEditText(profile_edt_location);
        Utils.disableEditText(profile_edt_gender);

        profile_edt_uname.setText("@"+hashMap.get(SessionManager.KEY_USERNAME));
        profile_edt_email.setText(hashMap.get(SessionManager.KEY_EMAIL));
        profile_edt_location.setText(hashMap.get(SessionManager.KEY_COUNTRY) + " (" + hashMap.get(SessionManager.KEY_CITY) + ")");
        profile_edt_gender.setText(hashMap.get(SessionManager.KEY_GENDER));
        Picasso.with(getActivity())
                .load(hashMap.get(SessionManager.KEY_PICTURE))
                .placeholder(R.drawable.head_photo)
                .into(img_profilepic);

        img_profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri picUri = Uri.parse(hashMap.get(SessionManager.KEY_PICTURE));
                Log.i(TAG, String.valueOf(picUri));
                //ImageResultActivity.startWithUri(getActivity(), picUri, hashMap.get(SessionManager.KEY_NAME), getActivity());
                Intent intent = new Intent(getActivity(), ImageResultActivity.class);

                intent.setData(picUri);
                intent.putExtra("name", hashMap.get(SessionManager.KEY_NAME));
//                Pair[] pairs = new Pair[1];
//                pairs[0] = new Pair<View, String>(img_profilepic, "imgTransition");
//
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                    options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), pairs);
//                }
                //startActivity(intent, options.toBundle());
                startActivity(intent);
            }
        });

        getProfile(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID));

        return view;
    }

    private void showDialog()
    {
        progressWheel.setVisibility(View.VISIBLE);
    }

    private void hideDialog()
    {
        progressWheel.setVisibility(View.GONE);
    }

    private void getProfile(String token, String user_id)
    {
        showDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_User> call = service.getUserProfile(token, user_id);
        call.enqueue(new Callback<Response_User>() {
            @Override
            public void onResponse(Call<Response_User> call,
                                   Response<Response_User> response) {
                hideDialog();
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK) {
                    sessionManager.updateData(
                            response.body().getData().getUser().getName(),
                            response.body().getData().getUser().getUsername(),
                            response.body().getData().getUser().getEmail(),
                            response.body().getData().getUser().getPicture(), //pictureURL
                            response.body().getData().getUser().isProfileComplete(),
                            response.body().getData().getUser().isProfilePublic(),
                            response.body().getData().getUser().isConfirmed(),
                            response.body().getData().getUser().getCountry(),
                            response.body().getData().getUser().getCity(),
                            response.body().getData().getUser().getPhoneNumber(),
                            response.body().getData().getUser().getGender(),
                            response.body().getData().getUser().getRole(),
                            response.body().getData().getUser().getDob(),
                            response.body().getData().getUser().isAPartner(),
                            response.body().getData().getUser().getFriends());
                    txt_ProfileName.setText(response.body().getData().getUser().getName());
                    txt_ProfileFollowers.setText(String.valueOf(response.body().getData().getUser().getFriends()) + " Friend(s)");
                    profile_edt_uname.setText("@" + response.body().getData().getUser().getUsername());
                    profile_edt_email.setText(response.body().getData().getUser().getEmail());
                    profile_edt_location.setText(response.body().getData().getUser().getCountry() + " (" + response.body().getData().getUser().getCity() + ")");
                    profile_edt_gender.setText(response.body().getData().getUser().getGender());
                    Picasso.with(getActivity())
                            .load(response.body().getData().getUser().getPicture())
                            .placeholder(R.drawable.head_photo)
                            .into(img_profilepic);
                    try
                    {
                        //checks if user is online
                        FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                try{
                                    String online = dataSnapshot.child("online").getValue().toString();
                                    if(online.equals("true")) {
                                        status.setVisibility(View.VISIBLE);
                                        status.setImageResource(R.color.cpb_green);
                                    }
                                }
                                catch (Exception e)
                                {
                                    Log.e(TAG, e.getMessage());
                                    status.setVisibility(View.VISIBLE);
                                    status.setImageResource(R.color.grey_text_);
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                status.setVisibility(View.VISIBLE);
                                status.setImageResource(R.color.grey_text_);
                            }
                        });
                    }catch (Exception e){
                        Log.e(TAG, e.getMessage());
                    }
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getActivity(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_User> call, Throwable t) {
                hideDialog();
                call.cancel();
                if(isAdded()) {
                    Toast.makeText(getActivity(), "Profile may not be up-to-date", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
