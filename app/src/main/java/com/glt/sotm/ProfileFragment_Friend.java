package com.glt.sotm;

import android.app.ActivityOptions;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.CircularProgressButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import Models.Response_User;
import Models.User;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import Utility.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by inventor on 7/7/17.
 */
public class ProfileFragment_Friend extends Fragment {

    private static final String TAG = ProfileFragment_Friend.class.getSimpleName();

    CircleImageView img_profilepic, status;
    RelativeLayout relativeLayout;
    TextView txt_ProfileName, txt_ProfileFollowers, txt_last_seen;
    EditText profile_edt_uname, profile_edt_email, profile_edt_location, profile_edt_gender, profile_edt_partner;

    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    ImageView img_backButton;
    TextView btn_chat;
    ProgressWheel progressWheel;
    String friend_userID = "";
    User user, self;
    ProgressDialog progressDialog;
    ActivityOptions options;
    boolean bool_showChatRoom = false;
    boolean isFriend = false;
    String name_f = "", picture_f = "";

    public ProfileFragment_Friend() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_friend, container, false);
        sessionManager = new SessionManager(getActivity());
        hashMap = sessionManager.getUserDetails();
        progressDialog = new ProgressDialog(getActivity());
        try
        {
            if(getActivity().getIntent().getStringExtra("message").equals("from_friends"))
            {
                friend_userID = getActivity().getIntent().getStringExtra("friend_id");
                Log.i(TAG, "friend_userID = " + friend_userID);
                if(getActivity().getIntent().getStringExtra("is_friend").equals("yes"))
                {
                    isFriend = true;
                }
                else if(getActivity().getIntent().getStringExtra("is_friend").equals("no"))
                {
                    isFriend = false;
                }
                name_f = getActivity().getIntent().getStringExtra("name");
                picture_f = getActivity().getIntent().getStringExtra("picture");
            }
            else if(getActivity().getIntent().getStringExtra("message").equals("from_map"))
            {
                friend_userID = getActivity().getIntent().getStringExtra("friend_userID");
                Log.i(TAG, "friend_userID = " + friend_userID);
            }
        }
        catch (NullPointerException e)
        {
            Log.i(TAG, e.getMessage());
        }

        bindViews(view);
        getUserProfile(hashMap.get(SessionManager.KEY_TOKEN), friend_userID);
        return view;
    }

    private void bindViews(View view) {
        txt_last_seen = (TextView) view.findViewById(R.id.txt_last_seen);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout);
        img_profilepic = (CircleImageView) view.findViewById(R.id.civProfilePic);
        status = (CircleImageView) view.findViewById(R.id.status);
        txt_ProfileName = (TextView) view.findViewById(R.id.txt_ProfileName);
        txt_ProfileFollowers = (TextView) view.findViewById(R.id.txt_ProfileFollowers);
        txt_ProfileFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FriendsFriendActivity.class);
                intent.putExtra("user_id", friend_userID);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().finish();
                getActivity().startActivity(intent);
            }
        });
        profile_edt_uname = (EditText) view.findViewById(R.id.profile_edt_uname);
        profile_edt_email = (EditText) view.findViewById(R.id.profile_edt_mail);
        profile_edt_location = (EditText) view.findViewById(R.id.profile_edt_location);
        profile_edt_gender = (EditText) view.findViewById(R.id.profile_edt_gender);
        profile_edt_partner = (EditText) view.findViewById(R.id.profile_edt_partner);
        Utils.disableEditText(profile_edt_uname);
        Utils.disableEditText(profile_edt_email);
        Utils.disableEditText(profile_edt_location);
        Utils.disableEditText(profile_edt_gender);
        Utils.disableEditText(profile_edt_partner);
        img_backButton = (ImageView) view.findViewById(R.id.img_backButton_friend);

        //img_profile_photo = (ImageView) view.findViewById(R.id.civProfilePic);
        progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel_friend);
        btn_chat= (TextView) view.findViewById(R.id.btn_chat);
        img_backButton.setImageDrawable(Utils.tintMyDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_back_white_36dp), Color.WHITE));
        img_backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getActivity().onBackPressed();
                getActivity().finish();
            }
        });

        btn_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, user.toString());
                getUserProfile_(hashMap.get(SessionManager.KEY_TOKEN),
                        friend_userID);

            }
        });
        img_profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri picUri = Uri.parse(picture_f);
                Log.i(TAG, String.valueOf(picUri));
                //ImageResultActivity.startWithUri(getActivity(), picUri, hashMap.get(SessionManager.KEY_NAME), getActivity());
                Intent intent = new Intent(getActivity(), ImageResultActivity.class);

                intent.setData(picUri);
                intent.putExtra("name", name_f);
//                Pair[] pairs = new Pair[1];
//                pairs[0] = new Pair<View, String>(img_profilepic, "imgTransition");
//
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                    options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), pairs);
//                }

                //getActivity().startActivity(intent, options.toBundle());
                getActivity().startActivity(intent);
            }
        });
    }
    private void showDialog()
    {
        status.setVisibility(View.GONE);
        relativeLayout.setVisibility(View.INVISIBLE);
        img_profilepic.setVisibility(View.GONE);
        txt_ProfileName.setVisibility(View.GONE);
        txt_ProfileFollowers.setVisibility(View.GONE);
        btn_chat.setVisibility(View.GONE);
        profile_edt_uname.setVisibility(View.GONE);
        profile_edt_email.setVisibility(View.GONE);
        profile_edt_location.setVisibility(View.GONE);
        profile_edt_gender.setVisibility(View.GONE);
        profile_edt_partner.setVisibility(View.GONE);

        //img_profile_photo.setVisibility(View.GONE);
        progressWheel.setVisibility(View.VISIBLE);
    }

    private void hideDialog(boolean isFriend)
    {
        status.setVisibility(View.VISIBLE);
        relativeLayout.setVisibility(View.VISIBLE);
        img_profilepic.setVisibility(View.VISIBLE);
        txt_ProfileName.setVisibility(View.VISIBLE);
        txt_ProfileFollowers.setVisibility(View.VISIBLE);
        profile_edt_uname.setVisibility(View.VISIBLE);
        profile_edt_email.setVisibility(View.VISIBLE);
        profile_edt_location.setVisibility(View.VISIBLE);
        profile_edt_gender.setVisibility(View.VISIBLE);
        profile_edt_partner.setVisibility(View.VISIBLE);

        //img_profile_photo.setVisibility(View.VISIBLE);
        progressWheel.setVisibility(View.GONE);

        if(isFriend)
        {
            btn_chat.setVisibility(View.VISIBLE);
        }
        else
        {
            btn_chat.setVisibility(View.GONE);
        }
    }


    private void getUserProfile(final String token, final String profile_id) {
        showDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_User> call = service.getUserProfile(token, profile_id);
        call.enqueue(new Callback<Response_User>() {
            @Override
            public void onResponse(Call<Response_User> call,
                                   Response<Response_User> response) {
                hideDialog(isFriend);
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                Log.i(TAG, response.body().toString());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Log.i(TAG, response.body().toString());
                    user = response.body().getData().getUser();
                    txt_ProfileName.setText(response.body().getData().getUser().getName());
                    txt_ProfileFollowers.setText(String.valueOf(response.body().getData().getUser().getFriends()) + " Friend(s)");
                    profile_edt_uname.setText("@"+response.body().getData().getUser().getUsername());
                    profile_edt_email.setText(response.body().getData().getUser().getEmail());
                    profile_edt_email.setVisibility(View.GONE);
                    profile_edt_location.setText(response.body().getData().getUser().getCountry() + " (" + response.body().getData().getUser().getCity() + ")");
                    profile_edt_gender.setText(response.body().getData().getUser().getGender());
                    profile_edt_partner.setText(maskPartner(String.valueOf(response.body().getData().getUser().isAPartner())));
                    Picasso.with(getActivity())
                            .load(response.body().getData().getUser().getPicture())
                            .placeholder(R.drawable.head_photo)
                            .into(img_profilepic);

                    Log.i(TAG + " getFriendProfile", response.body().toString());
                    Log.i(TAG + " getFriendID", friend_userID.toString());

                    try
                    {
                        //checks if user is online
                        FirebaseDatabase.getInstance().getReference().child("Users").child(friend_userID).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                try{
                                    if(dataSnapshot.hasChild("online"))
                                    {
                                        String online = dataSnapshot.child("online").getValue().toString();
                                        if(online.equals("true")) {
                                            status.setVisibility(View.VISIBLE);
                                            status.setImageResource(R.color.cpb_green);
                                            txt_last_seen.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            txt_last_seen.setVisibility(View.VISIBLE);
                                            GetTimeAgo getTimeAgo = new GetTimeAgo();
                                            long lastTime = Long.parseLong(online);
                                            String lastSeenTime = getTimeAgo.getTimeAgo(lastTime, getActivity());
                                            txt_last_seen.setText(lastSeenTime);
                                        }
                                    }
                                    else
                                    {
                                        txt_last_seen.setVisibility(View.GONE);
                                        status.setVisibility(View.VISIBLE);
                                        status.setImageResource(R.color.grey_text_);
                                    }

                                }
                                catch (Exception e)
                                {
                                    Log.e(TAG, e.getMessage());
                                    txt_last_seen.setVisibility(View.GONE);
                                    status.setVisibility(View.VISIBLE);
                                    status.setImageResource(R.color.grey_text_);
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                txt_last_seen.setVisibility(View.GONE);
                                status.setVisibility(View.VISIBLE);
                                status.setImageResource(R.color.grey_text_);
                            }
                        });
                    }catch (Exception e){
                        Log.e(TAG, e.getMessage());
                    }
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getActivity(), errorMessage);
                    txt_last_seen.setVisibility(View.GONE);
                    status.setVisibility(View.VISIBLE);
                    status.setImageResource(R.color.grey_text_);
                }


            }

            @Override
            public void onFailure(Call<Response_User> call, Throwable t) {
                progressWheel.setVisibility(View.GONE);
                call.cancel();

                if(isAdded())
                {
                    getActivity().finish();
                    Toast.makeText(getActivity(), Html.fromHtml(getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void getUserProfile_(final String token, final String profile_id) {
        showDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_User> call = service.getUserProfile(token, profile_id);
        call.enqueue(new Callback<Response_User>() {
            @Override
            public void onResponse(Call<Response_User> call,
                                   Response<Response_User> response) {
                hideDialog(isFriend);
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                Log.i(TAG, response.body().toString());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Log.i(TAG, response.body().toString());
                    user = response.body().getData().getUser();
                    txt_ProfileName.setText(response.body().getData().getUser().getName());
                    txt_ProfileFollowers.setText(String.valueOf(response.body().getData().getUser().getFriends()) + " Friend(s)");
                    profile_edt_uname.setText("@"+response.body().getData().getUser().getUsername());
                    profile_edt_email.setText(response.body().getData().getUser().getEmail());
                    profile_edt_email.setVisibility(View.GONE);
                    profile_edt_location.setText(response.body().getData().getUser().getCountry() + " (" + response.body().getData().getUser().getCity() + ")");
                    profile_edt_gender.setText(response.body().getData().getUser().getGender());
                    profile_edt_partner.setText(maskPartner(String.valueOf(response.body().getData().getUser().isAPartner())));
                    Picasso.with(getActivity())
                            .load(response.body().getData().getUser().getPicture())
                            .placeholder(R.drawable.head_photo)
                            .into(img_profilepic);

                    Log.i(TAG + " getFriendProfile", response.body().toString());
                    Log.i(TAG + " getFriendID", friend_userID.toString());
                    Intent intent = new Intent(getActivity(), UserFriendChatActivity.class);
                    intent.putExtra("friend_data", user);
                    Log.i(TAG, user.toString());
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(intent);
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getActivity(), errorMessage);
                }


            }

            @Override
            public void onFailure(Call<Response_User> call, Throwable t) {
                progressWheel.setVisibility(View.GONE);
                call.cancel();
                getActivity().finish();
                if(isAdded())
                {
                    Toast.makeText(getActivity(), Html.fromHtml(getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getSelfProfile(final String token, final String user_id) {
        showPDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_User> call = service.getUserProfile(token, user_id);
        call.enqueue(new Callback<Response_User>() {
            @Override
            public void onResponse(Call<Response_User> call,
                                   Response<Response_User> response) {
                hidePDialog();
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Log.i(TAG, response.body().toString());
                    self = response.body().getData().getUser();
                    Log.i(TAG + " Self", self.toString());
                    bool_showChatRoom = true;
                    Log.i(TAG + " getSelfProfile", response.body().toString());
                    Log.i(TAG + " getSelfProfile", self.toString());
                    Intent intent = new Intent(getActivity(), UserFriendChatActivity.class);
                    intent.putExtra("friend_data", user);
                    intent.putExtra("self_data", self);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(intent);
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getActivity(), errorMessage);
                }

            }

            @Override
            public void onFailure(Call<Response_User> call, Throwable t) {
                hidePDialog();
                call.cancel();
                Log.e(TAG, getString(R.string.reg_error_txt));
                bool_showChatRoom = false;
            }
        });

    }

    private void showPDialog()
    {
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void hidePDialog()
    {
        progressDialog.dismiss();
    }

    private String maskPartner(String b)
    {
        String s = "";
        if(b == "true")
        {
            s = "Yes";
        }
        else
        {
            s = "No";
        }
        return s;
    }

}

