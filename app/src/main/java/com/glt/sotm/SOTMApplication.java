package com.glt.sotm;

import android.app.Application;
import android.content.Intent;

/**
 * Created by inventor on 9/22/17.
 */
public class SOTMApplication extends Application {

    private static final String TAG = SOTMApplication.class.getSimpleName();

    private static boolean bool_IsChatActivityOpen = false;

    public static boolean isChatActivityOpen() {
        return bool_IsChatActivityOpen;
    }

    public static void setChatActivityOpen(boolean isChatActivityOpen) {
        SOTMApplication.bool_IsChatActivityOpen = isChatActivityOpen;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Intent intent = new Intent();
        intent.setAction("com.hoanganhtuan95ptit.JobSchedulerReceiver");
        sendBroadcast(intent);
    }


}
