/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.glt.sotm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;
import java.util.HashMap;

import Models.UpdateFCMTokenResponse;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SOTMFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        try{
            SessionManager tradUserSessionManager = new SessionManager(getApplicationContext());
            HashMap<String, String> hashMap = tradUserSessionManager.getUserDetails();
            if(tradUserSessionManager.isLoggedIn()){
                sendRegistrationTokenToServer(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID), refreshedToken);
            }
        }catch (Exception e)
        {
            Log.i(TAG, e.getMessage());
        }
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationTokenToServer(String token, String user_id, String fcmToken) {
        // TODO: Implement this method to send token to your app server.
        new SessionManager(getApplicationContext()).updateToken(fcmToken);
        //For Others aside chats
        APIinterface apIinterface = APIclient.createService(APIinterface.class, token, false);
        final Call<UpdateFCMTokenResponse> call = apIinterface.updateTokenOnServer(token, fcmToken);
        call.enqueue(new Callback<UpdateFCMTokenResponse>() {
            @Override
            public void onResponse(Call<UpdateFCMTokenResponse> call, Response<UpdateFCMTokenResponse> response) {
                if(response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                } else {
                    try {
                        Log.i(TAG, response.errorBody().string() + " " + response.code());
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<UpdateFCMTokenResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "Unable to submit post to Server.");
            }
        });
    }
}
