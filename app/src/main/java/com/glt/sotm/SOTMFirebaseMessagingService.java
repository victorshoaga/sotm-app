/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.glt.sotm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import Utility.Constants;
import Utility.SessionManager;
import event.PushNotificationEvent;

public class SOTMFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

//    public static void start(Context context) {
//        Intent startServiceIntent = new Intent(context, SOTMFirebaseInstanceIDService.class);
//        context.startService(startServiceIntent);
//
//        Intent notificationServiceIntent = new Intent(context, SOTMFirebaseMessagingService.class);
//        context.startService(notificationServiceIntent);
//    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.i(TAG, "From: " + remoteMessage.getFrom());

        try {
            Log.i(TAG, remoteMessage.getData().toString());
            Log.i(TAG, remoteMessage.getNotification().toString());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        if (remoteMessage.getNotification() != null) {
            Log.i(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            if (new SessionManager(getApplicationContext()).getUserSettings().isPushEnabled()) {
                sendConnectNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }
            sendBroadCast(true);
        }

        if (remoteMessage.getData().size() > 0) {
            Log.i(TAG, "Message data payload: " + remoteMessage.getData());

            String title = remoteMessage.getData().get("title");
            String message = remoteMessage.getData().get("text");
            String username = remoteMessage.getData().get("name");
            String user_img = remoteMessage.getData().get("user_img");
            String uid = remoteMessage.getData().get("uid");
            String fcmToken = remoteMessage.getData().get("fcm_token");

            if (!SOTMApplication.isChatActivityOpen()) {

                if (new SessionManager(getApplicationContext()).getUserSettings().isPushEnabled()) {
                    sendChatNotification(title, message, uid, username, user_img, fcmToken);
                }

            } else {
                EventBus.getDefault().post(new PushNotificationEvent(title,
                        message,
                        username,
                        uid,
                        fcmToken));
            }
            chat_sendBroadCast(true);
        }
    }

    private void sendConnectNotification(String messageTitle, String messageBody) {
        Intent intent;
        if (messageTitle.trim().contains("Someone wants to Connect")) {
            intent = new Intent(this, NotificationsActivity.class);
        } else if (messageTitle.trim().contains("New Connection")) {
            intent = new Intent(this, FriendsActivity.class);
        } else {
            intent = new Intent(this, HomeActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("title", messageTitle);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_new_logo)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        if (new SessionManager(getApplicationContext()).getUserSettings().isSoundEnabled()) {
            //sound
//            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            notificationBuilder.setSound(defaultSoundUri);
            playNotificationSound();
        }
        if (!new SessionManager(getApplicationContext()).getUserSettings().isSoundEnabled()) {
            //vibrate
            long[] v = {500, 1000};
            notificationBuilder.setVibrate(v);
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    private void sendChatNotification(String title, String message, String user_id, String user_name, String user_img, String fcm_token) {

        Intent intent = new Intent(this, UserFriendChatActivity.class);
        intent.putExtra("user_id", user_id);
        intent.putExtra("user_name", title);
        intent.putExtra("picture_url", user_img);
        intent.putExtra("fcm_token", fcm_token);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


//        Intent intent = new Intent(this, ChatsActivity.class);
//        intent.putExtra(Constants.ARG_RECEIVER, receiver);
//        intent.putExtra(Constants.ARG_RECEIVER_UID, receiverUid);
//        intent.putExtra(Constants.ARG_FIREBASE_TOKEN, firebaseToken);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_new_logo)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        if (new SessionManager(getApplicationContext()).getUserSettings().isSoundEnabled()) {
            //sound
//            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            notificationBuilder.setSound(defaultSoundUri);
            playNotificationSound();
        }
        if (!new SessionManager(getApplicationContext()).getUserSettings().isSoundEnabled()) {
            //vibrate
            long[] v = {500, 1000};
            notificationBuilder.setVibrate(v);
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }


    private void sendBroadCast(boolean showBadge) {
        Intent intent = new Intent();
        intent.putExtra("showBadge", showBadge);
        intent.setAction("NOW");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void chat_sendBroadCast(boolean showBadge) {
        Intent intent = new Intent();
        intent.putExtra("chat_showBadge", showBadge);
        intent.setAction("NOW");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + getApplicationContext().getPackageName() + "/raw/tone");
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


































































//    private static final String TAG = "MyFirebaseMsgService";
//
//    /**
//     * Called when message is received.
//     *
//     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
//     */
//    // [START receive_message]
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//
//        Log.i(TAG, "From: " + remoteMessage.getFrom());
//
//        try{
//            Log.i(TAG,  remoteMessage.getData().toString());
//            Log.i(TAG,  remoteMessage.getNotification().toString());
//        }catch (Exception e){
//            Log.e(TAG, e.getMessage());
//        }
//
//
//        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
////            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
////            if(new TradUserSessionManager(getApplicationContext()).getUserSettings().isPushEnabled())
////            {
////                sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
////            }
////            sendBroadCast(true);
//            if(remoteMessage.getNotification().getTitle().contains("Someone wants to Connect")){
//                Log.i(TAG, "Message data payload: " + remoteMessage.getData().toString());
//                String title = remoteMessage.getNotification().getTitle();
//                String message = remoteMessage.getNotification().getBody();
//                String uid = remoteMessage.getData().get("id");
//                if(new TradUserSessionManager(getApplicationContext()).getUserSettings().isPushEnabled()) {
//                    sendOtherNotification(title, message, uid);
//                }
//            }
//
//            else if(remoteMessage.getNotification().getTitle().contains("New Connection")){
//                Log.i(TAG, "Message data payload: " + remoteMessage.getData().toString());
//                String title = remoteMessage.getNotification().getTitle();
//                String message = remoteMessage.getNotification().getBody();
//                String uid = remoteMessage.getData().get("id");
//                if(new TradUserSessionManager(getApplicationContext()).getUserSettings().isPushEnabled()) {
//                    sendOtherNotification(title, message, uid);
//                }
//            }
//
//            else if(remoteMessage.getNotification().getTitle().contains("New Check-in Nearby")){
//                Log.i(TAG, "Message data payload: " + remoteMessage.getData().toString());
//                String title = remoteMessage.getNotification().getTitle();
//                String message = remoteMessage.getNotification().getBody();
//                if(new TradUserSessionManager(getApplicationContext()).getUserSettings().isPushEnabled()) {
//                    sendCheckInNotification(title, message);
//                }
//            }
//
//        }
//        else{
//            Log.i(TAG, "Message data payload: " + remoteMessage.getData());
//
//            String title = remoteMessage.getData().get("title");
//            String message = remoteMessage.getData().get("text");
//            String username = remoteMessage.getData().get("username");
//            String uid = remoteMessage.getData().get("uid");
//            String fcmToken = remoteMessage.getData().get("fcm_token");
//
//            // Don't show notification if chat activity is open.
//            if (!PlassApplication.isChatActivityOpen()) {
//
//                if(new TradUserSessionManager(getApplicationContext()).getUserSettings().isPushEnabled())
//                {
//                    sendChatNotification(title, message, username, uid, fcmToken);
//                }
//
//            } else {
//                EventBus.getDefault().post(new PushNotificationEvent(title,
//                        message,
//                        username,
//                        uid,
//                        fcmToken));
//            }
//            //chat_sendBroadCast(true);
//        }
//
//
//
//        // Also if you intend on generating your own notifications as a result of a received FCM
//        // message, here is where that should be initiated. See sendNotification method below.
//    }
//    // [END receive_message]
//
//
//    /**
//     * Create and show a simple notification containing the received FCM message.
//     *
//     * @param messageBody FCM message body received.
//     */
//    private void sendNotification(String messageTitle, String messageBody) {
//        Intent intent = new Intent(this, NetworksActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.putExtra("title", messageTitle);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.logo)
//                .setContentTitle(messageTitle)
//                .setContentText(messageBody)
//                .setAutoCancel(true)
//                .setContentIntent(pendingIntent);
//        if(new TradUserSessionManager(getApplicationContext()).getUserSettings().isSoundEnabled())
//        {
//            //sound
////            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
////            notificationBuilder.setSound(defaultSoundUri);
//            playNotificationSound();
//        }
//        if(!new TradUserSessionManager(getApplicationContext()).getUserSettings().isSoundEnabled())
//        {
//            //vibrate
//            long[] v = {500,1000};
//            notificationBuilder.setVibrate(v);
//        }
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//    }
//
//    private void sendChatNotification(String title, String message, String username, String uid, String firebaseToken)
//    {
//        Intent intent = new Intent(this, ChatsActivity.class);
//        intent.putExtra("action", "from_push");
//        intent.putExtra(Constants.ARG_RECEIVER, username);
//        intent.putExtra(Constants.ARG_RECEIVER_UID, uid);
//        intent.putExtra(Constants.ARG_FIREBASE_TOKEN, firebaseToken);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.logo)
//                .setContentTitle(title)
//                .setContentText(message)
//                .setAutoCancel(true)
//                .setContentIntent(pendingIntent);
//        if(new TradUserSessionManager(getApplicationContext()).getUserSettings().isSoundEnabled())
//        {
//            playNotificationSound();
//        }
//        if(!new TradUserSessionManager(getApplicationContext()).getUserSettings().isSoundEnabled())
//        {
//            long[] v = {500,1000};
//            notificationBuilder.setVibrate(v);
//        }
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(0, notificationBuilder.build());
//    }
//
//    private void sendOtherNotification(String title, String message, String uid)
//    {
//        Intent intent = new Intent(this, ProfileActivity_Friend.class);
//        intent.putExtra("message", "from_nearby");
//        intent.putExtra("friend_id", uid);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.logo)
//                .setContentTitle(title)
//                .setContentText(message)
//                .setAutoCancel(true)
//                .setContentIntent(pendingIntent);
//        if(new TradUserSessionManager(getApplicationContext()).getUserSettings().isSoundEnabled())
//        {
//            playNotificationSound();
//        }
//        if(!new TradUserSessionManager(getApplicationContext()).getUserSettings().isSoundEnabled())
//        {
//            long[] v = {500,1000};
//            notificationBuilder.setVibrate(v);
//        }
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(0, notificationBuilder.build());
//    }
//
//    private void sendCheckInNotification(String title, String message)
//    {
//        Intent intent = new Intent(this, NotificationsActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.logo)
//                .setContentTitle(title)
//                .setContentText(message)
//                .setAutoCancel(true)
//                .setContentIntent(pendingIntent);
//        if(new TradUserSessionManager(getApplicationContext()).getUserSettings().isSoundEnabled())
//        {
//            playNotificationSound();
//        }
//        if(!new TradUserSessionManager(getApplicationContext()).getUserSettings().isSoundEnabled())
//        {
//            long[] v = {500,1000};
//            notificationBuilder.setVibrate(v);
//        }
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(0, notificationBuilder.build());
//    }
//
//    private void sendBroadCast(boolean showBadge)
//    {
//        Intent intent = new Intent();
//        intent.putExtra("showBadge", showBadge);
//        intent.setAction("NOW");
//        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
//    }
//
//    private void chat_sendBroadCast(boolean showBadge)
//    {
//        Intent intent = new Intent();
//        intent.putExtra("chat_showBadge", showBadge);
//        intent.setAction("NOW");
//        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
//    }
//
//    public void playNotificationSound() {
//        try {
//            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                    + "://" + getApplicationContext().getPackageName() + "/raw/plucky");
//            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
//            r.play();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
