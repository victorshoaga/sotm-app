package com.glt.sotm;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapters.SelectFriendsAdapter;
import Models.MyFriendsDocs;
import Models.Response_AddGroupMember;
import Models.Response_MyConnects;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.EndlessRecyclerViewScrollListener;
import Utility.NewSimpleDividerItemDecoration;
import Utility.SessionManager;
import Utility.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

public class SelectFriendsActivity extends AppCompatActivity {

    private static final String TAG = SelectFriendsActivity.class.getSimpleName();

    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
    private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;
    private int PAGE_NUMBER = 1;

    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    RecyclerView rvListMessage;
    LinearLayoutManager mLinearLayoutManager;
    ProgressWheel progressWheel;
    //Button btn_load_more;
    ErrorView bad_internet_errorview, empty_adapter_errorview;

    ArrayList<MyFriendsDocs> docsList, selectedDocsList;
    SelectFriendsAdapter myConnectsAdapter;
    EndlessRecyclerViewScrollListener scrollListener;
    boolean loadingComplete = false;
    boolean isDataShown = false;
    LovelyStandardDialog lovelyStandardDialog;
    LovelyStandardDialog lovelyStandardDialog1;
    LovelyProgressDialog lovelyProgressDialog;
    private String GROUP_ID = "";
    boolean isSuccessful = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_friends);
        lovelyStandardDialog = new LovelyStandardDialog(this);
        lovelyStandardDialog1 = new LovelyStandardDialog(this);
        sessionManager = new SessionManager(this);
        hashMap = sessionManager.getUserDetails();
        docsList = new ArrayList<MyFriendsDocs>();
        selectedDocsList = new ArrayList<MyFriendsDocs>();
        lovelyProgressDialog = new LovelyProgressDialog(this);
        GROUP_ID = getIntent().getStringExtra("group_id");

        lovelyStandardDialog.setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.LEFT | Gravity.TOP)
                .setTitle("Select Group Members")
                .setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //do nothing :: do nothing
                    }
                })
                .setIcon(R.drawable.ic_groups_white)
                .setMessage("Please go ahead and select the friends you want to add to this group")
                .show();

        Toolbar toolbar = (Toolbar) findViewById(R.id.select_friends_toolbar);
        TextView toolbar_textView = (TextView) findViewById(R.id.toolbar_textView);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        //toolbar.setTitle(getIntent().getStringExtra("group_topic"));
        toolbar_textView.setText(getIntent().getStringExtra("group_topic"));
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
            }
        });
        bindViews();
        docsList.clear();
        scrollListener.resetState();

        getUserFriends(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID),
                String.valueOf(PAGE_NUMBER), true);

    }

    private void bindViews(){
        rvListMessage = (RecyclerView)findViewById(R.id.recycler_view_select_friends);
        progressWheel = (ProgressWheel) findViewById(R.id.select_friends_progress_wheel);
        bad_internet_errorview = (ErrorView) findViewById(R.id.select_friends_poor_internet_error_view);
        empty_adapter_errorview = (ErrorView) findViewById(R.id.select_friends_empty_error_view);
        //btn_load_more = (Button) findViewById(R.id.btn_friends_load_more);
        mLinearLayoutManager = new LinearLayoutManager(this);
        rvListMessage.setHasFixedSize(true);
        rvListMessage.setLayoutManager(mLinearLayoutManager);

        rvListMessage.addItemDecoration(new NewSimpleDividerItemDecoration(
                getApplicationContext(), NewSimpleDividerItemDecoration.DEFAULT_PADDING_LEFT
        ));
        //rvListMessage.setItemAnimator(new DefaultItemAnimator());
        scrollListener = new EndlessRecyclerViewScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //loadNextDataFromApi(page);
                if(!loadingComplete)
                {
                    //swipeRefreshLayout.setRefreshing(true);
                    Utils.initToast(SelectFriendsActivity.this, "Please wait, loading...", true);
                    PAGE_NUMBER++;
                    getUserFriends(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID),
                            String.valueOf(PAGE_NUMBER), false);
                }
            }
        };
        // Adds the scroll listener to RecyclerView
        rvListMessage.addOnScrollListener(scrollListener);
        myConnectsAdapter = new SelectFriendsAdapter(SelectFriendsActivity.this, SelectFriendsActivity.this, docsList, hashMap.get(SessionManager.KEY_TOKEN),
                hashMap.get(SessionManager.KEY_ID), selectedDocsList);
    }

    private void showDialog()
    {
        progressWheel.setVisibility(View.VISIBLE);
        bad_internet_errorview.setVisibility(View.GONE);
        empty_adapter_errorview.setVisibility(View.GONE);
        //btn_load_more.setVisibility(View.GONE);
    }

    private void hideDialog()
    {
        progressWheel.setVisibility(View.GONE);
    }

    private void showDialog_()
    {
        lovelyProgressDialog.setIcon(R.drawable.ic_cast_connected_white_36dp)
                .setTitle("Please wait, adding group members...")
                .setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .show();
    }

    private void hideDialog_()
    {
        lovelyProgressDialog.dismiss();
    }

    private void showErrorView(int type)
    {
        switch (type)
        {
            case 1:
                if(!isDataShown){
                    bad_internet_errorview.setVisibility(View.VISIBLE);
                    empty_adapter_errorview.setVisibility(View.GONE);
                    progressWheel.setVisibility(View.GONE);
                    //btn_load_more.setVisibility(View.GONE);
                    bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
                        @Override
                        public void onRetry() {
                            docsList.clear();
                            scrollListener.resetState();
                            getUserFriends(hashMap.get(SessionManager.KEY_TOKEN),
                                    hashMap.get(SessionManager.KEY_ID),
                                    String.valueOf(PAGE_NUMBER),true);
                        }
                    });
                }
                else{
                    Utils.initToast(SelectFriendsActivity.this, "Please check your internet connection");
                }

                break;
            case 2:
                if(!isDataShown)
                {
                    empty_adapter_errorview.setVisibility(View.VISIBLE);
                    bad_internet_errorview.setVisibility(View.GONE);
                    progressWheel.setVisibility(View.GONE);
                    empty_adapter_errorview.setOnRetryListener(new ErrorView.RetryListener() {
                        @Override
                        public void onRetry() {
                            //View Suggested Groups Here
                        }
                    });
                }
                else
                {
                    Utils.initToast(SelectFriendsActivity.this, "Please check your internet connection");
                }
                //btn_load_more.setVisibility(View.GONE);
                break;
        }
    }

    private void getUserFriends(String token, String user_id, String page, final boolean dialogPresent)
    {
        if(dialogPresent) {
            showDialog();
        }
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_MyConnects> call = service.getUserFriends(token, user_id, page);
        Log.i(TAG, call.request().url().toString());
        Log.i(TAG + "Token", token);
        call.enqueue(new Callback<Response_MyConnects>() {
            @Override
            public void onResponse(Call<Response_MyConnects> call, Response<Response_MyConnects> response) {
                if(dialogPresent) {
                    hideDialog();
                }
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    if(response.body().getMsg().equals("Success"))
                    {
                        if(response.body().getData().getDocs().size() != 0)
                        {
                            isDataShown = true;
                            docsList.clear();
                            myConnectsAdapter.notifyDataSetChanged();
                            Log.i(TAG, "Array Size = " + String.valueOf(response.body().getData().getDocs().size()));
                            for(int i = 0; i<response.body().getData().getDocs().size(); i++)
                            {
                                docsList.add(new MyFriendsDocs(true, response.body().getData().getDocs().get(i).getUser()));
                            }
                            rvListMessage.setAdapter(myConnectsAdapter);
                            if(response.body().getData().getPages_left() != 0)
                            {
                                loadingComplete = false;
                            }
                            else
                            {
                                loadingComplete = true;
                            }
                        }
                        else
                        {
                            if(docsList.isEmpty())
                            {
                                showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
                            }
                        }
                    }
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_MyConnects> call, Throwable t) {
                if(dialogPresent){
                    hideDialog();
                }
                showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
                call.cancel();
                Log.e(TAG, t.getMessage());
                Toast.makeText(getApplicationContext(), Html.fromHtml(getString(R.string.reg_error_txt)) , Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }

    private void addGroupMember(String token, String user_id, String group_id)
    {
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_AddGroupMember> call = service.addGroupMember(token, group_id, user_id);
        Log.i(TAG, call.request().url().toString());
        Log.i(TAG + "Token", token);
        call.enqueue(new Callback<Response_AddGroupMember>() {
            @Override
            public void onResponse(Call<Response_AddGroupMember> call, Response<Response_AddGroupMember> response) {
                hideDialog_();
                Log.i(TAG, response.code() + "");
                isSuccessful = true;
            }

            @Override
            public void onFailure(Call<Response_AddGroupMember> call, Throwable t) {
                hideDialog_();
                showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
                isSuccessful = false;
                call.cancel();
                Utils.initToast(getApplicationContext(), "Sorry, you can try again!");
                Log.i(TAG, t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_select, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_done) {
            for(int i = 0; i < selectedDocsList.size(); i++){
                showDialog_();
                Log.i(TAG, selectedDocsList.get(i).toString());
                addGroupMember(hashMap.get(SessionManager.KEY_TOKEN),
                        selectedDocsList.get(i).getUser().getId(),
                        GROUP_ID);
            }
            //if success
            //show success dialog
            if(isSuccessful){
                lovelyStandardDialog1.setTopColorRes(R.color.xml_startColor)
                        .setTitleGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP)
                        .setTitle("Members added successfully")
                        .setPositiveButton("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                lovelyStandardDialog1.dismiss();
                                Intent intent = new Intent(SelectFriendsActivity.this, GroupActivity.class);
                                finish();
                                startActivity(intent);
                                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                            }
                        })
                        .setIcon(R.drawable.ic_action_check)
                        .setMessage("Group has been successfully created. You can go ahead and commune with your colleagues in the newly created group")
                        .show();
            }
            else{
                Utils.initToast(SelectFriendsActivity.this, "Error while adding members to group. Please try again", true);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
