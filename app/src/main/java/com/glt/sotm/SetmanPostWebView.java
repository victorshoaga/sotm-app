package com.glt.sotm;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.percolate.caffeine.ViewUtils;
import com.percolate.mentions.Mentionable;
import com.percolate.mentions.Mentions;
import com.percolate.mentions.QueryListener;
import com.percolate.mentions.SuggestionsListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapters.RecyclerItemClickListener;
import Adapters.UsersAdapter;
import Models.AppMentionable;
import Models.ChatModel;
import Models.CommentModel;
import Models.Mention;
import Models.UserFriend;
import Utility.MentionsLoaderUtils;
import Utility.SessionManager;
import Utility.Utils;
import chat.ChatContract;
import chat.ChatPresenter;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class SetmanPostWebView extends AppCompatActivity implements ChatContract.View, QueryListener, SuggestionsListener {

    private static final String TAG = SetmanPostWebView.class.getSimpleName();
    private WebView mWebView;
    private ImageView imageView;
    String post_id, self_id, self_uname;
    ChatPresenter mChatPresenter;
    EmojiconEditText edit_comment;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    List<AppMentionable> appMentions;
    /**
     * Utility class to load from a JSON file.
     */
    private MentionsLoaderUtils mentionsLoaderUtils;

    /**
     * Mention object provided by library to configure at mentions.
     */
    private Mentions mentions;
    /**
     * Adapter to display suggestions.
     */
    private UsersAdapter usersAdapter;
    HashMap<String, String> hash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setman_post_web_view);
        sessionManager = new SessionManager(this);
        hashMap = new HashMap<>();
        appMentions = new ArrayList<>();
        hashMap = sessionManager.getUserDetails();
        hash = new HashMap<>();
        String title = getIntent().getStringExtra("title");
        String toolbar_title = getIntent().getStringExtra("toolbar_title");
        String htmlString = getIntent().getStringExtra("htmlString");
        final String cover_image = getIntent().getStringExtra("cover_image");
        post_id = getIntent().getStringExtra("post_id");
        self_id = hashMap.get(SessionManager.KEY_ID);
        self_uname = hashMap.get(SessionManager.KEY_USERNAME);

        Toolbar toolbar = (Toolbar) findViewById(R.id.webview_toolbar);
        toolbar.setTitle(toolbar_title);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

            }
        });
        mChatPresenter = new ChatPresenter(this);
        mWebView = (WebView) findViewById(R.id.webview_content);
        imageView = (ImageView) findViewById(R.id.cover_image);
        Button btn_comment = (Button) findViewById(R.id.btn_comment);
        TextView title_text = (TextView) findViewById(R.id.title_text);
        title_text.setText(title);
        edit_comment = (EmojiconEditText)findViewById(R.id.edit_comment);
        ImageView btEmoji = (ImageView) findViewById(R.id.buttonEmoji);
        EmojIconActions emojIcon = new EmojIconActions(getApplicationContext(), findViewById(R.id.root_view),edit_comment,btEmoji);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.happy_smiley);
        mentions = new Mentions.Builder(this, edit_comment)
                .suggestionsListener(this)
                .queryListener(this)
                .build();
        mentionsLoaderUtils = new MentionsLoaderUtils(this);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri picUri = Uri.parse(cover_image);
                //Log.i(TAG, String.valueOf(picUri));
                //ImageResultActivity.startWithUri(getActivity(), picUri, hashMap.get(SessionManager.KEY_NAME), getActivity());
                Intent intent = new Intent(SetmanPostWebView.this, ImageResultActivity.class);

                intent.setData(picUri);
                intent.putExtra("name", "Leverage Devotional");
//                Pair[] pairs = new Pair[1];
//                pairs[0] = new Pair<View, String>(img_profilepic, "imgTransition");
//
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                    options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), pairs);
//                }
                //startActivity(intent, options.toBundle());
                startActivity(intent);
            }
        });

        btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edit_comment.getText().toString().isEmpty())
                {
                    sendComment(edit_comment.getText().toString().trim(),
                            hashMap.get(SessionManager.KEY_USERNAME),
                            hashMap.get(SessionManager.KEY_ID),
                            post_id);
                    edit_comment.setText("");
                }
            }
        });

        if(cover_image == null || cover_image.isEmpty())
        {
            imageView.setVisibility(View.GONE);
        }
        else
        {
            Picasso.with(SetmanPostWebView.this)
                    .load(cover_image)
                    .into(imageView);
        }

        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });
        mWebView.loadData(htmlString, "text/html", null);
        setupMentionsList();

    }

    private void sendComment(String comment, String self_uname, String self_id, String post_id) {

        for(Mentionable mention : mentions.getInsertedMentions()){
            appMentions.add(new AppMentionable(mention.getMentionOffset(), mention.getMentionLength(), mention.getMentionName(), hash.get(mention.getMentionName())));
        }
        CommentModel commentModel = new CommentModel(self_uname, self_id,
                comment, System.currentTimeMillis(), appMentions);
        mChatPresenter.sendComment(getApplicationContext(),
                commentModel,
                self_id, post_id);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.comment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_comment) {
            finish();
            Log.i(TAG, post_id);
            Log.i(TAG, self_id);
            Log.i(TAG, self_uname);
            Intent intent = new Intent(SetmanPostWebView.this, UserCommentActivity.class);
            intent.putExtra("post_id", post_id);
            intent.putExtra("self_id", self_id);
            intent.putExtra("self_uname", self_uname);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSendMessageSuccess() {
        Toast.makeText(SetmanPostWebView.this, "Comment posted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSendMessageFailure(String message) {

    }

    @Override
    public void onGetMessagesSuccess(ChatModel chat) {

    }

    @Override
    public void onGetMessagesFailure(String message) {

    }

    @Override
    public void onGetCommentsSuccess(CommentModel commentModel) {

    }

    @Override
    public void onQueryReceived(String query) {
        final List<UserFriend> users = mentionsLoaderUtils.searchUsers(query);
        if (users != null && !users.isEmpty()) {
            usersAdapter.clear();
            usersAdapter.setCurrentQuery(query);
            usersAdapter.addAll(users);
            showMentionsList(true);
        } else {
            showMentionsList(false);
        }
    }

    @Override
    public void displaySuggestions(boolean display) {
        if (display) {
            ViewUtils.showView(this, R.id.mentions_list_layout);
        } else {
            ViewUtils.hideView(this, R.id.mentions_list_layout);
        }
    }

    /**
     * Private webview client to handle visibility of progress
     */
    private class WebViewClient extends android.webkit.WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }

    private void showMentionsList(boolean display) {
        ViewUtils.showView(this, R.id.mentions_list_layout);
        if (display) {
            ViewUtils.showView(this, R.id.mentions_list);
            ViewUtils.hideView(this, R.id.mentions_empty_view);
        } else {
            ViewUtils.hideView(this, R.id.mentions_list);
            ViewUtils.showView(this, R.id.mentions_empty_view);
        }
    }

    private void setupMentionsList() {
        final RecyclerView mentionsList = (RecyclerView) findViewById(R.id.mentions_list);
        mentionsList.setLayoutManager(new LinearLayoutManager(this));
        usersAdapter = new UsersAdapter(this);
        mentionsList.setAdapter(usersAdapter);

        // set on item click listener
        mentionsList.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(final View view, final int position) {
                final UserFriend user = usersAdapter.getItem(position);
                /*
                 * We are creating a mentions object which implements the
                 * <code>Mentionable</code> interface this allows the library to set the offset
                 * and length of the mention.
                 */
                if (user != null) {
                    final Mention mention = new Mention();
                    mention.setMentionName(user.getName());
                    hash.put(mention.getMentionName(), user.getId());
                    mentions.insertMention(mention);
                }

            }
        }));
    }
}
