package com.glt.sotm;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.suke.widget.SwitchButton;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.util.HashMap;

import Models.FeedBackModel;
import Utility.Constants;
import Utility.SessionManager;
import Utility.Utils;

public class SettingsActivity extends AppCompatActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();

    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    CardView logoutCard, card_linkedin_linking, delete_account_cardview, contact_us_cardview, feedback_cardview, report_block_cardview, view_blocked_cardview;
    SwitchCompat toggle_push_notification, toggle_sound_notification, toggle_high_res, toggle_go_offline;

    private static final String LOGOUT = "logout";

    SessionManager.Settings userSettings;

    LovelyTextInputDialog lovelyTextInputDialog;
    LovelyStandardDialog lovelyStandardDialog;
    boolean continue_ = false;

    //TextView terms_policy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.settings_toolbar);
        setSupportActionBar(toolbar);

        sessionManager = new SessionManager(SettingsActivity.this);
        hashMap = new HashMap<>();
        userSettings = sessionManager.getUserSettings();
        bindViews();
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
            }
        });
    }

    private void bindViews() {
        hashMap = sessionManager.getUserDetails();
        //report_block_cardview = (CardView) findViewById(R.id.report_block_cardview);
        //view_blocked_cardview = (CardView) findViewById(R.id.view_blocked_cardview);
        toggle_push_notification = (SwitchCompat) findViewById(R.id.toggle_push_notification);
        toggle_sound_notification = (SwitchCompat) findViewById(R.id.toggle_sound_notification);
        toggle_high_res = (SwitchCompat) findViewById(R.id.toggle_high_res);
        toggle_go_offline = (SwitchCompat) findViewById(R.id.toggle_go_offline);

        logoutCard = (CardView) findViewById(R.id.logout_card);
        contact_us_cardview = (CardView) findViewById(R.id.contact_us_cardview);
        feedback_cardview = (CardView) findViewById(R.id.feedback_cardview);
        //terms_policy = (TextView) findViewById(R.id.txt_terms_policy);

        lovelyTextInputDialog = new LovelyTextInputDialog(this);
        lovelyStandardDialog = new LovelyStandardDialog(this);

        toggle_push_notification.setChecked(sessionManager.getUserSettings().isPushEnabled());

        toggle_push_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                sessionManager.updatePushSetting(isChecked);
                Log.i(TAG, "" + sessionManager.getUserSettings().isPushEnabled());
            }
        });

        toggle_sound_notification.setChecked(sessionManager.getUserSettings().isSoundEnabled());

        toggle_sound_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                sessionManager.updateSoundSetting(isChecked);
                Log.i(TAG, "" + sessionManager.getUserSettings().isSoundEnabled());
            }
        });

        toggle_high_res.setChecked(sessionManager.getUserSettings().isLow_res_enabled());

        toggle_high_res.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sessionManager.updateLowResSetting(isChecked);
                Log.i(TAG, "" + sessionManager.getUserSettings().isLow_res_enabled());
            }
        });

        toggle_go_offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOfflineDialog();
            }
        });

        logoutCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.verifyConnection(getApplicationContext())){
                    showLogoutDialog();
                }else{
                    Utils.initToast(getApplicationContext(), "Please make sure you are online!");
                }

            }
        });

        feedback_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFeedBackDialog();
            }
        });

        contact_us_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:app@segunobadje.org?subject=" +
                        Uri.encode("User with ID " + hashMap.get(SessionManager.KEY_ID)) + "&amp;body=" +
                        Uri.encode("Please type your message here...")));
                startActivity(Intent.createChooser(intent, "Contact us via"));
            }
        });


//        report_block_cardview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), ReportBlockActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        view_blocked_cardview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), BlockedConnectsActivity.class);
//                startActivity(intent);
//            }
//        });


    }

    private void sendBroadCast(String val)
    {
        Intent intent = new Intent();
        intent.putExtra("fromSettingsActivity", val);
        intent.setAction("NOW");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void showFeedBackDialog()
    {
        lovelyTextInputDialog
                .setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.LEFT | Gravity.TOP)
                .setTitle("Compose Feedback")
                .setHint("e.g. I found a bug in Dashboard")
                .setIcon(R.mipmap.ic_n_white_chat_bubble)
                .setConfirmButton("SEND", new LovelyTextInputDialog.OnTextInputConfirmListener() {
                    @Override
                    public void onTextInputConfirmed(final String text) {
                        hideFeedBackDialog();
                        final FeedBackModel feedBackModel = new FeedBackModel(text, System.currentTimeMillis());
                        final String user_id = hashMap.get(SessionManager.KEY_ID);
                        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                        databaseReference.child(Constants.ARG_FEEDBACKS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Toast.makeText(getApplicationContext(), "Feedback sent successfully. Thank you. " , Toast.LENGTH_SHORT).show();

                                if (dataSnapshot.hasChild(user_id)) {
                                    Log.e(TAG, "sendFeedbackToFirebaseUser: " + user_id + " exists");
                                    databaseReference.child(Constants.ARG_FEEDBACKS).child(user_id).push().setValue(feedBackModel);


                                }else {
                                    Log.e(TAG, "sendFeedbackToFirebaseUser: success");
                                    databaseReference.child(Constants.ARG_FEEDBACKS).child(user_id).push().setValue(feedBackModel);
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Toast.makeText(getApplicationContext(), Html.fromHtml(getString(R.string.reg_error_txt)) , Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                })
                .setNegativeButton(android.R.string.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideFeedBackDialog();
                    }
                })
                .setMessage(R.string.feedBackText)
                .show();
    }

    private void hideFeedBackDialog()
    {
        lovelyTextInputDialog.dismiss();
    }

    private void showLogoutDialog()
    {
        lovelyStandardDialog
                .setIcon(R.mipmap.ic_n_logout)
                .setTitle("Logout")
                .setMessage("By doing this, you will not get any notifications from your friends and will miss updates from Pastor Segun Obadje. Are you sure, you really want to do this?")
                .setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.LEFT | Gravity.TOP)
                .setNegativeButton("CANCEL", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideLogoutDialog();
                    }
                })
                .setPositiveButton("YES", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(Utils.verifyConnection(getApplicationContext())){
                            try{
                                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
                                Utils.initToast(getApplicationContext(), "You are now logged out");
                                finish();
                                sendBroadCast(LOGOUT);
                            }
                            catch (Exception e)
                            {
                                Log.e(TAG, e.getMessage());
                            }
                        }else{
                            Utils.initToast(getApplicationContext(), "Please make sure you are online!");
                        }

                    }
                })
                .show();
    }

    private void hideLogoutDialog()
    {
        lovelyStandardDialog.dismiss();
    }

    private void showOfflineDialog()
    {
        lovelyStandardDialog
                .setIcon(R.drawable.ic_info_outline_white_36dp)
                .setTitle("Offline Mode")
                .setMessage("By doing this, your friends will not see you online but you will still get notifications regardless.")
                .setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.LEFT | Gravity.TOP)
                .setNegativeButton("CANCEL", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideOfflineDialog();
                        sessionManager.updateOfflineSetting(false);
                        toggle_go_offline.setChecked(sessionManager.getUserSettings().isGo_offline_enabled());
                    }
                })
                .setPositiveButton("CONTINUE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Utils.initToast(getApplicationContext(), "You are now in offline mode");
                        toggle_go_offline.setChecked(sessionManager.getUserSettings().isGo_offline_enabled());

                        toggle_go_offline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                sessionManager.updateOfflineSetting(isChecked);
                                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
                                Log.i(TAG, "" + sessionManager.getUserSettings().isGo_offline_enabled());
                            }
                        });
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void hideOfflineDialog()
    {
        lovelyStandardDialog.dismiss();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }

}
