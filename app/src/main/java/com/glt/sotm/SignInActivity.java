package com.glt.sotm;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import Models.Response_SignIn;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.EmailValidator;
import Utility.SessionManager;
import Utility.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    private static final String TAG = SignInActivity.class.getSimpleName();
    TextView signUpTextView;
    Button btn_sign_in;
    EditText edt_mail, edt_pWord;
    TextInputLayout sign_in_input_layout_mail, sign_in_input_layout_pWord;
    EmailValidator emailValidator;
    String email_address, password;
    LovelyProgressDialog lovelyProgressDialog;
    SessionManager sessionManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        mAuth = FirebaseAuth.getInstance();
        sessionManager = new SessionManager(getApplicationContext());
        lovelyProgressDialog = new LovelyProgressDialog(this);
        emailValidator = new EmailValidator();

        signUpTextView = (TextView) findViewById(R.id.txt_sign_in_sign_up_here);
        edt_mail = (EditText) findViewById(R.id.edt_sign_in_input_uname_email);
        edt_pWord = (EditText) findViewById(R.id.edt_sign_in_input_pWord);
        try{
            if(getIntent().getExtras().getString("email_address") != null)
            {
                email_address = getIntent().getExtras().getString("email_address");
                password = getIntent().getExtras().getString("password");
                edt_mail.setText(email_address);
                edt_pWord.setText(password);
            }
        }
        catch (NullPointerException e)
        {
            String err = (e.getMessage()==null)?"Null email address":e.getMessage();
            Log.e(TAG ,err);
        }
        edt_mail.addTextChangedListener(new MyTextWatcher(edt_mail));
        edt_pWord.addTextChangedListener(new MyTextWatcher(edt_pWord));
        sign_in_input_layout_mail = (TextInputLayout) findViewById(R.id.sign_in_input_layout_uname_email);
        sign_in_input_layout_pWord = (TextInputLayout) findViewById(R.id.sign_in_input_layout_pWord);
        btn_sign_in = (Button) findViewById(R.id.btn_sign_in);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + firebaseUser.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validateUsername_Mail())
                {
                    return;
                }
                if(!validatePassword())
                {
                    return;
                }
                log_in_user(edt_mail.getText().toString().trim(), edt_pWord.getText().toString().trim());
            }
        });

        signUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void resetValidators() {
        sign_in_input_layout_mail.setErrorEnabled(false);
        sign_in_input_layout_pWord.setErrorEnabled(false);
    }

    private boolean validatePassword() {
        if (edt_pWord.getText().toString().trim().isEmpty()) {
            //sign_in_input_layout_pWord.setError("Enter valid password");
            Utils.initToast(getApplicationContext(), "Enter valid password");
            requestFocus(sign_in_input_layout_pWord);
            return false;
        }
        else if(edt_pWord.getText().toString().trim().length() < 6)
        {
            //sign_in_input_layout_pWord.setError("6 chars or more");
            Utils.initToast(getApplicationContext(), "Enter valid password");
            requestFocus(sign_in_input_layout_pWord);
            return false;
        }
        else {
            sign_in_input_layout_pWord.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateUsername_Mail() {
        if (edt_mail.getText().toString().trim().isEmpty()) {
            //sign_in_input_layout_mail.setError("Enter valid username or e-mail");
            Utils.initToast(getApplicationContext(), "Enter valid email");
            requestFocus(sign_in_input_layout_mail);
            return false;
        }
        else if(edt_mail.getText().toString().trim().contains("@"))
        {
            if(!emailValidator.validateEmail(edt_mail.getText().toString().trim()))
            {
                //sign_in_input_layout_mail.setError("Enter valid e-mail");
                Utils.initToast(getApplicationContext(), "Enter valid email");
                requestFocus(sign_in_input_layout_mail);
                return false;
            }
        }
//        else if(!edt_mail.getText().toString().trim().matches("^[^\\d\\s]+$"))
//        {
//            sign_in_input_layout_mail.setError("Enter valid user name");
//            requestFocus(sign_in_input_layout_mail);
//            return false;
//        }
        else {
            sign_in_input_layout_mail.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            resetValidators();
        }

        public void afterTextChanged(Editable editable) {
        }
    }

    private void signIn(String token) {
        mAuth.signInWithCustomToken(token)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            hideDialog();
                            Log.d(TAG, "signInWithCustomToken:success");
                        } else {
                            hideDialog();
                            Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                            //Toast.makeText(getApplicationContext(), "Authentication failed.",
                                  //  Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void log_in_user(String email, final String password)
    {
        showDialog();
        APIinterface apIinterface = APIclient.RetrofitBuilder().create(APIinterface.class);
        final Call<Response_SignIn> response_logInCall = apIinterface.logInUser(email, password);
        response_logInCall.enqueue(new Callback<Response_SignIn>() {
            @Override
            public void onResponse(Call<Response_SignIn> call, Response<Response_SignIn> response) {
                hideDialog();
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Utils.initToast(getApplicationContext(), "Welcome...", true);
                    Log.i(TAG, response.body().toString());
                    Log.i(TAG, response.body().getData().getUser().getToken());
                    Log.i(TAG, "***********************************************************************************" + response.body().getData().getUser().getPhoneNumber());
                    sessionManager.createLoginSession(response.body().getData().getUser().getId(),
                            response.body().getData().getUser().getName(),
                            response.body().getData().getUser().getUsername(),
                            response.body().getData().getUser().getEmail(),
                            edt_pWord.getText().toString().trim(),
                            response.body().getData().getUser().getToken(),
                            response.body().getData().getUser().getPicture(), //pictureURL
                            response.body().getData().getUser().isProfileComplete(),
                            response.body().getData().getUser().isProfilePublic(),
                            response.body().getData().getUser().isConfirmed(),
                            response.body().getData().getUser().getCountry(),
                            response.body().getData().getUser().getCity(),
                            response.body().getData().getUser().getPhoneNumber(),
                            response.body().getData().getUser().getGender(),
                            response.body().getData().getUser().getRole(),
                            response.body().getData().getUser().getDob(),
                            response.body().getData().getUser().isAPartner(),
                            response.body().getData().getUser().getFriends());
                    signIn(response.body().getData().getUser().getToken());
                    Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                } else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_SignIn> call, Throwable t) {
                hideDialog();
                response_logInCall.cancel();
                Utils.initToast(getApplicationContext(), "Hey sorry, you can try again!");
                Log.i(TAG, t.getMessage());
            }
        });
    }

    private void showDialog()
    {
        lovelyProgressDialog.setIcon(R.drawable.ic_cast_connected_white_36dp)
                .setTitle("please wait, logging you in...")
                .setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .show();
    }

    private void hideDialog()
    {
        lovelyProgressDialog.dismiss();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
