package com.glt.sotm;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import Models.Response_SignUp;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.EmailValidator;
import Utility.SessionManager;
import Utility.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = SignUpActivity.class.getSimpleName();

    EditText edt_fName, edt_lName, edt_mail, edt_uname, edt_pWord, edt_phoneNumber;
    TextInputLayout sign_up_input_layout_fName, sign_up_input_layout_lName,
            sign_up_input_layout_mail, sign_up_input_layout_uname, sign_up_input_layout_pWord,
            sign_up_input_layout_phoneNumber;
    EmailValidator emailValidator;
    LovelyProgressDialog lovelyProgressDialog;
    TextView termsTextView;
    SessionManager sessionManager;

    Button btn_sign_up;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sessionManager = new SessionManager(SignUpActivity.this);
        emailValidator = new EmailValidator();
        lovelyProgressDialog = new LovelyProgressDialog(this);
        termsTextView = (TextView) findViewById(R.id.txt_terms_and_cond);

        edt_fName = (EditText) findViewById(R.id.edt_sign_up_input_fName);
        edt_lName = (EditText) findViewById(R.id.edt_sign_up_input_lName);
        edt_uname = (EditText) findViewById(R.id.edt_sign_up_input_uname);
        edt_mail = (EditText) findViewById(R.id.edt_sign_up_input_mail);
        edt_pWord = (EditText) findViewById(R.id.edt_sign_up_input_pWord);
        edt_phoneNumber = (EditText) findViewById(R.id.edt_sign_up_input_phoneNumber);

        edt_fName.setTextColor(getResources().getColorStateList(R.color.text_color));
        edt_lName.setTextColor(getResources().getColorStateList(R.color.text_color));
        edt_uname.setTextColor(getResources().getColorStateList(R.color.text_color));
        edt_mail.setTextColor(getResources().getColorStateList(R.color.text_color));
        edt_pWord.setTextColor(getResources().getColorStateList(R.color.text_color));
        edt_phoneNumber.setTextColor(getResources().getColorStateList(R.color.text_color));

        edt_fName.addTextChangedListener(new MyTextWatcher(edt_fName));
        edt_lName.addTextChangedListener(new MyTextWatcher(edt_lName));
        edt_uname.addTextChangedListener(new MyTextWatcher(edt_uname));
        edt_mail.addTextChangedListener(new MyTextWatcher(edt_mail));
        edt_pWord.addTextChangedListener(new MyTextWatcher(edt_pWord));
        edt_phoneNumber.addTextChangedListener(new MyTextWatcher(edt_phoneNumber));

        sign_up_input_layout_fName = (TextInputLayout) findViewById(R.id.sign_up_input_layout_fName);
        sign_up_input_layout_lName = (TextInputLayout) findViewById(R.id.sign_up_input_layout_lName);
        sign_up_input_layout_uname = (TextInputLayout) findViewById(R.id.sign_up_input_layout_uname);
        sign_up_input_layout_mail = (TextInputLayout) findViewById(R.id.sign_up_input_layout_mail);
        sign_up_input_layout_pWord = (TextInputLayout) findViewById(R.id.sign_up_input_layout_pWord);
        sign_up_input_layout_phoneNumber = (TextInputLayout) findViewById(R.id.sign_up_input_layout_phoneNumber);

        btn_sign_up = (Button) findViewById(R.id.btn_sign_up);

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validateFName())
                {
                    return;
                }
                if(!validateLName())
                {
                    return;
                }
                if(!validateUName())
                {
                    return;
                }
                if(!validateMail())
                {
                    return;
                }
                if(!validatePassword())
                {
                    return;
                }
                if(!validatePhoneNumber())
                {
                    return;
                }
                sign_up_user(edt_fName.getText().toString().trim() + " " + edt_lName.getText().toString().trim(),
                        edt_mail.getText().toString().trim(),
                        edt_pWord.getText().toString().trim(),
                        edt_phoneNumber.getText().toString().trim(),
                        edt_uname.getText().toString().trim());
            }
        });

        termsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    private void resetValidators() {
        sign_up_input_layout_fName.setErrorEnabled(false);
        sign_up_input_layout_lName.setErrorEnabled(false);
        sign_up_input_layout_uname.setErrorEnabled(false);
        sign_up_input_layout_mail.setErrorEnabled(false);
        sign_up_input_layout_pWord.setErrorEnabled(false);
        sign_up_input_layout_phoneNumber.setErrorEnabled(false);
    }

    private boolean validatePassword() {
        if (edt_pWord.getText().toString().trim().isEmpty()) {
            //sign_up_input_layout_pWord.setError("Enter valid password");
            Utils.initToast(getApplicationContext(), "Enter valid password");
            requestFocus(sign_up_input_layout_pWord);
            return false;
        }
        else if(edt_pWord.getText().toString().trim().length() < 6)
        {
            //sign_up_input_layout_pWord.setError("6 chars or more");
            Utils.initToast(getApplicationContext(), "Enter valid password : 6 chars or more");
            requestFocus(sign_up_input_layout_pWord);
            return false;
        }
        else {
            sign_up_input_layout_pWord.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateFName() {
        if (edt_fName.getText().toString().trim().isEmpty()) {
            //sign_up_input_layout_fName.setError("Enter valid first name");
            Utils.initToast(getApplicationContext(), "Enter valid first name");
            requestFocus(sign_up_input_layout_fName);
            return false;
        }
        else if(!edt_fName.getText().toString().trim().matches("^[^\\d\\s]+$"))
        {
            Utils.initToast(getApplicationContext(), "Enter valid first name");
            //sign_up_input_layout_fName.setError("Enter valid first name");
            requestFocus(sign_up_input_layout_fName);
            return false;
        }
        else {
            sign_up_input_layout_fName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLName() {
        if (edt_lName.getText().toString().trim().isEmpty()) {
            //sign_up_input_layout_lName.setError("Enter valid last name");
            Utils.initToast(getApplicationContext(), "Enter valid last name");
            requestFocus(sign_up_input_layout_lName);
            return false;
        }
        else if(!edt_lName.getText().toString().trim().matches("^[^\\d\\s]+$"))
        {
            //sign_up_input_layout_lName.setError("Enter valid last name");
            Utils.initToast(getApplicationContext(), "Enter valid last name");
            requestFocus(sign_up_input_layout_lName);
            return false;
        }
        else {
            sign_up_input_layout_lName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateUName() {
        if (edt_uname.getText().toString().trim().isEmpty()) {
            //sign_up_input_layout_uname.setError("Enter valid user name");
            Utils.initToast(getApplicationContext(), "Enter valid user name");
            requestFocus(sign_up_input_layout_uname);
            return false;
        }
        else if(edt_uname.getText().toString().trim().contains(" "))
        {
            //sign_up_input_layout_uname.setError("Enter valid user name");
            Utils.initToast(getApplicationContext(), "Enter valid user name");
            requestFocus(sign_up_input_layout_uname);
            return false;
        }
        else {
            sign_up_input_layout_uname.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePhoneNumber() {
        if (edt_phoneNumber.getText().toString().trim().isEmpty()) {
            //sign_up_input_layout_phoneNumber.setError("Enter valid phone number");
            Utils.initToast(getApplicationContext(), "Enter valid phone number");
            requestFocus(sign_up_input_layout_phoneNumber);
            return false;
        }
        else {
            sign_up_input_layout_phoneNumber.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMail() {
        if (edt_mail.getText().toString().trim().isEmpty()) {
            //sign_up_input_layout_mail.setError("Enter valid e-mail");
            Utils.initToast(getApplicationContext(), "Enter valid e-mail");
            requestFocus(sign_up_input_layout_mail);
            return false;
        }
        else if(!emailValidator.validateEmail(edt_mail.getText().toString().trim()))
        {
            //sign_up_input_layout_mail.setError("Enter valid e-mail");
            Utils.initToast(getApplicationContext(), "Enter valid e-mail");
            requestFocus(sign_up_input_layout_mail);
            return false;
        }
        else {
            sign_up_input_layout_mail.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            resetValidators();
        }

        public void afterTextChanged(Editable editable) {
        }
    }

    private void sign_up_user(String name, String email, final String password, String phoneNumber, String username)
    {
        showDialog();
        APIinterface apIinterface = APIclient.RetrofitBuilder().create(APIinterface.class);
        final Call<Response_SignUp> response_signUpCall = apIinterface.signUpUser(name, username, email, password, phoneNumber);
        response_signUpCall.enqueue(new Callback<Response_SignUp>() {
            @Override
            public void onResponse(Call<Response_SignUp> call, Response<Response_SignUp> response) {
                hideDialog();
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Utils.initToast(getApplicationContext(), "Registration Successful....Please Log in");
                    Log.i(TAG, response.body().getData().getUser().getToken());
                    Log.i(TAG, response.body().toString());
                    Log.i(TAG, "******************************************************************************" + response.body().getData().getUser().getPhoneNumber());
                    //sessionManager.updatePhoneNumber(response.body().getData().getUser().getPhoneNumber());
                    Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                    intent.putExtra("email_address", response.body().getData().getUser().getEmail());
                    intent.putExtra("password", password);
                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                } else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_SignUp> call, Throwable t) {
                hideDialog();
                response_signUpCall.cancel();
                Utils.initToast(getApplicationContext(), "Hey sorry, you can try again!");
                Log.i(TAG, t.getMessage());
            }
        });
    }

    private void showDialog()
    {
        lovelyProgressDialog.setIcon(R.drawable.ic_cast_connected_white_36dp)
                .setTitle("please wait, connecting...")
                .setTopColorRes(R.color.endColor)
                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .show();
    }

    private void hideDialog()
    {
        lovelyProgressDialog.dismiss();
    }
}
