package com.glt.sotm;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageActivity;
import com.theartofdev.edmodo.cropper.CropImageOptions;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.yalantis.ucrop.UCrop;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import Adapters.CountryDialogAdapter;
import Dialog_Fragments.CityDialog;
import Dialog_Fragments.CountryDialog;
import Dialog_Fragments.DobDialog;
import Dialog_Fragments.GenderDialog;
import Dialog_Fragments.PartnerDialog;
import Models.Response_UpdateProfile;
import Models.UpdateProfileBody;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.EmailValidator;
import Utility.SessionManager;
import Utility.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, EasyPermissions.PermissionCallbacks, CountryDialog.SelectCountryListener, CityDialog.SelectCityListener, GenderDialog.SelectGenderListener, PartnerDialog.SelectPartnerListener{

    private static final String TAG = UpdateProfileActivity.class.getSimpleName();
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;

    String country = "", city = "", dob = "", gender = "";
    boolean partner = false;
    ArrayList<String> gender_list, partner_list;

    CircleImageView img_ProfilePic;
    TextView txt_hi;
    EditText edt_fName, edt_mail, edt_uname, edt_pWord, edt_phoneNumber, edt_gender, edt_dob, edt_country, edt_city, edt_partner;
    TextInputLayout sign_up_input_layout_fName,
            sign_up_input_layout_mail, sign_up_input_layout_uname, sign_up_input_layout_pWord,
            sign_up_input_layout_phoneNumber, sign_up_input_layout_gender, sign_up_input_layout_dob,
            sign_up_input_layout_country, sign_up_input_layout_city, sign_up_input_layout_partner;
    EmailValidator emailValidator;
    LovelyProgressDialog lovelyProgressDialog;
    ArrayList<String> country_list, city_list;
    Uri selectedImage;
    boolean isPhotoDone = false;

    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    SimpleDateFormat simpleDateFormat;


    Button btn_choose_country;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        sessionManager = new SessionManager(UpdateProfileActivity.this);
        hashMap = new HashMap<>();
        hashMap = sessionManager.getUserDetails();
        emailValidator = new EmailValidator();
        lovelyProgressDialog = new LovelyProgressDialog(this);
        country_list = new ArrayList<>();
        city_list = new ArrayList<>();
        gender_list = new ArrayList<>();
        partner_list = new ArrayList<>();
        gender_list.add("male");
        gender_list.add("female");
        partner_list.add("Yes");
        partner_list.add("No");

        simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");


        img_ProfilePic = (CircleImageView) findViewById(R.id.img_profilePic);
        txt_hi = (TextView) findViewById(R.id.txt_welcome);
        edt_fName = (EditText) findViewById(R.id.edt_sign_up_input_fName);
        edt_uname = (EditText) findViewById(R.id.edt_sign_up_input_uname);
        edt_mail = (EditText) findViewById(R.id.edt_sign_up_input_mail);
        edt_pWord = (EditText) findViewById(R.id.edt_sign_up_input_pWord);
        edt_phoneNumber = (EditText) findViewById(R.id.edt_sign_up_input_phoneNumber);
        edt_gender = (EditText) findViewById(R.id.edt_sign_up_input_gender);
        edt_dob = (EditText) findViewById(R.id.edt_sign_up_input_dob);
        edt_country = (EditText) findViewById(R.id.edt_sign_up_input_country);
        edt_city = (EditText) findViewById(R.id.edt_sign_up_input_city);
        edt_partner = (EditText) findViewById(R.id.edt_sign_up_input_partner);

        edt_fName.addTextChangedListener(new MyTextWatcher(edt_fName));
        edt_uname.addTextChangedListener(new MyTextWatcher(edt_uname));
        edt_mail.addTextChangedListener(new MyTextWatcher(edt_mail));
        edt_pWord.addTextChangedListener(new MyTextWatcher(edt_pWord));
        edt_phoneNumber.addTextChangedListener(new MyTextWatcher(edt_phoneNumber));
        edt_gender.addTextChangedListener(new MyTextWatcher(edt_gender));
        edt_dob.addTextChangedListener(new MyTextWatcher(edt_dob));
        edt_country.addTextChangedListener(new MyTextWatcher(edt_country));
        edt_city.addTextChangedListener(new MyTextWatcher(edt_city));
        edt_partner.addTextChangedListener(new MyTextWatcher(edt_partner));

        sign_up_input_layout_fName = (TextInputLayout) findViewById(R.id.sign_up_input_layout_fName);
        sign_up_input_layout_uname = (TextInputLayout) findViewById(R.id.sign_up_input_layout_uname);
        sign_up_input_layout_mail = (TextInputLayout) findViewById(R.id.sign_up_input_layout_mail);
        sign_up_input_layout_pWord = (TextInputLayout) findViewById(R.id.sign_up_input_layout_pWord);
        sign_up_input_layout_phoneNumber = (TextInputLayout) findViewById(R.id.sign_up_input_layout_phoneNumber);
        sign_up_input_layout_gender = (TextInputLayout) findViewById(R.id.sign_up_input_layout_gender);
        sign_up_input_layout_dob = (TextInputLayout) findViewById(R.id.sign_up_input_layout_dob);
        sign_up_input_layout_country = (TextInputLayout) findViewById(R.id.sign_up_input_layout_country);
        sign_up_input_layout_city = (TextInputLayout) findViewById(R.id.sign_up_input_layout_city);
        sign_up_input_layout_partner = (TextInputLayout) findViewById(R.id.sign_up_input_layout_partner);

        edt_country.setFocusable(false);
        edt_country.setClickable(true);
        Utils.disableEditText(edt_country);

        edt_city.setFocusable(false);
        edt_city.setClickable(true);
        Utils.disableEditText(edt_city);

        edt_gender.setFocusable(false);
        edt_gender.setClickable(true);
        Utils.disableEditText(edt_gender);

        edt_dob.setFocusable(false);
        edt_dob.setClickable(true);
        Utils.disableEditText(edt_dob);

        edt_partner.setFocusable(false);
        edt_partner.setClickable(true);
        Utils.disableEditText(edt_partner);

        //slot in parameters from shared prefs
        txt_hi.setText("Hi " + hashMap.get(SessionManager.KEY_NAME));
        Picasso.with(UpdateProfileActivity.this)
                .load(hashMap.get(SessionManager.KEY_PICTURE))
                .placeholder(R.drawable.head_photo)
                .into(img_ProfilePic);
        edt_fName.setText(hashMap.get(SessionManager.KEY_NAME));
        edt_uname.setText(hashMap.get(SessionManager.KEY_USERNAME));
        edt_mail.setText(hashMap.get(SessionManager.KEY_EMAIL));
        edt_pWord.setText(hashMap.get(SessionManager.KEY_PASSWORD));
        Log.i(TAG, "******************************************************************************" + hashMap.get(SessionManager.KEY_PHONENUMBER));
        edt_phoneNumber.setText(hashMap.get(SessionManager.KEY_PHONENUMBER), TextView.BufferType.EDITABLE);

        if(sessionManager.isProfileCompete())
        {
            edt_gender.setText(hashMap.get(SessionManager.KEY_GENDER));
            edt_dob.setText(hashMap.get(SessionManager.KEY_DOB).substring(0, 10));
            edt_country.setText(hashMap.get(SessionManager.KEY_COUNTRY));
            edt_city.setText(hashMap.get(SessionManager.KEY_CITY));
            edt_partner.setText(maskPartner(hashMap.get(SessionManager.KEY_ISPARTNER)));
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbarTextView = (TextView) toolbar.findViewById(R.id.toolbar_textView);
        toolbar.setTitle("");
        toolbarTextView.setText("Update Profile");
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
            }
        });

        edt_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_city.setText("");
                GetCountryData getCountryData = new GetCountryData();
                getCountryData.execute("");
            }
        });

        edt_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetCitiesData getCitiesData = new GetCitiesData();
                getCitiesData.execute("");
            }
        });

        edt_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("gender_dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment dialogFragment = GenderDialog.newInstance(gender_list);
                dialogFragment.show(ft, "gender_dialog");
            }
        });

        edt_partner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("partner_dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment dialogFragment = PartnerDialog.newInstance(partner_list);
                dialogFragment.show(ft, "partner_dialog");
            }
        });

        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                Fragment prev = getSupportFragmentManager().findFragmentByTag("dob_dialog");
//                if (prev != null) {
//                    ft.remove(prev);
//                }
//                ft.addToBackStack(null);
//                DialogFragment dialogFragment = DobDialog.newInstance();
//                dialogFragment.show(ft, "dob_dialog");
                showDate(1980, 0, 1, R.style.DatePickerSpinner);
            }
        });

        img_ProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (EasyPermissions.hasPermissions(UpdateProfileActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE))
                {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, REQUEST_GALLERY_CODE);
                }
                else
                {
                    Log.e(TAG, "Permission not yet granted");
                    EasyPermissions.requestPermissions(UpdateProfileActivity.this, getString(R.string.read_file), READ_REQUEST_CODE, android.Manifest.permission.READ_EXTERNAL_STORAGE);
                }
            }
        });
    }

    private void resetValidators() {
        sign_up_input_layout_fName.setErrorEnabled(false);
        sign_up_input_layout_uname.setErrorEnabled(false);
        sign_up_input_layout_mail.setErrorEnabled(false);
        sign_up_input_layout_pWord.setErrorEnabled(false);
        sign_up_input_layout_phoneNumber.setErrorEnabled(false);
        sign_up_input_layout_gender.setErrorEnabled(false);
        sign_up_input_layout_dob.setErrorEnabled(false);
        sign_up_input_layout_country.setErrorEnabled(false);
        sign_up_input_layout_city.setErrorEnabled(false);
        sign_up_input_layout_partner.setErrorEnabled(false);
    }

    @Override
    public void onReturnValue(String country_selected) {
        country = country_selected;
        Log.i(TAG, country_selected);
        edt_country.setText(country_selected);
    }

    @Override
    public void onCityReturnValue(String city_selected) {
        city = city_selected;
        Log.i(TAG, city_selected);
        edt_city.setText(city_selected);
    }

    @Override
    public void onGenderReturnValue(String gender_selected) {
        gender = gender_selected;
        Log.i(TAG, gender_selected);
        edt_gender.setText(gender_selected);
    }

    @Override
    public void onPartnerReturnValue(String partner_selected) {
        switch (partner_selected)
        {
            case("Yes"):
                partner = true;
                break;
            case("No"):
                partner = false;
                break;
            default:
                partner = false;
                break;
        }
        Log.i(TAG, partner_selected);
        Log.i(TAG, String.valueOf(partner));
        edt_partner.setText(partner_selected);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        edt_dob.setText(simpleDateFormat.format(calendar.getTime()));
    }

    @VisibleForTesting
    void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        new SpinnerDatePickerDialogBuilder()
                .context(UpdateProfileActivity.this)
                .callback(UpdateProfileActivity.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(year, monthOfYear, dayOfMonth)
                .build()
                .show();
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            resetValidators();
        }

        public void afterTextChanged(Editable editable) {
        }
    }

    private void showDialog(String text_to_display)
    {
        lovelyProgressDialog.setIcon(R.drawable.ic_cast_connected_white_36dp)
                .setTitle("Segun Obadje Teaching Ministries")
                .setTopColorRes(R.color.endColor)
                .setMessage(text_to_display)
                .setMessageGravity(Gravity.CENTER_HORIZONTAL)
                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .show();
    }

    private void hideDialog()
    {
        lovelyProgressDialog.dismiss();
    }

    private class GetCountryData extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog("Updating Countries....");
        }

        @Override
        protected String doInBackground(String... strings) {
            String json = null;
            country_list.clear();
            try {
                InputStream is = getAssets().open("countriesToCities.json");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                json = new String(buffer, "UTF-8");
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    Iterator<String> countryIterator = jsonObject.keys();
                    while(countryIterator.hasNext())
                    {
                        country_list.add(countryIterator.next());
                    }
                    Collections.sort(country_list, new Comparator<String>() {
                        @Override
                        public int compare(String s1, String s2) {
                            return s1.compareToIgnoreCase(s2);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideDialog();
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("country_dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            DialogFragment dialogFragment = CountryDialog.newInstance(country_list);
            dialogFragment.show(ft, "country_dialog");
        }
    }

    private class GetCitiesData extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog("Updating Cities....");
        }

        @Override
        protected String doInBackground(String... strings) {
            String json = null;
            city_list.clear();
            try {
                InputStream is = getAssets().open("countriesToCities.json");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                json = new String(buffer, "UTF-8");
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    JSONArray jsonArray = new JSONArray();
                    if(!country.isEmpty()){
                        jsonArray = jsonObject.getJSONArray(country);

                        for (int i = 0; i<jsonArray.length(); i++)
                        {
                            city_list.add((String) jsonArray.get(i));
                        }
                        Collections.sort(city_list, new Comparator<String>() {
                            @Override
                            public int compare(String s1, String s2) {
                                return s1.compareToIgnoreCase(s2);
                            }
                        });
                    }else{
                        Utils.initToast(UpdateProfileActivity.this, "Please select country first.");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideDialog();
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("city_dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            DialogFragment dialogFragment = CityDialog.newInstance(city_list);
            dialogFragment.show(ft, "city_dialog");

        }
    }


    private boolean validatePassword() {
        if (edt_pWord.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_pWord.setError("Enter valid password");
            requestFocus(sign_up_input_layout_pWord);
            return false;
        }
        else if(edt_pWord.getText().toString().trim().length() < 6)
        {
            sign_up_input_layout_pWord.setError("6 chars or more");
            requestFocus(sign_up_input_layout_pWord);
            return false;
        }
        else {
            sign_up_input_layout_pWord.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateFullName() {
        if (edt_fName.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_fName.setError("Enter valid full name");
            requestFocus(sign_up_input_layout_fName);
            return false;
        }
        else if(edt_fName.getText().toString().trim().matches("^[^\\d\\s]+$"))
        {
            sign_up_input_layout_fName.setError("Enter valid full name");
            requestFocus(sign_up_input_layout_fName);
            return false;
        }
        else {
            sign_up_input_layout_fName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateUName() {
        if (edt_uname.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_uname.setError("Enter valid user name");
            requestFocus(sign_up_input_layout_uname);
            return false;
        }
        else if(!edt_uname.getText().toString().trim().matches("^[^\\d\\s]+$"))
        {
            sign_up_input_layout_uname.setError("Enter valid user name");
            requestFocus(sign_up_input_layout_uname);
            return false;
        }
        else {
            sign_up_input_layout_uname.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateGender() {
        if (edt_gender.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_gender.setError("Enter valid gender");
            requestFocus(sign_up_input_layout_gender);
            return false;
        }
        else if(!edt_gender.getText().toString().trim().matches("^[^\\d\\s]+$"))
        {
            sign_up_input_layout_gender.setError("Enter valid gender");
            requestFocus(sign_up_input_layout_gender);
            return false;
        }
        else {
            sign_up_input_layout_gender.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateDOB() {
        if (edt_dob.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_dob.setError("Enter valid date of birth");
            requestFocus(sign_up_input_layout_dob);
            return false;
        }
        else {
            sign_up_input_layout_dob.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCountry() {
        if (edt_country.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_country.setError("Enter valid country");
            requestFocus(sign_up_input_layout_country);
            return false;
        }else {
            sign_up_input_layout_country.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCity() {
        if (edt_country.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_city.setError("Please select country first");
            requestFocus(sign_up_input_layout_city);
            return false;
        }
        else if (edt_city.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_city.setError("Enter valid city");
            requestFocus(sign_up_input_layout_city);
            return false;
        }
        else {
            sign_up_input_layout_city.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePartner() {
        if (edt_partner.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_partner.setError("Enter valid option of partnership");
            requestFocus(sign_up_input_layout_partner);
            return false;
        }
        else if(!edt_partner.getText().toString().trim().matches("^[^\\d\\s]+$"))
        {
            sign_up_input_layout_partner.setError("Enter valid option of partnership");
            requestFocus(sign_up_input_layout_partner);
            return false;
        }
        else {
            sign_up_input_layout_partner.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePhoneNumber() {
        if (edt_phoneNumber.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_phoneNumber.setError("Enter valid phone number");
            requestFocus(sign_up_input_layout_phoneNumber);
            return false;
        }
        else {
            sign_up_input_layout_phoneNumber.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMail() {
        if (edt_mail.getText().toString().trim().isEmpty()) {
            sign_up_input_layout_mail.setError("Enter valid e-mail");
            requestFocus(sign_up_input_layout_mail);
            return false;
        }
        else if(!emailValidator.validateEmail(edt_mail.getText().toString().trim()))
        {
            sign_up_input_layout_mail.setError("Enter valid e-mail");
            requestFocus(sign_up_input_layout_mail);
            return false;
        }
        else {
            sign_up_input_layout_mail.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.update_profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id)
        {
            case R.id.action_done:
                //send to app server here
                done_clicked();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void done_clicked()
    {
        if(!validateFullName())
        {
            return;
        }
        if(!validateMail())
        {
            return;
        }
        if(!validateUName())
        {
            return;
        }
        if(!validatePassword())
        {
            return;
        }
        if(!validatePhoneNumber())
        {
            return;
        }
        if(!validateGender())
        {
            return;
        }
        if(!validateDOB())
        {
            return;
        }
        if(!validateCountry())
        {
            return;
        }
        if(!validateCity())
        {
            return;
        }
        if(!validatePartner())
        {
            return;
        }

        Log.d(TAG, hashMap.get(SessionManager.KEY_ID) + " ; " + hashMap.get(SessionManager.KEY_TOKEN) + " ; " +
                edt_uname.getText().toString().trim() + " ; " +  edt_fName.getText().toString().trim() + " ; " +
                edt_pWord.getText().toString().trim() + " ; " +  edt_mail.getText().toString().trim() + " ; " +
                edt_phoneNumber.getText().toString().trim() + " ; " +  edt_gender.getText().toString().trim() + " ; " +
                country + " ; " +  dob + " ; " +  city + " ; " + partner);

        update_user_profile(hashMap.get(SessionManager.KEY_TOKEN), hashMap.get(SessionManager.KEY_ID),
                edt_fName.getText().toString().trim(), edt_mail.getText().toString().trim(),
                edt_pWord.getText().toString().trim(), edt_phoneNumber.getText().toString().trim(),
                edt_uname.getText().toString().trim(), edt_dob.getText().toString().trim(),
                edt_gender.getText().toString().trim(), edt_city.getText().toString().trim(),
                edt_country.getText().toString().trim(), partner);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_GALLERY_CODE && resultCode == UpdateProfileActivity.this.RESULT_OK){
            selectedImage = data.getData();
            if (EasyPermissions.hasPermissions(UpdateProfileActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Have permissions, do the thing!
                //String filePath = getRealPathFromURIPath(selectedImage, UpdateProfileActivity.this);
                //startCropImageActivity(selectedImage);

                startCropActivity(data.getData());
            }
            else
            {
                //Request permissions.
                EasyPermissions.requestPermissions(UpdateProfileActivity.this, getString(R.string.read_file), READ_REQUEST_CODE, android.Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
        if (requestCode == UCrop.REQUEST_CROP) {
            try{
                handleCropResult(data);
            }
            catch (NullPointerException e)
            {
                Log.e(TAG, e.getMessage());
            }

        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if(selectedImage != null){
            String filePath = getRealPathFromURIPath(selectedImage, UpdateProfileActivity.this);
            File file = new File(filePath);
            Log.d(TAG, "Filename " + file.getName());
            Log.i(TAG, "File Size - " + String.valueOf(file.length()/1024));
            if ((file.length()/1024) <= 500) {
                isPhotoDone = true;
                Toast.makeText(UpdateProfileActivity.this, "Picture: Ok", Toast.LENGTH_SHORT).show();

                Picasso.with(UpdateProfileActivity.this)
                        .load(file)
                        .into(img_ProfilePic);
                sendImageToServer(selectedImage, hashMap.get(SessionManager.KEY_TOKEN),
                        hashMap.get(SessionManager.KEY_ID));
            }
            else
            {
                isPhotoDone = false;
                Toast.makeText(UpdateProfileActivity.this, "picture too large, please crop. Thank you", Toast.LENGTH_SHORT).show();

            }
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.i(TAG, "Permission to access gallery has been denied");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, getActivity());
    }

    private int getImageHeight(File file){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        Log.i(TAG,  "Image Height - " + imageHeight);
        return imageHeight;
    }

    private int getImageWidth(File file){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int imageWidth = options.outWidth;
        Log.i(TAG,  "Image Width - " + imageWidth);
        return imageWidth;
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void showImageUploadDialog()
    {
        lovelyProgressDialog.setIcon(R.drawable.ic_cast_connected_white_36dp)
                .setTitle("Segun Obadje Teaching Ministries")
                .setMessage("please wait, uploading image...")
                .setMessageGravity(Gravity.CENTER_HORIZONTAL)
                .setTopColorRes(R.color.xml_startColor)
                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .show();
    }

    private void sendImageToServer(final Uri fileUri, final String token, final String user_id) {
        showImageUploadDialog();
        APIinterface service = APIclient.createService(APIinterface.class, token, true);
        String filePath = getRealPathFromURIPath(fileUri, UpdateProfileActivity.this);
        File file = new File(filePath);
        Log.i(TAG, filePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
        Call<RequestBody> call = service.uploadAvatar(token, user_id, body);
        call.enqueue(new Callback<RequestBody>() {
            @Override
            public void onResponse(Call<RequestBody> call, Response<RequestBody> response) {
                hideImageUploadDialog();
                Log.i(TAG, String.valueOf(response.code()));
                try {
                    Log.i(TAG, response.errorBody().string());
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                }
                if(response.code() == Utils.STATUS_200_OK) {
                    Toast.makeText(UpdateProfileActivity.this, "Image uploaded", Toast.LENGTH_SHORT).show();
                }
                else {
                    Utils.initToast(UpdateProfileActivity.this, Utils.getErrorMessage(response));
                }
            }
            @Override
            public void onFailure(Call<RequestBody> call, Throwable t) {
                hideImageUploadDialog();
                Log.i(TAG, call.request().url().toString());
                call.cancel();
                //Toast.makeText(UpdateProfileActivity.this, "Poor internet connection", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void hideImageUploadDialog() {
        lovelyProgressDialog.dismiss();
    }

    private void update_user_profile(String token, String user_id, String name, String email, final String password,
                                     String phone_number, String username, String dob, String gender, String city, String country, boolean is_a_partner)
    {
        showDialog("please wait, updating your profile");
        UpdateProfileBody updateProfileBody = new UpdateProfileBody(name, email, username, phone_number, dob, gender, city, country, is_a_partner);
        APIinterface apIinterface = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_UpdateProfile> call = apIinterface.updateTradUserProfile(token, user_id,
                updateProfileBody);
        Log.i(TAG, updateProfileBody.toString());
        Log.i(TAG, token);
        call.enqueue(new Callback<Response_UpdateProfile>() {
            @Override
            public void onResponse(Call<Response_UpdateProfile> call, Response<Response_UpdateProfile> response) {
                hideDialog();
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Utils.initToast(getApplicationContext(), "Update Successful");
                    Log.i(TAG, response.body().getData().getUser().getToken());
                    Log.i(TAG, response.body().toString());
                    Log.i(TAG, response.body().getData().getUser().getToken());
                    sessionManager.updateData(
                            response.body().getData().getUser().getName(),
                            response.body().getData().getUser().getUsername(),
                            response.body().getData().getUser().getEmail(),
                            response.body().getData().getUser().getPicture(), //pictureURL
                            response.body().getData().getUser().isProfileComplete(),
                            response.body().getData().getUser().isProfilePublic(),
                            response.body().getData().getUser().isConfirmed(),
                            response.body().getData().getUser().getCountry(),
                            response.body().getData().getUser().getCity(),
                            response.body().getData().getUser().getPhoneNumber(),
                            response.body().getData().getUser().getGender(),
                            response.body().getData().getUser().getRole(),
                            response.body().getData().getUser().getDob(),
                            response.body().getData().getUser().isAPartner(),
                            response.body().getData().getUser().getFriends());
                    Intent intent = new Intent(UpdateProfileActivity.this, HomeActivity.class);
                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
                } else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_UpdateProfile> call, Throwable t) {
                hideDialog();
                call.cancel();
                Log.i(TAG, call.request().url().toString());
                Utils.initToast(getApplicationContext(), "Hey sorry, you can try again!");
                Log.i(TAG, t.getMessage());
            }
        });
    }

    private String maskPartner(String b)
    {
        String s = "";
        if(b == "true")
        {
            s = "Yes";
        }
        else
        {
            s = "No";
        }
        return s;
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        String filePath = getRealPathFromURIPath(resultUri, UpdateProfileActivity.this);
        File file = new File(filePath);
        Log.i(TAG, "Filename " + file.getName());
        Log.i(TAG, "File Size - " + String.valueOf(file.length()/1024));
        if (resultUri != null) {

            if ((file.length()/1024) <= 500) {
                isPhotoDone = true;
                Toast.makeText(UpdateProfileActivity.this, "Picture: Ok", Toast.LENGTH_SHORT).show();

                Picasso.with(UpdateProfileActivity.this)
                        .load(file)
                        .placeholder(R.drawable.head_photo)
                        .into(img_ProfilePic);
                sendImageToServer(resultUri, hashMap.get(SessionManager.KEY_TOKEN),
                        hashMap.get(SessionManager.KEY_ID));
            }
            else
            {
                isPhotoDone = false;
                Toast.makeText(UpdateProfileActivity.this, "Picture too large, 500kb is the cap.", Toast.LENGTH_SHORT).show();

            }
        } else {
            Toast.makeText(UpdateProfileActivity.this, "Cannot retrieve cropped image", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e(TAG, "handleCropError: ", cropError);
            Toast.makeText(UpdateProfileActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(UpdateProfileActivity.this, "Some unexpected error occured", Toast.LENGTH_SHORT).show();
        }
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = "SampleCropImage.png";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));

//        uCrop = basisConfig(uCrop);
//        uCrop = advancedConfig(uCrop);

        uCrop.start(UpdateProfileActivity.this);
    }

}
