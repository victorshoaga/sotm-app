package com.glt.sotm;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.percolate.caffeine.ViewUtils;
import com.percolate.mentions.Mentionable;
import com.percolate.mentions.Mentions;
import com.percolate.mentions.QueryListener;
import com.percolate.mentions.SuggestionsListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapters.CommentRecyclerAdapter;
import Adapters.RecyclerItemClickListener;
import Adapters.UsersAdapter;
import Models.AppMentionable;
import Models.ChatModel;
import Models.CommentModel;
import Models.Mention;
import Models.User;
import Models.UserFriend;
import Utility.MentionsLoaderUtils;
import Utility.SessionManager;
import Utility.Utils;
import chat.ChatContract;
import chat.ChatPresenter;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import tr.xip.errorview.ErrorView;

import static android.view.View.GONE;

public class UserCommentActivity extends AppCompatActivity implements View.OnClickListener, ChatContract.View, TextView.OnEditorActionListener, QueryListener, SuggestionsListener {

    private static final String TAG = UserCommentActivity.class.getSimpleName();
    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
    private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;
    SessionManager sessionManager;
    HashMap<String, String> hashMap;
    String self_id;
    private DatabaseReference mFirebaseDatabaseReference;
    private RecyclerView rvListMessage;
    private LinearLayoutManager mLinearLayoutManager;
    private Button btSendMessage;
    CommentRecyclerAdapter firebaseAdapter;
    private ChatPresenter mChatPresenter;
    ProgressWheel progressWheel;
    ErrorView bad_internet_errorview, empty_adapter_errorview;
    String post_id, self_uname;
    EmojiconEditText edit_comment;
    List<AppMentionable> appMentions;
    /**
     * Utility class to load from a JSON file.
     */
    private MentionsLoaderUtils mentionsLoaderUtils;

    /**
     * Mention object provided by library to configure at mentions.
     */
    private Mentions mentions;
    /**
     * Adapter to display suggestions.
     */
    private UsersAdapter usersAdapter;

    private HashMap<String, String> hash;
    LovelyProgressDialog lovelyProgressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_comment);
        sessionManager = new SessionManager(getApplicationContext());
        mChatPresenter = new ChatPresenter(this);
        hashMap = sessionManager.getUserDetails();
        appMentions = new ArrayList<>();
        self_id = getIntent().getStringExtra("self_id");
        self_uname = getIntent().getStringExtra("self_uname");
        post_id = getIntent().getStringExtra("post_id");

        hash = new HashMap<>();
        lovelyProgressDialog = new LovelyProgressDialog(UserCommentActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.user_comment_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_action_clear), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        bindViews();
    }

    private void bindViews(){

        edit_comment = (EmojiconEditText)findViewById(R.id.edit_message);
        edit_comment.setOnEditorActionListener(this);
        ImageView btEmoji = (ImageView) findViewById(R.id.buttonEmoji);
        EmojIconActions emojIcon = new EmojIconActions(getApplicationContext(), findViewById(R.id.root_view),edit_comment,btEmoji);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.happy_smiley);
        mentions = new Mentions.Builder(this, edit_comment)
                .suggestionsListener(this)
                .queryListener(this)
                .build();
        mentionsLoaderUtils = new MentionsLoaderUtils(this);
//        edMessage = (EditText) findViewById(R.id.edit_message);
//        edMessage.setOnEditorActionListener(this);
        btSendMessage = (Button) findViewById(R.id.btn_send);
        btSendMessage.setOnClickListener(this);
        rvListMessage = (RecyclerView)findViewById(R.id.comment_recyclerview);
        progressWheel = (ProgressWheel) findViewById(R.id.chat_progress_wheel);
        bad_internet_errorview = (ErrorView) findViewById(R.id.chat_poor_internet_error_view);
        empty_adapter_errorview = (ErrorView) findViewById(R.id.chat_empty_error_view);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        rvListMessage.setHasFixedSize(true);
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        showDialog();
        mChatPresenter.getComment(self_id, post_id);
        setupMentionsList();
    }

    private void showDialog()
    {
        progressWheel.setVisibility(View.VISIBLE);
        rvListMessage.setVisibility(GONE);
        bad_internet_errorview.setVisibility(GONE);
        empty_adapter_errorview.setVisibility(GONE);
    }

    private void hideDialog()
    {
        progressWheel.setVisibility(GONE);
        bad_internet_errorview.setVisibility(GONE);
        empty_adapter_errorview.setVisibility(GONE);
        rvListMessage.setVisibility(View.VISIBLE);
    }

    private void showErrorView(int type)
    {
        switch (type)
        {
            case 1:
                bad_internet_errorview.setVisibility(View.VISIBLE);
                empty_adapter_errorview.setVisibility(GONE);
                progressWheel.setVisibility(GONE);
                rvListMessage.setVisibility(GONE);
                bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
                    @Override
                    public void onRetry() {
                        mChatPresenter.getComment(self_id, post_id);
                    }
                });
                break;
            case 2:
                empty_adapter_errorview.setVisibility(View.VISIBLE);
                bad_internet_errorview.setVisibility(GONE);
                progressWheel.setVisibility(GONE);
                rvListMessage.setVisibility(GONE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_send:
                if(!edit_comment.getText().toString().isEmpty())
                {
                    sendMessage();
                }
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            if(!edit_comment.getText().toString().isEmpty())
            {
                sendMessage();
            }
            return true;
        }
        return false;
    }

    private void sendMessage() {
        String message = edit_comment.getText().toString();
        Log.i(TAG, self_id);
        Log.i(TAG, post_id);
        for(Mentionable mention : mentions.getInsertedMentions()){
            appMentions.add(new AppMentionable(mention.getMentionOffset(), mention.getMentionLength(), mention.getMentionName(), hash.get(mention.getMentionName())));
        }
        CommentModel commentModel = new CommentModel(self_uname, self_id,
                message, System.currentTimeMillis(), appMentions);

        mChatPresenter.sendComment(getApplicationContext(),
                commentModel,
                self_id, post_id);

        edit_comment.setText("");
    }

    @Override
    public void onSendMessageSuccess() {
        Toast.makeText(getApplicationContext(), "Comment posted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSendMessageFailure(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetMessagesSuccess(ChatModel chat) {

    }

    @Override
    public void onGetCommentsSuccess(CommentModel chat) {
        hideDialog();
        if(chat == null)
        {
            showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
        }
        else
        {
            if (firebaseAdapter == null) {
                firebaseAdapter = new CommentRecyclerAdapter(UserCommentActivity.this, new ArrayList<CommentModel>(), getApplicationContext(), lovelyProgressDialog);
                rvListMessage.setAdapter(firebaseAdapter);
            }

            firebaseAdapter.add(chat);
            rvListMessage.smoothScrollToPosition(firebaseAdapter.getItemCount() - 1);
        }
    }

    @Override
    public void onGetMessagesFailure(String message) {
        showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }

    @Override
    public void onQueryReceived(String query) {
        final List<UserFriend> users = mentionsLoaderUtils.searchUsers(query);
        if (users != null && !users.isEmpty()) {
            usersAdapter.clear();
            usersAdapter.setCurrentQuery(query);
            usersAdapter.addAll(users);
            showMentionsList(true);
        } else {
            showMentionsList(false);
        }
    }

    @Override
    public void displaySuggestions(boolean display) {
        if (display) {
            ViewUtils.showView(this, R.id.mentions_list_layout);
        } else {
            ViewUtils.hideView(this, R.id.mentions_list_layout);
        }
    }

    private void showMentionsList(boolean display) {
        ViewUtils.showView(this, R.id.mentions_list_layout);
        if (display) {
            ViewUtils.showView(this, R.id.mentions_list);
            ViewUtils.hideView(this, R.id.mentions_empty_view);
        } else {
            ViewUtils.hideView(this, R.id.mentions_list);
            ViewUtils.showView(this, R.id.mentions_empty_view);
        }
    }

    private void setupMentionsList() {
        final RecyclerView mentionsList = (RecyclerView) findViewById(R.id.mentions_list);
        mentionsList.setLayoutManager(new LinearLayoutManager(this));
        usersAdapter = new UsersAdapter(this);
        mentionsList.setAdapter(usersAdapter);

        // set on item click listener
        mentionsList.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(final View view, final int position) {
                final UserFriend user = usersAdapter.getItem(position);
                /*
                 * We are creating a mentions object which implements the
                 * <code>Mentionable</code> interface this allows the library to set the offset
                 * and length of the mention.
                 */
                if (user != null) {
                    final Mention mention = new Mention();
                    mention.setMentionName(user.getName());
                    hash.put(mention.getMentionName(), user.getId());
                    mentions.insertMention(mention);
                }

            }
        }));
    }

}
