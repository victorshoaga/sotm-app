package com.glt.sotm;
//
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.View;
//import android.view.inputmethod.EditorInfo;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.database.DatabaseReference;
//import com.pnikosis.materialishprogress.ProgressWheel;
//import com.squareup.picasso.Picasso;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import Adapters.ChatRecyclerAdapter;
//import Models.ChatModel;
//import Models.CommentModel;
//import Models.User;
//import Utility.SessionManager;
//import Utility.Utils;
//import chat.ChatContract;
//import chat.ChatPresenter;
//import de.hdodenhof.circleimageview.CircleImageView;
//import event.PushNotificationEvent;
//import tr.xip.errorview.ErrorView;
//
//import static android.view.View.GONE;
//
//public class UserFriendChatActivity extends AppCompatActivity implements View.OnClickListener, ChatContract.View, TextView.OnEditorActionListener{
//
//    private static final String TAG = UserFriendChatActivity.class.getSimpleName();
//    private static final int SHOW_BAD_INTERNET_ERRORVIEW = 1;
//    private static final int SHOW_EMPTY_ADAPTER_ERRORVIEW = 2;
//    SessionManager sessionManager;
//    HashMap<String, String> hashMap;
//    User friend_data, self_data;
//    private DatabaseReference mFirebaseDatabaseReference;
//    private RecyclerView rvListMessage;
//    private LinearLayoutManager mLinearLayoutManager;
//    private Button btSendMessage;
//    private EditText edMessage;
//    ChatRecyclerAdapter firebaseAdapter;
//    private ChatPresenter mChatPresenter;
//    ProgressWheel progressWheel;
//    ErrorView bad_internet_errorview, empty_adapter_errorview;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_user_friend_chat);
//        sessionManager = new SessionManager(getApplicationContext());
//        mChatPresenter = new ChatPresenter(this);
//        hashMap = sessionManager.getUserDetails();
//        //friend_data
//        friend_data = (User) getIntent().getParcelableExtra("friend_data");
//        self_data = (User) getIntent().getParcelableExtra("self_data");
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.user_friend_chat_toolbar);
//        CircleImageView imageView = (CircleImageView) findViewById(R.id.chat_img);
//        Picasso.with(UserFriendChatActivity.this)
//                .load(friend_data.getPicture())
//                .placeholder(R.drawable.head_photo)
//                .into(imageView);
//        TextView toolbarTextView = (TextView) findViewById(R.id.user_friend_chat_toolbar_TextView);
//        toolbar.setTitle("");
//        toolbarTextView.setText(friend_data.getName());//put friend name
//        setSupportActionBar(toolbar);
//        toolbar.setTitleTextColor(Color.WHITE);
//        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(UserFriendChatActivity.this, ChatsActivity.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
//
//            }
//        });
//
//
//        bindViews();
//        //Toast.makeText(getApplicationContext(), "messages queueing ...", Toast.LENGTH_SHORT).show();
//        //getFirebaseMessages(self_data.getUsername(), friend_data.getUsername());
//
//    }
//
//    private void bindViews(){
//
//        edMessage = (EditText) findViewById(R.id.edit_message);
//        edMessage.setOnEditorActionListener(this);
//        btSendMessage = (Button) findViewById(R.id.btn_send);
//        btSendMessage.setOnClickListener(this);
//        rvListMessage = (RecyclerView)findViewById(R.id.chat_recyclerview);
//        progressWheel = (ProgressWheel) findViewById(R.id.chat_progress_wheel);
//        bad_internet_errorview = (ErrorView) findViewById(R.id.chat_poor_internet_error_view);
//        empty_adapter_errorview = (ErrorView) findViewById(R.id.chat_empty_error_view);
//        mLinearLayoutManager = new LinearLayoutManager(this);
//        mLinearLayoutManager.setStackFromEnd(true);
//        rvListMessage.setHasFixedSize(true);
//        rvListMessage.setLayoutManager(mLinearLayoutManager);
//        showDialog();
//        mChatPresenter.getMessage(self_data.getId(), friend_data.getId());
//    }
//
//    private void showDialog()
//    {
//        progressWheel.setVisibility(View.VISIBLE);
//        rvListMessage.setVisibility(GONE);
//        bad_internet_errorview.setVisibility(GONE);
//        empty_adapter_errorview.setVisibility(GONE);
//    }
//
//    private void hideDialog()
//    {
//        progressWheel.setVisibility(GONE);
//        bad_internet_errorview.setVisibility(GONE);
//        empty_adapter_errorview.setVisibility(GONE);
//        rvListMessage.setVisibility(View.VISIBLE);
//    }
//
//    private void showErrorView(int type)
//    {
//        switch (type)
//        {
//            case 1:
//                bad_internet_errorview.setVisibility(View.VISIBLE);
//                empty_adapter_errorview.setVisibility(GONE);
//                progressWheel.setVisibility(GONE);
//                rvListMessage.setVisibility(GONE);
//                bad_internet_errorview.setOnRetryListener(new ErrorView.RetryListener() {
//                    @Override
//                    public void onRetry() {
//                        mChatPresenter.getMessage(self_data.getId(), friend_data.getId());
//                    }
//                });
//                break;
//            case 2:
//                empty_adapter_errorview.setVisibility(View.VISIBLE);
//                bad_internet_errorview.setVisibility(GONE);
//                progressWheel.setVisibility(GONE);
//                rvListMessage.setVisibility(GONE);
//                break;
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.btn_send:
//                if(!edMessage.getText().toString().isEmpty())
//                {
//                    sendMessage();
//                }
//                break;
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        SOTMApplication.setChatActivityOpen(true);
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        SOTMApplication.setChatActivityOpen(false);
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        EventBus.getDefault().unregister(this);
//    }
//
//
//    @Override
//    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//        if (actionId == EditorInfo.IME_ACTION_SEND) {
//            if(!edMessage.getText().toString().isEmpty())
//            {
//                sendMessage();
//            }
//            return true;
//        }
//        return false;
//    }
//
//    private void sendMessage() {
//        String message = edMessage.getText().toString();
//        edMessage.setText("");
//        Log.i(TAG, friend_data.toString());
//        Log.i(TAG, self_data.toString());
//        String receiverFirebaseToken = friend_data.getFcm_token();
//        ChatModel chat = new ChatModel(message,
//                self_data.getId(), System.currentTimeMillis(), friend_data.getId());
//        mChatPresenter.sendMessage(getApplicationContext(),
//                chat,
//                receiverFirebaseToken, self_data, friend_data);
//    }
//
//    @Override
//    public void onSendMessageSuccess() {
//        edMessage.setText("");
//        Toast.makeText(getApplicationContext(), "Message sent", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onSendMessageFailure(String message) {
//        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onGetMessagesSuccess(ChatModel chat) {
//        hideDialog();
//        if(chat == null)
//        {
//            showErrorView(SHOW_EMPTY_ADAPTER_ERRORVIEW);
//        }
//        else
//        {
//            if (firebaseAdapter == null) {
//                firebaseAdapter = new ChatRecyclerAdapter(new ArrayList<ChatModel>(), getApplicationContext(), self_data, friend_data);
//                rvListMessage.setAdapter(firebaseAdapter);
//            }
//
//            firebaseAdapter.add(chat);
//            rvListMessage.smoothScrollToPosition(firebaseAdapter.getItemCount() - 1);
//        }
//    }
//
//    @Override
//    public void onGetMessagesFailure(String message) {
//        showErrorView(SHOW_BAD_INTERNET_ERRORVIEW);
//        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onGetCommentsSuccess(CommentModel commentModel) {
//
//    }
//
//    @Subscribe
//    public void onPushNotificationEvent(PushNotificationEvent pushNotificationEvent) {
//        if (firebaseAdapter == null || firebaseAdapter.getItemCount() == 0) {
//            mChatPresenter.getMessage(FirebaseAuth.getInstance().getCurrentUser().getUid(),
//                    pushNotificationEvent.getUid());
//        }
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent = new Intent(UserFriendChatActivity.this, ChatsActivity.class);
//        startActivity(intent);
//        finish();
//        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
//    }
//}
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.percolate.caffeine.ViewUtils;
import com.percolate.mentions.Mentionable;
import com.percolate.mentions.Mentions;
import com.percolate.mentions.QueryListener;
import com.percolate.mentions.SuggestionsListener;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapters.MessageAdapter;
import Adapters.RecyclerItemClickListener;
import Adapters.UsersAdapter;
import Models.AppMentionable;
import Models.Mention;
import Models.Messages;
import Models.User;
import Models.UserFriend;
import Utility.MentionsLoaderUtils;
import Utility.SessionManager;
import de.hdodenhof.circleimageview.CircleImageView;
import fcm.FcmNotificationBuilder;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class UserFriendChatActivity extends AppCompatActivity implements QueryListener, SuggestionsListener {
    private String mChatUser;
    private Toolbar mChatToolbar;

    private DatabaseReference mRootRef;

    private TextView mTitleView;
    private TextView mLastSeenView;
    private CircleImageView mProfileImage;
    private SessionManager sessionManager;
    private HashMap<String, String> hashMap;
    //private FirebaseAuth mAuth;
    private String mCurrentUserId;

    //private ImageButton mChatAddBtn;
    private Button mChatSendBtn;
    private EmojiconEditText mChatMessageView;

    private RecyclerView mMessagesList;
    private SwipeRefreshLayout mRefreshLayout;

    private final List<Messages> messagesList = new ArrayList<>();
    private LinearLayoutManager mLinearLayout;
    private MessageAdapter mAdapter;

    private static final int TOTAL_ITEMS_TO_LOAD = 10;
    private int mCurrentPage = 1;

    private static final int GALLERY_PICK = 1;

    // Storage Firebase
    private StorageReference mImageStorage;

    TextView txt_istyping;


    //New Solution
    private int itemPos = 0;

    private String mLastKey = "";
    private String mPrevKey = "";

    private User friend_data;
            //self_data;
    private static final String TAG = UserFriendChatActivity.class.getSimpleName();
    String userName = "", fullName = "", pictureUrl = "", fcmToken = "";

    /**
     * Utility class to load from a JSON file.
     */
    private MentionsLoaderUtils mentionsLoaderUtils;

    /**
     * Mention object provided by library to configure at mentions.
     */
    private Mentions mentions;
    /**
     * Adapter to display suggestions.
     */
    private UsersAdapter usersAdapter;

    List<AppMentionable> appMentions;
    LovelyProgressDialog lovelyProgressDialog;
    HashMap<String, String> hash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        sessionManager = new SessionManager(this);
        hashMap = new HashMap<>();
        hash = new HashMap<>();
        hashMap = sessionManager.getUserDetails();
        appMentions = new ArrayList<>();
        lovelyProgressDialog = new LovelyProgressDialog(UserFriendChatActivity.this);

        mChatToolbar = (Toolbar) findViewById(R.id.chat_app_bar);
        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        mRootRef = FirebaseDatabase.getInstance().getReference();
        //mAuth = FirebaseAuth.getInstance();
        //mCurrentUserId = mAuth.getCurrentUser().getUid();
        mCurrentUserId = hashMap.get(SessionManager.KEY_ID);
        friend_data = (User) getIntent().getParcelableExtra("friend_data");
        if(friend_data != null)
        {
            mChatUser = friend_data.getId();
            userName = friend_data.getUsername();
            fullName = friend_data.getName();
            pictureUrl = friend_data.getPicture();
        }

        else
        {
            mChatUser = getIntent().getStringExtra("user_id");
            fullName = getIntent().getStringExtra("user_name");
            if(!(getIntent().getStringExtra("picture_url") == null)){
                pictureUrl = getIntent().getStringExtra("picture_url");
            }else{
                pictureUrl = "";
            }
            fcmToken = getIntent().getStringExtra("fcm_token");
        }

        try{
            Log.i(TAG + "mCurrentUserId", mCurrentUserId);
            Log.i(TAG + "mChatUser", mChatUser);
            Log.i(TAG + "userName", userName);
            Log.i(TAG + "fullName", fullName);
            Log.i(TAG + "pictureUrl", pictureUrl);
        }
        catch (NullPointerException e)
        {
            Log.e(TAG, e.getMessage());
        }

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(R.layout.chat_custom_bar, null);

        actionBar.setCustomView(action_bar_view);

        // ---- Custom Action bar Items ----

        mTitleView = (TextView) findViewById(R.id.custom_bar_title);
        mLastSeenView = (TextView) findViewById(R.id.custom_bar_seen);
        mProfileImage = (CircleImageView) findViewById(R.id.custom_bar_image);
        txt_istyping = (TextView) findViewById(R.id.txt_is_typing);

        //mChatAddBtn = (ImageButton) findViewById(R.id.chat_add_btn);
        mChatSendBtn = (Button) findViewById(R.id.chat_send_btn);
        mChatMessageView = (EmojiconEditText)findViewById(R.id.chat_message_view);
        ImageView btEmoji = (ImageView) findViewById(R.id.buttonEmoji);
        EmojIconActions emojIcon = new EmojIconActions(getApplicationContext(), findViewById(R.id.root_view),mChatMessageView,btEmoji);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.happy_smiley);
        mentions = new Mentions.Builder(this, mChatMessageView)
                .suggestionsListener(this)
                .queryListener(this)
                .build();
        mentionsLoaderUtils = new MentionsLoaderUtils(this);
        setupMentionsList();

        mAdapter = new MessageAdapter(UserFriendChatActivity.this,messagesList, lovelyProgressDialog);

        mMessagesList = (RecyclerView) findViewById(R.id.messages_list);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.message_swipe_layout);
        mLinearLayout = new LinearLayoutManager(this);

        mMessagesList.setHasFixedSize(true);
        mMessagesList.setLayoutManager(mLinearLayout);

        mMessagesList.setAdapter(mAdapter);

        //------- IMAGE STORAGE ---------
        mImageStorage = FirebaseStorage.getInstance().getReference();

        mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("seen").setValue(true);

        loadMessages();


        //mTitleView.setText(userName);
        mTitleView.setText(fullName);
        if(!pictureUrl.isEmpty()){
            Picasso.with(getApplicationContext())
                    .load(pictureUrl)
                    .placeholder(R.drawable.head_photo)
                    .into(mProfileImage);
        }else{
            Picasso.with(getApplicationContext())
                    .load(R.drawable.head_photo)
                    .placeholder(R.drawable.head_photo)
                    .into(mProfileImage);
        }


        mRootRef.child("Users").child(mChatUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try{
                    String online = dataSnapshot.child("online").getValue().toString();
                    //String image = dataSnapshot.child("image").getValue().toString();

                    if(online.equals("true")) {

                        mLastSeenView.setText("Online");

                    } else {

                        GetTimeAgo getTimeAgo = new GetTimeAgo();

                        long lastTime = Long.parseLong(online);

                        String lastSeenTime = getTimeAgo.getTimeAgo(lastTime, getApplicationContext());

                        mLastSeenView.setText(lastSeenTime);

                    }

                    String isTyping = dataSnapshot.child("isTyping").getValue().toString();

                    if(isTyping.equals("true"))
                    {
                        txt_istyping.setVisibility(View.VISIBLE);
                        txt_istyping.setText(fullName + " is typing.....");
                    } else {
                        txt_istyping.setVisibility(View.GONE);
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, e.getMessage());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mRootRef.child("Chat").child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(!dataSnapshot.hasChild(mChatUser)){

                    Map chatAddMap = new HashMap();
                    chatAddMap.put("seen", false);
                    chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                    Map chatUserMap = new HashMap();
                    chatUserMap.put("Chat/" + mCurrentUserId + "/" + mChatUser, chatAddMap);
                    chatUserMap.put("Chat/" + mChatUser + "/" + mCurrentUserId, chatAddMap);

                    mRootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if(databaseError != null){

                                Log.d("CHAT_LOG", databaseError.getMessage().toString());

                            }

                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        mChatSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendMessage();

            }
        });



//        mChatAddBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Utils.initToast(UserFriendChatActivity.this, "Coming Soon!!!...Stay tuned");
////                Intent galleryIntent = new Intent();
////                galleryIntent.setType("image/*");
////                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
////
////                startActivityForResult(Intent.createChooser(galleryIntent, "SELECT IMAGE"), GALLERY_PICK);
//
//            }
//        });



        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentPage++;
                itemPos = 0;
                loadMoreMessages();
            }
        });

        mChatToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserFriendChatActivity.this, ProfileActivity_Friend.class);
                intent.putExtra("message", "from_friends");
                intent.putExtra("is_friend", "yes");
                intent.putExtra("name", fullName);
                intent.putExtra("picture", pictureUrl);
                intent.putExtra("friend_id", mChatUser);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mChatMessageView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged (CharSequence s,int start, int count,
                                               int after){
                }
                @Override
                public void onTextChanged ( final CharSequence s, int start, int before,
                                            int count){
                }
                @Override
                public void afterTextChanged ( final Editable s){
                    //avoid triggering event when text is empty
                    if (s.length() > 0) {
                        FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("isTyping").setValue("true");
                    } else if (s.length() == 0){
                        FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("isTyping").setValue("false");
                    }
                }
            }

        );

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_PICK && resultCode == RESULT_OK){

            Uri imageUri = data.getData();

            final String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            final String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(mCurrentUserId).child(mChatUser).push();

            final String push_id = user_message_push.getKey();


            StorageReference filepath = mImageStorage.child("message_images").child( push_id + ".jpg");

            filepath.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    if(task.isSuccessful()){

                        String download_url = task.getResult().getDownloadUrl().toString();


                        Map messageMap = new HashMap();
                        messageMap.put("message", download_url);
                        messageMap.put("seen", false);
                        messageMap.put("type", "image");
                        messageMap.put("time", ServerValue.TIMESTAMP);
                        messageMap.put("from", mCurrentUserId);

                        Map messageUserMap = new HashMap();
                        messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
                        messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

                        mChatMessageView.setText("");

                        mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                if(databaseError != null){

                                    Log.d("CHAT_LOG", databaseError.getMessage().toString());

                                }

                            }
                        });


                    }

                }
            });

        }

    }

    private void loadMoreMessages() {

        DatabaseReference messageRef = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser);

        Query messageQuery = messageRef.orderByKey().endAt(mLastKey).limitToLast(10);

        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                Messages message_ = dataSnapshot.getValue(Messages.class);
                String messageKey = dataSnapshot.getKey();

                if(!mPrevKey.equals(messageKey)){

                    messagesList.add(itemPos++, message_);

                } else {

                    mPrevKey = mLastKey;

                }


                if(itemPos == 1) {

                    mLastKey = messageKey;

                }


                Log.d("TOTALKEYS", "Last Key : " + mLastKey + " | Prev Key : " + mPrevKey + " | Message Key : " + messageKey);

                mAdapter.notifyDataSetChanged();

                mRefreshLayout.setRefreshing(false);

                mLinearLayout.scrollToPositionWithOffset(10, 0);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void loadMessages() {

        DatabaseReference messageRef = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser);

        Query messageQuery = messageRef.limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD);


        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Messages message_ = dataSnapshot.getValue(Messages.class);
                itemPos++;

                if(itemPos == 1){

                    String messageKey = dataSnapshot.getKey();

                    mLastKey = messageKey;
                    mPrevKey = messageKey;

                }

                messagesList.add(message_);
                mAdapter.notifyDataSetChanged();

                mMessagesList.scrollToPosition(messagesList.size() - 1);

                mRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void sendMessage() {


        for(Mentionable mention : mentions.getInsertedMentions()){
            appMentions.add(new AppMentionable(mention.getMentionOffset(), mention.getMentionLength(), mention.getMentionName(), hash.get(mention.getMentionName())));
        }
        final String message = mChatMessageView.getText().toString();

        if(!TextUtils.isEmpty(message)){

            String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages")
                    .child(mCurrentUserId).child(mChatUser).push();

            String push_id = user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", message);
            messageMap.put("seen", false);
            messageMap.put("mentions", appMentions);
            messageMap.put("type", "text");
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", mCurrentUserId);

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

            mChatMessageView.setText("");

            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("seen").setValue(true);
            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("seen").setValue(false);
            mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    try{
                        if(friend_data != null){
                            Log.i(TAG, hashMap.get(SessionManager.KEY_NAME));
                            Log.i(TAG, message);
                            Log.i(TAG, friend_data.getId());
                            Log.i(TAG, sessionManager.getToken());
                            Log.i(TAG, friend_data.getFcm_token());

                            sendPushNotificationToReceiver(
                                    hashMap.get(SessionManager.KEY_NAME),
                                    hashMap.get(SessionManager.KEY_USERNAME),
                                    hashMap.get(SessionManager.KEY_PICTURE),
                                    message,
                                    hashMap.get(SessionManager.KEY_ID),
                                    sessionManager.getToken(),
                                    friend_data.getFcm_token());
                        }else{
                            Log.i(TAG, hashMap.get(SessionManager.KEY_NAME));
                            Log.i(TAG, message);
                            Log.i(TAG, mChatUser);
                            Log.i(TAG, sessionManager.getToken());
                            Log.i(TAG, fcmToken);

                            sendPushNotificationToReceiver(
                                    hashMap.get(SessionManager.KEY_NAME),
                                    hashMap.get(SessionManager.KEY_USERNAME),
                                    hashMap.get(SessionManager.KEY_PICTURE),
                                    message,
                                    hashMap.get(SessionManager.KEY_ID),
                                    sessionManager.getToken(),
                                    fcmToken);
                        }
                    }catch (Exception e){
                        Log.e(TAG, e.getMessage());
                    }

                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                }
            });
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("isTyping").setValue("false");

        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue(ServerValue.TIMESTAMP);
            }
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("isTyping").setValue("false");
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onQueryReceived(String query) {
        final List<UserFriend> users = mentionsLoaderUtils.searchUsers(query);
        if (users != null && !users.isEmpty()) {
            usersAdapter.clear();
            usersAdapter.setCurrentQuery(query);
            usersAdapter.addAll(users);
            showMentionsList(true);
        } else {
            showMentionsList(false);
        }
    }

    @Override
    public void displaySuggestions(boolean display) {
        if (display) {
            ViewUtils.showView(this, R.id.mentions_list_layout);
        } else {
            ViewUtils.hideView(this, R.id.mentions_list_layout);
        }
    }

    private void showMentionsList(boolean display) {
        ViewUtils.showView(this, R.id.mentions_list_layout);
        if (display) {
            ViewUtils.showView(this, R.id.mentions_list);
            ViewUtils.hideView(this, R.id.mentions_empty_view);
        } else {
            ViewUtils.hideView(this, R.id.mentions_list);
            ViewUtils.showView(this, R.id.mentions_empty_view);
        }
    }

    private void setupMentionsList() {
        final RecyclerView mentionsList = (RecyclerView) findViewById(R.id.mentions_list);
        mentionsList.setLayoutManager(new LinearLayoutManager(this));
        usersAdapter = new UsersAdapter(this);
        mentionsList.setAdapter(usersAdapter);

        // set on item click listener
        mentionsList.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(final View view, final int position) {
                final UserFriend user = usersAdapter.getItem(position);
                /*
                 * We are creating a mentions object which implements the
                 * <code>Mentionable</code> interface this allows the library to set the offset
                 * and length of the mention.
                 */
                if (user != null) {
                    final Mention mention = new Mention();
                    mention.setMentionName(user.getName());
                    hash.put(mention.getMentionName(), user.getId());
                    mentions.insertMention(mention);
                }

            }
        }));
    }

    private void sendPushNotificationToReceiver(String name, String username, String user_img, String message, String uid, String firebaseToken,
                                                String receiverFirebaseToken) {
        FcmNotificationBuilder.initialize()
                .title(name)
                .message(message)
                .username(username)
                .user_img(user_img)
                .uid(uid)
                .firebaseToken(firebaseToken)
                .receiverFirebaseToken(receiverFirebaseToken)
                .send();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SOTMApplication.setChatActivityOpen(true);
        try{
            if(!sessionManager.getUserSettings().isGo_offline_enabled()){
                FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("online").setValue("true");
            }
            FirebaseDatabase.getInstance().getReference().child("Users").child(hashMap.get(SessionManager.KEY_ID)).child("isTyping").setValue("false");
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SOTMApplication.setChatActivityOpen(false);
    }
}