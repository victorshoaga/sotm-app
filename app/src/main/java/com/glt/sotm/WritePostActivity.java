package com.glt.sotm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import Models.Response_Post;
import Rest.APIclient;
import Rest.APIinterface;
import Utility.SessionManager;
import Utility.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WritePostActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{

    private static final String TAG = WritePostActivity.class.getSimpleName();
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    private static final String POST_DONE = "post_done";
    LovelyProgressDialog lovelyProgressDialog;

    Uri selectedImage;
    boolean isPhotoDone = false;

    SessionManager sessionManager;
    HashMap<String, String> hashMap;

    Spinner scope_spinner;
    int scope_val;
    Button btn_upload_cover, btn_write_post, btn_submit_post;
    ProgressDialog progressDialog;
    IntentFilter mIntent;
    ImageView img_cover;

    String post_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_post);
        progressDialog = new ProgressDialog(this);
        sessionManager = new SessionManager(WritePostActivity.this);
        hashMap = new HashMap<>();
        hashMap = sessionManager.getUserDetails();
        lovelyProgressDialog = new LovelyProgressDialog(WritePostActivity.this);
        mIntent = new IntentFilter("NOW");

        scope_spinner = (Spinner) findViewById(R.id.scope_spinner);
        btn_upload_cover = (Button) findViewById(R.id.btn_upload_cover);
        img_cover = (ImageView) findViewById(R.id.img_cover);
        img_cover.setVisibility(View.GONE);
        btn_write_post = (Button) findViewById(R.id.btn_write_post);
        btn_submit_post = (Button) findViewById(R.id.btn_submit_post);

        Toolbar toolbar = (Toolbar) findViewById(R.id.write_post_toolbar);
        toolbar.setTitle("Create Post");
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(Utils.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp), Color.WHITE));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

            }
        });

        MySpinnerAdapter scope_spinnerAdapter = new MySpinnerAdapter(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(getResources().getStringArray(R.array.scope_array))
        );
        scope_spinner.setAdapter(scope_spinnerAdapter);
        scope_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                scope_val = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_upload_cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scope_val == 0)
                {
                    Utils.initToast(getApplicationContext(), "Please select who can view this. Thank you!");
                    return;
                }
                else{
                    if (EasyPermissions.hasPermissions(WritePostActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE))
                    {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, REQUEST_GALLERY_CODE);
                    }
                    else
                    {
                        Log.e(TAG, "Permission not yet granted");
                        EasyPermissions.requestPermissions(WritePostActivity.this, getString(R.string.read_file), READ_REQUEST_CODE, android.Manifest.permission.READ_EXTERNAL_STORAGE);
                    }
                }
            }
        });

        btn_write_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(scope_val == 0){
                Utils.initToast(getApplicationContext(), "Please select who can view this. Thank you!");
                return;
            }
            else{
                Intent intent = new Intent(WritePostActivity.this, PostContentActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
            }
            }
        });

        btn_submit_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scope_val == 0)
                {
                    Utils.initToast(getApplicationContext(), "Please select who can view this. Thank you!");
                    return;
                }
                if(btn_write_post.getText().toString().trim().equals("Write content"))
                {
                    Utils.initToast(getApplicationContext(), "Please make sure you have written the post content");
                    return;
                }
                if(!(btn_upload_cover.getText().toString().trim().equals("Good: Picture is Ok"))){
                    Utils.initToast(getApplicationContext(), "No photo attached. Please attach a photo");
                    return;
                }
                if(!Utils.verifyConnection(WritePostActivity.this)){
                    Utils.initToast(getApplicationContext(), "No internet connection. Please try again");
                    return;
                }
                createPost(hashMap.get(SessionManager.KEY_TOKEN), "Segun Obadje Teaching Ministries", "Segun Obadje Teaching Ministries", scope_val);

            }
        });
    }

    private static class MySpinnerAdapter extends ArrayAdapter<String> {
        private MySpinnerAdapter(Context context, int resource, List<String> items) {
            super(context, resource, items);
        }
        // Affects default (closed) state of the spinner
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTextSize(15);
            return view;
        }
        // Affects opened state of the spinner
        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getDropDownView(position, convertView, parent);
            view.setTextSize(15);
            return view;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_GALLERY_CODE && resultCode == WritePostActivity.this.RESULT_OK){
            selectedImage = data.getData();
            if (EasyPermissions.hasPermissions(WritePostActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Have permissions, do the thing!
                String filePath = getRealPathFromURIPath(selectedImage, WritePostActivity
                        .this);
                File file = new File(filePath);
                Log.d(TAG, "Filename " + file.getName());
                Log.i(TAG, "File Size - " + String.valueOf(file.length()/1024));
                if ((file.length()/1024)/1024 <= 1) {
                    isPhotoDone = true;
                    Toast.makeText(WritePostActivity
                            .this, "Picture: Ok", Toast.LENGTH_SHORT).show();

//                    Picasso.with(WritePostActivity
//                            .this)
//                            .load(file)
//                            .placeholder(R.drawable.head_photo)
//                            .into(img_ProfilePic);
                    if(Utils.verifyConnection(getApplicationContext()))
                    {
                        img_cover.setVisibility(View.VISIBLE);
                        Picasso.with(getApplicationContext())
                                .load(selectedImage)
                                .into(img_cover);
                        btn_upload_cover.setText("Good: Picture is Ok");
                    }

                }
                else
                {
                    isPhotoDone = false;
                    Toast.makeText(WritePostActivity
                            .this, "Cover Image too large, 1MB is the cap.", Toast.LENGTH_SHORT).show();

                }
            }
            else
            {
                //Request permissions.
                EasyPermissions.requestPermissions(WritePostActivity
                        .this, getString(R.string.read_file), READ_REQUEST_CODE, android.Manifest.permission.READ_EXTERNAL_STORAGE);
            }

        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if(selectedImage != null){
            String filePath = getRealPathFromURIPath(selectedImage, WritePostActivity
                    .this);
            File file = new File(filePath);
            Log.d(TAG, "Filename " + file.getName());
            Log.i(TAG, "File Size - " + String.valueOf(file.length()/1024));
            if ((file.length()/1024)/1024 <= 1) {
                isPhotoDone = true;
                Toast.makeText(WritePostActivity
                        .this, "Picture: Ok", Toast.LENGTH_SHORT).show();

//                Picasso.with(WritePostActivity
//                        .this)
//                        .load(file)
//                        .into(img_ProfilePic);
                if(Utils.verifyConnection(getApplicationContext()))
                {
                    img_cover.setVisibility(View.VISIBLE);
                    Picasso.with(getApplicationContext())
                            .load(selectedImage)
                            .into(img_cover);
                    btn_upload_cover.setText("Good: Picture is Ok");
                }
            }
            else
            {
                isPhotoDone = false;
                Toast.makeText(WritePostActivity
                        .this, "Cover Image too large, 1MB is the cap.", Toast.LENGTH_SHORT).show();

            }
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.i(TAG, "Permission to access gallery has been denied");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, getActivity());
    }

    private int getImageHeight(File file){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        Log.i(TAG,  "Image Height - " + imageHeight);
        return imageHeight;
    }

    private int getImageWidth(File file){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int imageWidth = options.outWidth;
        Log.i(TAG,  "Image Width - " + imageWidth);
        return imageWidth;
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

//    private void showImageUploadDialog()
//    {
//        lovelyProgressDialog.setIcon(R.drawable.ic_cast_connected_white_36dp)
//                .setTitle("Segun Obadje Teaching Ministries")
//                .setMessage("please wait, uploading image...")
//                .setMessageGravity(Gravity.CENTER_HORIZONTAL)
//                .setTopColorRes(R.color.xml_startColor)
//                .setTitleGravity(Gravity.CENTER_HORIZONTAL)
//                .setCancelable(false)
//                .show();
//    }

    private void sendImageToServer(final Uri fileUri, final String token, String post_id) {
        showPDialog("Please wait..Uploading ");
        APIinterface service = APIclient.createService(APIinterface.class, token, true);
        String filePath = getRealPathFromURIPath(fileUri, WritePostActivity.this);
        File file = new File(filePath);
        Log.i(TAG, filePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("cover_image", file.getName(), requestFile);
        Call<RequestBody> call = service.uploadCoverImage(token, post_id, body);
        call.enqueue(new Callback<RequestBody>() {
            @Override
            public void onResponse(Call<RequestBody> call, Response<RequestBody> response) {
                hidePDialog();
                Log.i(TAG, String.valueOf(response.code()));
                try {
                    Log.i(TAG, response.errorBody().string());
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                }
                if(response.code() == Utils.STATUS_200_OK) {
                    Toast.makeText(WritePostActivity
                            .this, "Cover Image uploaded", Toast.LENGTH_SHORT).show();
                }
//                else {
//                    Utils.initToast(WritePostActivity
//                            .this, Utils.getErrorMessage(response));
//                }
            }
            @Override
            public void onFailure(Call<RequestBody> call, Throwable t) {
                hidePDialog();
                Log.i(TAG, call.request().url().toString());
                call.cancel();
                //Toast.makeText(WritePostActivity
                // .this, "Poor internet connection", Toast.LENGTH_SHORT).show();
                updatePost(hashMap.get(SessionManager.KEY_TOKEN), "Segun Obadje Teaching Ministries", sessionManager.getSETMAN_POST(), scope_val);

            }
        });
    }

//    private void hideImageUploadDialog() {
//        lovelyProgressDialog.dismiss();
//    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String val = intent.getStringExtra("fromPostContentActivity");
            try
            {
                if(val.equals("POST SUCCESS"))
                {
                    btn_write_post.setText("Content processed successfully");
                }
            }
            catch (NullPointerException e)
            {
                Log.i(TAG, e.getMessage());
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(WritePostActivity.this).registerReceiver(broadcastReceiver, mIntent);
    }

    private void showPDialog(String t)
    {
        progressDialog.setMessage(t);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void hidePDialog()
    {
        progressDialog.dismiss();
    }

    private void updatePost(final String token, final String title, final String content, int scope) {
        showPDialog("Finishing up...");
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_Post> call = service.updatePost(token, post_id, title, content, scope);
        call.enqueue(new Callback<Response_Post>() {
            @Override
            public void onResponse(Call<Response_Post> call,
                                   Response<Response_Post> response) {
                hidePDialog();
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Log.i(TAG, response.body().toString());
                    Log.i(TAG + " Response body", response.body().toString());
//                    Intent intent = new Intent(WritePostActivity.this, WritePostActivity.class);
//                    intent.putExtra("post_id", response.body().getData().getPost().get_id());
//                    startActivity(intent);
                    WritePostActivity.this.finish();
                    sendBroadCast(POST_DONE);
                }
                else {
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_Post> call, Throwable t) {
                hidePDialog();
                call.cancel();
                Toast.makeText(getApplicationContext(), Html.fromHtml(getApplicationContext().getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createPost(final String token, final String title, final String content, int scope) {
        showPDialog("Creating post...");
        APIinterface service = APIclient.createService(APIinterface.class, token, false);
        final Call<Response_Post> call = service.createPost(token, title, content, scope);
        call.enqueue(new Callback<Response_Post>() {
            @Override
            public void onResponse(Call<Response_Post> call,
                                   Response<Response_Post> response) {
                hidePDialog();
                Log.i(TAG, call.request().url().toString());
                Log.i(TAG, "STATUS_CODE = " + response.code());
                if(response.code() == Utils.STATUS_200_OK)
                {
                    Log.i(TAG, response.body().toString());
                    Log.i(TAG + " Response body", response.body().toString());
                    post_id = response.body().getData().getPost().get_id();
                    Log.i(TAG, post_id);
                    sendImageToServer(selectedImage, hashMap.get(SessionManager.KEY_TOKEN), post_id);

                }
                else {
                    post_id = "";
                    String errorMessage = Utils.getErrorMessage(response);
                    Log.w(TAG, errorMessage);
                    Utils.initToast(getApplicationContext(), errorMessage);
                }
            }

            @Override
            public void onFailure(Call<Response_Post> call, Throwable t) {
                hidePDialog();
                post_id = "";
                call.cancel();
                Toast.makeText(getApplicationContext(), Html.fromHtml(getApplicationContext().getString(R.string.reg_error_txt)), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendBroadCast(String val)
    {
        Intent intent = new Intent();
        intent.putExtra("fromWritePostActivity", val);
        intent.setAction("NOW");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

}
